<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<section class="blog-detail heading-marg">
    <div class="container">
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="blog-top">
            <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
           
        </div>
        <div class="row blog-containt">
            <div class="col-sm-12">
                <div class="blog-left blog-e-content">
                    <div class="blog-status clearfix">
                        <!-- <div class="customer-health">
                        	    <?php $prev_post = get_previous_post(); ?>
                          
                            <a href="<?php echo get_permalink( $prev_post->ID ); ?>"><i class="fa fa-chevron-left" aria-hidden="true"></i> <?php echo $prev_post->post_title ?></a>
                        </div> -->
	
                        <div class="customer-social">
                            <ul>
                                <li><a class="comment_bottom"><i class="fa fa-commenting-o" aria-hidden="true"></i> <?php if(get_comments_number(get_the_ID())){echo $vart=get_comments_number(get_the_ID()); } else { echo $vart='0'; } ?></a></li>
                                <li><li><?php if( function_exists('zilla_likes') ) zilla_likes(); ?></li></li>
                            </ul>
                        </div>
                    </div>
                    <div class="blog-box">
                        <div class="blog-classnew">
                        <div class="blog-news-date">
                            BY <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo get_the_author_meta( 'display_name',$post->post_author); ?></a>    <?php echo get_the_time('M j, Y'); ?> 
                        </div>
                        <h1><?php the_title(); ?></h1>
                        <div class="blog-img">
	                        <?php if ( has_post_thumbnail() ) : ?>
	                        	<img src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
	                        <?php endif; ?>
                        </div>
                        <?php the_content(); ?>
                        
                        <div class="tags clearfix">
                            <span> Tags: </span>
                            <?php $tags = wp_get_post_tags($post->ID);
                            	if(!empty($tags)){
                            		foreach ( $tags as $tag ) {
                            		$tag_link = get_tag_link( $tag->term_id ); ?>
                            <ul>
                                <li><a href="<?php echo $tag_link; ?>"><?php echo $tag->name.' , '; ?></a></li>
                            </ul>
                            <?php }} ?>
                        </div>
                        <?php $image = get_field('omega3_innovations_image');?>
                        <?php if( !empty($image['url']) ): ?>
                        <div class="innovation">
                            <div class="inoo-pic">
                            	<?php $image = get_field('omega3_innovations_image');?>

								

									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

								
                            </div>
                            <div class="inno-text">
                                <?php the_field('omega3_innovations'); ?>
                            </div>
                        </div>
                        <?php endif; ?>
                        </div>
                        <div class="like-blog">
                            <h3>You may also like</h3>
                            <?php $idd=get_the_ID(); ?>
                            <?php $posts = get_field('related_post',$idd); ?>
                             <?php if( $posts ){ ?>
								    <div class="row">  	
        								<?php $i=0;
        								foreach ($posts as $p) {
        								if($i<2){ 
        								?>
                                        <div class="col-sm-6 <?php if($i == 0){ echo 'blog-first-box'; } ?>">
                                            <div class="blog_img">
                                            	  <?php 
        										$feat_image_url = wp_get_attachment_url(get_post_thumbnail_id($p,'thumbnail')); 
        										if ( $feat_image_url ) {  ?> 
                                                <a href="<?php echo get_the_permalink($p); ?>"><img src="<?php echo $feat_image_url;  ?>" alt=""></a>
                                                <?php } ?>
                                            </div>
                                            <div class="blog_cont">
                                                <h3><a href="<?php echo get_the_permalink($p); ?>"><?php echo get_the_title($p) ?></a></h3>
                                                <!-- <p>
                                                    <?php //echo wp_trim_words(get_the_content($p),10,'...'); ?> <a href="<?php //echo get_the_permalink($p); ?>">Read full article</a>
                                                </p> -->
                                            </div>
                                        </div>
                                        <?php }$i++; }  ?>
								    </div>
								<?php }else{
								     
								$categories = get_the_category(get_the_ID());
								$postid=array(); 
                                $year = date('Y');
                                $month = date('n');
	                         	foreach($categories as $cat) {
	                         	$catid=$cat->term_id;
                                $args=array(
                                      'post_type' => 'post',
                                      'cat'       => $catid,
                                      'post_status' => 'publish', 
                                      //'year'        => $year,
                                      //'monthnum'    => $month,                                     
                                      'showposts' => -1,
                                      // 'date_query' => array(
                                      //       array(
                                      //           'after'  => '30 days ago'
                                      //       ),
                                      //   ),
                                );
								query_posts($args);
								if (have_posts()) { while (have_posts()) { the_post();
								                         	   array_push($postid,get_the_ID());
								}}
								}
								wp_reset_query();									
								$unid=array_unique($postid);
								shuffle($unid);				
								if(($key = array_search(get_the_ID(), $unid)) !== false) {
								   unset($unid[$key]);
								}								
									?>
								    <div class="row">  	
        								<?php $i=0;
        								foreach ($unid as $value) {
        								if($i<2){ 
        								?>
                                        <div class="col-sm-6 <?php if($i == 0){ echo 'blog-first-box'; } ?>">
                                            <div class="blog_img">
                                            	  <?php 
        										$feat_image_url = wp_get_attachment_url(get_post_thumbnail_id($value,'thumbnail')); 
        										if ( $feat_image_url ) {  ?> 
                                                <a href="<?php the_permalink($value); ?>"><img src="<?php echo $feat_image_url;  ?>" alt=""></a>
                                                <?php } ?>
                                            </div>
                                            <div class="blog_cont">
                                                <h3><a href="<?php the_permalink($value); ?>"><?php echo get_the_title($value) ?></a></h3>
                                                <!-- <p>
                                                    <?php //echo wp_trim_words(get_the_content($value),10,'...'); ?> <a href="<?php //the_permalink($value); ?>">Read full article</a>
                                                </p> -->
                                            </div>
                                        </div>
                                        <?php }$i++; }  ?>
								    </div>
								<?php 
								} ?>  
                        </div>
                        <div class="leave-reply">
                        	<?php 
                        		if ( comments_open() || get_comments_number() ) {
									comments_template();
								}
							?>                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 category">
            	<?php //get_sidebar(); ?>
            </div>
        </div>
        <?php 
        $count_key = 'post_views_count'; 
        $count = get_post_meta($idd, $count_key, true);
         if($count == ''){
        $count = 1;
        delete_post_meta($idd, $count_key);
        add_post_meta($idd, $count_key, '1');
       }else{
        $count++; 
        update_post_meta($idd, $count_key, $count);

    }
        ?>
    <?php endwhile; ?>
    </div>
     <div class="contactBlog">
        <div class="container">
                <div class="signUp">
                <h4>SIGN UP FOR OUR NEWSLETTER</h4>

                <div class="signUp-in">
                    <!-- <input type="email" placeholder="Enter email address..." />
                    <input type="submit" value="" /> -->
                    
     <form action="//omega3innovations.us4.list-manage.com/subscribe/post?u=3689faabafe08edbf98e39a4f&amp;id=6ce5e63d91" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
		<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Enter email address...">
	    <input type="submit" value="" name="subscribe" id="mc-embedded-subscribe">	
	    <div id="mce-responses" class="clear">
			<div class="response" id="mce-error-response" style="display:none"></div>
			<div class="response" id="mce-success-response" style="display:none"></div>
		</div>   
     </form>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
                            
                </div>
            </div>
            <div class="socBlog">
                <h4>GET TO KNOW US</h4>

                <ul>
                    <li>
                        <a  target="_blank" href="<?php echo get_theme_mod( 'twitter' )?get_theme_mod( 'twitter' ):'javascript:void(0)'; ?>">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a  target="_blank" href="<?php echo get_theme_mod( 'facebook' )?get_theme_mod( 'facebook' ):'javascript:void(0)'; ?>">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a  target="_blank" href="<?php echo get_theme_mod( 'pinterest' )?get_theme_mod( 'pinterest' ):'javascript:void(0)';  ?>">
                            <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                        </a>
                    </li>
                    <!-- <li>
                        <a  target="_blank" href="<?php //echo get_theme_mod( 'instagram' )?get_theme_mod( 'instagram' ):'javascript:void(0)';?>">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                    </li> -->
                </ul>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>

