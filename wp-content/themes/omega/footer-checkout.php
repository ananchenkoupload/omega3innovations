<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
    <div class="footer-bottom">
        <div class="container">
        	<!--
            <div class="footer-btm-r">
                <?php //echo get_theme_mod( 'poweredbytitle' ); ?>
                <a target="_blank" href="<?php echo get_theme_mod( 'copylogo-url' ); ?>"><img src="<?php echo get_theme_mod( 'copylogo' ); ?>" alt="footer" /></a>
            </div>
            -->
            <div class="footer-btm-l">
                <?php echo get_theme_mod( 'copyrighttext' ); ?>
            </div>
        </div>
    </div>
    <?php wp_footer(); ?>
    <script type="text/javascript">
        $(window).ready(function(){
			
			$(document.body).on( 'updated_checkout', function()
			{
				if($('#customer_shipping_details .shop_table').length)
				{
					var cloneTable = $('#customer_shipping_details .shop_table').clone();
					jQuery('#order_checkout_payment .subs_help').html(cloneTable);
					setTimeout(function(){
						jQuery('#order_checkout_payment .subs_help table .cart-subtotal.recurring-total').find('th').text('Plan');
						
						if(jQuery('#order_checkout_payment .subs_help table .shipping').find('th').hasClass('package_price')){
							jQuery('#order_checkout_payment .subs_help table .shipping').find('th').html('Shipping Method:').removeClass('package_price');
							var $selectTd   = jQuery('#order_checkout_payment .subs_help table .shipping ').find('td');
							var $selectedID = $selectTd.find('input:checked').attr('id');
							var shippingVal = $selectTd.find('label[for="'+$selectedID+'"]').text();
							$selectTd.text(shippingVal);
						}
						else{
							var shipping = jQuery('#order_checkout_payment .subs_help table .shipping').find('th').text();
								shipping = shipping.replace('Shipping via ','');
							jQuery('#order_checkout_payment .subs_help table .shipping ').find('td').text(shipping);
							jQuery('#order_checkout_payment .subs_help table .shipping').find('th').text('Shipping Method:');
						}
					
						var firstRenew = jQuery('#order_checkout_payment .subs_help table .order-total').find('.first-payment-date small').text();
							firstRenew = firstRenew.replace('First renewal: ','');
						jQuery('#order_checkout_payment .subs_help table .order-total').find('th').text('First Renewal:');
						jQuery('#order_checkout_payment .subs_help table .order-total').find('td').text(firstRenew);
						jQuery('#order_checkout_payment .subs_help table').css('display','table');
					},50);
				}
			});

           $('.yith-wcms-button.next,.timeline').click(function() {
			var total = $('#customer_shipping_details .shop_table .cart-subtotal .totalorder').html();
			$('span.totalamount').html(total);
			
            var current_step = $(yith_wcms.dom.checkout_timeline).find(yith_wcms.dom.active_timeline).data('step');
            if (current_step == 'payment' ) {
                var total = $('#authnet-cc-form').find('.woocommerce-validated').length;
                $('#authnet-cc-form').find('.woocommerce-validated').each(function(index) {                    
                    if(!$(this).hasClass('cardfld')){
                        if(total > 3){
                            if (index != total - 1) {
                                $(this).addClass('validate-required cardfld woocommerce-invalid woocommerce-invalid-required-field').removeClass('woocommerce-validated');
                            }
                        }else{
                            $(this).addClass('validate-required cardfld woocommerce-invalid woocommerce-invalid-required-field').removeClass('woocommerce-validated');
                        }
                    }
                });
            }
             }); 

			 $('.yith-wcms-button.next,.yith-wcms-button.prev,.timeline').click(function() {
				var current_step = $(yith_wcms.dom.checkout_timeline).find(yith_wcms.dom.active_timeline).data('step');
				if (current_step == 'payment' ) {
					$('#form_actions').removeClass('orderStep');
					$('#form_actions').addClass('lastStep');
				}else if (current_step == 'order' ) {
					$('#form_actions').removeClass('lastStep');
					$('#form_actions').addClass('orderStep');
				}else if (current_step == 'billing' ){
					$('#form_actions').removeClass('orderStep');
					$('#form_actions').removeClass('lastStep');
				}
			 });  
			 
          });
    </script>
    </body>
</html>
