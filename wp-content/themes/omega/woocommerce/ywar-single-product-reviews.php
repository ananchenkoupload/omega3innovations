<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly


global $review_stats;
global $product;
?>

<div id="reviews_summary">
    <h3><?php _e('Customers\' review', 'yith-woocommerce-advanced-reviews') ?></h3>

    <?php do_action('ywar_summary_prepend', $product) ?>

    <?php do_action('ywar_summary_append') ?>

    <div id="reviews_header">
        <?php do_action('ywar_reviews_header', $review_stats) ?>
    </div>
</div>



