<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $woocommerce;
?>

<?php do_action( 'woocommerce_before_mini_cart' ); ?>

<div class="minicartdisplay cart-product" style="display:block !important;">	
    <?php if ( WC()->cart->is_empty() ){ $empty = 'emptycart'; } ?>
     <?php if ( ! WC()->cart->is_empty() ) : ?>
      <div class="free-shiping">
            <div class="free-inner">
                <?php 
                    $free = get_shipping_min_rate(); 
                    $subtotal = $woocommerce->cart->subtotal;
                    if($free > $subtotal){
                        $remain = $free - $subtotal;
						if($remain>0){
							echo 'You\'re <b>'.get_woocommerce_currency_symbol().round($remain, 2).'</b> away from free shipping';
						}else{
							echo 'Free Shipping';
						}
                    }else{
                        echo 'Free Shipping';
                    }
                ?>
                
                <?php $shop_page_url = get_permalink( get_page_by_path( 'store' ) ); ?>
                <a href="<?php echo $shop_page_url; ?>">Continue Shopping ></a>
            </div>
        </div>
        <?php endif; ?>
    <div class="cart-top <?php if(isset($empty) && !empty($empty)){ echo $empty; } ?>">        
        <?php if ( ! WC()->cart->is_empty() ) : 
        		$cart_contents_count = $woocommerce->cart->cart_contents_count;
        ?>
        	<a href="<?php echo get_permalink( woocommerce_get_page_id( 'cart' ) ); ?>" ><h2>Your Cart (<?php echo $cart_contents_count; ?>)</h2></a>
        <ul>
        	<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );
					$thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
					$product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
            <li>     
                <div class="cart_details">       	
                <div class="cart-productName cartpicSize">
                	<?php if ( ! $_product->is_visible() ) : ?>
							<?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ) . '&nbsp;'; ?>
						<?php else : ?>
							<a href="<?php echo esc_url( $product_permalink ); ?>">
								<?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ) . '&nbsp;'; ?>
							</a>
						<?php endif; ?>
            	</div>
                <div class="cart-productName cartText">
                	<h3><a href="<?php echo get_the_permalink($product_id); ?>"><?php echo $product_name; ?></a></h3>
                	<h4><?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</span>', $cart_item, $cart_item_key ); ?></h4>                    
                </div>
                <div class="cart-productName del">
                	<?php
					echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
						'<a href="%s" title="%s" data-product_id="%s" data-product_sku="%s"><i class="fa fa-trash" aria-hidden="true"></i></a>',
						esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
						__( 'Remove this item', 'woocommerce' ),
						esc_attr( $product_id ),
						esc_attr( $_product->get_sku() )
					), $cart_item_key );
					?>
            	</div>
                <div class="cart_flavor"><?php echo WC()->cart->get_item_data( $cart_item ); ?></div>
                </div>
                
            </li>
            <?php
				}
			}
		?>
    <?php else : ?>		
	        <h5><?php _e( 'No products in the cart.', 'woocommerce' ); ?></h5>
	<?php endif; ?>    
    </ul>    
    <?php if ( ! WC()->cart->is_empty() ) : ?>
        <div class="total-checkout clearfix">
            <div class="subtotal">
                <h3>Subtotal</h3>
                <h4><?php echo WC()->cart->get_cart_subtotal(); ?></h4>
            </div>
            <div class="checkSide">
                <a href="<?php echo get_permalink( woocommerce_get_page_id( 'cart' ) ); ?>" class="btn btn-default" ><?php _e( 'Checkout', 'woocommerce' ); ?></a>
            </div>
        </div>
      
    <?php endif; ?>
    </div>
</div>   

<?php do_action( 'woocommerce_after_mini_cart' ); ?>
