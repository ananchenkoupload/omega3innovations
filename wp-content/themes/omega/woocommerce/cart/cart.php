<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $woocommerce;
wc_print_notices(); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.woocommerce').addClass('woocommerce_cart');
	});
</script>

    <?php do_action( 'woocommerce_before_cart' ); ?>
       
            <div class="carTop">
            	  <?php if ( ! WC()->cart->is_empty() ) : ?>
            	 <!-- <div class="free-inner cart-freeship">
                <?php 
                    // $free = get_shipping_min_rate(); 
                    // $subtotal = $woocommerce->cart->subtotal;
                    // if($free > $subtotal){
                    //     $remain = $free - $subtotal;
                    //     echo 'You\'re <b>'.get_woocommerce_currency_symbol().$remain.'</b> away from free shipping';
                    // }else{
                    //     echo 'Free Shipping';
                    // }
                ?>
                
                <?php $shop_page_url = get_permalink( get_page_by_path( 'store' ) ); ?>
                <a href="<?php echo $shop_page_url; ?>">Continue Shopping ></a>
                </div> -->
                <?php endif; ?>
                <h2>Shopping Cart</h2>
                <div class="checkout-r">
                	<?php $shop_page_url = get_permalink( get_page_by_path( 'store' ) ); ?>
                    <a href="<?php echo $shop_page_url; ?>" class="continue-shop">Continue Shopping</a>
                    <a href="<?php echo esc_url( wc_get_checkout_url() ) ;?>" class="btn btn-default">
						<?php echo __( 'Checkout', 'woocommerce' ); ?>
				    </a>
                </div>
            </div>
			<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
			<?php do_action( 'woocommerce_before_cart_table' ); ?>
			<div class="checkoutnew">
				<table class="shop_table shop_table_responsive cart" cellspacing="0">
					<thead>
						<tr>							
							<th class="product-name"><?php _e( 'Item', 'woocommerce' ); ?></th>
							<th class="product-price"><?php _e( 'Unit Price', 'woocommerce' ); ?></th>
							<th class="product-quantity"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
							<th class="product-subtotal"><?php _e( 'Product Total', 'woocommerce' ); ?></th>
							<th class="product-remove">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php do_action( 'woocommerce_before_cart_contents' ); ?>

						<?php
						foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
							$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
							$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

							if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
								$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
								?>
								<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
									<td class="product-name">	
										<div class="cartnew_page">					
										<?php
											$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

											if ( ! $product_permalink ) {
												echo $thumbnail;
											} else {
												printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
											}
										?>
										<?php
											echo '<div class="cart_item_details">';
											if ( ! $product_permalink ) {
												echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
											} else {
												echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
											}

											do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

											// Meta data.
											echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.
											
											echo '</div>';
											// Backorder notification
											if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
												echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
											}
										?>
										</div>
									</td>

									<td class="product-price">
										<?php
											echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
										?>
									</td>

									<td class="product-quantity">
										 <div class="q_cell quantity">
				                                <button type="button" id="minus" class="minus"><img src="<?php echo get_template_directory_uri(); ?>/images/minus.jpg" alt=""></button>
				                                <input type="text" name='<?php echo "cart[{$cart_item_key}][qty]" ; ?>' value="<?php echo $cart_item['quantity']; ?>" inputmode="<?php if(isset($inputmode) && !empty($inputmode)){ echo esc_attr( $inputmode ); } ?>"  class="qnt input-text qty text" readonly="readonly">
				                                <button type="button" id="plus" class="plus"><img src="<?php echo get_template_directory_uri(); ?>/images/plus.jpg" alt=""></button>    
				                         </div>
				                         <?php
												if ( $_product->is_sold_individually() ) {
												$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
												} else {
												$product_quantity = woocommerce_quantity_input( array(
												'input_name'  => "cart[{$cart_item_key}][qty]",
												'input_value' => $cart_item['quantity'],
												'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
												'min_value'   => '0'
												), $_product, false );
												}
												
												echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
										?>


									</td>

									<td class="product-subtotal">
										<?php
											echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
										?>
									</td>
									<td class="product-remove checkout-table-cell">
										<?php
											echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
												'<a href="%s"  title="%s" data-product_id="%s" data-product_sku="%s"><i class="fa fa-trash" aria-hidden="true"></i></a>',
												esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
												__( 'Remove this item', 'woocommerce' ),
												esc_attr( $product_id ),
												esc_attr( $_product->get_sku() )
											), $cart_item_key );
										?>
									</td>
								</tr>
								<?php
							}
						}

						do_action( 'woocommerce_cart_contents' );
						?>
							<tr>
							<td colspan="6" class="actions coupon-sec">
                               
                            <!-- <div class="coupon-outer">
                             <a href="javascript:void(0)" id="coupon-code">+ Have a coupon click here</a>
                            </div> -->
                  
                       <?php if ( wc_coupons_enabled() ) { ?>
									<div class="coupon" style="display: none;">

										<label for="coupon_code"><?php _e( 'Coupon:', 'woocommerce' ); ?></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <input type="submit" class="btn btn-default" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" />

										<?php do_action( 'woocommerce_cart_coupon' ); ?>
									</div>
								<?php } ?>
								<input type="submit" class="btn btn-default" id="update_cart" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>" />

								<?php do_action( 'woocommerce_cart_actions' ); ?>

								<?php wp_nonce_field( 'woocommerce-cart' ); ?>
							</td>
						</tr>
						<?php do_action( 'woocommerce_after_cart_contents' ); ?>
					</tbody>
				</table>
			</div>
			<?php do_action( 'woocommerce_after_cart_table' ); ?>
			

            <div class="checkout-r">
               <span class="subtotalChck"><strong>Subtotal</strong></span>
               <span><?php echo WC()->cart->get_cart_subtotal(); ?></span>
               <a href="<?php echo esc_url( wc_get_checkout_url() ) ;?>" class="btn btn-default btn-custom">
					<?php echo __( 'Checkout', 'woocommerce' ); ?>
			   </a>              
            </div>
        </form>
        <div class="other-products">
            <h2><?php echo get_field('related_product_title');?></h2>
            <h3><?php echo get_field('related_product_subtitle');?></h3>
             <div class="feature-cont">
            	<?php  global $product;

						if ( ! $crosssells = WC()->cart->get_cross_sells() ) {
							return;
						}
						
						$args = array(
							'post_type'           => 'product',
							'ignore_sticky_posts' => 1,
							'no_found_rows'       => 1,
							'posts_per_page'      => 2,
							'post__in'            => $crosssells,
							'meta_query'          => WC()->query->get_meta_query()
						);
                        $r = new WP_Query( $args ); ?>
				     <?php while ($r->have_posts()) : $r->the_post(); global $product; ?>
		                <div class="feature-cont-col" data-toggle="modal" data-target='#myModal_<?php echo get_the_ID(); ?>'>
		                    <div class="product-box">
		                        <a href="javascript:void(0)" class="product-details-Icon">
		                            <i class="fa fa-plus" aria-hidden="true"></i>
		                        </a>
		                        <div class="clearfix">
		                            <a href="javascript:void(0)" class="box-pic">
		                            <?php
		                            $thumb_image=wp_get_attachment_url(get_post_thumbnail_id($r->ID)); 
		                            if (has_post_thumbnail()){
		                            	  echo '<img src="'.$thumb_image.'" alt="product" />'; } else { echo '<img src="'. woocommerce_placeholder_img_src() .'" alt="Placeholder"  />'; } 
		                            ?>
		    	                  </a>
		                          <div class="box-text">
		                              <h4><a href="javascript:void(0)"><?php the_title(); ?></a></h4>		                        
		                        	  <h5><?php echo get_woocommerce_currency_symbol().$product->get_price(); ?></h5>
		                          </div>
		                        </div>                  
		                    </div>
		                </div>
		                <div class="modal cart-modal fade" id="myModal_<?php echo get_the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						      </div>
						      <div class="modal-body">
						        <div class="row">
						            <div class="col-sm-6">
						            	<?php
			                            $thumb_image=wp_get_attachment_url(get_post_thumbnail_id($r->ID)); 
			                            if (has_post_thumbnail()){
			                            	  echo '<img src="'.$thumb_image.'" alt="product" />'; } else { echo '<img src="'. woocommerce_placeholder_img_src() .'" alt="Placeholder"  />'; } 
			                            ?>
						            </div>
						            <div class="col-sm-6">
						                <div class="custom-product-details">
						                    <div class="custom-product-detailsA">
						                      <a href="<?php echo get_the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
						                      <span> <?php echo get_woocommerce_currency_symbol().$product->get_price(); ?> </span>
						                      <p><?php the_excerpt(); ?></p>
						                    </div>
						                    <!--<div class="custom-product-detailsA clearfix">
						                         <div class="pull-left">
						                            <span class="woocommerce-Price-amount amount">
						                            	<span class="woocommerce-Price-currencySymbol"><?php //echo get_woocommerce_currency_symbol(); ?></span><?php //echo $product->get_price(); ?>
					                            	</span>
						                        </div> 
						                        <div class="pull-right">
						                            <div class="q_cell quantity">
						                                <button type="button" id="minus" class="minus"><img src="/wp-content/themes/omega/images/minus.jpg" alt=""></button>
						                                <input name="cart[e8508ac900d640d2eddde1f39c3da529][qty]" value="1" inputmode="" class="qnt input-text qty text" readonly="readonly" type="text">
						                                <button type="button" id="plus" class="plus"><img src="/wp-content/themes/omega/images/plus.jpg" alt=""></button>    
										            </div> 
						                        </div>
						                    </div>-->
						                    <div class="custom-product-detailsA">
						                        <a href="<?php echo get_the_permalink(); ?>" class="btn btn-default">Details</a>
						                    </div>
						                </div>
						            </div>
						        </div>
						      </div>
						    </div>
						  </div>
						</div>
                  <?php endwhile;wp_reset_query(); ?>
            </div>  
        </div>
        <!-- <div class="cart-collaterals">

	<?php do_action( 'woocommerce_cart_collaterals' ); ?>

</div> -->
        
<?php do_action( 'woocommerce_after_cart' ); ?>
