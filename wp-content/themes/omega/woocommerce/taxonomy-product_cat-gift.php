<?php

get_header(); ?>

<section class="productMain heading-marg">
    <div class="prodBanner">
        <div class="container">
            <img src="https://richpanel.com/wp-content/themes/omega/images/bannerP.jpg" alt="" />
            
            <div class="bannerP-in">
                <h1>Because Good Health Is On <br/>
                Everybody’s Wish List…</h1>

                <p>
                When considering gifts this year, how about less stuff and more life?<br/>
                For easy gifting, we’ve bundled together our favorite product combinations for sharing with family and friends.
                </p>
            </div>
        </div>
    </div>

    <div class="prodBundles">
        <div class="container">
            <h4 class="text-uppercase text-center">Omega Bundles</h4>
            <div class="bundleShow">
                <div class="bundle-e">
                    <figure>
                            <img src="https://richpanel.com/wp-content/themes/omega/images/prod1.png" alt="" />
                    </figure>

                    <div class="infoProd-e-bn">
                        <a href="javascript:void(0)">The Travel Bundle</a>
                        <p>For the regular jetsetter</p>
                        <h6>From $89.90</h6>
                    </div>
                </div>
                
                <div class="bundle-e">
                    <figure>
                            <img src="https://richpanel.com/wp-content/themes/omega/images/prod2.png" alt="" />
                    </figure>

                    <div class="infoProd-e-bn">
                        <a href="javascript:void(0)">The Workout Bundle</a>
                        <p>For athletes and gym regulars</p>
                        <h6>From $89.90</h6>
                    </div>
                </div>
                    
                <div class="bundle-e">
                    <figure>
                            <img src="https://richpanel.com/wp-content/themes/omega/images/prod2.png" alt="" />
                    </figure>

                    <div class="infoProd-e-bn">
                        <a href="javascript:void(0)">The Family Bundle</a>
                        <p>For kids from 1 to 92</p>
                        <h6>From $84.95</h6>
                    </div>
                </div>
                
                <div class="bundle-e">
                    <figure>
                            <img src="https://richpanel.com/wp-content/themes/omega/images/prod3.png" alt="" />
                    </figure>

                    <div class="infoProd-e-bn">
                        <a href="javascript:void(0)">The Great Expectation Bundle</a>
                        <p>For expectant mothers</p>
                        <h6>From $84.95</h6>
                    </div>
                </div>
                    
                <div class="bundle-e">
                    <figure>
                            <img src="https://richpanel.com/wp-content/themes/omega/images/prod4.png" alt="" />
                    </figure>

                    <div class="infoProd-e-bn">
                        <a href="javascript:void(0)">The Holiday Bundle</a>
                        <p>Pre-Order Today, Ships in December</p>
                        <h6>$99.95 / bundle ($115.95 value)</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="howWorksP">
        <div class="container">
            <h4 class="text-uppercase text-center">How it works</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="howCell text-center">
                        <div class="howImg" style="background-image: url(https://richpanel.com/wp-content/themes/omega/images/gift.png);"></div>
                        <h5>1. Pick your bundle</h5>

                        <p>Select your bundle, and choose whether you want to give it as a one-time gift or prepay for a set period of time.</p>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="howCell text-center">
                        <div class="howImg" style="background-image: url(https://richpanel.com/wp-content/themes/omega/images/cartI.png);"></div>
                        <h5>2. Write a note</h5>

                        <p>If you choose to write a note, we’ll include it, handwritten, with your order.</p>
                    </div>
                </div>
                    
                <div class="col-sm-4">
                    <div class="howCell text-center">
                        <div class="howImg" style="background-image: url(https://richpanel.com/wp-content/themes/omega/images/smileI.png);"></div>
                        <h5>1. Pick your bundle</h5>

                        <p>After you place your order, your bundle will ship on the nearest Monday - Wednesday. If you need your order to ship for a specific date, please include it in your order note.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="cardGift">
        <div class="container">
            <div class="cardIn-Gift">
                <div class="row">
                    <div class="col-sm-offset-6 col-sm-6">
                        <h4>Gift cards</h4>
                        <p>
                                Not sure what your loved one might want? Choose a gift card that can be applied to all our regular products, subscriptions and bundles. 
                        </p>

                        <h5>Starts at $25.00.</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>