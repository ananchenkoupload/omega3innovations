<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

?>

<?php
	// Availability
	$availability      = $product->get_availability();
	$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';

	echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
?>
<span class="orderArrival">Orders usually arrive within 3 business days</span>
<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="single_form cart" method="post" enctype='multipart/form-data'>
	 	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

	 	<?php
	 		if ( ! $product->is_sold_individually() ) {
	 			woocommerce_quantity_input( array(
	 				'min_value'   => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
	 				'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product ),
	 				'input_value' => ( isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 )
	 			) );
	 		}
	 	?>
		<input type="hidden" name="quantity" value="1" />
	 	<input type="hidden" name="product_id" value="<?php echo esc_attr( $product->id ); ?>" />

	 	<button type="submit" class="button single_add_to_cart_button btn btn-default"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
<?php if ( $product->is_in_stock() ) : ?>
	<?php
		$free = get_woocommerce_currency_symbol().get_shipping_min_rate();

		$product = new WC_Product($product->id);
    	$upsells = $product->get_upsells();
    	if(!empty($upsells)){
    		$thisproduct = wc_get_product( $upsells[0] );
		?>
		<a href="<?php echo get_permalink($upsells[0]); ?>" class="btn btn-product">
			<img src="<?php echo get_template_directory_uri();?>/images/circle.png"> 
			<span class="product-ab">
				<img src="<?php echo get_template_directory_uri();?>/images/circle-hvr.png">
			</span> 
			<?php
			$per_unit = get_field('units_in_subscription');
			$sub_price = $thisproduct->get_price();
					if(!empty($per_unit) && ($per_unit > 0)){
						$sub_price = ($sub_price/$per_unit);
						$sub_price = number_format((float)$sub_price, 2, '.', '');
					}
			?>
			<?php echo 'Subscription available at: '.get_woocommerce_currency_symbol().$sub_price; ?>
		</a>		
	<?php } ?>
	<h4 class="free-ship">Free Shipping on orders above <?php echo $free; ?>.  In Stock.  Cancel any time. </h4>
	

<?php endif; ?>
