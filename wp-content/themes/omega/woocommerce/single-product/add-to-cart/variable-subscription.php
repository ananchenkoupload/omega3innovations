<?php
/**
 * Variable subscription product add to cart
 *
 * @author  WooThemes
 * @package WooCommerce-Subscriptions/Templates
 * @version 2.0.9
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'print_attribute_radio' ) ) {
	function print_attribute_radio( $checked_value, $value, $label, $name ) {
		// This handles < 2.4.0 bw compatibility where text attributes were not sanitized.
		$checked = sanitize_title( $checked_value ) === $checked_value ? checked( $checked_value, sanitize_title( $value ), false ) : checked( $checked_value, $value, false );
		$input_name = 'attribute_' . esc_attr( $name ) ;
		$esc_value = esc_attr( $value );
		$id = esc_attr( $name . '_v_' . $value );
		$filtered_label = apply_filters( 'woocommerce_variation_option_name', $label );
		printf( '<div class="check_in" data-val="%6$s"><input type="radio" name="%1$s" value="%2$s" id="%3$s" %4$s><label for="%3$s">%5$s</label></div>', $input_name, $esc_value, $id, $checked, $filtered_label, $esc_value );
	}
}

global $product;

$attribute_keys = array_keys( $attributes );
$user_id = get_current_user_id();

do_action( 'woocommerce_before_add_to_cart_form' ); ?>

<form class="variations_form cart" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->id ); ?>" data-product_variations="<?php echo esc_attr( json_encode( $available_variations ) ) ?>">
	<?php do_action( 'woocommerce_before_variations_form' ); ?>
	
	<?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
		<p class="stock out-of-stock"><?php esc_html_e( 'This product is currently out of stock and unavailable.', 'woocommerce-subscriptions' ); ?></p>
	<?php else : ?>
		<?php if ( ! $product->is_purchasable() && 0 != $user_id && 'no' != $product->limit_subscriptions && ( ( 'active' == $product->limit_subscriptions && wcs_user_has_subscription( $user_id, $product->id, 'on-hold' ) ) || $user_has_subscription = wcs_user_has_subscription( $user_id, $product->id, $product->limit_subscriptions ) ) ) : ?>
			<?php if ( 'any' == $product->limit_subscriptions && $user_has_subscription && ! wcs_user_has_subscription( $user_id, $product->id, 'active' ) && ! wcs_user_has_subscription( $user_id, $product->id, 'on-hold' ) ) : // customer has an inactive subscription, maybe offer the renewal button ?>
				<?php $resubscribe_link = wcs_get_users_resubscribe_link_for_product( $product->id ); ?>
				<?php if ( ! empty( $resubscribe_link ) ) : ?>
					<a href="<?php echo esc_url( $resubscribe_link ); ?>" class="button product-resubscribe-link"><?php esc_html_e( 'Resubscribe', 'woocommerce-subscriptions' ); ?></a>
				<?php endif; ?>
			<?php else : ?>
				<p class="limited-subscription-notice notice"><?php esc_html_e( 'You have an active subscription to this product already.', 'woocommerce-subscriptions' ); ?></p>
			<?php endif; ?>
		<?php else : ?>
			<table class="gift-crd" cellspacing="0">
					<tbody>
			<?php
						$terms = wp_get_post_terms( get_the_ID(), 'product_cat' );
						foreach ( $terms as $term ) $categories[] = $term->slug;
						
						if (in_array("gift", $categories)){
						?>
						<?php
						// check if the repeater field has rows of data
						if( have_rows('whats_in_the_box') ):
						?>
						<tr class="cart-item">
							<td class="label boxHdr"><h4 for="flavor">
								<span class="figIco">
									<img src="https://richpanel.com/wp-content/themes/omega/images/gift.png" alt="" />
								</span>
								What's in the box</h4>
							</td>
							<td class="value">
								<div class="boxListing">
									<ul>
									<?php
									 	$i=1;
									    while ( have_rows('whats_in_the_box') ) : the_row();
									        echo '<li>'.$i.'. '.get_sub_field('enter_box_feature').'</li>';
											$i++;
									    endwhile;
									?>
									</ul>
								</div>
							</td>
						</tr>
						<?php
						endif;
						}
						?>
						</tbody>
				</table>
				<table class="variations" cellspacing="0">
					<tbody>
						
						<?php foreach ( $attributes as $name => $options ) : ?>
							<tr class="cart-item">
								<td class="label"><h4 for="<?php echo sanitize_title( $name ); ?>"><?php echo wc_attribute_label( $name ); ?></h4></td>
								<?php
								$sanitized_name = sanitize_title( $name );
								if ( isset( $_REQUEST[ 'attribute_' . $sanitized_name ] ) ) {
									$checked_value = $_REQUEST[ 'attribute_' . $sanitized_name ];
								} elseif ( isset( $selected_attributes[ $sanitized_name ] ) ) {
									$checked_value = $selected_attributes[ $sanitized_name ];
								} else {
									$checked_value = '';
								}
								?>
								<td class="value">
									<?php
									if ( ! empty( $options ) ) {
										if ( taxonomy_exists( $name ) ) {
											// Get terms if this is a taxonomy - ordered. We need the names too.
											$terms = wc_get_product_terms( $product->id, $name, array( 'fields' => 'all' ) );

											foreach ( $terms as $term ) {
												if ( ! in_array( $term->slug, $options ) ) {
													continue;
												}
												print_attribute_radio( $checked_value, $term->slug, $term->name, $sanitized_name );
											}
										} else {
											foreach ( $options as $option ) {
												print_attribute_radio( $checked_value, $option, $option, $sanitized_name );
											}
										}
									}

									echo end( $attribute_keys ) === $name ? apply_filters( 'woocommerce_reset_variations_link', '<a class="reset_variations" href="#">' . __( 'Clear', 'woocommerce' ) . '</a>' ) : '';
									?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<?php if(get_field('family_dose_calculator')) {?>
                <div class="cart-status">
                   not sure what you want? <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">click here</a>
                </div>
                <?php } ?>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-calculator" role="document">
                        <div class="btn-cross"> 
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>

                        <h2>Family dose calculator</h2>

                        <div class="family-member">
                         <div class="member-row">
                            <div class="member-l">Flavor</div>	
                            <div class="div-radio">						
								<div class="check_in" data-val="Citrus">
									<input type="radio" name="attribute_flavor_popup" id="flavr_Citrus">
									<label for="flavr_Citrus">Citrus</label>
								</div>
								<div class="check_in" data-val="Unflavored">
									<input type="radio" name="attribute_flavor_popup" id="flavr_Unflavored">
									<label for="flavr_Unflavored">Unflavored</label>
								</div>
							</div>	
							</div>
                            <div class="member-row">
                                <div class="member-l">Adults</div>
                                <div class="q_cell">
                                    <button class="adlt"><img src="<?php echo get_template_directory_uri();?>/images/minus.jpg" alt=""></button>
                                    <input type="text" value="1" name="adlt" class="qnt" readonly="readonly">
                                    <button class="plus"><img src="<?php echo get_template_directory_uri();?>/images/plus.jpg" alt=""></button>    
                                </div>
                            </div>
                            <div class="member-row">
                                <div class="member-l">Children(0-5)</div>
                                <div class="q_cell">
                                    <button class="minus"><img src="<?php echo get_template_directory_uri();?>/images/minus.jpg" alt=""></button>
                                    <input type="text" value="0" class="qnt" name="chld0" readonly="readonly">
                                    <button class="plus"><img src="<?php echo get_template_directory_uri();?>/images/plus.jpg" alt=""></button>    
                                </div>
                            </div>
                            <div class="member-row">
                                <div class="member-l">Children(5-15)</div>
                                <div class="q_cell">
                                    <button class="minus"><img src="<?php echo get_template_directory_uri();?>/images/minus.jpg" alt=""></button>
                                    <input type="text" value="0" name="chld5" class="qnt" readonly="readonly">
                                    <button class="plus"><img src="<?php echo get_template_directory_uri();?>/images/plus.jpg" alt=""></button>    
                                </div>
                            </div>
                        </div>
                        <div class="cart-status cart-member">
                            You will receive: <span><div id="qnty">3</div> bottles</span>  ships every <span><div id="days">51</div> days <div id="weeks">(7 weeks)</div></span> 

                        </div>
                        <div class="add-cart">
                            <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
								<?php
								/**
								 * woocommerce_before_single_variation Hook
								 */
								do_action( 'woocommerce_before_single_variation' );

								/**
								 * woocommerce_single_variation hook. Used to output the cart button and placeholder for variation data.
								 * @since 2.4.0
								 * @hooked woocommerce_single_variation - 10 Empty div for variation data.
								 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
								 */
								do_action( 'woocommerce_single_variation' );

								/**
								 * woocommerce_after_single_variation Hook
								 */
								do_action( 'woocommerce_after_single_variation' );
								?>
							<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
                        </div>

                        <div class="cart-status consultation">
                           free dose consultation <a href="javascript:void(0)" id="click1" data-toggle="modal" data-target="#myModal2">click here</a>
                        </div>

                    </div>
				</div>
               
                 <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                     <div class="modal-dialog modal-calculator outer-consult" role="document">
                        <div class="btn-cross"> 
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                         <div class="thank-pic"><img src="<?php echo get_template_directory_uri();?>/images/thankyou.jpg" alt="thankyou" /></div>
                        <h2>Thank You</h2>
                         <div class="consult-cont">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        </div>
                         <div class="add-cart">
                         	 <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close"> ok</button>
                        </div>
                     </div>    
                </div>
               
				<span class="orderArrival">Orders usually arrive within 3 business days</span>
				<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

				<div class="single_variation_wrap" style="display:none;">
					<?php
					/**
					 * woocommerce_before_single_variation Hook
					 */
					do_action( 'woocommerce_before_single_variation' );
	
					/**
					 * woocommerce_single_variation hook. Used to output the cart button and placeholder for variation data.
					 * @since 2.4.0
					 * @hooked woocommerce_single_variation - 10 Empty div for variation data.
					 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
					 */
					do_action( 'woocommerce_single_variation' );
	
					/**
					 * woocommerce_after_single_variation Hook
					 */
					do_action( 'woocommerce_after_single_variation' );
					?>
				</div>
	
				<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
				<?php
					$product = new WC_Product($product->id);
			    	$upsells = $product->get_upsells();
			    	if(!empty($upsells)){
			    		$thisproduct = wc_get_product( $upsells[0] );
					?>
					<a href="<?php echo get_permalink($upsells[0]); ?>" class="btn btn-product">
						<img src="<?php echo get_template_directory_uri();?>/images/circle.png"> 
						<span class="product-ab">
							<img src="<?php echo get_template_directory_uri();?>/images/circle-hvr.png">
						</span> 
						<?php 
						$sub_price = $thisproduct->get_price();
						$sub_price = number_format((float)$sub_price, 2, '.', '');
						echo 'Subscription available at: '.get_woocommerce_currency_symbol().$sub_price; ?>
					</a>		
				<?php }
				$free = get_woocommerce_currency_symbol().get_shipping_min_rate();
				?>
				<?php if (in_array("gift", $categories)):/*?>
				<div class="giftChoice">
					<label>
						<input type="checkbox" />
						<span>
							This will be a gift
						</span>
					</label>
				</div>
				<?php */ else:?>
				<h4 class="free-ship">Free Shipping on orders above <?php echo $free; ?>. In Stock. Cancel any time. </h4>
                <?php endif;?>
				
			
		<?php endif; ?>
	<?php endif; ?>

	<?php do_action( 'woocommerce_after_variations_form' ); ?>
</form>

 
<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<div class="modal fade" id="myModal2" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-calculator outer-consult" role="document">
        <div class="btn-cross"> 
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <h2>Free dose consultation</h2>
        <div class="account checkout-ship freedose">
            <?php echo do_shortcode('[contact-form-7 id="750" title="Free dose consultation"]'); ?>
        </div>
    </div>    
</div>
