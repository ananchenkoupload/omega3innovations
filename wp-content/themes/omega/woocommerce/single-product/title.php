<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author     WooThemes
 * @package    WooCommerce/Templates
 * @version    1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
global $product; if ( $product->is_type( 'gift-card' ) ) { ?><div class="row"><div class="gift-deta"><?php 
the_title( '<h1 itemprop="name">', '</h1>' ); } else{
	the_title( '<h1 itemprop="name">', '</h1>' );
}
$terms = wp_get_post_terms( get_the_ID(), 'product_cat' );
foreach ( $terms as $term ) $categories[] = $term->slug;

if (in_array("gift", $categories)){
	echo '<h5 class="prodCls">'.get_field('subtitle').'</h5>';
}
