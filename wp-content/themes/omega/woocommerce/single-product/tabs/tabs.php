<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );
$heading = apply_filters( 'woocommerce_product_additional_information_heading', __( 'Overview', 'woocommerce' ) );

?>

<?php 	                      	
if( have_rows('product_overview') ): while ( have_rows('product_overview') ) : the_row();
		if( get_row_layout() == 'product_overview' ):
			
			?>	
<div class="over-iiner">
    <h3><?php echo get_sub_field('overview_title'); ?></h3>
    <?php echo get_sub_field('overview_content'); ?>
</div>
 <?php   
	endif;
	endwhile;
	endif;
	
 ?>
<div class="suppliment row">
	 <div class="col-sm-7">
        <div class="recomend">
        	 <?php 	                      	
        if( have_rows('recommended_section') ): while ( have_rows('recommended_section') ) : the_row();
		if( get_row_layout() == 'recommended_section' ):			
			?>	
            <div class="over-iiner">
                <h3><?php echo get_sub_field('title'); ?></h3>
              <?php echo get_sub_field('content'); ?>
            </div>
             <?php   
		endif;
		endwhile;
		endif;
		
	 ?>
             <?php 	                      	
        if( have_rows('care_tips_sect') ): while ( have_rows('care_tips_sect') ) : the_row();
		if( get_row_layout() == 'care_tips_section' ):			
			?>	
            <div class="over-iiner">
                 <h3><?php echo get_sub_field('care_tips_title'); ?></h3>
                <?php echo get_sub_field('care_tips_content'); ?>
            </div>
		 <?php   
				endif;
				endwhile;
				endif;
				
			 ?>
        </div>
    </div>
    <div class="col-sm-5">
    	<?php                       	
        if( have_rows('suppliment_content_') ): while ( have_rows('suppliment_content_') ) : the_row();
		if( get_row_layout() == 'suppliment_content_' ):
			
			?>	
        <h3> <?php echo get_sub_field('title'); ?></h3>
        <?php echo get_sub_field('content'); ?>
        <?php   
		endif;
		endwhile;
		endif;
		
	 ?>
        <div class="market-table-outer">
            <div class="market-table">
            	<?php                       	
        if( have_rows('suppliment_content_') ): while ( have_rows('suppliment_content_') ) : the_row();
		if( get_row_layout() == 'suppliment_content_' ):
			
			?>	
                <div class="market-row table-head">
                    <div class="market-cell"><?php echo get_sub_field('table_first_tile'); ?></div>
                    <div class="market-cell"><?php echo get_sub_field('table_second_title'); ?></div>
                    <div class="market-cell"><?php echo get_sub_field('table_third_title'); ?></div>
                </div>
                  <?php   
		endif;
		endwhile;
		endif;
		
	 ?>
                <?php 	                      	
                if( have_rows('suppliment_content_') ): while ( have_rows('suppliment_content_') ) : the_row();
					if( get_row_layout() == 'suppliment_content_' ):
							                      	
                if( have_rows('suppliment_table') ): while ( have_rows('suppliment_table') ) : the_row();
					if( get_row_layout() == 'suppliment_table' ):
						?>	
                <div class="market-row">
                    <div class="market-cell">  <?php echo get_sub_field('omega-3'); ?></div>
                    <div class="market-cell">  <?php echo get_sub_field('amt_per_serving'); ?></div>
                    <div class="market-cell">  <?php echo get_sub_field('%_daily_value'); ?></div>
                </div>
                 <?php   
				endif;
				endwhile;
				endif;
				
				endif;
				endwhile;
				endif;
				
			 ?>
            </div>
        </div>
        <?php	                      	
        if( have_rows('suppliment_content_') ): while ( have_rows('suppliment_content_') ) : the_row();
		if( get_row_layout() == 'suppliment_content_' ):			
			?>	
        <div class="slider-detail">
            <h3><?php echo get_sub_field('suppliment_below_table_title'); ?></h3>
        </div>
         <?php   
		endif;
		endwhile;
		endif;
	
	 ?>
    </div>
   
</div>
