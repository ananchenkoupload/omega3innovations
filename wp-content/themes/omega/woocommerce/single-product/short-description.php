<?php
/**
 * Single product short description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

if ( ! $post->post_excerpt ) {
	return;
}

?>
<div class="summary-list-outer clearfix" itemprop="description">
	<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
</div>
<!-- <div class="quantity">
	 <div class="q_cell quantity">
	 		<span>Quantity</span>
	 		<div>
	        <button type="button" id="minus" class="minus"><img src="/wp-content/themes/omega/images/minus.jpg" alt=""></button>
	        <input type="number" step="1" min="1" name="quantity" value="1" title="<?php //echo esc_attr_x( 'Qty', 'Product quantity input tooltip', 'woocommerce' ) ?>" class="qnt input-text qty text" size="2" pattern="<?php //echo esc_attr( $pattern ); ?>" inputmode="<?php //echo esc_attr( $inputmode ); ?>" />
	        <button type="button" id="plus" class="plus"><img src="https://omega3innovations.swiftpim.in/wp-content/themes/omega/images/plus.jpg" alt=""></button>
	        </div>   
	 </div>	
</div> -->
