<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>
	<?php while ( have_posts() ) : the_post();?>
	<?php
		/**
		 * woocommerce_before_single_product hook.
		 *
		 * @hooked wc_print_notices - 10
		 */
		 do_action( 'woocommerce_before_single_product' );
	
		 if ( post_password_required() ) {
		 	echo get_the_password_form();
		 	return;
		 }
	?>
		<section class="product-detail heading-marg" itemscope itemtype="http://schema.org/Product" id="product-305">
			<div class="container">
		    	<div class="row product">
		    		<div class="col-sm-6">
						<?php
							/**
							 * woocommerce_before_single_product_summary hook.
							 *
							 * @hooked woocommerce_show_product_sale_flash - 10
							 * @hooked woocommerce_show_product_images - 20
							 */
							do_action( 'woocommerce_before_single_product_summary' );
						?>	
					</div>
					<div class="col-sm-6">
						<div class="summary">
							<?php
								/**
								 * woocommerce_single_product_summary hook.
								 *
								 * @hooked woocommerce_template_single_title - 5
								 * @hooked woocommerce_template_single_rating - 10
								 * @hooked woocommerce_template_single_price - 10
								 * @hooked woocommerce_template_single_excerpt - 20
								 * @hooked woocommerce_template_single_add_to_cart - 30
								 * @hooked woocommerce_template_single_meta - 40
								 * @hooked woocommerce_template_single_sharing - 50
								 */
								
								
								do_action( 'woocommerce_single_product_summary' );
								
								
							?>
						</div>
						<?php /*?><div class="summary">
							<h1 itemprop="name">Omega Passion Subscription</h1>
							<h5 class="prodCls">For the regular jetsetter</h5>
						<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
							<h4>&#36;71.8/subscription</h4>
							<meta itemprop="price" content="71.8" />
							<meta itemprop="priceCurrency" content="USD" />
							<link itemprop="availability" href="http://schema.org/InStock" />
						</div>
					<!-- <div class="summary-list-outer clearfix" itemprop="description">
						<ul style="width: 100%">
							<li><b>4 tins at $17.95/tin </b></li>
						</ul>
						<ul>
							<li>300 mg EPA/DHA per truffle</li>
							<li>Non-alkalized dark chocolate</li>
						</ul>
						<ul>
							<li>Only 4 grams of sugar</li>
							<li>Great as dessert or snack</li>
						</ul>
					</div> -->
					<form class="variations_form cart" method="post" enctype='multipart/form-data' data-product_id="305" data-product_variations="[{&quot;variation_id&quot;:4192,&quot;variation_is_visible&quot;:true,&quot;variation_is_active&quot;:true,&quot;is_purchasable&quot;:true,&quot;display_price&quot;:71.8,&quot;display_regular_price&quot;:71.8,&quot;attributes&quot;:{&quot;attribute_flavor&quot;:&quot;Mint&quot;,&quot;attribute_plan&quot;:&quot;8 weeks&quot;},&quot;image_src&quot;:&quot;&quot;,&quot;image_link&quot;:&quot;&quot;,&quot;image_title&quot;:&quot;&quot;,&quot;image_alt&quot;:&quot;&quot;,&quot;image_caption&quot;:&quot;&quot;,&quot;image_srcset&quot;:&quot;&quot;,&quot;image_sizes&quot;:&quot;&quot;,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt; &lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;&#036;&lt;\/span&gt;71.80&lt;\/span&gt; &lt;span class=\&quot;subscription-details\&quot;&gt; every 8 weeks&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;availability_html&quot;:&quot;&lt;p class=\&quot;stock in-stock\&quot;&gt;36 in stock&lt;\/p&gt;&quot;,&quot;sku&quot;:&quot;SUB-4MINT_8W&quot;,&quot;weight&quot;:&quot; kg&quot;,&quot;dimensions&quot;:&quot;&quot;,&quot;min_qty&quot;:1,&quot;max_qty&quot;:36,&quot;backorders_allowed&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_downloadable&quot;:false,&quot;is_virtual&quot;:false,&quot;is_sold_individually&quot;:&quot;no&quot;,&quot;variation_description&quot;:&quot;&quot;},{&quot;variation_id&quot;:4193,&quot;variation_is_visible&quot;:true,&quot;variation_is_active&quot;:true,&quot;is_purchasable&quot;:true,&quot;display_price&quot;:71.8,&quot;display_regular_price&quot;:71.8,&quot;attributes&quot;:{&quot;attribute_flavor&quot;:&quot;Mint&quot;,&quot;attribute_plan&quot;:&quot;6 weeks&quot;},&quot;image_src&quot;:&quot;&quot;,&quot;image_link&quot;:&quot;&quot;,&quot;image_title&quot;:&quot;&quot;,&quot;image_alt&quot;:&quot;&quot;,&quot;image_caption&quot;:&quot;&quot;,&quot;image_srcset&quot;:&quot;&quot;,&quot;image_sizes&quot;:&quot;&quot;,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt; &lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;&#036;&lt;\/span&gt;71.80&lt;\/span&gt; &lt;span class=\&quot;subscription-details\&quot;&gt; every 6 weeks&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;availability_html&quot;:&quot;&lt;p class=\&quot;stock in-stock\&quot;&gt;36 in stock&lt;\/p&gt;&quot;,&quot;sku&quot;:&quot;SUB-4MINT_6W&quot;,&quot;weight&quot;:&quot; kg&quot;,&quot;dimensions&quot;:&quot;&quot;,&quot;min_qty&quot;:1,&quot;max_qty&quot;:36,&quot;backorders_allowed&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_downloadable&quot;:false,&quot;is_virtual&quot;:false,&quot;is_sold_individually&quot;:&quot;no&quot;,&quot;variation_description&quot;:&quot;&quot;},{&quot;variation_id&quot;:4194,&quot;variation_is_visible&quot;:true,&quot;variation_is_active&quot;:true,&quot;is_purchasable&quot;:true,&quot;display_price&quot;:71.8,&quot;display_regular_price&quot;:71.8,&quot;attributes&quot;:{&quot;attribute_flavor&quot;:&quot;Mint&quot;,&quot;attribute_plan&quot;:&quot;4 weeks&quot;},&quot;image_src&quot;:&quot;&quot;,&quot;image_link&quot;:&quot;&quot;,&quot;image_title&quot;:&quot;&quot;,&quot;image_alt&quot;:&quot;&quot;,&quot;image_caption&quot;:&quot;&quot;,&quot;image_srcset&quot;:&quot;&quot;,&quot;image_sizes&quot;:&quot;&quot;,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt; &lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;&#036;&lt;\/span&gt;71.80&lt;\/span&gt; &lt;span class=\&quot;subscription-details\&quot;&gt; every 4 weeks&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;availability_html&quot;:&quot;&lt;p class=\&quot;stock in-stock\&quot;&gt;36 in stock&lt;\/p&gt;&quot;,&quot;sku&quot;:&quot;SUB-4MINT_4W&quot;,&quot;weight&quot;:&quot; kg&quot;,&quot;dimensions&quot;:&quot;&quot;,&quot;min_qty&quot;:1,&quot;max_qty&quot;:36,&quot;backorders_allowed&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_downloadable&quot;:false,&quot;is_virtual&quot;:false,&quot;is_sold_individually&quot;:&quot;no&quot;,&quot;variation_description&quot;:&quot;&quot;},{&quot;variation_id&quot;:4195,&quot;variation_is_visible&quot;:true,&quot;variation_is_active&quot;:true,&quot;is_purchasable&quot;:true,&quot;display_price&quot;:71.8,&quot;display_regular_price&quot;:71.8,&quot;attributes&quot;:{&quot;attribute_flavor&quot;:&quot;Mint&quot;,&quot;attribute_plan&quot;:&quot;2 weeks&quot;},&quot;image_src&quot;:&quot;&quot;,&quot;image_link&quot;:&quot;&quot;,&quot;image_title&quot;:&quot;&quot;,&quot;image_alt&quot;:&quot;&quot;,&quot;image_caption&quot;:&quot;&quot;,&quot;image_srcset&quot;:&quot;&quot;,&quot;image_sizes&quot;:&quot;&quot;,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt; &lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;&#036;&lt;\/span&gt;71.80&lt;\/span&gt; &lt;span class=\&quot;subscription-details\&quot;&gt; every 2 weeks&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;availability_html&quot;:&quot;&lt;p class=\&quot;stock in-stock\&quot;&gt;36 in stock&lt;\/p&gt;&quot;,&quot;sku&quot;:&quot;SUB-4MINT_2W&quot;,&quot;weight&quot;:&quot; kg&quot;,&quot;dimensions&quot;:&quot;&quot;,&quot;min_qty&quot;:1,&quot;max_qty&quot;:36,&quot;backorders_allowed&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_downloadable&quot;:false,&quot;is_virtual&quot;:false,&quot;is_sold_individually&quot;:&quot;no&quot;,&quot;variation_description&quot;:&quot;&quot;},{&quot;variation_id&quot;:4196,&quot;variation_is_visible&quot;:true,&quot;variation_is_active&quot;:true,&quot;is_purchasable&quot;:true,&quot;display_price&quot;:71.8,&quot;display_regular_price&quot;:71.8,&quot;attributes&quot;:{&quot;attribute_flavor&quot;:&quot;Classic&quot;,&quot;attribute_plan&quot;:&quot;8 weeks&quot;},&quot;image_src&quot;:&quot;&quot;,&quot;image_link&quot;:&quot;&quot;,&quot;image_title&quot;:&quot;&quot;,&quot;image_alt&quot;:&quot;&quot;,&quot;image_caption&quot;:&quot;&quot;,&quot;image_srcset&quot;:&quot;&quot;,&quot;image_sizes&quot;:&quot;&quot;,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt; &lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;&#036;&lt;\/span&gt;71.80&lt;\/span&gt; &lt;span class=\&quot;subscription-details\&quot;&gt; every 8 weeks&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;availability_html&quot;:&quot;&lt;p class=\&quot;stock in-stock\&quot;&gt;36 in stock&lt;\/p&gt;&quot;,&quot;sku&quot;:&quot;SUB-4CLASSIC_8W&quot;,&quot;weight&quot;:&quot; kg&quot;,&quot;dimensions&quot;:&quot;&quot;,&quot;min_qty&quot;:1,&quot;max_qty&quot;:36,&quot;backorders_allowed&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_downloadable&quot;:false,&quot;is_virtual&quot;:false,&quot;is_sold_individually&quot;:&quot;no&quot;,&quot;variation_description&quot;:&quot;&quot;},{&quot;variation_id&quot;:4197,&quot;variation_is_visible&quot;:true,&quot;variation_is_active&quot;:true,&quot;is_purchasable&quot;:true,&quot;display_price&quot;:71.8,&quot;display_regular_price&quot;:71.8,&quot;attributes&quot;:{&quot;attribute_flavor&quot;:&quot;Classic&quot;,&quot;attribute_plan&quot;:&quot;6 weeks&quot;},&quot;image_src&quot;:&quot;&quot;,&quot;image_link&quot;:&quot;&quot;,&quot;image_title&quot;:&quot;&quot;,&quot;image_alt&quot;:&quot;&quot;,&quot;image_caption&quot;:&quot;&quot;,&quot;image_srcset&quot;:&quot;&quot;,&quot;image_sizes&quot;:&quot;&quot;,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt; &lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;&#036;&lt;\/span&gt;71.80&lt;\/span&gt; &lt;span class=\&quot;subscription-details\&quot;&gt; every 6 weeks&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;availability_html&quot;:&quot;&lt;p class=\&quot;stock in-stock\&quot;&gt;36 in stock&lt;\/p&gt;&quot;,&quot;sku&quot;:&quot;SUB-4CLASSIC_6W&quot;,&quot;weight&quot;:&quot; kg&quot;,&quot;dimensions&quot;:&quot;&quot;,&quot;min_qty&quot;:1,&quot;max_qty&quot;:36,&quot;backorders_allowed&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_downloadable&quot;:false,&quot;is_virtual&quot;:false,&quot;is_sold_individually&quot;:&quot;no&quot;,&quot;variation_description&quot;:&quot;&quot;},{&quot;variation_id&quot;:4198,&quot;variation_is_visible&quot;:true,&quot;variation_is_active&quot;:true,&quot;is_purchasable&quot;:true,&quot;display_price&quot;:71.8,&quot;display_regular_price&quot;:71.8,&quot;attributes&quot;:{&quot;attribute_flavor&quot;:&quot;Classic&quot;,&quot;attribute_plan&quot;:&quot;4 weeks&quot;},&quot;image_src&quot;:&quot;&quot;,&quot;image_link&quot;:&quot;&quot;,&quot;image_title&quot;:&quot;&quot;,&quot;image_alt&quot;:&quot;&quot;,&quot;image_caption&quot;:&quot;&quot;,&quot;image_srcset&quot;:&quot;&quot;,&quot;image_sizes&quot;:&quot;&quot;,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt; &lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;&#036;&lt;\/span&gt;71.80&lt;\/span&gt; &lt;span class=\&quot;subscription-details\&quot;&gt; every 4 weeks&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;availability_html&quot;:&quot;&lt;p class=\&quot;stock in-stock\&quot;&gt;36 in stock&lt;\/p&gt;&quot;,&quot;sku&quot;:&quot;SUB-4CLASSIC_4W&quot;,&quot;weight&quot;:&quot; kg&quot;,&quot;dimensions&quot;:&quot;&quot;,&quot;min_qty&quot;:1,&quot;max_qty&quot;:36,&quot;backorders_allowed&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_downloadable&quot;:false,&quot;is_virtual&quot;:false,&quot;is_sold_individually&quot;:&quot;no&quot;,&quot;variation_description&quot;:&quot;&quot;},{&quot;variation_id&quot;:4199,&quot;variation_is_visible&quot;:true,&quot;variation_is_active&quot;:true,&quot;is_purchasable&quot;:true,&quot;display_price&quot;:71.8,&quot;display_regular_price&quot;:71.8,&quot;attributes&quot;:{&quot;attribute_flavor&quot;:&quot;Classic&quot;,&quot;attribute_plan&quot;:&quot;2 weeks&quot;},&quot;image_src&quot;:&quot;&quot;,&quot;image_link&quot;:&quot;&quot;,&quot;image_title&quot;:&quot;&quot;,&quot;image_alt&quot;:&quot;&quot;,&quot;image_caption&quot;:&quot;&quot;,&quot;image_srcset&quot;:&quot;&quot;,&quot;image_sizes&quot;:&quot;&quot;,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt; &lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;&#036;&lt;\/span&gt;71.80&lt;\/span&gt; &lt;span class=\&quot;subscription-details\&quot;&gt; every 2 weeks&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;availability_html&quot;:&quot;&lt;p class=\&quot;stock in-stock\&quot;&gt;36 in stock&lt;\/p&gt;&quot;,&quot;sku&quot;:&quot;SUB-4CLASSIC_2W&quot;,&quot;weight&quot;:&quot; kg&quot;,&quot;dimensions&quot;:&quot;&quot;,&quot;min_qty&quot;:1,&quot;max_qty&quot;:36,&quot;backorders_allowed&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_downloadable&quot;:false,&quot;is_virtual&quot;:false,&quot;is_sold_individually&quot;:&quot;no&quot;,&quot;variation_description&quot;:&quot;&quot;}]">
						<table class="variations" cellspacing="0">
							<tbody>
								<tr class="cart-item">
									<td class="label boxHdr"><h4 for="flavor">
										<span class="figIco">
											<img src="https://richpanel.com/wp-content/themes/omega/images/gift.png" alt="" />
										</span>
										What's in the box</h4>
									</td>
									<td class="value">
										<div class="boxListing">
											<ul>
												<li>
													1.  Omega Cure -15 ml servings (17 bottles)
												</li>
												<li>
												2.  Omega Cure Extra Strength
												</li>
												<li>
												3.  Omega Cookies (7 cookies)
												</li>
												<li>
												4.  Omega Heaven (10 cookies)
												</li>
											</ul>
										</div>
									</td>
								</tr>

								<tr class="cart-item">
									<td class="label"><h4 for="flavor">Choose a flavor for your omega 3 vials</h4></td>
									<td class="value">
									<div class="check_in" data-val="Classic"><input type="radio" name="attribute_flavor" value="Citrus" id="flavor_v_Classic" ><label for="flavor_v_Classic">Citrus</label></div><div class="check_in" data-val="Mint"><input type="radio" name="attribute_flavor" value="Unflavored" id="flavor_v_Mint" ><label for="flavor_v_Mint">Unflavored</label></div>								</td>
								</tr>
								<tr class="cart-item">
									<td class="label"><h4 for="plan">Choose your plan</h4></td>
									<td class="value">
									<div class="check_in" data-val="2 weeks">
										<input type="radio" name="attribute_plan" value="Buy once" id="plan_v_2 weeks" >
										<label for="plan_v_2 weeks">Buy once</label>
									</div>
									<div class="check_in" data-val="4 weeks">
										<input type="radio" name="attribute_plan" value="3 month supply" id="plan_v_4 weeks" >
										<label for="plan_v_4 weeks">3 month supply</label>
									</div>
									<div class="check_in" data-val="6 weeks">
										<input type="radio" name="attribute_plan" value="6 month supply" id="plan_v_6 weeks" >
										<label for="plan_v_6 weeks">6 month supply</label>
									</div>
									<a class="reset_variations" href="#">Clear</a>								</td>
								</tr>
							</tbody>
						</table>
				        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		                    <div class="modal-dialog modal-calculator" role="document">
		                        <div class="btn-cross"> 
		                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                        </div>
		
		                        <h2>Family dose calculator</h2>
		
		                        <div class="family-member">
		                         <div class="member-row">
		                            <div class="member-l">Flavor</div>	
		                            <div class="div-radio">						
										<div class="check_in" data-val="Citrus">
											<input type="radio" name="attribute_flavor_popup" id="flavr_Citrus">
											<label for="flavr_Citrus">Citrus</label>
										</div>
										<div class="check_in" data-val="Unflavored">
											<input type="radio" name="attribute_flavor_popup" id="flavr_Unflavored">
											<label for="flavr_Unflavored">Unflavored</label>
										</div>
									</div>	
									</div>
		                            <div class="member-row">
		                                <div class="member-l">Adults</div>
		                                <div class="q_cell">
		                                    <button class="adlt"><img src="https://richpanel.com/wp-content/themes/omega/images/minus.jpg?x11860" alt=""></button>
		                                    <input type="text" value="1" name="adlt" class="qnt" readonly="readonly">
		                                    <button class="plus"><img src="https://richpanel.com/wp-content/themes/omega/images/plus.jpg?x11860" alt=""></button>    
		                                </div>
		                            </div>
		                            <div class="member-row">
		                                <div class="member-l">Children(0-5)</div>
		                                <div class="q_cell">
		                                    <button class="minus"><img src="https://richpanel.com/wp-content/themes/omega/images/minus.jpg?x11860" alt=""></button>
		                                    <input type="text" value="0" class="qnt" name="chld0" readonly="readonly">
		                                    <button class="plus"><img src="https://richpanel.com/wp-content/themes/omega/images/plus.jpg?x11860" alt=""></button>    
		                                </div>
		                            </div>
		                            <div class="member-row">
		                                <div class="member-l">Children(5-15)</div>
		                                <div class="q_cell">
		                                    <button class="minus"><img src="https://richpanel.com/wp-content/themes/omega/images/minus.jpg?x11860" alt=""></button>
		                                    <input type="text" value="0" name="chld5" class="qnt" readonly="readonly">
		                                    <button class="plus"><img src="https://richpanel.com/wp-content/themes/omega/images/plus.jpg?x11860" alt=""></button>    
		                                </div>
		                            </div>
		                        </div>
		                        <div class="cart-status cart-member">
		                            You will receive: <span><div id="qnty">3</div> bottles</span>  ships every <span><div id="days">51</div> days <div id="weeks">(7 weeks)</div></span> 
		
		                        </div>
                        		<div class="add-cart">
                            		<div class="woocommerce-variation single_variation"></div>
                            		<div class="woocommerce-variation-add-to-cart variations_button add-cart">
										<button type="submit" class="single_add_to_cart_button button alt btn btn-default">ADD TO CART</button>
										<input type="hidden" name="add-to-cart" value="305" />
										<input type="hidden" name="product_id" value="305" />
										<input type="hidden" name="variation_id" class="variation_id" value="0" />
									</div>
							    </div>
		                        <div class="cart-status consultation">
		                           free dose consultation <a href="javascript:void(0)" id="click1" data-toggle="modal" data-target="#myModal2">click here</a>
		                        </div>
                    		</div>
						</div>
               
		                 <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		                     <div class="modal-dialog modal-calculator outer-consult" role="document">
		                        <div class="btn-cross"> 
		                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                        </div>
		                         <div class="thank-pic"><img src="https://richpanel.com/wp-content/themes/omega/images/thankyou.jpg?x11860" alt="thankyou" /></div>
		                        <h2>Thank You</h2>
		                         <div class="consult-cont">
		                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
		                        </div>
		                         <div class="add-cart">
		                         	 <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close"> ok</button>
		                        </div>
		                     </div>    
						</div>
						
						<div class="giftChoice">
							<label>
								<input type="checkbox" />
								<span>
									This will be a gift
								</span>
							</label>
						</div>
						<div class="add-cart">
							<button type="button" class="btn btn-default"> Add to cart</button>
						</div>
						
                		<!-- <h4 class="free-ship">Free Shipping on orders above &#36;110. In Stock. Cancel any time. </h4> -->

						<div class="single_variation_wrap" style="display:none;">
							<div class="woocommerce-variation single_variation"></div><div class="woocommerce-variation-add-to-cart variations_button add-cart">
								<button type="submit" class="single_add_to_cart_button button alt btn btn-default">Add to cart</button>
								<input type="hidden" name="add-to-cart" value="305" />
								<input type="hidden" name="product_id" value="305" />
								<input type="hidden" name="variation_id" class="variation_id" value="0" />
							</div>
						</div>				
					</form>
					<div class="modal fade" id="myModal2" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
					    <div class="modal-dialog modal-calculator outer-consult" role="document">
					        <div class="btn-cross"> 
					            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        </div>
					        <h2>Free dose consultation</h2>
					        <div class="account checkout-ship freedose">
					            [contact-form-7 404 "Not Found"]        </div>
					    </div>    
					</div>
					<p class="first-payment-date"><small></small></p>
					<div class="product_meta">
						<span class="posted_in">Category: <a href="https://richpanel.com/product-category/subscription/cookies-subscriptionn/" rel="tag">Cookies &amp; more subscription</a></span>
					</div>
				</div><!-- .summary -->
				<?php */?>
			</div>
		</div>
	</div>


	  <div class="product-panel sequance">
    <?php  $i=0;		                      	
        if( have_rows('products_content') ): while ( have_rows('products_content') ) : the_row();
				if( get_row_layout() == 'products_content' ):		
		?>	
        <div class="seq-outer">
           <div class="seq-list" style="background-image: url(<?php echo get_sub_field('product_image'); ?>);">
                
            </div> 
            <div class="container text-container">
                <div class="row">
                    <div class="col-sm-5 <?php if($i%2==1){ ?>col-sm-offset-7<?php } $i++;?>">
                        <div class="seq-inner">
                             <h2>  <?php echo get_sub_field('product_title'); ?></h2>
                             <?php echo get_sub_field('product_content'); ?>
                        </div>    
                    </div>
                </div>
            </div>
        </div>    
        
        <?php   
			endif;endwhile;endif;
			wp_reset_query();
	   ?>
    </div>
    <div class="container">
    <?php comments_template(); ?>
    </div>
	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</section><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>

<?php endwhile; // end of the loop. ?>

<?php
	/**
	 * woocommerce_after_main_content hook.
	 *
	 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
	 */
	do_action( 'woocommerce_after_main_content' );
?>
<?php get_footer( 'shop' ); ?>