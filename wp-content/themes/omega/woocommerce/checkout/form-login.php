<?php
/**
 * Login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( is_user_logged_in() ) {
	return;
}

?>
<form method="post" class="login" <?php if ( $hidden ) echo 'style="display:none;"'; ?>>

	<?php do_action( 'woocommerce_login_form_start' ); ?>

	<?php if ( $message ) echo wpautop( wptexturize( $message ) ); ?>

	<div class="account">
		<div class="check-input">
			<input type="text" class="form-control" placeholder="Email" name="username" value="register" id="username" />
		</div>
		<div class="check_in">
			<input id="order1" type="radio" name="radio-2" data-name="register">
			<label for="order1">
				<h4>New Customers</h4>
				<div class="signindtls">
					<h5>Create an account with us for quick checkout, 
						to keep track of your order, and more.</h5>
				</div>
			</label>   
		</div>
		<div class="check_in">
			<label>Email</label>
			<input id="order2" type="radio" name="radio-2" data-name="signin" value="signin">
			<label for="order2">
				<h4>Returning Customers</h4>
			</label>   
		</div>
		<div class="check-input">
			<label>Password</label>
			<input class="form-control" type="password" name="password" id="password" />
		</div>
		<!-- <div class="create-part">
			<input type="submit" class="btn btn-default" value="Create Account" /> 
			<a href="#">Sign in with Facebook</a>
		</div> -->
		<?php do_action( 'woocommerce_login_form' ); ?>

		<div class="form-row">
			<?php wp_nonce_field( 'woocommerce-login' ); ?>
			<input type="submit" class="btn btn-default form_txt" name="login" value="<?php esc_attr_e( 'Sign in', 'woocommerce' ); ?>" />
			<input type="hidden" name="redirect" value="<?php echo esc_url( $redirect ) ?>" />
			<label for="rememberme" class="inline">
				<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php _e( 'Remember me', 'woocommerce' ); ?>
			</label>
		</div>
		<div class="lost_password signindtls">
			<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>?referer=<?php echo rawurlencode(get_the_permalink());?>"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>
		</div>
	</div>
	<div class="clear"></div>

	<?php do_action( 'woocommerce_login_form_end' ); ?>

</form>
