<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

?>
<script src="<?php echo get_template_directory_uri().'/js/jquery.validate.min.js';?>"></script>
<?php
$enable_checkout_login_reminder = 'yes' == get_option( 'woocommerce_enable_checkout_login_reminder', 'yes' ) ? true : false;
$is_user_logged_in = is_user_logged_in();
$show_login_step = ! $is_user_logged_in && $enable_checkout_login_reminder;

$button_class = apply_filters( 'yith_wcms_step_button_class', 'button alt yith-wcms-button' );

$labels = apply_filters( 'yith_wcms_timeline_labels', array(
        'next'     => __( 'Next', 'yith-woocommerce-multi-step-checkout' ),
        'prev'     => __( 'Previous', 'yith-woocommerce-multi-step-checkout' ),
        'login'    => _x( 'Login', 'Checkout: user timeline', 'yith-woocommerce-multi-step-checkout' ),
        'billing'  => _x( 'Billing', 'Checkout: user timeline', 'yith-woocommerce-multi-step-checkout' ),
        'shipping' => _x( 'Shipping', 'Checkout: user timeline', 'yith-woocommerce-multi-step-checkout' ),
        'order'    => _x( 'Order Info', 'Checkout: user timeline', 'yith-woocommerce-multi-step-checkout' ),
        'payment'  => _x( 'Select Card', 'Checkout: user timeline', 'yith-woocommerce-multi-step-checkout' )
    )
);

$display = apply_filters( 'yith_wcms_timeline_display', 'horizontal' );

$timeline_args = array(
    'labels'            => $labels,
    'is_user_logged_in' => $is_user_logged_in,
    'display'           => $display,
    'style'             => get_option( 'yith_wcms_timeline_template', 'text' )
);

$timeline_template = apply_filters( 'yith_wcms_timeline_template', 'checkout-timeline.php' );

$enable_nav_button = 'yes' == get_option( 'yith_wcms_nav_buttons_enabled', 'yes' ) ? true : false;

$enable_checkout_login_reminder = 'yes' == get_option( 'woocommerce_enable_checkout_login_reminder', 'yes' ) ? true : false;

$login_message = apply_filters( 'yith_wcms_form_checkout_login_message', __( 'If you have already registered, please, enter your details in the boxes below. If you are a new customer, please, go to the Billing &amp; Shipping section.', 'yith-woocommerce-multi-step-checkout' ) );

//load timeline template
function_exists( 'yith_wcms_checkout_timeline' ) && yith_wcms_checkout_timeline( $timeline_template, $timeline_args );

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );
?>

    <div id="checkout-wrapper" class="row timeline-<?php echo $display ?>">
        <div id="checkout_coupon" class="woocommerce_checkout_coupon">
            <?php //do_action( 'yith_woocommerce_checkout_coupon', $checkout ); ?>
        </div>
        <?php if( $enable_checkout_login_reminder ) : ?>
            <div id="checkout_login" class="woocommerce_checkout_login">
                <?php if(!is_user_logged_in()){ ?>
                <div class="account-creat">  
                <?php
                woocommerce_login_form(
                    array(
                        'message'  =>  __( '<h2 class="form_txt">Sign In</h2>', 'yith-woocommerce-multi-step-checkout' ),
                        'redirect' => wc_get_checkout_url(),
                        'hidden'   => false
                    )
                ); 
                echo do_shortcode('[woocommerce_simple_registration]');
                }
                ?>
                </div>
            </div>
        <?php endif; ?>

        <?php

        // If checkout registration is disabled and not logged in, the user cannot checkout
        if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
            echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in before proceeding to checkout.', 'woocommerce' ) );
            return;
        }

        // filter hook for include new pages inside the payment method
        $get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', wc_get_checkout_url() ); ?>
        <div class="coupon-outer">
            <a href="#" class="showcoupon">+ Have a coupon click here</a>
        </div>
		<form class="checkout_coupon" method="post" style="display:none">		
			<p class="form-row form-row-first">
				<input type="text" name="coupon_code" class="input-text check-cou" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" id="coupon_code" value="" />
			</p>		
			<p class="form-row form-row-last">
				<input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" />
			</p>
		</form>
		<form class="ywgc-enter-code" method="post" style="display: none;">
			<p class="form-row n-focus">
				<input type="text" name="gift_card_code" class="input-text" placeholder="Gift card code" id="giftcard_code" value="">
				<input type="submit" class="button" name="apply_gift_card" value="Apply gift card">
				<input type="hidden" name="is_gift_card" value="1">
			</p>
		
			<p class="form-row form-row-last">
		
			</p>
		
			<div class="clear"></div>
		</form>
        <form name="checkout" method="post" class="checkout woocommerce-checkout clearfix" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data" <?php if(!$is_user_logged_in){ echo 'style="display:none;"'; } ?>>
             <div class="row prevsteps">
                 <div class="col-sm-8 main-checkout">  
                <?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

                    <div class="checkout_billing <?php echo $is_user_logged_in ? 'logged-in' : 'not-logged-in'; echo $enable_checkout_login_reminder ? ' show-login-reminder' : ' hide-login-reminder'; ?>" id="customer_billing_details">
                        <?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
                        <?php do_action( 'woocommerce_checkout_billing' ); ?>                    
                    </div>

                    <div class="checkout_shipping account" id="customer_shipping_details">
                    	  <?php if ( ! WC()->cart->is_empty() ) : ?>
                    	 <!-- <div class="free-inner check-freeship">
                <?php global $woocommerce;
                    $free = get_shipping_min_rate(); 
                    $subtotal = $woocommerce->cart->subtotal;
                    if($free > $subtotal){
                        $remain = $free - $subtotal;
                        echo 'You\'re <b>'.get_woocommerce_currency_symbol().$remain.'</b> away from free shipping';
                    }else{
                        echo 'Free Shipping';
                    }
                ?>
                
                <?php $shop_page_url = get_permalink( get_page_by_path( 'store' ) ); ?>
                <a href="<?php echo $shop_page_url; ?>">Continue Shopping ></a>
            </div> -->
            <?php endif; ?>
                        <?php
                        do_action( 'woocommerce_before_checkout_shipping_form', $checkout );
                        do_action( 'woocommerce_checkout_shipping' );
                        do_action( 'woocommerce_checkout_after_customer_details' );
                        ?>      
                         <h3 class="shipping_help">How should we ship todays order?</h3>
                        
                        <?php do_action( 'yith_woocommerce_checkout_order_review' ); ?>
						<?php do_action( 'woocommerce_checkout_order_review' ); ?>
                        <div class="note-card">
                            <a href="javascript:void(0)" id="note-card">+ Add a note</a>
                        </div>
                        <div class="note-box">
                            <textarea name="order_comments" class="input-text form-control" id="order_comments" placeholder="Notes about your order, e.g. special notes for delivery." ></textarea>
                        </div>              
                    </div>
                 <?php endif; ?>
                 <?php do_action( 'woocommerce_checkout_before_order_review' ); ?>                
                    <div id="order_info" class="woocommerce-checkout-review-order account">   
                        <?php do_action( 'yith_woocommerce_checkout_payment' ); ?>
                    </div>
                <?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
               
                </div>  
                <?php //if(is_user_loggedin()):?>
                <div class="col-sm-3 col-sm-offset-1 <?php if(!$is_user_logged_in){ echo 'signindtls'; } ?>">
                	
                    <div class="checkout-ship">
                         <h3>Order Summary</h3>
                         <div class="cart-product">
                            <ul>
                                <?php
                                do_action( 'woocommerce_review_order_before_cart_contents' );
                                foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                                $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                                $product_id = $_product->ID;
								
                                if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                                $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                                ?>
                            <li>
                                <?php
                                $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

                                if ( ! $product_permalink ) {
                                    echo $thumbnail;
                                } else {
                                    printf( '<div class="cart-productName cartpicSize"><a href="%s">%s</a></div>', esc_url( $product_permalink ), $thumbnail );
                                }
                                echo '<div class="cart-productName cartText">';
                                    if ( ! $product_permalink ) {
                                        echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
                                    } else {
                                        echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<h3><a href="%s">%s</a></h3>', esc_url( $product_permalink ),$_product->get_title() ), $cart_item, $cart_item_key );
                                    }
                                if($_product->product_type=='gift-card'):?>
                                	<h4><?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s%s', $cart_item['quantity'], get_woocommerce_currency_symbol(), $_product->price ) . '</span>', $cart_item, $cart_item_key ); ?></h4>
          						<?php else:?>
                                	<h4><?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $_product->get_price_html() ) . '</span>', $cart_item, $cart_item_key ); ?></h4>
                                <?php endif;?>
                                <?php                                
                                echo '</div>';
                                ?>
                                <div class="cart-productName del">
                                    <?php
                                        echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                            '<a href="%s"  title="%s" data-product_id="%s" data-product_sku="%s"><i class="fa fa-trash" aria-hidden="true"></i></a>',
                                            esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
                                            __( 'Remove this item', 'woocommerce' ),
                                            esc_attr( $product_id ),
                                            esc_attr( $_product->get_sku() )
                                        ), $cart_item_key );
                                    ?>
                                </div>
                            </li>
                            <?php           
                                    }
                                }

                                do_action( 'woocommerce_review_order_after_cart_contents' );
                            ?>
                            </ul>
                             <div class="productAmount">
                                <ul>
                                    <li>
                                        <span class="pull-left">Subtotal</span>
                                        <span id="sbtotal" class="pull-right"><?php wc_cart_totals_subtotal_html(); ?></span>
                                    </li>

                                    <?php 
                                    if( !WC()->cart->get_coupons() ){
                                        $displaycoupon = 'style="display: none;"';
                                    }else{
                                        $displaycoupon = 'style="display: block;"';
                                    } ?>
                                    <li <?php echo $displaycoupon; ?>>
                                        <span class="pull-left">Coupon</span>
                                        <span class="pull-right cartamount"><?php
                                        foreach ( WC()->cart->get_coupons() as $code => $coupon ) :
                                            wc_cart_totals_coupon_html( $coupon ); 
                                        endforeach;
                                         ?></span>
                                    </li>
                                    
                                    <?php 
                                    if( !WC()->cart->applied_gift_cards ){
                                        $displaygift = 'style="display: none;"';
                                    }else{
                                        $displaygift = 'style="display: block;"';
                                    } ?>
                                    <li <?php echo $displaygift; ?> class="gift_card_listing">
                                    	<div class="checkout_gift_cards_container">
                                        <span class="pull-left">Gift Card: </span>
                                        <span class="pull-right giftamount"><?php
                                        foreach ( WC()->cart->applied_gift_cards as $code => $gift ) :
                                        ?>
                                        <?php echo WC()->cart->applied_gift_cards[$code];?> - <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol();?></span><?php echo WC()->cart->applied_gift_cards_amounts[WC()->cart->applied_gift_cards[$code]];?></span> 
                                        <a href="<?php echo WC()->cart->get_cart_url().'?remove_gift_card_code='.WC()->cart->applied_gift_cards[$code];?>" class="ywgc-remove-gift-card pull-right" data-gift-card-code="<?php echo WC()->cart->applied_gift_cards[$code];?>">[Remove]</a>
                                        </br>
                                        <?php
                                        endforeach;
                                         ?></span>
                                        </div>
                                    </li>
                                    <?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

                                    <?php do_action( 'woocommerce_review_order_before_shipping' ); ?>
                                     <?php 
                                        $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );    
								                               
                                        //wc_cart_totals_shipping_html(); 
                                        $packages = WC()->shipping->get_packages();  
										
                                    ?>
                                    <?php $cart_total = WC()->cart->get_cart_total();    $free = get_shipping_min_rate();
                                    //if($cart_total > $free){?>
                                    <!-- <script>
                                    	 $(window).load(function(){
								        	setTimeout(function(){
									           $('.shipping_method').each(function(){
										           //	var _this = $(this);
										           	
										           		$('#shipping_method_0_free_shipping6').attr('checked', true);
										          $("#freeval").html($("#que_id").val());
									           })
									        }, 550); });
                                    	  
                                    </script> -->
                                    <?php //} ?>
                                    <li>
                                    	
                                        <span class="pull-left">Shipping</span>
                                        <span class="pull-right shippingright">
                                        <?php foreach ($packages as $i => $package['rates']) {
                                           $chosen_method = isset(WC()->session->chosen_shipping_methods[$i]) ? WC()->session->chosen_shipping_methods[$i] : '';
                                          ?><input type="hidden" id="que_id" value='Free Standard Shipping (2-3 days)' /><?php
												 echo '<span id="freeval">'.$packages[0]['rates'][$chosen_method]->label.' : '.'</span>';
												  echo '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">'.get_woocommerce_currency_symbol().'</span>'.$packages[0]['rates'][$chosen_method]->cost.'</span>';
										
                                          
                                            }     
                                        ?>                                        
                                        </span>
                                    </li>
                                   

                                    <?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

                                    <?php endif; ?>
                                    
                                    <?php if ( wc_tax_enabled() && 'excl' === WC()->cart->tax_display_cart ) : ?>
                                        <?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
                                            <?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
                                                <li>
                                                    <span class="pull-left">Tax</span>
                                                    <span class="pull-right"><?php echo wp_kses_post( $tax->formatted_amount ); ?></span>
                                                </li>                       
                                            <?php endforeach; ?>
                                        <?php else : ?>
                                            <li>
                                                <span class="pull-left">Tax</span>
                                                <span class="pull-right">Free</span>
                                            </li>   
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php //do_action( 'woocommerce_review_order_before_order_total' ); ?>
                                    <li>
                                        <span class="pull-left">Total</span>
                                        <span class="pull-right totalamount"><?php wc_cart_totals_order_total_html(); ?></span>
                                    </li>
                                    <?php //do_action( 'woocommerce_review_order_after_order_total' ); ?>
                                </ul>
                             </div>
                             <p class="form-row account">
                                <label for="coupon" class="">Coupon Code</label>
                                <input type="text" class="input-text applycoupon">
                             </p>
                             <div class="apply">
                                <a href="javascript:void(0);" class="btn btn-default applycouponbtn">Apply</a>
                             </div>
                             
                             <p class="form-row account gift-card">
                                <label for="coupon" class="">Gift card code</label>
                                <input type="text" class="input-text giftcoupon">
                             </p>
                             <div class="apply">
                                <a href="javascript:void(0);" class="btn btn-default giftcouponbtn">Apply</a>
                             </div>
                             
                         </div>
                    </div>
                </div>
				<?php //endif;?>
                </div>
            </div>
            <div class="row review">
                <div class="col-sm-12">
                    <?php do_action( 'woocommerce_checkout_before_order_review' ); ?>                
                        <div id="order_checkout_payment" class="yith-woocommerce-checkout-payment">
                            <?php //do_action( 'woocommerce_checkout_order_review' ); ?>
                            <?php //do_action( 'yith_woocommerce_checkout_order_review' ); ?>                  
                        <div class="submit-purchase">
                            <div class="purchase-input">
                                 <input type="submit" class="btn btn-default alt" name="woocommerce_checkout_place_order" id="place_order" value="submit purchase" data-value="Add to cart">
                                <span class="totalamount"><?php wc_cart_totals_order_total_html(); ?></span>
                            </div>
                            <p>By making this purchase, you accept our <a href="/order-policies/">Order Policies</a> and <a href="<?php echo get_permalink( get_page_by_path( 'terms' ) ); ?>" target="_blank">Terms of Use</a></p>
                        </div>
                        <div class="address-border clearfix">
                            <div class="address-w">
                                <div class="user-address">
                                    <h3 class="subscriptionItem">Billing Address</h3>
                                </div>
                                <address id="order_billingaddr"></address>
                                <a href="javascript:void(0)" id="bill_edit">Edit</a>
                            </div>
                            <div class="address-w">
                                <div class="user-address">
                                    <h3 class="subscriptionItem">Shipping Address</h3>
                                </div>
                                <address id="order_shippingaddr"></address>
                                <a href="javascript:void(0)" id="ship_edit">Edit</a>
                            </div>
                        </div>
						<div class="subs_help">
							
						</div>
                        
                        <div class="checkout-table-outer">
                            <div class="checkout-table">
                                <div class="checkout-table-row checkout-head">
                                    <div class="checkout-table-cell">
                                        Item
                                    </div>
                                    <div class="checkout-table-cell">
                                        Unit Price
                                    </div>
                                    <div class="checkout-table-cell">
                                        Quantity
                                    </div>
                                    <div class="checkout-table-cell">
                                        Product Total
                                    </div>
                                   
                                </div>
                                <?php do_action( 'woocommerce_before_cart_contents' ); ?>

                                <?php
                                foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                                    $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                                    $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                                    if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                                        $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                                ?>
                                    <div class="checkout-table-row">
                                        <div class="checkout-table-cell">
                                            <div class="product-view">
                                                <?php
                                                    $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
                                                    if ( ! $product_permalink ) {
                                                        echo $thumbnail;
                                                    } else {
                                                        printf( '%s%s', $thumbnail,$_product->get_name() );
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                         <div class="checkout-table-cell">
                                            <?php
                                                echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                                            ?>
                                        </div>
                                         <div class="checkout-table-cell">
                                          <?php echo $cart_item['quantity']; ?>
                                        </div>
                                         <div class="checkout-table-cell">
                                           <?php
                                            echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
                                            ?>
                                        </div>                                    
                                    </div>  
                                    <?php
                                    }
                                }

                                    do_action( 'woocommerce_cart_contents' );
                        ?>   
                            </div>
                        </div>
                        <div class="productAmount review-pro ">
                            <ul>
                                <li>
                                    <span class="pull-left">Subtotal</span>
                                    <span class="pull-right"><?php wc_cart_totals_subtotal_html(); ?></span>
                                </li>

                                <li <?php echo $displaycoupon; ?>>
                                    <span class="pull-left">Coupon</span>
                                    <span class="pull-right cartamount"><?php
                                    foreach ( WC()->cart->get_coupons() as $code => $coupon ) :
                                        wc_cart_totals_coupon_html( $coupon ); 
                                    endforeach;
                                     ?></span>
                                </li>
                                
                                <li <?php echo $displaygift; ?> class="gift_card_listing">
                                	<div class="checkout_gift_cards_container">
                                    <span class="pull-left">Gift Card: </span>
                                    <span class="pull-right giftamount"><?php
                                    foreach ( WC()->cart->applied_gift_cards as $code => $gift ) :
                                    ?>
                                    <?php echo WC()->cart->applied_gift_cards[$code];?> - <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol();?></span><?php echo WC()->cart->applied_gift_cards_amounts[WC()->cart->applied_gift_cards[$code]];?></span> 
                                    <a href="<?php echo WC()->cart->get_cart_url().'?remove_gift_card_code='.WC()->cart->applied_gift_cards[$code];?>" class="ywgc-remove-gift-card pull-right" data-gift-card-code="<?php echo WC()->cart->applied_gift_cards[$code];?>">[Remove]</a>
                                    </br>
                                    <?php
                                    endforeach;
                                     ?></span>
                                    </div>
                                </li>
                                
                                <?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

                                <?php do_action( 'woocommerce_review_order_before_shipping' ); ?>
                                 <?php 
                                    $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );                                     
                                    //wc_cart_totals_shipping_html(); 
                                    $packages = WC()->shipping->get_packages();  

                                ?>
                                <li>
                                    <span class="pull-left">Shipping</span>
                                    <span class="pull-right shippingright">
                                    <?php foreach ($packages as $i => $package['rates']) {
                                       $chosen_method = isset(WC()->session->chosen_shipping_methods[$i]) ? WC()->session->chosen_shipping_methods[$i] : '';
                                        echo $packages[0]['rates'][$chosen_method]->label.' : ';
                                        echo '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">'.get_woocommerce_currency_symbol().'</span>'.$packages[0]['rates'][$chosen_method]->cost.'</span>';
                                        }     
                                    ?>                                        
                                    </span>
                                </li>                               

                                <?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

                                <?php endif; ?>
                                
                                <?php if ( wc_tax_enabled() && 'excl' === WC()->cart->tax_display_cart ) : ?>
                                    <?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
                                        <?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
                                            <li>
                                                <span class="pull-left">Tax</span>
                                                <span class="pull-right"><?php echo wp_kses_post( $tax->formatted_amount ); ?></span>
                                            </li>                       
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <li>
                                            <span class="pull-left">Tax</span>
                                            <span class="pull-right">Free</span>
                                        </li>   
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php do_action( 'woocommerce_review_order_before_order_total' ); ?>
                                <li>
                                    <span class="pull-left">Total</span>
                                    <span class="pull-right totalamount"><?php wc_cart_totals_order_total_html(); ?></span>
                                </li>
	
                             </ul>
                            <input type="submit" class="btn btn-default" value="submit purchase">
                         </div>
                      </div>
                    
                </div>
            </div>
        </form>
        <div id="form_actions" class="<?php echo $enable_nav_button ? 'enabled' : 'disabled'; ?>" data-step="<?php echo $show_login_step ? 'login' : 'billing'; ?>">
            <input type="button" class="<?php echo $button_class ?> prev" name="checkout_prev_step" value="<?php echo $labels['prev'] ?>" data-action="prev">
            <?php if($is_user_logged_in){ ?>
            <input type="button" class="<?php echo $button_class ?> next" name="checkout_next_step" value="<?php echo $enable_checkout_login_reminder && ! is_user_logged_in() ? $labels['skip_login'] : $labels['next'] ?>" data-action="next">
            <?php } ?>
        </div>
    </div>

<?php do_action( 'woocommerce_after_checkout_form', $checkout );?>
<script>
+function($){
	$('#billing_first_name,#billing_last_name,#shipping_first_name,#shipping_last_name').bind('keyup blur', function(e) {
        var node = $(this);
		node.val(node.val().replace(/[^a-zA-Z]/g,'') );
   	});
   	
   	var email_regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
   	var phone_regex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
   	var postcode_regex = /^\d{5}(?:-\d{4})?$/;
   	var cvv = /^[0-9]{3,4}$/;
   	var scroll = false;
   	function ValidateCreditCardNumber() {

	  var ccNum = document.getElementById("authnet-card-number").value;
	  //alert(ccNum);
	  ccNum = ccNum.split(' ').join('');
	  var visaRegEx = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
	  var mastercardRegEx = /^(?:5[1-5][0-9]{14})$/;
	  var amexpRegEx = /^(?:3[47][0-9]{13})$/;
	  var discovRegEx = /^6(?:011|5[0-9]{2})[0-9]{12}$/;
	  var isValid = false;
	
	  if (visaRegEx.test(ccNum)) {
	    isValid = true;
	  } else if(mastercardRegEx.test(ccNum)) {
	    isValid = true;
	  } else if(amexpRegEx.test(ccNum)) {
	    isValid = true;
	  } else if(amexpRegEx.test(ccNum)) {
	    isValid = true;
	  }
      else if(discovRegEx.test(ccNum)) {
	    isValid = true;
	  }
	
	  return isValid;
	}
	var mark_error = 0;
	setInterval(function(){
		if($('#timeline-billing').hasClass('active')){
			scroll = false;
			var error = 0;
			if($('#billing_first_name').val()==''){
				if(!$('#billing_first_name').closest('p').hasClass('has_validation_error') && (mark_error=='billing_first_name' || mark_error==1)){
					$('#billing_first_name').closest('p').append('<div class="checkout_validation_error">Please enter billing first name.</div>');
					$('#billing_first_name').closest('p').addClass('warning');
				}
				if(!scroll){
					scroll = $('#billing_first_name');
				}
				if(mark_error=='billing_first_name' || mark_error==1){
					$('#billing_first_name').closest('p').addClass('has_validation_error');
				}
				error = 1;
			}else{
				$('#billing_first_name').closest('p').find('.checkout_validation_error').remove();
				$('#billing_first_name').closest('p').removeClass('has_validation_error');
				//scroll = false;
			}
			
			if($('#billing_last_name').val()==''){
				if(!$('#billing_last_name').closest('p').hasClass('has_validation_error') && (mark_error=='billing_last_name' || mark_error==1)){
					$('#billing_last_name').closest('p').append('<div class="checkout_validation_error">Please enter billing last name.</div>');
					$('#billing_last_name').closest('p').addClass('warning');
				}
				if(!scroll){
					scroll = $('#billing_last_name');
				}
				if(mark_error=='billing_last_name' || mark_error==1){
					$('#billing_last_name').closest('p').addClass('has_validation_error');
				}
				error = 1;	
			}else{
				$('#billing_last_name').closest('p').find('.checkout_validation_error').remove();
				$('#billing_last_name').closest('p').removeClass('has_validation_error');
				//scroll = false;
			}
			
			if(!email_regex.test($('#billing_email').val())){
				if(!$('#billing_email').closest('p').hasClass('has_validation_error') && (mark_error=='billing_email' || mark_error==1)){
					$('#billing_email').closest('p').append('<div class="checkout_validation_error">Please enter valid billing email address.</div>');
					$('#billing_email').closest('p').addClass('warning');
				}
				if(!scroll){
					scroll = $('#billing_email');
				}
				if(mark_error=='billing_email' || mark_error==1){
					$('#billing_email').closest('p').addClass('has_validation_error');
				}
				error = 1;	
			}else{
				$('#billing_email').closest('p').find('.checkout_validation_error').remove();
				$('#billing_email').closest('p').removeClass('has_validation_error');
				//scroll = false;
			}
			
			if(!phone_regex.test($('#billing_phone').val())){
				if(!$('#billing_phone').closest('p').hasClass('has_validation_error') && (mark_error=='billing_phone' || mark_error==1)){
					$('#billing_phone').closest('p').append('<div class="checkout_validation_error">Please enter valid billing phone number.</div>');
					$('#billing_phone').closest('p').addClass('warning');
				}
				if(!scroll){
					scroll = $('#billing_phone');
				}
				if(mark_error=='billing_phone' || mark_error==1){
					$('#billing_phone').closest('p').addClass('has_validation_error');
				}
				error = 1;	
			}else{
				$('#billing_phone').closest('p').find('.checkout_validation_error').remove();
				$('#billing_phone').closest('p').removeClass('has_validation_error');
				//scroll = false;
			}
			
			if($('#billing_address_1').val()==''){
				if(!$('#billing_address_1').closest('p').hasClass('has_validation_error') && (mark_error=='billing_address_1' || mark_error==1)){
					$('#billing_address_1').closest('p').append('<div class="checkout_validation_error">Please enter billing address.</div>');
					$('#billing_address_1').closest('p').addClass('warning');
				}
				if(!scroll){
					scroll = $('#billing_address_1');
				}
				if(mark_error=='billing_address_1' || mark_error==1){
					$('#billing_address_1').closest('p').addClass('has_validation_error');
				}
				error = 1;	
			}else{
				$('#billing_address_1').closest('p').find('.checkout_validation_error').remove();
				$('#billing_address_1').closest('p').removeClass('has_validation_error');
				//scroll = false;
			}
			
			if($('#billing_city').val()==''){
				if(!$('#billing_city').closest('p').hasClass('has_validation_error') && (mark_error=='billing_city' || mark_error==1)){
					$('#billing_city').closest('p').append('<div class="checkout_validation_error">Please enter billing city.</div>');
					$('#billing_city').closest('p').addClass('warning');
				}
				if(!scroll){
					scroll = $('#billing_city');
				}
				if(mark_error=='billing_city' || mark_error==1){
					$('#billing_city').closest('p').addClass('has_validation_error');
				}
				error = 1;	
			}else{
				$('#billing_city').closest('p').find('.checkout_validation_error').remove();
				$('#billing_city').closest('p').removeClass('has_validation_error');
				//scroll = false;
			}
			
			if($('#billing_state').val()==''){
				if(!$('#billing_state').closest('p').hasClass('has_validation_error') && (mark_error=='billing_state' || mark_error==1)){
					$('#billing_state').closest('p').append('<div class="checkout_validation_error">Please select state.</div>');
					$('#billing_state').closest('p').addClass('warning');
				}
				if(!scroll){
					scroll = $('#billing_state');
				}
				if(mark_error=='billing_state' || mark_error==1){
					$('#billing_state').closest('p').addClass('has_validation_error');
				}
				error = 1;	
			}else{
				$('#billing_state').closest('p').find('.checkout_validation_error').remove();
				$('#billing_state').closest('p').removeClass('has_validation_error');
				$('#billing_state').closest('p').removeClass('warning');
				//scroll = false;
			}
			
			if(!postcode_regex.test($('#billing_postcode').val())){
				if(!$('#billing_postcode').closest('p').hasClass('has_validation_error') && (mark_error=='billing_postcode' || mark_error==1)){
					$('#billing_postcode').closest('p').append('<div class="checkout_validation_error">Please enter valid billing zipcode.</div>');
					$('#billing_postcode').closest('p').addClass('warning');
				}
				if(!scroll){
					scroll = $('#billing_postcode');
				}
				if(mark_error=='billing_postcode' || mark_error==1){
					$('#billing_postcode').closest('p').addClass('has_validation_error');
				}
				error = 1;	
			}else{
				$('#billing_postcode').closest('p').find('.checkout_validation_error').remove();
				$('#billing_postcode').closest('p').removeClass('has_validation_error');
				//scroll = false;
			}
			
			if(error==1){
				$('.timeline').css('pointer-events','none');
				$('.next').css('pointer-events','none');
			}else{
				$('.timeline').removeAttr('style');
				$('.next').removeAttr('style');
			}
		}else{
			if($('#timeline-shipping').hasClass('active')){
				if($('.shipping_address').is(":visible")){
					scroll = false;
					var error = 0;
					if($('#shipping_first_name').val()==''){
						if(!$('#shipping_first_name').closest('p').hasClass('has_validation_error') && (mark_error=='shipping_first_name' || mark_error==1)){
							$('#shipping_first_name').closest('p').append('<div class="checkout_validation_error">Please enter shipping first name.</div>');
							$('#shipping_first_name').closest('p').addClass('warning');
						}
						if(!scroll){
							scroll = $('#shipping_first_name');
						}
						if(mark_error=='shipping_first_name' || mark_error==1){
							$('#shipping_first_name').closest('p').addClass('has_validation_error');
						}	
						error = 1;
					}else{
						$('#shipping_first_name').closest('p').find('.checkout_validation_error').remove();
						$('#shipping_first_name').closest('p').removeClass('has_validation_error');
					}
					
					if($('#shipping_last_name').val()==''){
						if(!$('#shipping_last_name').closest('p').hasClass('has_validation_error') && (mark_error=='shipping_last_name' || mark_error==1)){
							$('#shipping_last_name').closest('p').append('<div class="checkout_validation_error">Please enter shipping last name.</div>');
							$('#shipping_last_name').closest('p').addClass('warning');
							if(!scroll){
								scroll = $('#shipping_last_name');
							}
						}
						if(mark_error=='shipping_last_name' || mark_error==1){
							$('#shipping_last_name').closest('p').addClass('has_validation_error');
						}
						error = 1;	
					}else{
						$('#shipping_last_name').closest('p').find('.checkout_validation_error').remove();
						$('#shipping_last_name').closest('p').removeClass('has_validation_error');
					}
					
					if($('#shipping_address_1').val()==''){
						if(!$('#shipping_address_1').closest('p').hasClass('has_validation_error') && (mark_error=='shipping_address_1' || mark_error==1)){
							$('#shipping_address_1').closest('p').append('<div class="checkout_validation_error">Please enter shipping address.</div>');
							$('#shipping_address_1').closest('p').addClass('warning');
						}
						if(!scroll){
							scroll = $('#shipping_address_1');
						}
						if(mark_error=='shipping_address_1' || mark_error==1){
							$('#shipping_address_1').closest('p').addClass('has_validation_error');
						}
						error = 1;	
					}else{
						$('#shipping_address_1').closest('p').find('.checkout_validation_error').remove();
						$('#shipping_address_1').closest('p').removeClass('has_validation_error');
					}
					
					if($('#shipping_city').val()==''){
						if(!$('#shipping_city').closest('p').hasClass('has_validation_error') && (mark_error=='shipping_city' || mark_error==1)){
							$('#shipping_city').closest('p').append('<div class="checkout_validation_error">Please enter shipping city.</div>');
							$('#shipping_city').closest('p').addClass('warning');
						}
						if(!scroll){
							scroll = $('#shipping_city');
						}
						if(mark_error=='shipping_city' || mark_error==1){
							$('#shipping_city').closest('p').addClass('has_validation_error');
						}
						error = 1;	
					}else{
						$('#shipping_city').closest('p').find('.checkout_validation_error').remove();
						$('#shipping_city').closest('p').removeClass('has_validation_error');
					}
					
					if($('#shipping_state').val()==''){
						if(!$('#shipping_state').closest('p').hasClass('has_validation_error') && (mark_error=='shipping_state' || mark_error==1)){
							$('#shipping_state').closest('p').append('<div class="checkout_validation_error">Please select state.</div>');
							$('#shipping_state').closest('p').addClass('warning');
						}
						if(!scroll){
							scroll = $('#shipping_state');
						}
						if(mark_error=='shipping_state' || mark_error==1){
							$('#shipping_state').closest('p').addClass('has_validation_error');
						}
						error = 1;	
					}else{
						$('#shipping_state').closest('p').find('.checkout_validation_error').remove();
						$('#shipping_state').closest('p').removeClass('has_validation_error');
						$('#shipping_state').closest('p').removeClass('warning');
						//scroll = false;
					}
					
					if(!postcode_regex.test($('#shipping_postcode').val())){
						if(!$('#shipping_postcode').closest('p').hasClass('has_validation_error') && (mark_error=='shipping_postcode' || mark_error==1)){
							$('#shipping_postcode').closest('p').append('<div class="checkout_validation_error">Please enter valid shipping zipcode.</div>');
							$('#shipping_postcode').closest('p').addClass('warning');
						}
						if(!scroll){
							scroll = $('#shipping_postcode');
						}
						if(mark_error=='shipping_postcode' || mark_error==1){
							$('#shipping_postcode').closest('p').addClass('has_validation_error');
						}
						error = 1;	
					}else{
						$('#shipping_postcode').closest('p').find('.checkout_validation_error').remove();
						$('#shipping_postcode').closest('p').removeClass('has_validation_error');
					}
					
					if(error==1){
						$('.timeline').css('pointer-events','none');
						$('.next').css('pointer-events','none');
					}else{
						$('.timeline').removeAttr('style');
						$('.next').removeAttr('style');
					}
				}else{
					$('.timeline').removeAttr('style');
					$('.next').removeAttr('style');
					scroll = false;
				}
			}else{
				if($('#timeline-order').hasClass('active')){
					if($('.hide-if-token').is(':visible')){
						scroll = false;
						var error = 0;
						var validate_card = ValidateCreditCardNumber();
						if(!validate_card){
							if(!$('#authnet-card-number').closest('p').hasClass('has_validation_error')){
								$('#authnet-card-number').closest('p').append('<div class="checkout_validation_error">Please enter valid card number.</div>');
								$('#authnet-card-number').closest('p').addClass('warning');
							}
							if(!scroll){
								scroll = $('#authnet-card-number');
							}
							$('#authnet-card-number').closest('p').addClass('has_validation_error');
							error = 1;
						}else{
							$('#authnet-card-number').closest('p').find('.checkout_validation_error').remove();
							$('#authnet-card-number').closest('p').removeClass('has_validation_error');
						}
						if($('#authnet-card-expiry').val()==''){
							if(!$('#authnet-card-expiry').closest('p').hasClass('has_validation_error')){
								$('#authnet-card-expiry').closest('p').append('<div class="checkout_validation_error">Please enter valid expiry date..</div>');
								$('#authnet-card-expiry').closest('p').addClass('warning');
							}
							if(!scroll){
								scroll = $('#authnet-card-expiry');
							}
							$('#authnet-card-expiry').closest('p').addClass('has_validation_error');
							error = 1;	
						}else{
							$('#authnet-card-expiry').closest('p').find('.checkout_validation_error').remove();
							$('#authnet-card-expiry').closest('p').removeClass('has_validation_error');
						}
						if(!cvv.test($('#authnet-card-cvc').val())){
							if(!$('#authnet-card-cvc').closest('p').hasClass('has_validation_error')){
								$('#authnet-card-cvc').closest('p').append('<div class="checkout_validation_error">Please enter valid cvv/cvc code.</div>');
								$('#authnet-card-cvc').closest('p').addClass('warning');
							}
							if(!scroll){
								scroll = $('#authnet-card-cvc');
							}
							$('#authnet-card-cvc').closest('p').addClass('has_validation_error');
							error = 1;	
						}else{
							$('#authnet-card-cvc').closest('p').find('.checkout_validation_error').remove();
							$('#authnet-card-cvc').closest('p').removeClass('has_validation_error');
						}
						if(!$('#authnet-save-card').closest('p').hasClass('checker')){
							if(!$('#authnet-save-card').is(':checked')){
								$('#authnet-save-card').closest('p').html('<div class="custom-checkbox"><input type="checkbox" id="authnet-save-card" class="wc-credit-card-form-save-card" name="authnet-save-card"><label for="authnet-save-card">Save card for future use?</label></div>');
								$('#authnet-save-card').prop('checked', true);
							}
							$('#authnet-save-card').closest('p').addClass('checker');
						}
						if(error==1){
							$('.timeline').css('pointer-events','none');
							$('.next').css('pointer-events','none');
						}else{
							$('.timeline').removeAttr('style');
							$('.next').removeAttr('style');
						}
					}else{
						$('.timeline').removeAttr('style');
						$('.next').removeAttr('style');
						scroll = false;
					}
				}else{
					$('.timeline').removeAttr('style');
					$('.next').removeAttr('style');
					scroll = false;
				}
			}
		}
		$('.woocommerce-validated').removeClass('woocommerce-validated');
	}, 100);
	$('.wc-credit-card-form-save-card').prop('checked', true);
	document.addEventListener("click",function(e){
	    var target = e.target;
	    var id = target.id;
	    if((id=='form_actions' || id=='checkout_timeline') && typeof scroll!='undefined' && scroll!=false){
	    	mark_error = 1;
	    	$('html, body').animate({ scrollTop: scroll.offset().top-$('header').height()-20 }, 'slow');
	    }
	});
	$('.input-text').bind('blur', function(){
		mark_error = $(this).attr('id');
		if(typeof mark_error=='undefined'){
			mark_error = 1;
		}
	});
	/*document.querySelector("button").addEventListener("click",function(){
	  document.getElementById("timeline-2").style.display = "block";
	});*/
}(jQuery);
</script>
