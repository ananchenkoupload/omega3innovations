<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
if(isset($_POST['register']) && !empty($_POST['register']))
{
	$checked = 'checked="checked"';
	$check = '';
}else{
	$check = 'checked="checked"';
	$checked = '';
}
?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

<div class="u-columns col2-set" id="customer_login">

	<div class="u-column1">

<?php endif; ?>

	<div class="account-creat">

		<form method="post" class="login" <?php echo isset($_POST['register']) && !empty($_POST['register'])?'style="display:none;"':'style="display:block;"';?>>
			<h2><?php _e( 'Login', 'woocommerce' ); ?></h2>
          <?php  //do_action( 'wordpress_social_login' ); ?>
          	<span class="login_other_option express_login"><span class="express_inner">Express Login1</span></span></br>
          	<?php echo do_shortcode('[social_login]');?>
			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<div class="account cusexist">
				<div class="check_in">
					<input id="order2" type="radio" name="radio-2" data-name="signin" value="signin" <?php echo $check; ?>>
					<label for="order2">
						<h4>Login</h4>
					</label>   
				</div>
				<div class="check_in">
					<input id="order1" type="radio" name="radio-2" data-name="register" <?php echo $checked; ?>>
					<label for="order1">
						<h4>Signup</h4>
						<!--<div class="signindtls">
							<h5>Create an account with us for quick checkout, 
								to keep track of your order, and more.</h5>
						</div>-->
					</label>   
				</div>
				<div class="check-input">
					<label>Username</label>
					<input type="text" class="form-control" name="username" id="username" autocomplete="off" />
				</div>
				<div class="check-input">
					<label>Password</label>
					<input class="form-control" type="password" name="password" id="password" autocomplete="off" />
				</div>
				<!-- <div class="create-part">
					<input type="submit" class="btn btn-default" value="Create Account" /> 
					<a href="#">Sign in with Facebook</a>
				</div> -->
				<?php do_action( 'woocommerce_login_form' ); ?>

				<div class="form-row">
					<?php wp_nonce_field( 'woocommerce-login' ); ?>
					<input type="submit" class="btn btn-default form_txt" name="login" value="<?php esc_attr_e( 'Sign in', 'woocommerce' ); ?>" />					
					<label for="rememberme" class="inline jetremember">
						<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php _e( 'Remember me', 'woocommerce' ); ?>
					</label>
				</div>
				<div class="lost_password">
					<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>?referer=<?php echo rawurlencode(get_the_permalink());?>"><?php _e( 'Forgot password?', 'woocommerce' ); ?></a>
				</div>
			</div>
			<div class="clear"></div>

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>
		<?php echo do_shortcode('[woocommerce_simple_registration]'); ?>
		</div>	

<?php //if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

	</div>
</div>
<?php //endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
