<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<h4>Account Information</h4>
<ul>
    <li>
        <span class="subscriptionItem">Name:</span>
        <span class=""><?php if(!empty($current_user->last_name) && !empty($current_user->first_name)){ echo esc_html($current_user->first_name).' '.esc_html($current_user->last_name); }else{ echo esc_html( $current_user->display_name ); } ?></span>
         <a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-account', 'Account Information' ) ); ?>"><?php _e( 'Edit', 'woocommerce' ); ?></a>
    </li>
    <li>
        <span class="subscriptionItem">Email:</span>
        <a href="mailto:amit@swiftpim.com" class="mail"><?php echo esc_html( $current_user->user_email ); ?></a>
    </li>
    <li>
        <span class="subscriptionItem">Phone:</span>
        <?php if(!empty($current_user->phone)){ ?>
         <span><?php echo esc_html( $current_user->phone ); ?></span>
        <?php }elseif(!empty(get_user_meta( $current_user->ID, 'billing_phone', true ))){ ?>
        <span><?php echo get_user_meta( $current_user->ID, 'billing_phone', true ); ?></span>
        <?php }else{ ?>
        <span style="font-style: italic;">Not Added</span>
        <a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-account', 'Account Information' ) ); ?>" style="float: none; margin-left:10px">Add</a>
        <?php } ?>
    </li>
</ul>
<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );
?>
