<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
	 
	 global $product;
	 if(!$product->is_type( 'gift-card' )):
?>
<div class="paginationIn heading-marg bgGrey">
    <div class="container">
        <ul>
            <li>
                <a href="<?php echo site_url('/store/'); ?>">
                    Products
                </a>
            </li>
            <li>
                <a href="<?php the_permalink();?>">
                    <?php the_title();?>
                </a>
            </li>
        </ul>
    </div>
</div>	
  <section class="productVw bgGrey">
        <div class="container">
            <div class="inProductRow">
                <div class="row">
                    <div class="col-md-7 col-sm-6">
                        <div class="productSlide">
                            <div class="prodSlider">
                            	<?php if(has_post_thumbnail()):
                            	$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');	
                            	?>
                                <div class="sliderProd-e">
                                    <a rel="prettyPhoto[pp_gal]" href="<?php echo esc_url($featured_img_url);?>">
                                        <img src="<?php echo esc_url($featured_img_url);?>" alt="">
                                    </a>
                                </div>
                                <?php endif;?>
                                <?php
                                $attachment_ids = $product->get_gallery_attachment_ids();
								if(!empty($attachment_ids)):
								foreach( $attachment_ids as $attachment_id ):
								?>
                                <div class="sliderProd-e">
                                    <a rel="prettyPhoto[pp_gal]" href="<?php echo esc_url(wp_get_attachment_url( $attachment_id ));?>">
                                        <img src="<?php echo esc_url(wp_get_attachment_url( $attachment_id ));?>" alt="">
                                    </a>
                                </div>
                                <?php endforeach; endif;?>
                            </div>

                            <div class="prodSlider-thumb">
                            	<?php if(has_post_thumbnail()):
                            	$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'thumbnail');	
                            	?>
                                <div class="sliderProd-e">
                                    <figure style="background-image: url(<?php echo esc_url($featured_img_url);?>);"></figure>
                                </div>
                                <?php endif;?>
                                <?php
                                if(!empty($attachment_ids)):
								foreach( $attachment_ids as $attachment_id ):
								$image_attributes = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
								?>
                                <div class="sliderProd-e">
                                    <figure style="background-image: url(<?php echo esc_url($image_attributes[0]); ?>);"></figure>
                                </div>
                                <?php endforeach; endif;?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-6">
                    	
                        <div class="summary">
                        	<div class="price_container">
	                            <h1><i><?php the_title();?></i> <span><?php echo get_woocommerce_currency_symbol().$product->get_price($product->ID);?></span></h1>
	                            <div class="servingItem">
	                                <?php the_field('product_subtitle');?>
	                            </div>
	                            <div class="showWriteRvw clearfix">
	                            	<div class="linkRvwp">
	                            	<a href="#sec-review">
	                                <div class="showRate pull-left">
	                             
	                                    <!-- <figure>
	                                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/starBg.png" alt="">
	                                    </figure> -->
	                                    <?php
	                                    $rating_count = $product->get_rating_count();
										$average = $product->get_average_rating();
										?>
										<div class="star-rating" title="Rated <?php echo $average;?> out of 5">
											<span style="width:<?php echo $average*20;?>%" title="<?php echo $average;?> out of 5">
												<?php /*?><strong class="rating"><?php echo $average;?></strong> out of 5<?php */?>
											</span>
										</div>
										<?php
										if($rating_count):
										?>
	                                    <span><strong><?php echo $average;?></strong><em>(<?php echo $rating_count;?>)</em></span>
	                                    <?php endif;?>
	                                </div>
									</a>
	                                <div class="linkRvw">
	                                    <a href="#sec-review">
	                                        Write a review
	                                    </a>
	                                </div>
                       			 </div>
                           	</div>
							</div>
                            <div class="typeItem cart-item">
                                <?php
                                $attributes = $product->get_attributes();
								$available_attributes = $product->get_variation_attributes();
								$default_attributes = $product->get_variation_default_attributes();
								/*$variations = $product->get_available_variations();
								echo '<pre>';
								print_r($variations);
								echo '</pre>';
								if(!empty($variations)):
								?>
								<script>var variable = [];</script>
								<?php
								foreach($variations as $key=>$var):
								foreach($var['attributes'] as $key2=>$var2):
								?>
								<script>variable[] = ;</script>
								<?php
								endforeach;
								endforeach;
								endif;*/
								// echo '<pre>';
								// print_r($default_attributes);
								// echo '</pre>';
								if(!empty($attributes)):
								foreach ( $attributes as $key=>$attribute ) {
								        echo '<div class="attr_listings"><label class="label" for="'.$key.'">'.$attribute['name'] . "</label>";
								        $product_attributes = array();
								        $product_attributes = explode('|',$attribute['value']);
								
								        $attributes_dropdown = '<div class="selectBox"><select class="attr_sel" name="attribute_'.$key.'"><option value="">--Select--</option>';
								
								        foreach ( $product_attributes as $pa ) {
								        	if (in_array(trim($pa), $available_attributes[$attribute['name']])){
								        		if(isset($default_attributes[$key]) && $default_attributes[$key]==trim($pa)){
								        			$attributes_dropdown .= '<option value="'.trim($pa).'" selected>' . $pa . '</option>';
								        		}else{
								        			$attributes_dropdown .= '<option value="'.trim($pa).'">' . $pa . '</option>';
								        		}
								        	}
								        }
								
								        $attributes_dropdown .= '</select></div></div>';
								
								        echo $attributes_dropdown;
								}
								endif;
		                    	 $prod = wc_get_product(get_the_ID());
		                		 $available_variations = $prod->get_available_variations();
								 //$modal_attr = isset($available_variations[0]) && $available_variations[count($available_variations)-1]['is_in_stock']?'data-toggle="modal" data-target="#modalCounter" disabled':'disabled';
								?>
								<span class="out-of-stock"><?php //echo isset($available_variations[0]) && $available_variations[count($available_variations)-1]['is_in_stock']?'':'Out of stock';?></span>
                            </div>
                            <?php
                            $product = new WC_Product($product->id);
							$upsells = $product->get_upsells();
							?>
                            <div class="optionsItem <?php echo empty($upsells)?'non-subscribe':'subscribe';?> <?php echo isset($available_variations[0]) && $available_variations[count($available_variations)-1]['is_in_stock']?'':'stockOuter';?>">
                                <div class="btnCase btncart">
                                    <button class="addtoCart btn btnSplit text-uppercase">
                                        <span class="labelBtn">Add to cart</span>
                                        <span><?php echo get_woocommerce_currency_symbol().$product->get_price($product->ID);?></span>
                                    </button>
                                </div>
                                <?php
									
							    	if(!empty($upsells)){
							    		$thisproduct = wc_get_product( $upsells[0] );
									?>
									<div class="btnCase btnSubsc">
										<a href="<?php echo get_permalink($upsells[0]); ?>" class="subsC btn btnSplit text-uppercase">
											<span class="labelBtn">Subscription <em class="available">Available</em></span>
											<?php 
											echo '<span>'.get_field('subscription_price',$product->ID).'</span>'; ?>
										</a>	
									</div>	
								<?php }?>
                            </div>

                            <div class="deliverFtPref">
                                <div class="ftDeliver">
                                    <?php the_field('delivery_info');?>
                                </div>
                                <div class="prodInfosm">
                                	<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="sectionOverviewProduct">
        <div class="container">
            <div class="txtBlock">
                <?php 	                      	
				if( have_rows('product_overview') ): while ( have_rows('product_overview') ) : the_row();
						if( get_row_layout() == 'product_overview' ):
							
							?>	
				
				    <h6><?php echo get_sub_field('overview_title'); ?></h6>
				    <?php echo get_sub_field('overview_content'); ?>
				
				 <?php   
					endif;
					endwhile;
					endif;
					
				 ?>
            
                
				<div class="rec_dose">
                 <?php 	                      	
		        if( have_rows('recommended_section') ): while ( have_rows('recommended_section') ) : the_row();
				if( get_row_layout() == 'recommended_section' ):			
					?>	
		            
		                <h6><?php echo get_sub_field('title'); ?></h6>
		              <?php echo get_sub_field('content'); ?>
		            
		             <?php   
				endif;
				endwhile;
				endif;
				
			 ?>
             <?php 	                      	
        if( have_rows('care_tips_sect') ): while ( have_rows('care_tips_sect') ) : the_row();
		if( get_row_layout() == 'care_tips_section' ):			
			?>	
            <div class="over-iiner">
                 <h6><?php echo get_sub_field('care_tips_title'); ?></h6>
                <?php echo get_sub_field('care_tips_content'); ?>
            </div>
		 <?php   
				endif;
				endwhile;
				endif;
				
			 ?>
			 </div>
			 <aside class="factsSupp">
                    <?php                       	
				        if( have_rows('suppliment_content_') ): while ( have_rows('suppliment_content_') ) : the_row();
						if( get_row_layout() == 'suppliment_content_' ):
							
							?>	
				        <h6> <?php echo get_sub_field('title'); ?></h6>
				        <?php echo get_sub_field('content'); ?>
				        <?php   
						endif;
						endwhile;
						endif;
						
					 ?>
                    <div class="tableInOvw">
                        <table>
                           <?php                       	
					        if( have_rows('suppliment_content_') ): while ( have_rows('suppliment_content_') ) : the_row();
							if( get_row_layout() == 'suppliment_content_' ):
								
								?>	
					                <tr>
					                    <th><?php echo get_sub_field('table_first_tile'); ?></th>
					                    <th><?php echo get_sub_field('table_second_title'); ?></th>
					                    <th><?php echo get_sub_field('table_third_title'); ?></th>
					                </tr>
					                  <?php   
							endif;
							endwhile;
							endif;
								                      	
			                if( have_rows('suppliment_content_') ): while ( have_rows('suppliment_content_') ) : the_row();
								if( get_row_layout() == 'suppliment_content_' ):
										                      	
			                if( have_rows('suppliment_table') ): while ( have_rows('suppliment_table') ) : the_row();
								if( get_row_layout() == 'suppliment_table' ):
									?>	
			                <tr>
			                    <td>  <?php echo get_sub_field('omega-3'); ?></td>
			                    <td>  <?php echo get_sub_field('amt_per_serving'); ?></td>
			                    <td>  <?php echo get_sub_field('%_daily_value'); ?></td>
			                </tr>
			                 <?php   
							endif;
							endwhile;
							endif;
							
							endif;
							endwhile;
							endif;
							?>
                        </table>
                    </div>
					<?php	                      	
			        if( have_rows('suppliment_content_') ): while ( have_rows('suppliment_content_') ) : the_row();
					if( get_row_layout() == 'suppliment_content_' ):			
						?>	
                    <h6><?php echo get_sub_field('suppliment_below_table_title'); ?></h6>
                    <?php   
						endif;
						endwhile;
						endif;
					
					 ?>
                </aside>
            </div>
        </div>
    </section>
    
    <section class="productPara bgGrey">
        <div class="product-panel">
        	 <?php  $i=1;		                      	
        if( have_rows('products_content') ): while ( have_rows('products_content') ) : the_row();
				if( get_row_layout() == 'products_content' ):		
		?>	
            <div class="seq-outer">
                <div class="seq-list" style="background-image: url(<?php echo get_sub_field('product_image'); ?>);">
                
                </div>
                <div class="container text-container">
                    <div class="row">
                        <div class="col-sm-5 col-lg-5 <?php echo $i%2==0?'':'col-sm-offset-7 col-lg-offset-7';?>">
                            <div class="seq-inner">
                            	<h2>  <?php echo get_sub_field('product_title'); ?></h2>
                             	<?php echo get_sub_field('product_content'); ?>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
             <?php   
                $i++;
				endif;endwhile;endif;
				wp_reset_query();
		   ?>
        </div>
    </section>
		
    <section class="sectionReviews" id="sec-review">
        <div class="container">
            <div class="caseInRvw">
                <header class="reviewsMain clearfix">
                    <h3 class="pull-left">Customer reviews</h3>
                    <div class="pull-right triggerAddrevw">
                        <a href="javascript:void(0)" class="triggerRvw">
                            Add a review
                        </a>
                    </div>
                </header>
                <div class="addReview-in">
                    <form action="<?php echo site_url('/');?>wp-comments-post.php" method="post">
                        <div class="row">
                        	<?php if(!is_user_logged_in()):?>
                        	<script>var userchecking = true;</script>
                            <div class="col-sm-6">
                                <div class="inRvw">
                                    <input id="author" name="author" type="text" value="" size="30" aria-required="true" placeholder="Name*" required="required">
                                    <p style="color:red;"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="inRvw">
                                    <input id="email" name="email" type="email" value="" size="30" aria-required="true" placeholder="Email*" required="required">
                                    <p style="color:red;"></p>
                                </div>
                            </div>
                            <?php endif;?>
                            <div class="col-sm-12">
                                <div class="inRvw inputRvwStar">
                                   <div class="rating_area"></div>
                                   <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                                   <input type="hidden" name="rating" id="rating" required="" />
                                   <p style="color:red;"></p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="inRvw reviewText">
                                	<input type="text" name="title" id="title" aria-required="true" placeholder="Review title*" required="required">
									<textarea id="comment" name="comment" cols="45" rows="8" placeholder="Your Review..." aria-required="true" required="required"></textarea>
									<p style="color:red;"></p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <input type="hidden" name="action" value="submit-form">
                                <input type="hidden" name="comment_post_ID" value="<?php echo get_the_ID();?>" id="comment_post_ID">
                                <input name="submit" type="hidden" id="submit" class="submit" value="Submit">
                                <div class="inRvw reviwSubmit btnCase">
                                    <button class="btn text-uppercase review-submit" type="submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
            
            <div class="showReviews-in">
                <h4><?php $comments_count = wp_count_comments(get_the_ID()); echo $comments_count->approved; ?> reviews for <?php the_title();?></h4>

                <div class="listedRvws">
                    
                    	<?php
                        	$args = array (
							    'post_type' => 'product', 
							    'post_id' => get_the_ID(),  // Product Id
							    'status' => "approve", // Status you can also use 'hold', 'spam', 'trash', 
							    //'number' => -1  // Number of comment you want to fetch I want latest approved post soi have use 1
							    );
							$comments = get_comments($args);
							// echo '<pre>';
							// print_r($comments);
							// echo '</pre>';
							$count = 1;
							foreach($comments as $com):

							?>
							<?php
							if ($count%2== 1) echo '<div class="row">';?>
								
	                        <div class="col-sm-6">
	                            <div class="reviwListed-e">
	                            	 <?php
	                                global $wpdb;
									$results = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = '_ywar_comment_id' and meta_value = '{$com->comment_ID}'", OBJECT );
									$comment_post_id = $results->post_id;
									$rating = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = '_ywar_rating' and post_id = '{$comment_post_id}'", OBJECT );
									?>
	                                <h5><?php echo get_the_title($comment_post_id);?></h5>
	                                <div class="ratedCourtesy">
	                                	<?php if(isset($rating) && $rating->meta_value):?>
	                                    <div class="rating-e">
	                                        <div class="star-rating" title="Rated <?php echo $rating->meta_value;?> out of 5">
												<span style="width:<?php echo $rating->meta_value*20;?>%">
													<strong class="rating"><?php echo $rating->meta_value;?></strong> out of 5
												</span>
											</div>
	                                        <span><?php echo $rating->meta_value;?></span>
	                                    </div>
	                                    <?php endif;?>
	                                    by
	                                    <a href="<?php echo $com->comment_author_url?$com->comment_author_url:'javascript:void(0)';?>" class="reviewr">
	                                            <?php echo $com->comment_author;?>
	                                    </a>
	                                    on <?php echo date("M d, Y", strtotime($com->comment_date));?>
	                                </div>
	
	                                <div class="rateBody">
	                                    <p>
	                                        <?php echo $com->comment_content;?>
	                                    </p>
	                                </div>
	                            </div>
	                        </div>
	                       <?php if ($count%2 == 0) echo '</div>';?>
						   
	                      <?php $count++; endforeach;?>
	                      <?php if ($count%2 != 1) echo "</div>";?>
	                      
                </div>
            </div>
        </div>
    </section>
    
        <!-- Modal -->
		<div class="modal fade modalItems" id="modalCounter" tabindex="-1" role="dialog" aria-labelledby="modalCounterLabel" aria-hidden="true">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close text-uppercase" data-dismiss="modal" aria-label="Close">
		                    Close <span aria-hidden="true">&times;</span>
		                </button>
		            </div>
		            <div class="modal-body">
		                <div class="viewItemM text-center">
		                	<?php
		                    $prod = wc_get_product(get_the_ID());
		                    $available_variations = $prod->get_available_variations();
							//print_r($available_variations);
							?>
		                    <form class="variations_form cart" method="post" enctype="multipart/form-data" data-product_id="<?php get_the_ID();?>">
		                    	<?php if(has_post_thumbnail()):?>
		                        <figure class="">
		                            <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');?>
		                            <img src="<?php echo esc_url($featured_img_url);?>" alt="">
		                        </figure>
		                        <?php endif;?>
		                        <p>How many do you need?</p>
		
		                        <div class="spinnerItem">
		                            <div class="number-spinner">
		                                <span class="ns-btn">
		                                        <a data-dir="dwn"><span class="icon-minus"></span></a>
		                                </span>
		                                <input type="text" class="pl-ns-value" value="1" maxlength="2" min="1" name="quantity" readonly="readonly">
		                                <span class="ns-btn">
		                                        <a data-dir="up"><span class="icon-plus"></span></a>
		                                </span>
		                            </div>
		                        </div>
		                        <?php
		                        if(!empty($attributes)):
		                        foreach ( $attributes as $key=>$attribute ) {
									echo '<input type="hidden" id="attribute_'.$key.'" name="attribute_'.$key.'" value="">';
								}
								endif;
								?>
		                        <div class="btnCase submitCounter">
		                            <button type="submit" class="btn text-uppercase addToCartNow modalAddToCart">OK</button>
		                            <input name="add-to-cart" value="<?php echo get_the_ID();?>" type="hidden">
									<input name="product_id" value="<?php echo get_the_ID();?>" type="hidden">
									<?php //print_r($available_variations);?>
									<input name="variation_id" class="variation_id" value="<?php echo isset($available_variations[0])?$available_variations[count($available_variations)-1]['variation_id']:'';?>" type="hidden">
									<input type="hidden" name="action" value="customAddToCart" />
		                        </div>
		                    </form>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
		
		
		
		<script>
        
        "use strict";

        +function ($) {
        $('.triggerRvw').click(function () {
            $('.stars').each(function () {
            $(this).remove();
            });
            $('.rating_area').html("<p class=\"stars\"><label>Your Rating</label><span><a class=\"str star-1\" href=\"javascript:void(0);\">1</a><a class=\"str star-2\" href=\"javascript:void(0);\">2</a><a class=\"str star-3\" href=\"javascript:void(0);\">3</a><a class=\"str star-4\" href=\"javascript:void(0);\">4</a> <a class=\"str star-5\" href=\"javascript:void(0);\">5</a></span></p>");
        });
        $(document).on('click', '.str', function () {
            var rate = $(this).text();
            $('#rating').val(rate);
            $('.str.active').removeClass('active');
            $(this).addClass('active');
            $(this).closest('p').addClass('selected');
        });
        $('.review-submit').click(function (e) {
            var error = false;

            if (typeof userchecking != 'undefined') {
            if ($('#author').val() == '') {
                $('#author').closest('.inRvw').find('p').text('Please enter your name.');
                error = true;
            } else {
                $('#author').closest('.inRvw').find('p').text('');
            }

            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            if ($('#email').val() == '' || !re.test($('#email').val())) {
                $('#email').closest('.inRvw').find('p').text('Please enter valid email address.');
                error = true;
            } else {
                $('#email').closest('.inRvw').find('p').text('');
            }
            }

            if ($('#rating').val() == '') {
            $('#rating').next('p').text('Please select your rating.');
            error = true;
            } else {
            $('#rating').next('p').text('');
            }

            if ($('#title').val() == '') {
            $('#title').closest('.inRvw').find('p').text('Please enter a title.');
            error = true;
            } else {
            $('#title').closest('.inRvw').find('p').text('');
            }

            if ($('#comment').val() == '') {
            $('#comment').closest('.inRvw').find('p').text('Please enter your comment.');
            error = true;
            } else {
            if ($('#title').val() != '') {
                $('#comment').closest('.inRvw').find('p').text('');
            }
            }

            if (error) {
            e.preventDefault();
            }
        });
        $('.attr_sel').change(function () {
            var attr_val = $(this).val();
            var attr_name = $(this).attr('name');
            $('#' + attr_name).val($.trim(attr_val));
        });
        $('.addtoCart').click(function () {
            var blank = false;
            var outOfStock = false;
            $('.selectBox select').each(function () {
            if ($(this).val() == '') {
                blank = true;
                $(this).css('border-color', 'red');
            } else {
                $(this).removeAttr('style');
            }
            });

            if ($('.out-of-stock').text() == 'Out of stock') {
            outOfStock = true;
            }

            if (blank) {
            $('.out-of-stock').text('Please select your option(s)');
            $('.optionsItem').addClass('stockOuter');
            $("html, body").animate({
                scrollTop: $('.attr_listings').offset().top - $('header').height() - 30
            }, 1000);
            } else {
            $('.attr_sel').each(function () {
                var attr_val = $(this).val();
                var attr_name = $(this).attr('name');
                $('#' + attr_name).val($.trim(attr_val));
            });

            if (outOfStock) {
                $('.out-of-stock').text('Out of stock');
                $('.optionsItem').addClass('stockOuter');
                $("html, body").animate({
                scrollTop: $('.attr_listings').offset().top - $('header').height() - 30
                }, 1000);
            } else {
                $('.optionsItem').removeClass('stockOuter');
                $('#modalCounter').modal();
            }
            }
        });


        // BABEL 1

        <?php if(isset($_POST['add-to-cart']) && $_POST['add-to-cart']==get_the_ID()):?>
        $(window).load(function(){
            $('.minicartdisplay').slideDown('10000');
        });
        <?php endif;?>

        // BABEL 2
        $('.selectBox select').change(function(){
            if ($(void 0).val() == '') {
                //$('.addtoCart').prop('disabled',true);
                $('.out-of-stock').text('Please select your option');
                $('.optionsItem').removeClass('stockOuter');
            }else{
                var blank = false;
                $('.selectBox select').each(function () {
                if ($(this).val() == '') {
                    blank = true;
                }
                });
                if (blank) {
                    //$('.addtoCart').prop('disabled',true);
                    $('.out-of-stock').text('Please select your option');
                    $('.optionsItem').removeClass('stockOuter');
                    $("html, body").animate({
                    scrollTop: $('.attr_listings').offset().top - $('header').height() - 30
                    }, 1000);
                }else{
                    var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
                    var product_id = '<?php echo get_the_ID();?>';
                    var query_string = 'action=fetch_variation_id&product_id=' + product_id;
                    $('.selectBox select').each(function () {
                    var name = $(this).attr('name');
                    var value = $(this).val();
                    query_string += '&' + name + '=' + value;
                    });
                    $('.addtoCart').prop('disabled', true);
                    $('.selectBox select').removeAttr('style');
                    $.ajax({
                    url: ajaxurl,
                    cache: false,
                    type: 'POST',
                    data: query_string,
                    success: function success(results) {
                        var res = $.trim(results);
                        var data = JSON.parse(res);
                        $('.variation_id').val(data.variation_id);

                        if (data.in_stock) {
                        // $('.addtoCart').attr('data-toggle','modal');
                        // $('.addtoCart').attr('data-target','#modalCounter');
                        $('.addtoCart').prop('disabled', false);
                        $('.out-of-stock').text('');
                        $('.optionsItem').removeClass('stockOuter');
                        } else {
                        // $('.addtoCart').removeAttr('data-toggle');
                        // $('.addtoCart').removeAttr('data-target');
                        $('.addtoCart').prop('disabled', false);
                        $('.out-of-stock').text('Out of stock');
                        $('.optionsItem').addClass('stockOuter');
                        $("html, body").animate({
                            scrollTop: $('.attr_listings').offset().top - $('header').height() - 30
                        }, 1000);
                        }
                    }
                    });	
                }
            }
        });
        // Complied by BABEL till this point

        // NOT COMPLIED BY BABEL
        <?php
        if (isset($default_attributes) && count($default_attributes) > 0) {
            foreach($default_attributes as $key=>$da){
        ?>
            $('[name="attribute_<?php echo $key;?>"]').val('<?php echo $da;?>').change();
        <?php
            }
        }
        ?>
        }(jQuery);
        // NOT COMPLIED BY BABEL
		</script>


    <?php else:?>
    <section  style="display:block;" class="product-detail heading-marg" itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>">
	<div class="container">
    	<div class="row product">
    		<?php global $product; if ( ! $product->is_type( 'gift-card' ) ) { ?>
    		<div class="col-sm-6">
				<?php
					/**
					 * woocommerce_before_single_product_summary hook.
					 *
					 * @hooked woocommerce_show_product_sale_flash - 10
					 * @hooked woocommerce_show_product_images - 20
					 */
					 
					do_action( 'woocommerce_before_single_product_summary' );
				?>				
			</div>
			<?php } ?>

			<div <?php if ( ! $product->is_type( 'gift-card' ) ) {?>class="col-sm-6"<?php } else {?>class="col-xs-12" <?php } ?> >
				<div class="summary <?php if ( $product->is_type( 'gift-card' ) ) {?>giftpdts-cnt"<?php } ?> ">

					<?php
						/**
						 * woocommerce_single_product_summary hook.
						 *
						 * @hooked woocommerce_template_single_title - 5
						 * @hooked woocommerce_template_single_rating - 10
						 * @hooked woocommerce_template_single_price - 10
						 * @hooked woocommerce_template_single_excerpt - 20
						 * @hooked woocommerce_template_single_add_to_cart - 30
						 * @hooked woocommerce_template_single_meta - 40
						 * @hooked woocommerce_template_single_sharing - 50
						 */
						do_action( 'woocommerce_single_product_summary' );
						
						
					?>

				</div><!-- .summary -->
			</div>
		</div>
	</div>
	<div class="overview">
        <div class="container">
	<?php
		/**
		 * woocommerce_after_single_product_summary hook.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>
		</div>
	</div>
	  <div class="product-panel sequance">
    <?php  $i=0;		                      	
        if( have_rows('products_content') ): while ( have_rows('products_content') ) : the_row();
				if( get_row_layout() == 'products_content' ):		
		?>	
        <div class="seq-outer">
           <div class="seq-list" style="background-image: url(<?php echo get_sub_field('product_image'); ?>);">
                
            </div> 
            <div class="container text-container">
                <div class="row">
                    <div class="col-sm-5 <?php if($i%2==1){ ?>col-sm-offset-7<?php } $i++;?>">
                        <div class="seq-inner">
                             <h2>  <?php echo get_sub_field('product_title'); ?></h2>
                             <?php echo get_sub_field('product_content'); ?>
                        </div>    
                    </div>
                </div>
            </div>
        </div>    
        
        <?php   
			endif;endwhile;endif;
			wp_reset_query();
	   ?>
    </div>
    <div class="container">
    <?php comments_template(); ?>
    </div>
	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</section><!-- #product-<?php the_ID(); ?> -->
    <?php endif;?>
    <?php do_action( 'woocommerce_after_single_product' ); ?>