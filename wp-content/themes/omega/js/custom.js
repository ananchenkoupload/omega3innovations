$(function(){
  $('.linkRvw a, .linkRvwp a').click(function() {
   if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
     var target = $(this.hash);
     target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
     if (target.length) {
       $('html, body').animate({
         scrollTop: target.offset().top - 70/*- or + value for target element position*/
       }, 900);
       return false;
     }
   }
  });
});
	$(document).ready(function() {
        $('.triggerRvw').click(function() {
            $(this).parents('.caseInRvw').stop().toggleClass('reviewSpace');
        });

        $('#shipping_method input:radio').change(function() {
            alert("click");
        });

        $('#wc-authorize-net-cim-credit-card-use-new-payment-method').change(function() {
           // window.location='my-account/payment-methods/';
        });
        //$(".woocommerce-order-pay .user_add_new").hide();


        
       

        var numberSpinner = (function() {
            $('.number-spinner>.ns-btn>a').click(function() {
                var btn = $(this),
                oldValue = btn.closest('.number-spinner').find('input').val().trim(),
                newVal = 0;

                if (btn.attr('data-dir') === 'up') {
                    newVal = parseInt(oldValue) + 1;
                }
                else {
                    if (oldValue > 1) {
                        newVal = parseInt(oldValue) - 1;
                    } else {
                        newVal = 1;
                    }
                }
                btn.closest('.number-spinner').find('input').val(newVal);
            });
            $('.number-spinner>input').keypress(function(evt) {
              evt = (evt) ? evt : window.event;
              var charCode = (evt.which) ? evt.which : evt.keyCode;
              if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
              }
              return true;
            });
        })();

        $('.slider-prod').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            adaptiveHeight: true,
            asNavFor: '.slider-thumb'
          });
          $('.slider-thumb').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider-prod',
            dots: true,
            centerMode: true,
            focusOnSelect: true
          });

          $('.prodSlider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: false,
            adaptiveHeight: true,
            asNavFor: '.prodSlider-thumb',
            responsive: [
              {
                breakpoint: 767,
                settings: {
                  dots: true
                }
              }
            ]
          });
          $('.prodSlider-thumb').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.prodSlider',
            dots: false,
            centerMode: false,
            focusOnSelect: true,
            responsive: [
              {
                breakpoint: 767,
                settings: {
                  slidesToShow: 4
                }
              },
              {
                breakpoint: 640,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1
                }
              }
            ]

            
          });

          /* LITEHOUSE MOD: FIX PARAGRAPH AFTER ZIPCODE IN CUSTOMER ACCOUNT WHEN CHANGING ADDRESSES */
        $(window).load(function(){ 
          $('#shipping_postcode_field').after('<div class="clear"></div>');
        });

        $(document).on('ready click', function() {
        	$('.form-row input, .form-row textarea, .form-row select').each(
	            function(){
	                var val = $(this).val().trim();

                  // LITEHOUSE MOD: NEW VERSION OF WOOCOM PUTS <span> AROUND INPUT FIELDS, REMOVE IT TO FIX CUSTOM STYLES
                  //if ($(this).parent().hasClass(".woocommerce-input-wrapper")) -> DOESN'T WORK
                  if ($(this).parent().is(".woocommerce-input-wrapper")) 
                  {
                    $(this).unwrap();
                  }

                  // don't add n-focus to select fields
                  //if ($(this).not("select"))
                  if (!$(this).is("select"))
                  {
  	                if (val == ''){
  	                    //alert('input empty');
  	                }
  	                else {
  	                    
                        $(this).parent('.form-row').addClass('n-focus');
  	                }
                  }
	        });
        })

        $(document).on('click', '#ship-to-different-address label',function (){

            $('.address-plus').toggleClass('address-minus');
        });

        $('.nav_icon').click(function(){
            $("body").toggleClass('open_nav');
        });
         $('.overlay').click(function(){
           $('body').removeClass("open_nav");
        });

        $('.menu-item-has-children').click(function(){
            $("body").toggleClass('submenu_open');
        });
        $('.account_menu').click(function() {
            $("body").toggleClass('submenu_acount');
        });
        $('ul.nav-dropdown:before').click(function() {
             $("body").removeClass('submenu_acount');
        });

        $('.reco-a').click(function (){
            $('.recomend').slideToggle();
        });
            $(document).on('click',".dropdown-menu",function(e){
                e.stopPropagation();
            });
       		$(document).on('click',".cart-top",function(e){
                e.stopPropagation();
            });
//        product slider
         $('.slider-for').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              fade: true,
             adaptiveHeight: false,
              asNavFor: '.slider-nav',
             responsive: [
               {
                     breakpoint: 768,
                        settings: {
                        arrows:true
                    }
                 }
             ]


            });
            $('.slider-nav').slick({
              slidesToShow: 3,
              slidesToScroll: 1,
              asNavFor: '.slider-for',
              centerMode: true,
              focusOnSelect: true
            });


        $(function() {
          return $(".modal").on("show.bs.modal", function() {
            var curModal;
            curModal = this;
            $(".modal").each(function() {
              if (this !== curModal) {
                $(this).modal("hide");
              }
            });
          });
        });

});

//        sticky header
 $(window).scroll(function(){
     var scroll = $(window).scrollTop();

     if($(window).width() >= 990){
         if(scroll >= 1){

            $('.header').addClass('sticky');
        }
        else {
             $('.header').removeClass('sticky');
        }

     }

});
$(window).on('load resize', function() {

	history.replaceState({}, document.title, ".");

    if($(window).width() <767) {

        	$('#tab_selector').on('change', function (e) {
                $('.form-tabs li a').eq($(this).val()).tab('show');
            });

        // $('.account .page-account .nav-tabs').each(function() {
        //     var list = $(this), select = $(document.createElement('select')).insertBefore($(this).hide());

        //     $('>li a', this).each(function() {
        //         var target = $(this).attr('target'),
        //         option = $(document.createElement('option'))
        //             .appendTo(select)
        //             .val(this.href)
        //             .html($(this).html())
        //             .click(function(){
        //                 if(target==='_blank') {
        //                     window.open($(this).val());
        //                 }
        //                 else {
        //                     window.location.href = $(this).val();
        //                 }
        //             });
        //     });
        //     list.remove();
        // });
    }
})


	//Footer fixed
//	$(window).bind("load", function() {
//       var footerHeight = 0,
//           footerTop = 0,
//           $footer = $(".footer_outer");
//       positionFooter();
//       function positionFooter() {
//            footerHeight = $footer.height();
//            footerTop = ($(window).scrollTop()+$(window).height()-footerHeight)+"px";
//           if ( ($(document.body).height()+footerHeight) < $(window).height()) {
//               $footer.css({
//                    position: "fixed",
//                    bottom: "0px",
//                    left: "0",
//                    right: "0"
//               })
//           } else {
//               $footer.css({
//                    position: "relative",
//                    display: "block"
//               })
//           }
//       }
//       $(window)
//	       .scroll(positionFooter)
//	       .resize(positionFooter)
//
//	});
