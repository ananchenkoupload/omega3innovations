//Subscription Calculation
$(document).ready(function(){
	//$('#shipping_method').find('.shipping_method').prop('checked', false);
	//$('.shipping_method').attr({"checked":false}).prop({"checked":false});
	//$('#shipping_method').find('#shipping_method_0_free_shipping6').attr('checked', 'checked');
	var adult = 3;
	var child0 = 0.5;
	var child5 = 1;
	var noofbottle = 3;
	var per_bottle_contain = 51;
	$('.minus,.plus,.adlt').click(function (e) {
		e.preventDefault();    
		var $input = $(this).siblings('.qnt');
		if($(this).hasClass('adlt') && ($input.val() > 1 ))
		{
			$input.val( +$input.val() - 1 );
			//$input.attr('value',$input.val());
			var dynmcadlt = $("input[name=adlt]").val();
			var dynmcchld0 = $("input[name=chld0]").val();
			var dynmcchld5 = $("input[name=chld5]").val();
			var result = Math.round((per_bottle_contain*noofbottle)/((dynmcadlt*adult)+(dynmcchld5*child5)+(dynmcchld0*child0)));			
			$('#days').text(result);
			var week = parseInt(result/7);
			$('.cart-item').find('.check_in')
	        .each(function() {
	            var adminwk = $(this).data('val');
	            if(adminwk){
				var totalwk = +adminwk.split(' ')[0];
				if((totalwk != '')&& (week != '')){
					if(totalwk == week)
					{
						if(week > 1)
						{
							var wks = 'weeks';
						}else{
							var wks = 'week';
						}
						$(this).find('input[name=attribute_plan]').trigger('click');
						var weeks = ' ('+week+' '+wks+' )';
						$('#weeks').text(weeks);
						return false;
					}
				}
			}
	        });
		}
		else if($(this).hasClass('minus') && ($input.val() > 0 ))
		{
			$input.val( +$input.val() - 1 );
			//$input.attr('value',$input.val());
			var dynmcadlt = $("input[name=adlt]").val();
			var dynmcchld0 = $("input[name=chld0]").val();
			var dynmcchld5 = $("input[name=chld5]").val();
			var result = Math.round((per_bottle_contain*noofbottle)/((dynmcadlt*adult)+(dynmcchld5*child5)+(dynmcchld0*child0)));	

			$('#days').text(result);
			var week = parseInt(result/7);
			$('.cart-item').find('.check_in')
	        .each(function() {
	            var adminwk = $(this).data('val');
	            if(adminwk){
				var totalwk = +adminwk.split(' ')[0];
				if((totalwk != '')&& (week != '')){
					if(totalwk == week)
					{
						if(week > 1)
						{
							var wks = 'weeks';
						}else{
							var wks = 'week';
						}
						$(this).find('input[name=attribute_plan]').trigger('click');
						var weeks = ' ('+week+' '+wks+' )';
						$('#weeks').text(weeks);
						return false;
					}
				}
			}
	        });
		}else if($(this).hasClass('plus') && ($input.val() < 10 )){
			$input.val( +$input.val() + 1 );
			//$input.attr('value',$input.val());
			var dynmcadlt = $("input[name=adlt]").val();
			var dynmcchld0 = $("input[name=chld0]").val();
			var dynmcchld5 = $("input[name=chld5]").val();
			var result = Math.round((per_bottle_contain*noofbottle)/((dynmcadlt*adult)+(dynmcchld5*child5)+(dynmcchld0*child0)));			
			$('#days').text(result);
			var week = parseInt(result/7);
			$('.cart-item').find('.check_in')
	        .each(function() {
	            var adminwk = $(this).data('val');
	            if(adminwk){
				var totalwk = +adminwk.split(' ')[0];
				if((totalwk != '')&& (week != '')){
					if(totalwk == week)
					{
						if(week > 1)
						{
							var wks = 'weeks';
						}else{
							var wks = 'week';
						}
						$(this).find('input[name=attribute_plan]').trigger('click');
						var weeks = ' ('+week+' '+wks+' )';
						$('#weeks').text(weeks);
						return false;
					}
				}
			}
	        });


		}
	});
	$('.shopping').on('click', '#plus',function(){
		var qty = $(this).parent().next().find('.qty').attr('value');
		qty = parseInt(qty) + 1;
		$(this).prev('.qty').attr('value', qty);
		$(this).parent().next().find('.qty').attr('value', qty);				
		$("input[name = 'update_cart']").removeAttr('disabled');
    	$("#update_cart").trigger('click');
	});

	$('.shopping').on('click', '#minus',function(){
		var qty = $(this).parent().next().find('.qty').attr('value');
		if(qty > 0){
			qty = parseInt(qty) - 1;
		}
		$(this).next('.qty').attr('value', qty);
		$(this).parent().next().find('.qty').attr('value', qty);		
		$("input[name = 'update_cart']").removeAttr('disabled');
    	$("#update_cart").trigger('click');
	});

	$('.cart-top').enscroll({
	    showOnHover: false,
	    verticalTrackClass: 'track3',
	    verticalHandleClass: 'handle3'
	});
	
	/*woocommerce single page*/
	if($('button').hasClass('single_add_to_cart_button')==true){
			       window.alert=function() {};	
			      // if($('div').hasClass('spj')==false)
			}
	$('.single_add_to_cart_button').click(function(){
		var checked = $("input:radio[name='attribute_flavor']").is(":checked");
		var checked1 = $("input:radio[name='attribute_plan']").is(":checked");				
		var exsist;
		var target = $('table.variations');
				
				if($("input:radio[name='attribute_plan']").length>0)
				{
					exsist=1;
				}
				else
				{
				exsist=0;
				}
				
				if($(this).hasClass('wc-variation-selection-needed')==true)
				{
				if(exsist >0)
				{
				
				if((checked==false) && (checked1==false)){
				
				if($('div').hasClass('spj'))
				{
				$('.spj').text('Please select flavor and plan.');		
				$('table.variations tr.cart-item:first-child').addClass('validate_box');		
	    		if (target.length) {
		            $('html,body').animate({
		              scrollTop: target.offset().top - 100
		            }, 500);
		            return false;
		        }
				}
				else
				{
				$(this).closest('form').before('<div class="spj">Please select flavor and plan.</div>');
				$('table.variations tr.cart-item:first-child').addClass('validate_box');
				if (target.length) {
		            $('html,body').animate({
		              scrollTop: target.offset().top - 100
		            }, 500);
		            return false;
		        }
				}

				}
				else if(checked==true && checked1==false){
				
				if($('div').hasClass('spj'))
				{
				$('.spj').text('Please select a plan.');
				if (target.length) {
		            $('html,body').animate({
		              scrollTop: target.offset().top - 100
		            }, 500);
		            return false;
		        }
				}
				else
				{
				$(this).closest('form').before('<div class="spj">Please select a plan.</div>');
				if (target.length) {
		            $('html,body').animate({
		              scrollTop: target.offset().top - 100
		            }, 500);
		            return false;
		        }
				}
				
				}
				else if(checked==false && checked1==true){
				
				if($('div').hasClass('spj'))
				{
				$('.spj').text('Please select a flavor.');
				$('table.variations tr.cart-item:first-child').addClass('validate_box');
				if (target.length) {
		            $('html,body').animate({
		              scrollTop: target.offset().top - 100
		            }, 500);
		            return false;
		        }
				}
				else
				{
				$(this).closest('form').before('<div class="spj">Please select a flavor.</div>');
				$('table.variations tr.cart-item:first-child').addClass('validate_box');
				if (target.length) {
		            $('html,body').animate({
		              scrollTop: target.offset().top - 100
		            }, 500);
		            return false;
		        }
				}
				
				
				}
				
				}
				
				else
				{
				
				if(checked==false){
				
				if($('div').hasClass('spj'))
				{
				$('.spj').text('Please select a flavor.');
				$('table.variations tr.cart-item:first-child').addClass('validate_box');
				if (target.length) {
		            $('html,body').animate({
		              scrollTop: target.offset().top - 100
		            }, 500);
		            return false;
		        }
				}
				else
				{
				$(this).closest('form').before('<div class="spj">Please select a flavor.</div>');
				$('table.variations tr.cart-item:first-child').addClass('validate_box');
					if (target.length) {
			            $('html,body').animate({
			              scrollTop: target.offset().top - 100
			            }, 500);
			            return false;
			        }
				}			
				
				}
				
				}
				
				}
				else
				{
				$('.spj').remove();
				$('table.variations tr.cart-item:first-child').removeClass('validate_box');
				}
				
				
				});

		$('input[type="radio"]').click(function(){			
			$('.spj').text('');
			$('table.variations tr.cart-item:first-child').removeClass('validate_box');
		});	
		
	$('input[name=radio-2]').click(function(){
		if($(this).data('name') === 'register')
		{			
	 		$('#reg_email').val($('#username').val());
	 		$('#reg_password').val($('#password').val());	 		
			//$('form.login').hide();
			if($('body').hasClass('page-template-checkout')){
				$('.u-column1').hide();
			}else{
				$('form.login').hide();
			}
			$('form.register').show();
			$(this).prop("checked", true);
			$('#order3').prop("checked", true);
			$('#order1').prop("checked", true);
			if($('#reg_email').val() != ''){
				$('#reg_email').parent().addClass('n-focus');	
			}else{
				$('#reg_email').parent().removeClass('n-focus');
			}
			if($('#reg_password').val() != ''){
				$('#reg_password').parent().addClass('n-focus');	
			}else{
				$('#reg_password').parent().removeClass('n-focus');
			}
		}else{
			$('#username').val($('#reg_email').val());
	 		$('#password').val($('#reg_password').val());
			//$('form.login').show();
			if($('body').hasClass('page-template-checkout')){
				$('.u-column1').show();
				$('.u-column1 .login').show();
			}else{
				$('form.login').show();
			}
			$('form.register').hide();
			$('#order2').prop("checked", true);
			$('#order4').prop("checked", true);
			if($('#username').val() != ''){
				$('#username').parent().addClass('n-focus');	
			}else{
				$('#username').parent().removeClass('n-focus');
			}
			if($('#password').val() != ''){
				$('#password').parent().addClass('n-focus');	
			}else{
				$('#password').parent().removeClass('n-focus');
			}
		}
	});	
	$('#ship-to-different-address-checkbox').on('click',function(){
    	if($(this).is(':checked')){
    		$('.shipping_address').show();
    	}else{
    		$('.shipping_address').hide();
    	}    
	});
	$( "#note-card" ).click(function() {
	  $( ".note-box" ).slideToggle( "slow");
	});
	

	$('.yith-wcms-button,.timeline').click(function(){
		var current_step = $('li.active').data('step');
		if(current_step == 'payment')
		{	
			$('.main-checkout').addClass('col-sm-12');
			$('.main-checkout').removeClass('col-sm-8');
			$('.col-sm-3.col-sm-offset-1').hide();		
			$('.prevsteps').hide();				
			var billing_addr = $('#billing_first_name').val()+' '+$('#billing_last_name').val()+'<br/>'+$('#billing_address_1').val()+"<br/>"+$('#billing_city').val()+", "+$('#billing_state').val()+" "+$('#billing_postcode').val()+", "+$('#billing_country').val();
			if($('#ship-to-different-address').hasClass('address-minus')){
				var shipping_addr = $('#shipping_first_name').val()+' '+$('#shipping_last_name').val()+'<br/>'+$('#shipping_address_1').val()+"<br/>"+$('#shipping_city').val()+", "+$('#shipping_state').val()+" "+$('#shipping_postcode').val()+", "+$('#shipping_country').val();
			}else{
				var shipping_addr = $('#billing_first_name').val()+' '+$('#billing_last_name').val()+'<br/>'+$('#billing_address_1').val()+"<br/>"+$('#billing_city').val()+", "+$('#billing_state').val()+" "+$('#billing_postcode').val()+", "+$('#billing_country').val();
			}
			if(billing_addr !== '')
			{
				$('#order_billingaddr').html(billing_addr);
			}
			if(shipping_addr !== '')
			{
				$('#order_shippingaddr').html(shipping_addr);
			}					
		}
		else if(current_step == 'shipping')
		{
			$('.col-sm-3.col-sm-offset-1').hide();
			$('.main-checkout').addClass('col-sm-12');
			$('.main-checkout').removeClass('col-sm-8');
			$('.prevsteps').show();
		}
		else{
			$('.col-sm-3.col-sm-offset-1').show();
			$('.main-checkout').removeClass('col-sm-12');
			$('.main-checkout').addClass('col-sm-8');
			$('.prevsteps').show();	
		}

		$('#authnet-cc-form').find('.woocommerce-invalid').each(function(){
			if(!$(this).is(':visible')){
				$(this).removeClass('woocommerce-invalid woocommerce-invalid-required-field validate-required');
				if(!$('#authnet-token').parent().hasClass('n-focus')){
					$('#authnet-token').parent().addClass('n-focus');
				}
			}
		});

		// if(current_step == 3)
		// {
		// 	var cardnumber = $('#authnet-card-number').val();
		// 	if(cardnumber == ''){
		// 		if($('#authnet-card-number').parent().hasClass('woocommerce-validated'))
		// 		{
		// 			$('#authnet-card-number').parent().removeClass('woocommerce-validated').addClass('woocommerce-invalid woocommerce-invalid-required-field');
		// 		}
		// 	}
		// }
	});

	$('#payment').find('.woocommerce-invalid').each(function(){
		if(!$(this).is(':visible')){
			$(this).removeClass('woocommerce-invalid woocommerce-invalid-required-field validate-required');
		}
	});

	setTimeout(check, 2000);
	function check(){
		var current_step = $('li.active').data('step');
		if(current_step == 'payment'){
			$('.prevsteps').hide();	
		}else{
			$('.prevsteps').show();	
		}
		return current_step;
	}
	$( "#coupon-code" ).click(function() {
	 $( ".coupon" ).slideToggle("slow");
	}); 	    
	$( "#bill_edit" ).click(function() {
		$("#timeline-billing").trigger('click');
	}); 
	$( "#ship_edit" ).click(function() {
		$("#timeline-shipping").trigger('click');
	});
	$( "#flavr_Citrus" ).click(function() {
		$("#flavor_v_Citrus").trigger('click');
	});
	$( "#flavr_Unflavored" ).click(function() {
		$("#flavor_v_Unflavored").trigger('click');
	});
	$('#ship-to-different-address-checkbox').removeAttr('checked');

	$('.yith-wcms-button.next,.timeline').click(function() {
		$('.woocommerce-error').delay(2000).slideUp();
        var target = $('.woocommerce-invalid,.woocommerce-error,.woocommerce-error,.woocommerce-message,.woocommerce-thankyou-order-received');
        if(target.parent('div').is(':visible')){        	
	        if (target.length) {
	            $('html,body').animate({
	              scrollTop: target.offset().top - 100
	            }, 500);
	            return false;
	        }
    	}else{
    		var target = $('.checkout.heading-marg');
    		if (target.length) {
	            $('html,body').animate({
	              scrollTop: target.offset().top - 100
	            }, 500);
	            return false;
	        }
    	}    	
 	});

 	$(window).load(function(){ 
 		$('body').removeClass('ldd');
 		setTimeout(function(){
	  		$('.woocommerce-message').remove();
	  		$('body').removeClass('product-addedToCart');
		}, 5000000);
 	});
	if($('.woocommerce-message').length > 0){
		$('body').addClass('product-addedToCart');
	}
	$('.carticon').on('click', function(){
		 $('.cart-tab').find("a.dropdown-toggle").dropdown("toggle");
          return false;
	});
	$( document.body ).on( 'added_to_cart',function(){
    	$('.cart-tab').addClass('open');
	});
	if($("textarea[name='order_comments']").length > 0){
		$("textarea[name='order_comments']").attr('maxlength', '200');
	}
	$('.myaccount').hover(function(){
		$('.header_right_mnu').toggle();
	});
	$('.select2-results').click(function(){
		$('#one_page_searchable_customer_search').submit();
	});
	$('.blog-status').on('click','.zilla-likes',function(){
		$('body').addClass('wait');
		setTimeout(function(){
		  $('body').removeClass('wait');
		}, 4000);
	});
	$('.comment_bottom').click(function(){
		var target = $('.leave-reply');
		$('html,body').animate({
          scrollTop: target.offset().top - 100
        }, 1000);
	});
	$('.blog-box img').removeAttr('height');
	$('.blog-box img').removeAttr('width');
//Register Start
	$('.cusnew .form-control').focus(function(){
		if( $(this).val().length === 0 ) {
			$(this).closest('.check-input').addClass('n-focus');
		}
	});
	$('.cusnew .form-control').blur(function(){		
		if( $(this).val().length === 0 ) {
			$(this).closest('.check-input').removeClass('n-focus');
	        $(this).closest('.check-input').addClass('warning');
	    }else{
	    	$(this).closest('.check-input').removeClass('warning');
	    }
	});
	$('.cusnew .form-control').bind('click',function(){
		$(this).closest('.check-input').removeClass('warning');
	});
//Register End
// Sign In Start
	$('.cusexist .form-control').focus(function(){
		if( $(this).val().length === 0 ) {
			$(this).parent().addClass('n-focus');
		}
	});
	$('.cusexist .form-control').blur(function(){		
		if( $(this).val().length === 0 ) {
			$(this).parent().removeClass('n-focus');
	        $(this).parent().addClass('warning');
	    }else{
	    	$(this).parent().removeClass('warning');
	    }
	});
	$('.cusexist .form-control').bind('click',function(){
		$(this).parent().removeClass('warning');
	});
//Sign In End
// Billing Form Start
	$('.form-row .input-text').focus(function(){
		if( $(this).val().length === 0 ) {
			$(this).parent().addClass('n-focus');
		}
	});
	$('.form-row .input-text').blur(function(){		
		if( $(this).val().length === 0 ) {
			$(this).parent().removeClass('n-focus');
	        $(this).parent().addClass('warning');
	    }else{
	    	$(this).parent().removeClass('warning');
	    }
	});
	$('.form-row .input-text').bind('click',function(){
		$(this).parent().removeClass('warning');
	});
//Billing Form End
// Contact Form Start
	$('.check-input.form-row .input-text').focus(function(){
		if( $(this).val().length === 0 ) {
			$(this).parent('span').parent().addClass('n-focus');
		}
	});
	$('.check-input.form-row .input-text').blur(function(){		
		if( $(this).val().length === 0 ) {
			$(this).parent('span').parent().removeClass('n-focus');
	        $(this).parent('span').parent().addClass('warning');
	    }else{
	    	$(this).parent('span').parent().removeClass('warning');
	    }
	});
	$('.check-input.form-row .input-text').bind('click',function(){
		$(this).parent('span').parent().removeClass('warning');
	});
//Contact Form End

       $('input[type="radio"]').click(function() {
            var idVal = $(this).attr("id");
        });
        $('.button').click(function() {
       setTimeout(function(){ $('.woocommerce-message').slideUp('slow'); }, 2000);
       });
	   
	$(".checkout").on('click','.shipping_method', function(){
		var shipping = $(this).next().text();
		$('.shippingright').text(shipping);		
		setTimeout(function(){ var total = $('.totalorder').html(); $('.totalamount').html(total); }, 2000);		
	});
	
	var checkoutInterval = '';

	$("#order_checkout_payment").on('click','input.btn.btn-default', function(){
		$("#order_checkout_payment").find('input.btn.btn-default').addClass('disabled');
		checkoutInterval = setInterval(checkoutProcess, 500);
	});
	
	function checkoutProcess() {
		if(!$(".checkout.woocommerce-checkout").hasClass('processing')){
			$("#order_checkout_payment").find('input.btn.btn-default').removeClass('disabled');
			clearInterval(checkoutInterval);
		}
	}
	
    $( ".main-checkout .form-row" ).each(function() {
  		var valfld = $( this ).find('.input-text').val();
  		$( this ).find('.input-text').attr('placeholder','');
  		if(valfld){ 
  			$(this).addClass('n-focus');
  		}
	});
	$('.update_totals_on_change').removeClass('n-focus');	
	setTimeout(function(){
		if($('.productAmount li:nth-child(3)').is(':visible')){
			if($('.wc_payment_method').length == 0){
				if($(".applied_price_content").length == 0) {
					$('#order_info #payment').append('<div class="applied_price_content"></div>');
				}
				$('.applied_price_content').html('<p><strong>Payment made using gift card. Proceed to order review.</strong></p>');
			}
		}
	},2000);
	
	$('.applycouponbtn').click(function(){
		$('#coupon_code').val($('.applycoupon').val());
		$("input[name=apply_coupon]").trigger('click');
		$('html,body').animate({
	              scrollTop: $('.checkout').offset().top - 100
	            }, 500);
		setTimeout(function(){  
			var carttotalprice = $('.cartprice').html();
			$('.cartamount').html(carttotalprice); 
			$('.cartamount').parent().show();
			var total = $('.totalorder').html(); 
			$('.totalamount').html(total);
		}, 5000);
	});
	
	$('.checkout').on('click','.giftcouponbtn',function(){
		$('.checkout-ship').css('opacity','0.5');
		$('.checkout-ship').append('<div class="loader"></div>');
		var gif_card_val = $('.giftcoupon').val();
		$('#giftcard_code').val(gif_card_val).change();
		$("[name='apply_gift_card']").trigger('click');
		$('html,body').animate({
	              scrollTop: $('.checkout').offset().top - 100
	            }, 500);
		setTimeout(function(){  
			/*var carttotalprice = $('.cartprice').html(); 
			$('.cartamount').html(carttotalprice); */
			var total = $('.totalorder').html();
			$('.totalamount').html(total);
			//$('.cartamount').parent().html('<span class="pull-left">Gift card:</span><span class="pull-right giftamount">'+gif_card_val+'</span><a href="'+cart_url+'?remove_gift_card_code='+gif_card_val+'" class="ywgc-remove-gift-card pull-right" data-gift-card-code="'+gif_card_val+'">[Remove]</a>').show();
			var data = {
				'action': 'gift_card_checkout_update',
			};
	
			// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
			$.post(ajaxurl, data, function(response) {
				$('.gift_card_listing').html(response).show();
				$('.checkout-ship').css('opacity','1');
				$('.checkout-ship').find('.loader').remove();
				$('.giftcoupon').val('');
				if($('.wc_payment_method').length == 0){
					if($(".applied_price_content").length == 0) {
						$('#order_info #payment').append('<div class="applied_price_content"></div>');
						$('.applied_price_content').html('<p><strong>Payment made using gift card. Proceed to order review.</strong></p>');
					}else{
						$('.applied_price_content').html('<p><strong>Payment made using gift card. Proceed to order review.</strong></p>');
					}
				}
			});
		}, 5000);
	});
	////////////gift coupon removal ////////
	
	$(".checkout").on('click','.woocommerce-remove-coupon', function(){
		$('.cartamount').parent().hide();
		setTimeout(function(){ 
			var carttotalprice = $('.cartprice').html(); 			
			var total = $('.totalorder').html(); 
			$('.cartamount').html(carttotalprice);
			$('.totalamount').html(total);
		}, 5000);

	});
	
	$(".checkout").on('click','.ywgc-remove-gift-card', function(){
		$('.checkout-ship').css('opacity','0.5');
		$('.checkout-ship').append('<div class="loader"></div>');
		$('html,body').animate({
	              scrollTop: $('.checkout').offset().top - 100
	            }, 500);
	    setTimeout(function(){
	    	var total = $('.totalorder').html();
			$('.totalamount').html(total);
	    	var data = {
				'action': 'gift_card_checkout_update',
			};
		    $.post(ajaxurl, data, function(response) {
		    	if($.trim(response)=='no'){
		    		$('.gift_card_listing').html('').hide();
		    		if($(".applied_price_content").length > 0) {
		    			$('#order_info #payment .applied_price_content').remove();
		    		}
		    	}else{
		    		$('.gift_card_listing').html(response).show();
		    		
		    		if($('.wc_payment_method').length == 0){
			    		if($(".applied_price_content").length == 0) {
							$('#order_info #payment').append('<div class="applied_price_content"></div>');
							$('.applied_price_content').html('<p><strong>Payment made using gift card. Proceed to order review.</strong></p>');
						}else{
							$('.applied_price_content').html('<p><strong>Payment made using gift card. Proceed to order review.</strong></p>');
						}
					}
		    	}
				$('.checkout-ship').css('opacity','1');
				$('.checkout-ship').find('.loader').remove();
				$('.giftcoupon').val('');
			});
		 },5000);
	});
	
	// if($('.account .check-input').find('input[type=text]').val() != ''){
	// 	$('.account .check-input').addClass('n-focus');	
	// }
  

	$(document.body).on('checkout_error', function () {
	    $(document.body).stop( );
	    $('html, body').animate({
	        scrollTop: ($('body').offset().top - 400)
	    }, 1000);
	});
	$("#cartplus").click(function(){
		var counter = $(this).prev().val();
        counter++ ;
        $(this).prev().val(counter);
	});
	$("#cartminus").click(function(){
		var counter = $(this).next().val();
		if(counter > 1){
			counter-- ;
		}        
        $(this).next().val(counter);
	});
	$("#review_form").on('click','#reply-title',function(){		      
        $(this).next('form').slideToggle('slow');
	});
	$(".woocommerce-review-link").attr('href','#reviews_summary');
	
	
	
    $('.comment-tran input').blur(function() {
		$(this).parents('.check-input').addClass('warning');
	});

	$('.comment-tran input').focus(function() {
		$(this).parents('.check-input').removeClass('warning');
	});

	$(document).click(function() {
		if($('.menu-item .nav-dropdown').is(':visible')) {
			$('body').addClass('submenu_open');
		}
		else {
			$('body').removeClass('submenu_open');
		}
	})
	$(window).load(function() {
		setTimeout(function(){
			$('#authnet-cc-form').find(".form-row-last").addClass('cardfld');
		},1000);
		
	});
	
    $('#credit-cards').text(function(i, oldText) {
        return oldText === 'My Credit Cards' ? 'Default Payment Method' : oldText;
    });
    
    $('#credit-cards').append('<p>THE BELOW PAYMENT METHOD(S) CAN BE USED FOR FUTURE ORDERS. TO CHANGE YOUR PAYMENT METHOD FOR AN EXISTING SUBSCRIPTION, PLEASE WRITE TO US AT <a href="mailto:contacts@omega3innovations.com" target="_blank">contacts@omega3innovations.com</a> OR CALL US AT 1-866-414-0188.</p>');

    $('.edit-card').click(function(){
    	setTimeout(function(){
    		$('.edit-card-heading').prepend('<p>Leave blank to leave unchanged</p>');
    	},1)
    	
    })

    $('.billingshipedit .address-field input').attr('placeholder', '');
    
    $(document).on('click','body', function(e){
	    var container = $(".minicartdisplay");
	
	    // if the target of the click isn't the container nor a descendant of the container
	    if (!container.is(e.target) && container.has(e.target).length === 0) 
	    {
	        container.hide();
	    }
	});
	
	$('.cart-pic').closest('a').on('click',function(){
		$(".minicartdisplay").show();
	});

	$(document).on('keydown','.woocommerce-checkout', function(e){
	    var container1 = $("form.login input");
	    var container2 = $("form.register input");
	    if (!container1.is(e.target) && container1.has(e.target).length === 0 && !container2.is(e.target) && container2.has(e.target).length === 0 && e.keyCode == 13) 
	    {
	        e.preventDefault();
      		return false;
	    }
	});
	// $('.woocommerce-checkout').on('keydown',function(event){
  	// alert(event.target.attr('id'));
    // if(event.keyCode == 13) {
      // event.preventDefault();
      // return false;
    // }
  //});
  
});

jQuery(window).load(function(){
	if(jQuery('.myaccount_order .payment_box select').length > 0){
		jQuery('.myaccount_order .payment_box select option[value=""]').remove();
	}
});