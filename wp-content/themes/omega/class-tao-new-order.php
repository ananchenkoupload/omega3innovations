<?php

/**
 * This is an example trigger that is triggered via a WordPress action and includes a user data item.
 *
 * This example is triggered with the following code
 * do_action('my_custom_action', $user_id );
 *
 * @class My_AutomateWoo_Custom_Trigger
 */
class Tao_New_Order_Trigger extends AW_Trigger
{
	/**
	 * A unique ID for the trigger
	 * @var string
	 */
	public $name = 'order_placed';


	/**
	 * Define which data items this trigger will supply to the actions
	 * @var array
	 */
	public $supplied_data_items = [ 'user', 'order', 'shop' ];


	/**
	 * Construct
	 */
	public function init()
	{
		$this->title = __('New Order', 'automatewoo-custom');
		$this->description = __( 'Fires as soon as an order is created in the database regardless of its status and happens before payment is confirmed.', 'automatewoo' );
		$this->group = __( 'Tao', 'automatewoo' );


		// Registers the trigger
		parent::init();
	}


	/**
	 * Defines when the trigger is run
	 */
	public function register_hooks()
	{
		add_action( 'woocommerce_api_create_order', [ $this, 'catch_hooks' ], 1000 );
		add_action( 'woocommerce_checkout_order_processed', [ $this, 'catch_hooks' ], 1000 );
	}


	/**
	 * Catches the action and calls the maybe_run() method.
	 *
	 * @param $user_id
	 */
	function catch_hooks( $order_id ) {
		$order = wc_get_order( $order_id );
		$user = AW()->order_helper->prepare_user_data_item( $order );

		$this->maybe_run([
			'order' => $order,
			'user' => $user,
		]);
		// Ensure only triggers once per order
		if ( get_post_meta( $order_id, '_aw_checkout_order_processed', true ) ) return;

		add_post_meta( $order_id, '_aw_checkout_order_processed', true );

		parent::catch_hooks( $order_id );
	}


	/**
	 * Performs any validation if required. If this method returns true the trigger will fire.
	 *
	 * @param $workflow AW_Model_Workflow
	 *
	 * @return bool
	 */
	public function validate_workflow( $workflow )
	{
		// Get the trigger object with options loaded
		$trigger = $workflow->get_trigger();

		// Get any data items
		$user = $workflow->get_data_item('user');
		$order = $workflow->get_data_item('order');

		// don't trigger if there is no user
		if ( ! $user || ! $order )
			return false;

		return true;
	}

}