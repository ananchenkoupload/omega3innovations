<?php

/**
 * This is an example trigger that is triggered via a WordPress action and includes a user data item.
 *
 * This example is triggered with the following code
 * do_action('my_custom_action', $user_id );
 *
 * @class My_AutomateWoo_Custom_Trigger
 */
class Tao_Abandoned_Cart_Trigger extends AW_Trigger
{
	/**
	 * A unique ID for the trigger
	 * @var string
	 */
	public $name = 'abandoned_cart';
	public $supplied_data_items = [ 'cart', 'user', 'shop' ];


	/**
	 * Construct
	 */
	public function init()
	{
		$this->title = __('Abandoned Cart (Existing User)', 'automatewoo-custom');
		$this->group = __( 'Tao', 'automatewoo' );

		// Registers the trigger
		parent::init();
	}


	/**
	 * Add any fields to the trigger. Can be left blank.
	 */
	public function load_fields()
	{
		$delay = ( new AW_Field_Number_Input() )
			->set_name('delay')
			->set_title( __( 'Hours After Abandoned', 'automatewoo' ) )
			->set_min('0.25')
			->set_max('720')
			->set_description(__( "It is possible to use decimal values e.g. '1.5 = 1 hour and 30 minutes'. Max is 720 hours (30 days).", 'automatewoo' ) )
			->add_extra_attr('step', '0.25')
			->set_required();

		$this->add_field( $delay );
		$this->add_field_user_pause_period();
	}


	/**
	 * Defines when the trigger is run
	 */
	public function register_hooks()
	{
		add_action( 'automatewoo_thirty_minute_worker', [ $this, 'catch_hooks' ] );
	}


	/**
	 * Catches the action and calls the maybe_run() method.
	 *
	 * @param $user_id
	 */
	public function catch_hooks( $user_id )
	{
		$this->maybe_run(array(
			'user' => get_user_by( 'id', $user_id ),
		));
	}


	/**
	 * Performs any validation if required. If this method returns true the trigger will fire.
	 *
	 * @param $workflow AW_Model_Workflow
	 *
	 * @return bool
	 */
	public function validate_workflow_created_date_for_cart( $workflow, $cart ) {
		$grace_period = DAY_IN_SECONDS * 3;

		// the created property was added in v2.8
		if ( ! $cart->created ) {
			$cart->created = $cart->last_modified;
			$cart->save();
		}

		return strtotime( $workflow->get_date_created() ) - $grace_period < strtotime( $cart->created );
	}
	public function validate_workflow( $workflow ) {

		/** @var AW_Model_Abandoned_Cart $cart */
		$user = $workflow->get_data_item( 'user' );
		$cart = $workflow->get_data_item('cart');
		$date_modified = $cart->get_date_last_modified();

		$delay = floatval( $workflow->get_trigger_option('delay') );

		if ( ! $user || ! $cart || ! $delay || ! $date_modified ) {
			return false;
		}

		if ( ! $this->validate_workflow_created_date_for_cart( $workflow, $cart ) )
			return false;

		$delay_mins = round( $delay * 60 );
		$delay_date = new DateTime(); // UTC
		$delay_date->modify("-$delay_mins minutes");


		// Was cart last updated longer than the delay date
		if ( $date_modified->getTimestamp() > $delay_date->getTimestamp() )
			return false;


		if ( ! $this->validate_field_user_pause_period( $workflow ) )
			return false;


		// Now check our logs
		// Only run each once foreach workflow for each stored cart
		$log_query = ( new AW_Query_Logs() )
			->where( 'workflow_id', $workflow->get_translation_ids() )
			->where( 'cart_id', $cart->id )
			->set_limit(1);

		if ( $log_query->get_results() )
			return false;

		return true;
	}

}