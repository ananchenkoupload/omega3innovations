<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<section class="blog-detail heading-marg">
<!--    subscription detail part starts-->
    

    <div class="container">
        <div class="blog-top">
             <?php //echo do_shortcode( '[searchandfilter fields="search,category"]' ); ?>
             <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
           
        </div>
    </div>
    <div class="blog-banner">
        <div class="container">
            <?php //query_posts('post_type=post&showposts=1&order=DESC&orderby=date');
            $i=0;
	           while (have_posts()) : the_post();  
	           if($i==0):
	           ?>
            <div class="row">
                <div class="col-sm-7">
                    <div class="blog-bnr">
                    	<?php if(has_post_thumbnail()):?>
                        <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('top-blog');?>
                        </a>
                        <?php endif;?>
                    </div>
                </div>
                <div class="col-sm-5">
                	<div class="blog-cont-new">
                    <div class="blog-news-date">
                    BY <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo get_the_author_meta( 'display_name'); ?></a>  <?php echo get_the_time('M j, Y'); ?>
                    </div>
                    <h2><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></h2>
                    <p><?php echo wp_trim_words(get_the_content(),25,'...')?><a href="<?php the_permalink(); ?>">Read full story</a></p>
                    </div>
                </div>
            </div> 
            <?php endif; $i++; endwhile;
             ?>
        </div>
    </div>   
    <div class="container">
        <div class="row blog-containt">
            <div class="col-md-9"> 
        	<?php 
        	$i=0;
	           while (have_posts()) : the_post();  
	           if($i>0): ?>            
            <div class="blog-left">
                <div class="blog-box blog-listing clearfix">
                    <a href="<?php the_permalink(); ?>" class="blog-list-pic">
                        <?php $thumb_image=wp_get_attachment_url(get_post_thumbnail_id(), 'bottom-blog', true);   ?>
                        <div class="blog-picnew" style="background-image: url('<?php echo $thumb_image; ?>')"></div>
                    
                    </a>
                    <div class="blog-list-text">
                        <div class="blog-news-date">
                            <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo get_the_author_meta( 'display_name',$post->post_author); ?></a>    <?php echo get_the_time('M j, Y'); ?>
                        </div>
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <!-- <p><?php echo wp_trim_words( get_the_content(), 40, '...' ); ?><a href="<?php the_permalink(); ?>">Read full article</a></p> -->
                        <div class="customer-social list-social">
                            <ul>
                                <li><a href="<?php the_permalink(); ?>/#respond"><i class="fa fa-commenting-o" aria-hidden="true"></i> <?php if(get_comments_number(get_the_ID())){echo $vart=get_comments_number(get_the_ID()); } else { echo $vart='0'; } ?></a></li>
                                <li><?php if( function_exists('zilla_likes') ) zilla_likes(); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>           
           <?php endif; $i++; endwhile; ?>
           <?php the_posts_pagination( array(
				'prev_text'          => __( '<i class="fa fa-angle-left" aria-hidden="true"></i>', 'twentysixteen' ),
				'next_text'          => __( '<i class="fa fa-angle-right" aria-hidden="true"></i>', 'twentysixteen' ),				
			) ); ?>
            </div>
            <div class="col-md-3">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>

   <div class="contactBlog">
        <div class="container">
            <div class="signUp">
                <h4>SIGN UP FOR OUR NEWSLETTER</h4>

                <div class="signUp-in">
                    <!-- <input type="email" placeholder="Enter email address..." />
                    <input type="submit" value="" /> -->
                    
     <form action="//omega3innovations.us4.list-manage.com/subscribe/post?u=3689faabafe08edbf98e39a4f&amp;id=6ce5e63d91" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
		<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Enter email address...">
	    <input type="submit" value="" name="subscribe" id="mc-embedded-subscribe">	
	    <div id="mce-responses" class="clear">
			<div class="response" id="mce-error-response" style="display:none"></div>
			<div class="response" id="mce-success-response" style="display:none"></div>
		</div>   
     </form>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
                            
                </div>
            </div>
           <div class="socBlog">
                <h4>GET TO KNOW US</h4>

                <ul>
                    <li>
                        <a  target="_blank" href="<?php echo get_theme_mod( 'twitter' )?get_theme_mod( 'twitter' ):'javascript:void(0)'; ?>">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a  target="_blank" href="<?php echo get_theme_mod( 'facebook' )?get_theme_mod( 'facebook' ):'javascript:void(0)'; ?>">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a  target="_blank" href="<?php echo get_theme_mod( 'pinterest' )?get_theme_mod( 'pinterest' ):'javascript:void(0)';  ?>">
                            <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                        </a>
                    </li>
                    <!-- <li>
                        <a  target="_blank" href="<?php //echo get_theme_mod( 'instagram' )?get_theme_mod( 'instagram' ):'javascript:void(0)';?>">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                    </li> -->
                </ul>
            </div>
        </div>
    </div>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
