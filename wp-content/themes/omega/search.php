<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<section class="blog-detail heading-marg">
		<div class="container">
        <div class="blog-top">
             <?php //echo do_shortcode( '[searchandfilter fields="search,category"]' ); ?>
             <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
            <div class="top-search list-top">
                <span>Top Searches:</span>
                <ol class="breadcrumb">
                  <li><a href="#">Brain Health</a></li>
                  <li><a href="#">Omeag-3</a></li>
                  <li class="active">Heart Health</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="blog-banner">
            <div class="container">            
                <div class="row">                   
                    <div class="col-sm-12">               
                       	<h2><?php printf( __( 'Search Results for: %s', 'twentysixteen' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h2>                      
                    </div>
                </div> 
            </div>
        </div>
	    <div class="container">
        <div class="row blog-containt">
            <div class="col-sm-9"> 
		<?php if ( have_posts() ) : ?>

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			// End the loop.
			endwhile;

			// Previous/next page navigation.
		 the_posts_pagination( array(
				'prev_text'          => __( '<i class="fa fa-angle-left" aria-hidden="true"></i>', 'twentysixteen' ),
				'next_text'          => __( '<i class="fa fa-angle-right" aria-hidden="true"></i>', 'twentysixteen' ),
				
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	  </div>
            <div class="col-sm-3 widget_categories">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</section>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
