<?php
/**
 * Registration form.
 *
 * @author 	Jeroen Sormani
 * @package 	WooCommerce-Simple-Registration
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;
wp_enqueue_script( 'wc-password-strength-meter' );

if(isset($_POST['register']) && !empty($_POST['register']))
{
	$checked = 'checked="checked"';
	$check = '';
}else{
	$check = 'checked="checked"';
	$checked = '';
}
if(isset($_GET['msg']) && $_GET['msg']=='failed'){
	wc_print_notice( __( 'Your email id is already registered. Please try to login with your username and password.', 'woocommerce' ), 'error' );
}
?>


<div class="registration-form woocommerce">

	<?php wc_print_notices(); ?>	

	<form method="post" class="register" <?php echo isset($_POST['register']) && !empty($_POST['register'])?'style="display:block;"':'style="display:none;"';?>>

		<h2 class="form_txt"><?php _e( 'Signup', 'woocommerce' ); ?></h2>
		<span class="login_other_option express_signup"><span class="express_inner">Express Signup</span></span></br>
		<?php echo do_shortcode('[social_signup]');?>
		 <?php //do_action( 'wordpress_social_login' ); ?>
		 <?php //echo do_shortcode('[register_form]');?>
          
		<?php do_action( 'woocommerce_register_form_start' ); ?>

		<div class="account cusnew">
		<div class="check_in jetaccnt">
			<input id="order4" type="radio" name="radio-2" data-name="signin" <?php echo $check; ?>>
			<label for="order4">
				<h4>Login</h4>
			</label>   
		</div>
		<div class="check_in jetaccnt">
			<input id="order3" type="radio" name="radio-2" data-name="register" <?php echo $checked; ?>>
			<label for="order3">
				<h4>Signup</h4>
					<!--<h5>Create an account with us for quick checkout, 
						to keep track of your order, and more.</h5>-->
			</label>   
		</div>
		<div class="check-input">
			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
				<input type="text" class="form-control" placeholder="Username" name="username" id="reg_username" autocomplete="off" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
			<?php endif; ?>
			<label>Email</label>
			<input type="email" autocomplete="off" class="form-control" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
		</div>
		<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
		<div class="check-input">
			<label>Password</label>
			<input type="password" class="form-control" name="password" autocomplete="off" />
		</div>
		<?php endif; ?>
	

		<!-- Spam Trap -->
		<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

		<?php do_action( 'woocommerce_register_form' ); ?>
		<?php do_action( 'register_form' ); ?>

		<p class="woocomerce-FormRow form-row">
			<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
			<input type="submit" class="btn btn-default form_txt" name="register" value="<?php esc_attr_e( 'Create Account', 'woocommerce' ); ?>" />
		</p>
		</div>

		<?php do_action( 'woocommerce_register_form_end' ); ?>

	</form>

</div>
