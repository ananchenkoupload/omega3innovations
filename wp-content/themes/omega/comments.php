<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<div class="row">
	<div class="col-sm-12">

	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php
				$comments_number = get_comments_number();
				if ( 1 === $comments_number ) {
					/* translators: %s: post title */
					printf( _x( 'One thought on &ldquo;%s&rdquo;', 'comments title', 'twentysixteen' ), get_the_title() );
				} else {
					printf(
						/* translators: 1: number of comments, 2: post title */
						_nx(
							'%1$s thought on &ldquo;%2$s&rdquo;',
							'%1$s thoughts on &ldquo;%2$s&rdquo;',
							$comments_number,
							'comments title',
							'twentysixteen'
						),
						number_format_i18n( $comments_number ),
						get_the_title()
					);
				}
			?>
		</h2>

		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 42,
					'type' => 'comment',
					'callback' => 'mytheme_comment',
					
				) );
			?>
		</ol><!-- .comment-list -->

		<?php the_comments_navigation(); ?>

	<?php endif; // Check for have_comments(). ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'twentysixteen' ); ?></p>
	<?php endif; ?>

	<?php
	$fields =  array(
			  'author' =>
			    '<div class="check-input xxx"><label for="author">Name</label><div class="comment-tran"><input id="author" autocomplete="off" name="author" class="form-control" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
			    '" size="30"' . $aria_req . ' /></div></div>',

			  'email' =>
			    '<div class="check-input xxx"><label for="email">Email</label><div class="comment-tran"><input id="email" autocomplete="off" name="email" class="form-control" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
			    '" size="30"' . $aria_req . ' /></div></div>',

			  // 'url' =>
			    // '<div class="check-input"><label for="url">Call back number</label><input id="url" name="url" type="text" class="form-control" value="' . esc_attr( $commenter['comment_author_url'] ) .
			    // '" size="30" /></div>',
			);
	$args = array(
			  'id_form'           => 'commentform',
			  'class_form'        => 'comment-form cusnew',
			  'id_submit'         => 'submit',
			  'class_submit'      => 'btn btn-default',
			  'name_submit'       => 'Post Comment',
			  'title_reply_before'=> '<h3>',
			  'title_reply_after' => '</h3>',
			  'title_reply'       => __( 'Leave a Reply' ),
			  'title_reply_to'    => __( 'Leave a Reply to %s' ),
			  'cancel_reply_link' => __( 'Cancel Reply' ),
			  'comment_notes_before'=>'<p class="comment-notes">' . __( 'Your email address will not be published.' ).'</p>',
			  'label_submit'      => __( 'Post Comment' ),
			  'format'            => 'xhtml',
			  'fields' 			  => apply_filters( 'comment_form_default_fields', $fields ),
			  'comment_field' 	  =>  '<div class="check-input chckTextearea"><label for="comment">Comment</label><div class="comment-tran"><textarea id="comment" name="comment" class="form-control" aria-required="true"></textarea></div></div>',
			  'must_log_in' 	  => '<p class="must-log-in">' .
			    sprintf(
			      __( 'You must be <a href="%s">logged in</a> to post a comment.' ),
			      wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
			    ) . '</p>',
			  'logged_in_as' => '<p class="logged-in-as">' .
			    sprintf(
			    __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ),
			      admin_url( 'profile.php' ),
			      $user_identity,
			      wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
			    ) . '</p>'			 
			);

		comment_form($args, $post_id);
	?>

	</div>
</div><!-- .comments-area -->
