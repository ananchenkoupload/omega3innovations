<?php
/**
 * Twenty Sixteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentysixteen_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own twentysixteen_setup() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentysixteen
	 * If you're building a theme based on Twenty Sixteen, use a find and replace
	 * to change 'omega' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'omega' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for custom logo.
	 *
	 *  @since Twenty Sixteen 1.2
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 240,
		'width'       => 240,
		'flex-height' => true,
	) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'omega' ),
		'topright' => __( 'Top Right Menu', 'omega' ),
		'social'  => __( 'Social Links Menu', 'omega' ),
		'footermenu' => __( 'Footer Menu', 'omega' ),
		'pagemenu' => __( 'Page Menu', 'omega' ),
		'blogsubmenu' => __( 'Subblog Menu', 'omega' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', twentysixteen_fonts_url() ) );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // twentysixteen_setup
add_action( 'after_setup_theme', 'twentysixteen_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'twentysixteen_content_width', 840 );
}
add_action( 'after_setup_theme', 'twentysixteen_content_width', 0 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'omega' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'omega' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Content Bottom 1', 'omega' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'omega' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Content Bottom 2', 'omega' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'omega' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
}
add_action( 'widgets_init', 'twentysixteen_widgets_init' );

if ( ! function_exists( 'twentysixteen_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Sixteen.
 *
 * Create your own twentysixteen_fonts_url() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function twentysixteen_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'omega' ) ) {
		$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
	}

	/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'omega' ) ) {
		$fonts[] = 'Montserrat:400,700';
	}

	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'omega' ) ) {
		$fonts[] = 'Inconsolata:400';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentysixteen_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentysixteen-fonts', twentysixteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );

	// Theme stylesheet.
	wp_enqueue_style( 'twentysixteen-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie7', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'twentysixteen-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'twentysixteen-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'twentysixteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20160816', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentysixteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20160816' );
	}

	wp_enqueue_script( 'twentysixteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20160816', true );

	wp_localize_script( 'twentysixteen-script', 'screenReaderText', array(
		'expand'   => __( 'expand child menu', 'omega' ),
		'collapse' => __( 'collapse child menu', 'omega' ),
	) );
}
add_action( 'wp_enqueue_scripts', 'twentysixteen_scripts' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function twentysixteen_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'twentysixteen_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function twentysixteen_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Register Custom Navigation Walker
 */
require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentysixteen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	840 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';

	if ( 'page' === get_post_type() ) {
		840 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	} else {
		840 > $width && 600 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		600 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentysixteen_content_image_sizes_attr', 10 , 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function twentysixteen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		! is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentysixteen_post_thumbnail_sizes_attr', 10 , 3 );

/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 *
 * @since Twenty Sixteen 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array A new modified arguments.
 */
function twentysixteen_widget_tag_cloud_args( $args ) {
	$args['largest'] = 1;
	$args['smallest'] = 1;
	$args['unit'] = 'em';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'twentysixteen_widget_tag_cloud_args' );

/* Woocommerce Support */
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}



function contact_details( $wp_customize ) {
   $wp_customize->add_section(
       'example_section_one',
       array(
           'title' => 'Contact information',
           'description' => 'Insert your contact informations',
           'priority' => 35,
       )
   );
$wp_customize->add_setting(
   'contacttitle'
);
$wp_customize->add_setting(
   'address'
);
$wp_customize->add_setting(
   'phonenumber'
);
$wp_customize->add_setting(
   'copyright'
);

$wp_customize->add_control(
   'contacttitle',
   array(
       'label' => 'Contact Title',
       'section' => 'example_section_one',
       'type' => 'text',
   )
);
$wp_customize->add_control(
   'address',
   array(
       'label' => 'Address',
       'section' => 'example_section_one',
       'type' => 'textarea',
   )
);
$wp_customize->add_setting( 'themeslug_logo' );
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
    'label'    => __( 'Footer logo'),
    'section'  => 'example_section_one',
    'settings' => 'themeslug_logo',
) ) );
$wp_customize->add_control(
   'phonenumber',
   array(
       'label' => 'Phone number',
       'section' => 'example_section_one',
       'type' => 'text',
   )
);
$wp_customize->add_control(
   'copyright',
   array(
       'label'	 => 'Copyright',
       'section' => 'example_section_one',
       'type' 	 => 'text',
   )
);

}
add_action( 'customize_register', 'contact_details' );



function social_customizer( $wp_customize ) {
   $wp_customize->add_section(
       'example_section_two',
       array(
           'title' => 'Social Links For Site',
           'description' => 'Insert your site Social links',
           'priority' => 36,
       )
   );
$wp_customize->add_setting(
   'facebook'
);
$wp_customize->add_setting(
   'google_plus'
);
$wp_customize->add_setting(
   'instagram'
);
$wp_customize->add_setting(
   'twitter'
);
$wp_customize->add_setting(
   'pinterest'
);
$wp_customize->add_control(
   'facebook',
   array(
       'label' => 'Facebook',
       'section' => 'example_section_two',
       'type' => 'text',
   )
);
$wp_customize->add_control(
   'google_plus',
   array(
       'label' => 'Googleplus',
       'section' => 'example_section_two',
       'type' => 'text',
   )
);
$wp_customize->add_control(
   'instagram',
   array(
       'label' => 'Instagram',
       'section' => 'example_section_two',
       'type' => 'text',
   )
);
$wp_customize->add_control(
   'twitter',
   array(
       'label' => 'Twitter',
       'section' => 'example_section_two',
       'type' => 'text',
   )
);
$wp_customize->add_control(
   'pinterest',
   array(
       'label' => 'Pinterest',
       'section' => 'example_section_two',
       'type' => 'text',
   )
);
}
add_action( 'customize_register', 'social_customizer' );


function copyright( $wp_customize ) {
   $wp_customize->add_section(
       'example_section_three',
       array(
           'title' => 'Copyright section',
           'description' => 'Insert your copyright details',
           'priority' => 37,
       )
   );
$wp_customize->add_setting(
   'poweredbytitle'
);
$wp_customize->add_setting( 'copylogo' );
$wp_customize->add_setting(
   'copyrighttext'
);
$wp_customize->add_control(
   'poweredbytitle',
   array(
       'label' => 'Poweredby Title',
       'section' => 'example_section_three',
       'type' => 'text',
   )
);
	
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'copylogo', array(
    'label'    => __( 'Poweredby logo'),
    'section'  => 'example_section_three',
    'settings' => 'copylogo',
) ) );
$wp_customize->add_control(
   'copyrighttext',
   array(
       'label' => 'Copyright text',
       'section' => 'example_section_three',
       'type' => 'text',
   )
);
}
add_action( 'customize_register', 'copyright' );

/*02.11.2016*/

/*05.05.2017*/
add_action( 'after_setup_theme', 'image_hard_crop' );
function image_hard_crop() {
    add_image_size( 'top-blog', 653, 377, array( 'center', 'center' ) );
	add_image_size( 'bottom-blog', 310, 370, array( 'center', 'center' ) );
}

add_action( 'admin_menu', 'mailchimp_admin_menu' );

function mailchimp_admin_menu() {
	add_menu_page( 'Jetcommerce Mailchimp Subscriber Listing', 'Mailchimp Subscribers', 'manage_options', 'mailchimp-subscribers.php', 'mailchimp_subscribers', '
dashicons-admin-users', 1  );
}
function mailchimp_subscribers(){
	?>
		<?php
		$api_key = '7573b65799d49737321f85bda7326aae-us4';
		$list_id = '6ce5e63d91';
		$dc = substr($api_key,strpos($api_key,'-')+1); // us5, us8 etc
		$args = array(
		 	'headers' => array(
				'Authorization' => 'Basic ' . base64_encode( 'user:'. $api_key )
			)
		);
		 
		// connect
		$response = wp_remote_get( 'https://'.$dc.'.api.mailchimp.com/3.0/lists/'.$list_id, $args );
		 
		// decode the response
		$body = json_decode( $response['body'] );
		 
		if ( $response['response']['code'] == 200 ) :
		 
			// subscribers count
			$member_count = $body->stats->member_count;
			$subscriber = array();
		 
			for( $offset = 0; $offset < $member_count; $offset += 50 ) :
		 
				$response = wp_remote_get( 'https://'.$dc.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members?offset=' . $offset . '&count=50', $args );
				// decode the result
				$body = json_decode( $response['body'] );
				if ( $response['response']['code'] == 200 ) {
					$i=0;
					foreach ( $body->members as $member ) {
						if($member->status!='pending'){
							$subscriber[$i]['email_address'] = $member->email_address;
							$subscriber[$i]['first_name'] = $member->merge_fields->FNAME;
							$subscriber[$i]['last_name'] = $member->merge_fields->LNAME;
							$subscriber[$i]['contact_rating'] = $member->member_rating;
							$subscriber[$i]['date_added'] = date_format(date_create($member->timestamp_opt),"m/d/Y h:i A");
							$subscriber[$i]['last_changed'] = date_format(date_create($member->last_changed),"m/d/Y h:i A");
							$i++;
						}
						
					}
				}
		 
			endfor;
		 
		endif;
		usort($subscriber,function ($a,$b){
		    $t1 = strtotime($a['date_added']);
		    $t2 = strtotime($b['date_added']);
		    if ($t1 == $t2) {
		        return 0;
		    }
		    return ($t1 < $t2) ? -1 : 1;
		});
		/*echo '<pre>';
		print_r($subscriber);
		echo '</pre>';*/
		?>
		
		
		<div class="wrap">
			<h1 class="wp-heading-inline">Mailchimp Subscribers</h1>
			<hr class="wp-header-end">
			<br class="clear">
			<h2 class="screen-reader-text">Subcribers List</h2>
			<table class="wp-list-table widefat fixed striped posts">
				<thead>
				<tr>
					<th scope="col" id="name" class="manage-column column-name column-email column-primary">
						<span>Email</span>
					</th>
					<th scope="col" id="first_name" class="manage-column column-first_name">
						<span>First Name</span>
					</th>
					<th scope="col" id="last_name" class="manage-column column-last_name">
						<span>Last Name</span>
					</th>
					<th scope="col" id="contact_rating" class="manage-column column-contact_rating">
						<span>Contact Rating</span>
					</th>
					<th scope="col" id="date_added" class="manage-column column-date_added">
						<span>Date Added</span>
					</th>
					<th scope="col" id="last_changed" class="manage-column column-last_changed">
						<span>Last Changed</span>
					</th>
				</tr>
				</thead>
				<tbody>
					<?php if(!empty($subscriber)):?>
						<?php foreach($subscriber as $subs):?>
						<tr>
							<td><?php echo $subs['email_address'];?></td>
							<td><?php echo $subs['first_name'];?></td>
							<td><?php echo $subs['last_name'];?></td>
							<td><?php echo $subs['contact_rating'];?></td>
							<td><?php echo $subs['date_added'];?></td>
							<td><?php echo $subs['last_changed'];?></td>
						</tr>
						<?php endforeach;?>
					<?php endif;?>
				</tbody>
			</table>
		</div>
	<?php
}
add_filter( 'get_comment_author_link', 'jetcommerce_remove_comment_author_link', 10, 3 );
function jetcommerce_remove_comment_author_link( $return, $author, $comment_ID ) {
return $author;
}
function mytheme_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
   	<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
	 <footer class="comment-meta">
    
      <div class="comment-author vcard">
         <?php echo get_avatar($comment,$size='42'); ?>

         <?php printf(__('<b class="fn">%s</b> <span class="says">says:</span>'), get_comment_author_link()) ?>
      </div>
      <?php if ($comment->comment_approved == '0') : ?>
         <em><?php _e('Your comment is awaiting moderation.') ?></em>
         <br />
      <?php endif; ?>

      <div class="comment-meta commentmetadata"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?><?php edit_comment_link(__('(Edit)'),'  ','') ?></div>

      <?php comment_text() ?>

      <div class="reply">
         <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
      </div>
     </footer>
    </article>
    </li>
<?php
        }

add_filter( 'template_include', 'so_25789472_template_include' );

function so_25789472_template_include( $template ) {
  if ( is_singular('product') && (has_term( 'gift', 'product_cat')) ) {
    $template = get_stylesheet_directory() . '/woocommerce/single-product-gift.php';
  } 
  return $template;
}
add_action('wp_head', 'site_address_assign', 1);
function site_address_assign(){
	echo '<script>site_url="'.site_url('/').'"</script>';
	global $woocommerce;
	$cart_url = $woocommerce->cart->get_cart_url();
	echo '<script>cart_url="'.$cart_url.'"</script>';
}
add_action( 'wp_ajax_gift_card_checkout_update', 'gift_card_checkout_update' );
add_action( 'wp_ajax_nopriv_gift_card_checkout_update', 'gift_card_checkout_update' );
function gift_card_checkout_update(){
	$currency_symbol = get_woocommerce_currency_symbol();
    $cart_total = WC()->cart->subtotal;
	$gift_cards = WC()->session->get( 'applied_gift_cards', array() );
	if( isset($gift_cards) && $gift_cards ):
	?>
	<div class="checkout_gift_cards_container">
    <span class="pull-left">Gift Card: </span>
    <span class="pull-right giftamount"><?php
    foreach ( $gift_cards as $card ) :
		$gift = get_page_by_title( $card, '', 'gift_card' );
		$gift_price = get_post_meta($gift->ID, '_ywgc_amount_total', true);
    ?>
    <?php echo $card;?> - <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"><?php echo $currency_symbol;?></span><?php echo $gift_price>$cart_total?$cart_total:$gift_price;?></span> 
    <a href="<?php echo WC()->cart->get_cart_url().'?remove_gift_card_code='.$card;?>" class="ywgc-remove-gift-card pull-right" data-gift-card-code="<?php echo $card;?>">[Remove]</a>
    </br>
    <?php
    endforeach;
     ?></span>
    </div>
<?php
	exit;
	else:
	die('no');
	endif;
}
add_action( 'wp_ajax_fetch_variation_id', 'fetch_variation_id' );
add_action( 'wp_ajax_nopriv_fetch_variation_id', 'fetch_variation_id' );

function fetch_variation_id(){
	foreach($_POST as $key=>$value){
		if($key!='action' && $key!='product_id'){
			$data[$key]=$value;
		}
	}
	$product_id = $_POST['product_id'];
	$product = wc_get_product( $product_id );
	$variations = $product->get_available_variations();
	$variation_id = false;
	$in_stock = false;
	if(!empty($variations)):
		foreach($variations as $key=>$var):
			if($var['attributes']==$data){
				$variation_id = $var['variation_id'];
				$in_stock = $var['is_in_stock'];
			}
			if(isset($variation_id) && $variation_id){
				break;
			}
		endforeach;
	endif;
	$variation_arr = array(
		'variation_id'	=> $variation_id,
		'in_stock'		=>	$in_stock
	);
	echo json_encode($variation_arr);
	die();
}

add_action( 'wp_ajax_mc_subscribe', 'mc_subscribe' );
add_action( 'wp_ajax_nopriv_mc_subscribe', 'mc_subscribe' );
function mc_subscribe() {
	$apiKey = '7573b65799d49737321f85bda7326aae-us4';
    $listId = '6ce5e63d91';

    $memberId = md5(strtolower($data['email']));
    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
    $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;

    $json = json_encode([
        'email_address' => $_POST['email'],
        'status'        => "pending", // "subscribed","unsubscribed","cleaned","pending"
    ]);

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                                                                 

    $result = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

   	echo $httpCode;
	die();
}

/*customer post type*/
function customer_post_type() {
    $labels = array(
        'name'                => __( 'Customer' ),
        'singular_name'       => __( 'Customer' ),
        'menu_name'           => __( 'Customer'),
        'parent_item_colon'   => __( 'Parent Customer'),
        'all_items'           => __( 'All Customer' ),
        'view_item'           => __( 'View Customer' ),
        'add_new_item'        => __( 'Add New Customer' ),
        'add_new'             => __( 'Add New' ),
        'edit_item'           => __( 'Edit Customer' ),
        'update_item'         => __( 'Update Customer' ),
        'search_items'        => __( 'Search Customer' ),
        'not_found'           => __( 'Not Found' ),
        'not_found_in_trash'  => __( 'Not found in Trash' )
    );

    $args = array(
        'label'               => __( 'Customer'),
        'description'         => __( 'Customers description' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields','post-formats','page-attributes','trackbacks'),
        'taxonomies'          => array( 'genres' ),  
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 30,
        'menu_icon'           => 'dashicons-calendar-alt',
        'can_export'          => true,
        'has_archive'         => true,
        'rewrite' => array('slug' => 'customer'),
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post'
    );
    register_post_type( 'customer', $args );
}
add_action( 'init', 'customer_post_type', 0 );

add_filter('woocommerce_save_account_details_required_fields', 'wc_save_account_details_required_fields' );
function wc_save_account_details_required_fields( $required_fields ){
    unset( $required_fields['account_display_name'] );
    return $required_fields;
}

function wpb_woo_my_account_order() {
	$myorder = array(
		'dashboard'       => __( 'Account Information', 'woocommerce' ),
		'orders'          => __( 'My Orders', 'woocommerce' ),
		'gift-cards'      => __( 'Gift Cards', 'woocommerce' ),
		'subscriptions'   => __( 'Omega Subscriptions', 'woocommerce' ),
		'payment-methods' => __( 'Payment Methods', 'woocommerce' ),
		'customer-logout' => __( 'Sign Out', 'woocommerce' ),
	);

	return $myorder;
}
add_filter ( 'woocommerce_account_menu_items', 'wpb_woo_my_account_order' );