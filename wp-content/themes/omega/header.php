<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="Jm9jmgfZ1CxqV4xat84cNsf6CcRi9gHwN5Sb9G1pN5s" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<!-- favicon -->
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon">
	<?php wp_head(); 
	global $wp_scripts;
	?>

    <!-- css link -->

    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>    
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css" />
    <?php if(is_page_template( 'template-new-product.php' )):?>
    <link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'  href='<?php echo site_url('/');?>wp-content/plugins/woocommerce/assets/css/prettyPhoto.css?x11860' type='text/css' media='all' />
    <?php endif;?>
    <link href="<?php echo get_template_directory_uri(); ?>/css/fonts.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/custom.css?ver=1.1.0" rel="stylesheet" type="text/css" />
    <link href="<?php echo get_template_directory_uri(); ?>/css/developer.css" rel="stylesheet" type="text/css" /> 
    <link href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" rel="stylesheet" type="text/css" />   

    <!-- js link -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.12.0.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.placeholder.1.3.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/enscroll-0.6.0.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js" type="text/javascript"></script> 
    <!-- custom js -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/custom.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/developer.js" type="text/javascript"></script>

   
    </head>

<body <?php body_class('ldd'); ?>>
    <div class="overlay"></div>
<!-- header part starts-->
    <header class="header header-rel">
        <div class="container-fluid">
            <div class="navbar-absolute">
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'topright',
                            'menu_class'    => 'nav-list menu-right clearfix',
                            'container' => ''
                            
                         ) );
                    ?>
            </div>
        </div>




        <div class="container-fluid nav-fluid">
            <div class="navbar-header">
                <div class="nav_icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
              <?php if ( get_header_image() ) : ?>						
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="navbar-brand">
					<img src="<?php header_image(); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
				</a>		

			<?php endif; // End header image check. ?>

            </div>
            <div class="navbar-collapse main_nav_outer" id="bs-example-navbar-collapse-1">
            	 
            	<?php
                    wp_nav_menu( array(
                        'theme_location' => 'primary',
                        'menu_class'     => 'nav-list nav-left',
                        'container'      => '',
                        'depth'          => 2,
                        'walker'         => new wp_bootstrap_navwalker()                        
                     ) );
                ?>
           
            </div>
        </div>
<?php if (is_home() || is_page('omega3-innovations-customer-testimonials') || is_page('videos')) { ?> 

<div class="blogTopLinks">
        <div class="container">
            <?php
                        wp_nav_menu( array(
                            'theme_location' => 'blogsubmenu',
                          
                            
                         ) );
                    ?>
        </div>
    </div>
<?php } else if( is_singular('post') ||  is_archive()) { ?>
<div class="blogTopLinks">
        <div class="container">
              <?php
                        wp_nav_menu( array(
                            'theme_location' => 'blogsubmenu',
                          
                            
                         ) );
                    ?>
        </div>
    </div>
 <?php } else { } ?>


    </header>
    <?php if(is_cart()){  ?>
    <section class="shopping heading-marg">
        <div class="container">
    <?php } ?>
    <?php
    if(isset($_POST['login']) && !empty($_POST['login'])){
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#order4').trigger('click').delay(1000);
            });
        </script>
        <?php
    }
?>