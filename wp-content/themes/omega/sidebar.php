<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<?php if ( is_active_sidebar( 'sidebar-1' )  ) : ?>
	<aside id="secondary" class="sidebar widget-area" role="complementary">
		 <section id="categories-2" class="widget widget_categories">
    <h2 class="widget-title">Popular Posts</h2>
    <ul>
    <?php
    query_posts('post_type=post&meta_key=post_views_count&orderby=meta_value_num&posts_per_page=5');
    if (have_posts()) :
        while (have_posts()) : 
            the_post(); 
            ?>
            <li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>      
    <?php
        endwhile; 
    endif;
    wp_reset_query();
    ?> 
    </ul>
    </section>
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
		
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>
