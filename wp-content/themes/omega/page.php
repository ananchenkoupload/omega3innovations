<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<section class="about-us">
    <div class="about-oil-detail default-page detail_page container">
        <div class="container">
    		<div class="row">
                <div class="col-sm-12">
                    <?php
                    // Start the loop.
                    while ( have_posts() ) : the_post();

                        // Include the page content template.
                        get_template_part( 'template-parts/content', 'page' );

                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) {
                            comments_template();
                        }

                        // End of the loop.
                    endwhile;
                    ?>
                </div>
                <!-- <div class="col-sm-3">
                    <div class="menu">
                         <?php
                                wp_nav_menu( array(
                                    'theme_location' => 'pagemenu',   
                                    'menu_class'     => 'custom_class', 

                                 ) );
                            ?>
                     </div>
                </div> -->
            </div>
        </div>
	</div>
</section>

<?php get_footer(); ?>