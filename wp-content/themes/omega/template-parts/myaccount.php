<?php
/**
 * Template Name: My Account Page
 *
 */
get_header(); ?>
<section class="acount heading-marg">
    <div class="container">
        <h2 class="text-center"><?php echo get_the_title(); ?></h2>
         <?php wc_print_notices(); ?>
        <div class="account">
            <?php //echo do_shortcode('[woocommerce_my_account]'); 
            while ( have_posts() ) : the_post();
           	the_content();
           	endwhile;
			?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
