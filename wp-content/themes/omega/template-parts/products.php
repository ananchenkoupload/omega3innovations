<?php
/**
 * Template Name: Products page
 */

get_header(); ?>

 <!-- header next -->
 <section class="text-center header_next heading-marg_ext">
     <h2><?php the_field('free_shipping_title'); ?><!-- Enjoy Free Shipping <span>on orders above $110</span> --></h2>
 </section>
 
 <!-- header next -->
 <section class="subscription">
     <div class="section_head text-center">
         <div class="container">
             <h1><?php $product_categories = get_term_by( 'slug', 'omega-cure', 'product_cat'); echo $product_categories->name; ?></h1>
             <p><?php echo $product_categories->description; ?></p>
         </div>
     </div>
     <div class="container">
         <div class="row d-flex justify-content-center flex-wrap">
           <?php $i=1; $args = array(
                    'post_type' =>'product',
                    'showposts' => -1,
                    'tax_query' => array( 
                        array( 
                                'taxonomy'  => 'product_cat', 
                                'field' => 'slug',
                                'terms'  =>'omega-cure'
                                )
                        )
                    );
				    $loop = new WP_Query( $args );while ( $loop->have_posts() ) : $loop->the_post();  global $product; ?>
             <div class="col-xs-12 col-sm-6 col-md-4">
                 <div class="product_inner text-center">
                 	 <?php if(get_field('with_melatonin')) {?>
                        <div class="batch_single">With Melatonin</div>
                        <?php } ?>
                     
                     <div class="img_product_outer">
                       <a href="<?php the_permalink(); ?>"> <img src="<?php $thumb_image=wp_get_attachment_url(get_post_thumbnail_id($loop->ID));  echo $thumb_image; ?>" alt="product" /></a>
                     </div>
                     <div class="product_des">
                     <div class="product_head">
                     	
                         <h2><?php the_title(); ?></h2>
                         <div class="price_pro">
                          <?php echo get_woocommerce_currency_symbol().$product->get_display_price(); ?>
                           <span> <?php echo get_field('pricing_area'); ?></span>
                         </div>
                     </div>
                      <?php echo get_field('short_content'); ?>
                    <?php
                        $product = new WC_Product($product->id);
                        $upsells = $product->get_upsells();
                        if(!empty($upsells)){
                            $thisproduct = wc_get_product( $upsells[0] );
                        ?>
                        <a href="<?php echo get_permalink($upsells[0]); ?>" class="btn btn-product">
                            
                            <?php
                            $per_unit = get_field('units_in_subscription');
                            $sub_price = $thisproduct->get_price();
                                    if(!empty($per_unit) && ($per_unit > 0)){
                                        $sub_price = ($sub_price/$per_unit);
                                        $sub_price = number_format((float)$sub_price, 2, '.', '');
                                    }
                            ?>
                            <?php echo 'Subscription available at: '.get_woocommerce_currency_symbol().$sub_price; ?>
                        </a>        
                        <?php } ?>
                 </div>
                 </div>
             </div>
             <?php $i++; endwhile; wp_reset_postdata(); ?>
             
         </div>
     </div>

     <div class="section_head text-center">
         <div class="container">
              <h2><?php $product_categories1 = get_term_by( 'slug', 'cookiesmore', 'product_cat'); echo $product_categories1->name; ?></h2>
             <p><?php echo $product_categories1->description; ?></p>
         </div>
     </div>

    <div class="container">
         <div class="row d-flex justify-content-center flex-wrap">
         
            <?php $i=1; $args1 = array(
                'post_type' =>'product',
                'showposts' => -1,
                'tax_query' => array( 
                    array( 
                            'taxonomy'  => 'product_cat', 
                            'field' => 'slug',
                            'terms'  =>'cookiesmore'
                            )
                    )
                );
			$loop = new WP_Query( $args1 );while ( $loop->have_posts() ) : $loop->the_post();  global $product; ?>
             <div class="col-xs-12 col-sm-6 col-md-4">
                 <div class="product_inner text-center">
                <?php if(get_field('with_melatonin')) {?>
                        <div class="batch_single">With Melatonin</div>
                        <?php } ?>
                     <div class="img_product_outer">
                       <a href="<?php the_permalink(); ?>">  <img src="<?php $thumb_image=wp_get_attachment_url(get_post_thumbnail_id($loop->ID));  echo $thumb_image; ?>" alt="product" /></a>
                     </div>
                     <div class="product_des">
                     <div class="product_head">
                        <h2><?php the_title(); ?></h2>
                         <div class="price_pro">
                         <?php echo get_woocommerce_currency_symbol().$product->get_display_price(); ?>
                          <span> <?php echo get_field('pricing_area'); ?></span>
                         </div>
                     </div>
                      <?php echo get_field('short_content'); ?>
                      <?php
                        $product = new WC_Product($product->id);
                        $upsells = $product->get_upsells();
                        if(!empty($upsells)){
                            $thisproduct = wc_get_product( $upsells[0] );
                        ?>
                        <a href="<?php echo get_permalink($upsells[0]); ?>" class="btn btn-product">
                           
                            <?php
                            $per_unit = get_field('units_in_subscription');
                            $sub_price = $thisproduct->get_price();
                                    if(!empty($per_unit) && ($per_unit > 0)){
                                        $sub_price = ($sub_price/$per_unit);
                                        $sub_price = number_format((float)$sub_price, 2, '.', '');
                                    }
                            ?>
                            <?php echo 'Subscription available at: '.get_woocommerce_currency_symbol().$sub_price; ?>
                        </a>        
                        <?php } ?>
                 </div>
                 </div>
             </div>
              <?php $i++; endwhile; wp_reset_postdata(); ?>
             
         </div>
     </div>


     <section class="story_section">
    <div class="container">
    	<?php $query = new WP_Query( array( 'pagename' => 'our-story' ) );
		        while ( $query->have_posts() ) {
		        $query->the_post();  ?>
        <div class="row">
            <div class="col-sm-7 pull-right center_align">
                <div class="story_cont">
                    <h2><?php the_title(); ?></h2>
                    <p>
                       <?php echo wp_trim_words(get_the_content(),40,'...'); ?> 
                        <a href="<?php echo esc_url(home_url('/about-us')) ?>">Read more</a>
                    </p>
                </div>
            </div>
            <div class="col-sm-5 text-center">
               <?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');?>
    				<img src="<?php echo $large_image_url[0]; ?>"alt="" />
            </div>
        </div>
        <?php   }
		    wp_reset_postdata();
		?>
    </div>
</section> 
</section>

<?php get_footer(); ?>
