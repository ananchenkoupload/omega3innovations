<?php
/**
 * Template Name: Phone Order
 */

get_header(); ?>
<script type="text/javascript">
    $(document).ready(function(){
        delete yith_wcms["dom"];
    });
</script>
<section class="about-us phone-order">
    <div class="about-oil-detail default-page detail_page container">
        <div class="container">
    		<div class="row">
                <div class="col-sm-12">
                    <?php
                    // Start the loop.
                    while ( have_posts() ) : the_post();

                        // Include the page content template.
                        get_template_part( 'template-parts/content', 'page' );

                        // If comments are open or we have at least one comment, load up the comment template.
                        // if ( comments_open() || get_comments_number() ) {
                        //     comments_template();
                        // }

                        // End of the loop.
                    endwhile;
                    ?>
                </div>
            </div>
        </div>
	</div>
</section>

<?php get_footer(); ?>