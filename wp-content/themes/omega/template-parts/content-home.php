<?php
/**
 * Template Name: Home Page
 *
 */

get_header(); ?>
<!--banner part start-->
<?php 					                      	
if( have_rows('home_banner_details') ): while ( have_rows('home_banner_details') ) : the_row();
	if( get_row_layout() == 'home_banner_details' ): ?>	
        <section class="banner heading-marg">
          <figure>
              <img src="<?php echo get_sub_field('banner_image'); ?>" >
          </figure>
            <div class="banner-text">
                <div class="container">
                    <div class="banner-cont">
                        <h1><?php echo get_sub_field('banner_content',false,false); ?></h1>       
                        <a href="<?php echo get_sub_field('explore_product_link'); ?>" class="btn btn-default">Explore products</a>
                    </div>
                </div>
            </div>
            <div class="text_quote">
                <p><?php the_field('free_shipping_text'); ?><!-- <span>Enjoy Free Shipping</span>  on all orders above $11000 --></p>
            </div>
        </section>
<?php endif;endwhile;endif; ?>
<!--banner part close-->

<!--feature part starts-->
<section class="feature">
    <div class="container">
        <h3><?php echo get_field('featured_in_title')?></h3>
        <ul>
        	
			 <?php while (have_posts()) : the_post(); 		                      	
	         if( have_rows('featured_in') ): while ( have_rows('featured_in') ) : the_row();
			 if( get_row_layout() == 'featured_in' ):?>	
            <li><a href="<?php if(get_sub_field('link')) {?><?php echo get_sub_field('link'); } else {?>javascript:void()<?php } ?>" target="_blank"> <img src="<?php echo get_sub_field('image'); ?>" alt="fea-1"></a></li>
            <?php   
			endif;
			endwhile;
			endif;
			endwhile;
		    ?>
        </ul>
    </div>
</section>
<!--feature part closed-->
<!--different_oilArea starts-->
<section class="different_oilArea">
    <div class="container">
        <div class="omega_hdng">
            <h2><?php the_field('different_oil_section_heading'); ?></h2>
        </div>
        <div class="bottle_outerBx">
            <div class="bottle_pic">
                <img src="<?php the_field('oil_bottle_image'); ?>" />
            </div>
            <div class="bottle_text">
            	<?php 
            	$oil_ary=array('one','two','three','four','five','six','seven'); $d=0;
            	while(the_repeater_field('oil_features')): ?>
	                <div class="oil_bttlText oil_<?php echo $oil_ary[$d]; ?>">
	                    <h4><?php the_sub_field('feature'); ?></h4>
	                </div>
	            <?php $d++; endwhile; ?>
            </div>
            <div class="more_btn">
                <a href="<?php echo esc_url(home_url('/about-our-oil/')); ?>" class="btn btn-default"><?php the_field('learn_more_oil_button_text'); ?></a>
            </div>
        </div>
    </div>
<!--different_oilArea close-->
</section>
<!--feature product part starts-->
<section class="feature-product redesignHome">
    <div class="container">
        <div class="ftShow">
            <div class="row feature-cont">      
                <?php 
                    $meta_query   = WC()->query->get_meta_query();
                    $meta_query[] = array(	'key'   => '_featured','value' => 'yes');
                    $args = array('post_type' =>'product','showposts' => -1,'order' =>'ASC','meta_query' => $meta_query );
                    $loop = new WP_Query( $args );while ( $loop->have_posts() ) : $loop->the_post();?>	
                <div class="col-md-4 col-sm-6 col-xs-6 feature-cont-col">
                    <div class="product-box">
                        <a href="<?php the_permalink($loop->ID); ?>" class="box-pic">
                            <figure>
                                <img src="<?php $thumb_image=wp_get_attachment_url(get_post_thumbnail_id($loop->ID));  echo $thumb_image; ?>" alt="product" /> 
                            </figure>
                        </a>
                        <div class="shortPd">
                            <h4><a href="<?php the_permalink($loop->ID); ?>"><?php the_title(); ?></a> </h4>
                            
                            <h5>
                            <?php 
                                $sub_price = $product->get_price();
                                $sub_price = number_format((float)$sub_price, 2, '.', '');
                                echo get_woocommerce_currency_symbol().$sub_price; 
                            ?> 
                            </h5>

                            <a href="<?php the_permalink(); ?>" class="btn btn-default">
                                Add to cart
                            </a>
                        </div>
                    </div>
                </div>
                
            <?php endwhile; 
            wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</section>
<!--feature product part closed-->

<!-- meet steps Show -->
<section class="meetShowtxt">
    <div class="container">
        <div class="inMeet">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-6">
                    <h2><?php the_field('steps_heading'); ?></h2>
                    <?php the_field('steps_description'); ?>

                    <a href="<?php echo (get_field('steps_button_link')) ? get_field('steps_button_link') : 'javascript:void(0)'; ?>" class="btn btn-default">
                        <?php the_field('steps_button_text'); ?>
                    </a>
                </div>
            </div>
        </div>
        <figure class="showMobileDev">
            <img src="/wp-content/uploads/2019/01/omega-good-sleep.jpg" alt="">
        </figure>
    </div>
</section>
<!-- meet steps Adv -->

<!-- testimonial blocks -->
<section class="testimonialBlocks">
    <div class="container">
        <h2 class="text-center"><?php the_field('customer_section_heading'); ?></h2>

        <div class="testimonialShow">
            <div class="row">
            	<?php
				    $cstmr_query = new WP_Query( array( 'post_type' => 'customer','order' => 'ASC','posts_per_page' => -1,'post_status' => 'publish'  ) );
				        if ( $cstmr_query->have_posts() ) {
				            while ( $cstmr_query->have_posts()) {
					            $cstmr_query->the_post();           
				?> 
	                <div class="col-md-4 col-xs-6">
	                    <div class="testimonial-block-e">
	                        <div class="rateMe">
	                            <!-- <img src="<?php echo get_template_directory_uri(); ?>/images/stars.png" alt="" /> -->
                                <?php
                                for($k=1;$k<=get_field('rating');$k++) { ?>
                                    <span><img src="<?php echo get_template_directory_uri(); ?>/images/star_new.png" alt="" /></span>
                                <?php } ?>
                                <!-- <span><img src="<?php echo get_template_directory_uri(); ?>/images/star_new.png" alt="" /></span>
                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/star_new.png" alt="" /></span>
                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/star_new.png" alt="" /></span>
                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/star_new.png" alt="" /></span> -->
	                        </div>
	                        <div class="rateTxt">
	                            <p>
	                                &quot;<?php echo get_the_content(); ?>&quot;
	                            </p>
	                        </div>
	                        <div class="nameT">
	                            <strong><?php the_title(); ?></strong>, <?php echo get_the_excerpt(); ?>
	                        </div>
	                    </div>
	                </div>
	            <?php } wp_reset_postdata(); } ?>
            </div>
        </div>
    </div>
</section>
<!-- testimonial blocks -->

<!--blog-section-start-->
<section class="blog_section redesignHome">
    <div class="container">
    	<?php //$query1 = new WP_Query( array( 'pagename' => 'blog' ) );
		        //while ( $query1->have_posts() ) {
		       // $query1->the_post();
		      
		  ?>
        <div class="blog_heading">
            <h2><?php echo get_field('home_blog_title',145); ?></h2>
           <?php echo get_field('home_blog_content',145); ?>
        </div>
         <?php  // }
		   // wp_reset_postdata();
		?>
        <div class="row blog_row">
            <?php query_posts('showposts=3');
		while (have_posts()) : the_post();  ?>
            <div class="col-lg-4 col-md-4 col-xs-6">                
                <?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full'); ?>                
                <a href="<?php the_permalink(); ?>" class="blog_home blogImg" style="background-image: url('<?php if ( !empty($large_image_url[0]) ) { echo $large_image_url[0]; }else{
                    echo get_template_directory_uri()."/images/no-image.png";  } ?>');">
                </a>
                <div class="blog_like_cell clearfix">
                    <div class="date_cell"><?php echo get_the_time('M d,  Y'); ?></div>
                    <ul class="like_cell">
                         <li><span><img src="<?php echo get_template_directory_uri(); ?>/images/eye_icon.png" alt="" /></span> <span><?php echo pvc_get_post_views( get_the_ID() );?></span></li>
                         <li><?php if( function_exists('zilla_likes') ) zilla_likes(); ?></li>
                        <li><a href="<?php the_permalink(); ?>"><span><img src="<?php echo get_template_directory_uri(); ?>/images/chat_icon.png" alt="" /></span><span> <?php if(get_comments_number(get_the_ID())){echo $vart=get_comments_number(get_the_ID()); } else { echo $vart='0'; } ?> </span></a></li>
                    </ul>
                </div>
                <div class="blog_cont">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </h3>
                    <p>
                        <?php echo wp_trim_words(get_the_content(),17,'...'); ?>  <a href="<?php the_permalink(); ?>">Read full article</a>
                    </p>
                </div>
            </div>
            <?php endwhile; 
            wp_reset_query();?>
        </div>
        <div class="text-center"><a class="btn btn-default visit_btn" href="<?php echo esc_url(home_url('/blog')); ?>">Visit our Blog</a></div>
    </div>
</section>
<section class="story_section redesignHome">
    <div class="container">
    	<?php $query = new WP_Query( array( 'pagename' => 'our-story' ) );
		        while ( $query->have_posts() ) {
		        $query->the_post();
		      
		  ?>
        <div class="row">
            <div class="col-sm-6 pull-right center_align">
                <div class="story_cont">
                    <h2><?php the_title(); ?></h2>
                    <p>
                       <?php echo wp_trim_words(get_the_content(),40,'...'); ?> 
                       <a href="<?php echo esc_url(home_url('/about-us')) ?>">Read more</a>
                    </p>
                </div>
            </div>
            <div class="col-sm-6 text-center">
               <?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');?>
    				<img src="<?php echo $large_image_url[0]; ?>"alt="" />
            </div>
        </div>
        <?php   }
		    wp_reset_postdata();
		?>
    </div>
</section> 
<!--blog-section-end-->
<!--story-section-start-->

<?php get_footer(); ?>
