<?php
/**
 * Template Name: Ourteam page
 *
 */

get_header(); ?>
 <section class="blog-detail heading-marg">   
    <div class="container">
    	<h2 class="our-team-new"><?php the_title(); ?></h2>
        <div class="row blog-containt">
            <div class="col-sm-12"> 
            	<?php while ( have_posts() ) : the_post();          
		  if( have_rows('our_team') ): while ( have_rows('our_team') ) : the_row();
			 if( get_row_layout() == 'our_team' ):?> 
        	  
            <div class="blog-left">
                <div class="blog-box blog-listing clearfix">   
                	<div class="blog-list-pic">    
                    <img src="<?php echo get_sub_field('image');?>" alt="blog">
                    </div> 
                    <div class="blog-list-text">
                       
                        <h3><?php echo get_sub_field('title');?></h3>
                      <?php echo get_sub_field('content');?>                  
                    </div>
                </div>
            </div>           
          <?php endif;
				endwhile;
				endif;
             endwhile; ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
