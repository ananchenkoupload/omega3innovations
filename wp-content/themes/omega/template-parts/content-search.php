<?php
/**
 * The template part for displaying results in search pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

    <div class="blog-left">
                <div class="blog-box blog-listing clearfix">
                    <a href="<?php the_permalink(); ?>" class="blog-list-pic">
                    <?php $thumb_image=wp_get_attachment_url(get_post_thumbnail_id());   ?>
                    <img src="<?php echo $thumb_image; ?>" alt="blog"></a>
                    <div class="blog-list-text">
                        <div class="blog-news-date">
                            <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo get_the_author_meta( 'display_name',$post->post_author); ?></a>    <?php echo get_the_time('M j,Y'); ?>
                        </div>
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <p><?php echo wp_trim_words( get_the_content(), 40, '...' ); ?><a href="<?php the_permalink(); ?>">Read full article</a></p>
                        <div class="customer-social list-social">
                            <ul>
                                <li><a href="<?php the_permalink(); ?>"><i class="fa fa-commenting-o" aria-hidden="true"></i> <?php if(get_comments_number(get_the_ID())){echo $vart=get_comments_number(get_the_ID()); } else { echo $vart='0'; } ?></a></li>
                                <li><?php if( function_exists('zilla_likes') ) zilla_likes(); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
