<?php
/**
 * Template Name: Contact page
 *
 */

get_header(); ?>
 <section class="product-detail heading-marg">  
    <div class="container">
        <h1><?php the_title(); ?></h1>
        <div class="contactClass">
            <div class="row contactTop">
                <div class="col-sm-4">
                    <div class="contactBox">
                      <?php echo get_field('contact_box1'); ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="contactBox">
                       <?php echo get_field('contact_box2'); ?>
                       
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="contactBox">
                         <?php echo get_field('contactbox3'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="account checkout-ship">
                        <?php echo do_shortcode('[contact-form-7 id="2597" title="Contact"]'); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3551.321008400272!2d-82.38260138535883!3d27.114697383041037!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88c3507b77da8aed%3A0x46b914f322f7e7bb!2s727+Commerce+Dr%2C+Venice%2C+FL+34292%2C+USA!5e0!3m2!1sen!2sin!4v1481522269288" width="550" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>           
    </div>   
</section> 

<?php get_footer(); ?>
