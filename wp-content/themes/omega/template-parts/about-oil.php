<?php
/**
 * Template Name: About oil
 *
 */

get_header(); ?>
<section class="about-oil heading-marg">
    <div class="about-oil-detail abt-dt text-center">
        <div class="container">
            <h1><?php echo get_field('about_title'); ?></h1>
            <h3><?php echo get_field('about_sub_title'); ?></h3>
            <?php 
                while (have_posts()) : the_post();  
            	   the_content(); 
            	endwhile; 
            ?>   
        </div>
    </div>
    <div class="oil-market">
        <div class="container">
            <h2 class="text-center"><?php echo get_field('freshest_omega-3_oil_title'); ?></h2>
            <div class="row">
                <div class="col-sm-6">
                    <div class="market-pic"><img src="<?php echo get_field('freshest_omega-3_oil_image'); ?>" alt="market" /></div>
                </div>
                <div class="col-sm-6">
                    <div class="market-text">
                        <h4><?php echo get_field('freshest_omega-3_oil_subtitle'); ?></h4>
                        <?php echo get_field('freshest_omega-3_oil_first_content'); ?>
                    </div>
                </div>
                <div class="col-sm-6 pull-right">
                    <div class="market-table-outer">
                        <div class="market-table">
                            <div class="market-row table-head">
                                <div class="market-cell"><?php echo get_field('analysis'); ?></div>
                                <div class="market-cell"><?php echo get_field('result'); ?></div>
                                <div class="market-cell"><?php echo get_field('unit_title'); ?></div>
                            </div>
                           
					        <?php 	                      	
					        if( have_rows('market_table_details') ): while ( have_rows('market_table_details') ) : the_row();
									if( get_row_layout() == 'market_table' ):	?>	
                                <div class="market-row">
                                    <div class="market-cell"><?php echo get_sub_field('analysis_title'); ?></div>
                                    <div class="market-cell"><?php echo get_sub_field('result_title'); ?></div>
                                    <div class="market-cell"><?php echo get_sub_field('unit'); ?></div>
                                </div>
                           <?php   
							endif;endwhile;endif;							
						   ?>
                        </div>
                    </div> 
                </div>
                <div class="col-sm-6">
                    <div class="market-text">
                        <?php echo get_field('freshest_omega_oil_thirdcontent'); ?>
                    </div>    
                </div>
            </div>
        </div>
    </div>
    <div class="sequance">
    	<?php
    	 $i=0;                      	
        if( have_rows('sequance_details') ): while ( have_rows('sequance_details') ) : the_row();
			if( get_row_layout() == 'sequance' ):	?>	
            <div class="seq-outer">
                <div class="seq-list" style="background-image: url(<?php echo get_sub_field('sequance_image'); ?>);">
                    
                </div>
                <div class="container text-container">
                    <div class="row">
                        <div class="col-sm-6 <?php if($i%2==0){ ?>col-sm-offset-6<?php } $i++;?>">
                            <div class="seq-inner">
                                <h2><?php echo get_sub_field('sequance_title'); ?></h2>
                               <?php echo get_sub_field('sequance_content'); ?>
                                <p>  <?php echo wp_trim_words(get_sub_field('sequance_content_second'),100,'...'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>          
          <?php  endif;endwhile;endif; ?>  
    </div>
</section>
<?php get_footer(); ?>
