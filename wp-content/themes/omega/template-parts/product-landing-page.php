<?php
/**
 * Template Name: Products Landing page
 */

get_header('shop'); ?>

<section class="productMain heading-marg">
    <div class="prodBanner">
    	<?php $terms = get_term_by('slug','gift','product_cat' );
		$taxonomy='product_cat';
		 $idd=$terms->term_id;
    ?>
    
        <div class="container_fluid">
        	 <?php $thumbnail_id = get_woocommerce_term_meta($idd, 'thumbnail_id', true );
	    $image = wp_get_attachment_url( $thumbnail_id ); ?>
	   
            <img src="<?php echo $image; ?>" alt="" />
            
            <div class="bannerP-in">
            	  <h1><?php echo get_field('gift_banner_title_section', $taxonomy . '_' . $idd); ?></h1>
                <p><?php echo $terms->description; ?></p>
            </div>
        </div>
     
    </div>

    <div class="prodBundles">
        <div class="container">
            <h4 class="text-uppercase text-center">Omega Bundles</h4>
            <div class="bundleShow">
            	<?php $args = array(
                                    'post_type' =>'product',
                                    'showposts' => -1,
                                    'tax_query' => array( 
                                                        array( 
                                                                'taxonomy'  => 'product_cat', 
                                                                'field' => 'slug',
                                                                'terms'  =>'gift'
                                                                )
                                                        )
                                    );
				    $loop = new WP_Query( $args );while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="bundle-e">
                    <figure>
                             <a href="<?php the_permalink(); ?>"><img src="<?php $thumb_image=wp_get_attachment_url(get_post_thumbnail_id($loop->ID));  echo $thumb_image; ?>" alt="" /></a>
                    </figure>

                    <div class="infoProd-e-bn">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                         <p><?php //the_content(); 
                        echo get_field('subtitle')?></p>
                        <h6>From <?php echo str_replace("From:","",$product->get_price_html()); ?></h6>
                    </div>
                </div>
                <?php endwhile; ?>
                
        
            </div>
        </div>
    </div>

    <div class="howWorksP">
        <div class="container">
            <h4 class="text-uppercase text-center"><?php echo get_field('how_it_works_title', $taxonomy . '_' . $idd); ?></h4>

            <div class="row">
               <?php while( have_rows('how_it_works_section', $taxonomy . '_' . $idd) ): the_row(); ?> 
                <div class="col-sm-4">
                    <div class="howCell text-center">
                        <div class="howImg" style="background-image: url(<?php echo the_sub_field('image'); ?>);"></div>
                        <h5><?php echo the_sub_field('title'); ?></h5>
                        <?php echo the_sub_field('content'); ?>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>

    <div class="cardGift">
        <div class="container">
        	 <?php while( have_rows('gift_card_options',53754) ): the_row(); ?> 
            <div class="cardIn-Gift">
                <div class="row">
                	<div class="col-sm-6">
                		<a href="<?php echo esc_url(home_url('/gift-card')); ?>">
                		<img src="<?php echo the_sub_field('image',53754); ?>">
                		</a>
                	</div>
                    <div class="col-sm-6">
                        <a href="<?php echo esc_url(home_url('/gift-card')); ?>"><h4>Gift cards</h4></a>
                        <?php echo the_sub_field('content'); ?>

                        <h5>Starts at <?php echo the_sub_field('starting_price'); ?>.</h5>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>

<?php get_footer('shop'); ?>
