<?php
/**
 * Template Name: Product subscription
 *
 */

get_header(); ?>
  <!-- header next -->
 <section class="text-center header_next heading-marg_ext">
     <h2>Enjoy Free Shipping <span>on all subscriptions</span></h2>
 </section>
 <section class="instraction text-center">
     <div class="container">
          <div class="row">
         	<?php  while ( have_rows('product_steps') ) : the_row(); ?>
             <div class="col-sm-4">
                 <div class="instraction_inner">
                 <h2><?php echo get_sub_field('title'); ?></h2>
                 <?php echo get_sub_field('content'); ?>
</div>
             </div>
             <?php endwhile; ?>
          
         </div>
     </div>
 </section>
 <!-- header next -->
 <section class="subscription">
     <div class="section_head text-center">
         <div class="container">
             <h2><?php $product_categories = get_term_by( 'slug', 'for-individuals', 'product_cat'); echo $product_categories->name; ?></h2>
             <p><?php echo $product_categories->description; ?></p>
         </div>
     </div>
     <div class="container">
         <div class="row d-flex justify-content-center flex-wrap">
           <?php $i=1; $args = array(
                                    'post_type' =>'product',
                                    'showposts' => -1,
                                    'tax_query' => array( 
                                                        array( 
                                                                'taxonomy'  => 'product_cat', 
                                                                'field' => 'slug',
                                                                'terms'  =>'for-individuals'
                                                                )
                                                        )
                                    );
				    $loop = new WP_Query( $args );while ( $loop->have_posts() ) : $loop->the_post();  ?>
             <div class="col-xs-12 col-sm-6 col-md-4">
                 <div class="product_inner text-center">
                 	 <?php if(get_field('with_melatonin')) {?>
                        <div class="batch_single">With Melatonin</div>
                        <?php } ?>
                     
                     <div class="img_product_outer">
                         <a href="<?php the_permalink(); ?>"> <img src="<?php $thumb_image=wp_get_attachment_url(get_post_thumbnail_id($loop->ID));  echo $thumb_image; ?>" alt="product" /></a>
                     </div>
                     <div class="product_des">
                     <div class="product_head">
                     	
                         <h2><?php the_title(); ?></h2>
                         <div class="price_pro">
                           <?php echo get_field('pricing_area_for_subscription'); ?>
                           <span> <?php echo get_field('pricing_area'); ?></span>
                         </div>
                     </div>
                      <?php echo get_field('short_content'); ?>
                    
                 </div>
                 </div>
             </div>
             <?php $i++; endwhile; wp_reset_postdata(); ?>
             
         </div>
     </div>

     <div class="section_head text-center">
         <div class="container">
              <h2><?php $product_categories1 = get_term_by( 'slug', 'cure-subscription', 'product_cat'); echo $product_categories1->name; ?></h2>
             <p><?php echo $product_categories1->description; ?></p>
         </div>
     </div>

    <div class="container">
         <div class="row d-flex justify-content-center flex-wrap">
         
            <?php $i=1; $args1 = array(
                                    'post_type' =>'product',
                                    'showposts' => -1,
                                    'tax_query' => array( 
                                                        array( 
                                                                'taxonomy'  => 'product_cat', 
                                                                'field' => 'slug',
                                                                'terms'  =>'cure-subscription'
                                                                )
                                                        )
                                    );
				    $loop = new WP_Query( $args1 );while ( $loop->have_posts() ) : $loop->the_post();  ?>
             <div class="col-xs-12 col-sm-6 col-md-4">
                 <div class="product_inner text-center">
                <?php if(get_field('with_melatonin')) {?>
                        <div class="batch_single">With Melatonin</div>
                        <?php } ?>
                     <div class="img_product_outer">
                       <a href="<?php the_permalink(); ?>"> <img src="<?php $thumb_image=wp_get_attachment_url(get_post_thumbnail_id($loop->ID));  echo $thumb_image; ?>" alt="product" /></a>
                     </div>
                     <div class="product_des">
                     <div class="product_head">
                        <h2><?php the_title(); ?></h2>
                         <div class="price_pro">
                        <?php echo get_field('pricing_area_for_subscription'); ?>
                           <span> <?php echo get_field('pricing_area'); ?></span>
                         </div>
                     </div>
                      <?php echo get_field('short_content'); ?>
                 </div>
                 </div>
             </div>
              <?php $i++; endwhile; wp_reset_postdata(); ?>
             
         </div>
     </div>

     <div class="section_head text-center">
         <div class="container">
              <h2><?php $product_categories1 = get_term_by( 'slug', 'cookies-subscription', 'product_cat'); echo $product_categories1->name; ?></h2>
             <p><?php echo $product_categories1->description; ?></p>
         </div>
     </div>

    <div class="container">
         <div class="row d-flex justify-content-center flex-wrap">
         
            <?php $i=1; $args1 = array(
                                    'post_type' =>'product',
                                    'showposts' => -1,
                                    'tax_query' => array( 
                                                        array( 
                                                                'taxonomy'  => 'product_cat', 
                                                                'field' => 'slug',
                                                                'terms'  =>'cookies-subscription'
                                                                )
                                                        )
                                    );
                    $loop = new WP_Query( $args1 );while ( $loop->have_posts() ) : $loop->the_post();  ?>
             <div class="col-xs-12 col-sm-6 col-md-4">
                 <div class="product_inner text-center">
                <?php if(get_field('with_melatonin')) {?>
                        <div class="batch_single">With Melatonin</div>
                        <?php } ?>
                     <div class="img_product_outer">
                       <a href="<?php the_permalink(); ?>"> <img src="<?php $thumb_image=wp_get_attachment_url(get_post_thumbnail_id($loop->ID));  echo $thumb_image; ?>" alt="product" /></a>
                     </div>
                     <div class="product_des">
                     <div class="product_head">
                        <h2><?php the_title(); ?></h2>
                         <div class="price_pro">
                        <?php echo get_field('pricing_area_for_subscription'); ?>
                           <span> <?php echo get_field('pricing_area'); ?></span>
                         </div>
                     </div>
                      <?php echo get_field('short_content'); ?>
                 </div>
                 </div>
             </div>
              <?php $i++; endwhile; wp_reset_postdata(); ?>
             
         </div>
     </div>


</section>

<?php get_footer(); ?>
