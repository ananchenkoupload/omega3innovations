<?php
/**
 * Template Name: Testimonial page
 *
 */

get_header(); ?>
 <section class="about-us heading-marg">
    <div class="about-oil-detail text-center">
    
        <div class="container">
            <h1><?php echo get_field('page_title'); ?> </h1>
            <?php echo get_field('page_content'); ?> 
        </div>
      
    </div>
   
        <div class="container">
        		<?php 	$i=0;                      	
	         if( have_rows('testimonial_content') ): while ( have_rows('testimonial_content') ) : the_row();
			 if( get_row_layout() == 'testimonial_content' ):
			 if($i%2==0){?>	
            <div class="testi_row col-sm-6 col-xs-12 inr_con">
                <div class="inr_con_row">
                    <div class="col-xs-12 center_align video_con">
                        <?php echo get_sub_field('embed_video_code');?>
                    </div>
                    <div class="col-xs-12 text_con text-center">
                        <div class="post_meta_name">
                            <a href="<?php echo esc_url(home_url('/omega3-innovations-customer-testimonials')); ?>">Video</a>
                        </div>
                	   <h2><?php echo get_sub_field('title');?></h2>
                        <div class="testi_cont">
                            <span><?php echo get_sub_field('quotation');?></span>
                            <?php echo get_sub_field('content');?>
                        </div>
                    </div>
                </div>
            </div>
            <?php } else{?>
            <div class="testi_row col-sm-6 col-xs-12 inr_con"> 
                <div class="inr_con_row">
                    <div class="col-xs-12 center_align video_con">
                        <?php echo get_sub_field('embed_video_code');?>
                    </div>
                    <div class="col-xs-12 text-center text_con">
                        <div class="post_meta_name">
                            <a href="<?php echo esc_url(home_url('/omega3-innovations-customer-testimonials')); ?>">Video</a>
                        </div>
                	   <h2><?php echo get_sub_field('title');?></h2>
                        <div class="testi_cont">
                            <span><?php echo get_sub_field('quotation');?></span>
                            <?php echo get_sub_field('content');?>
                        </div>
                    </div>
                </div>
            </div>
         <?php   } $i++;?>
             <?php   
			endif;
			endwhile;
			endif;
			
		    ?>
        </div>
   
 </section> 
 
 
 <?php get_footer(); ?>
