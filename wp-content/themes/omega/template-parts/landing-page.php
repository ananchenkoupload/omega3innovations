<?php
/**
 * Template Name: Landing Page
 *
 */
get_header('checkout'); ?>
<section class="checkout heading-marg">
    <div class="container">
        <?php 
            wc_print_notices();
            while ( have_posts() ) : the_post();
            the_content();
            endwhile;
		?>
    </div>
</section>
<?php get_footer('checkout'); ?>
