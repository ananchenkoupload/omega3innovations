<?php
/**
 * Template Name: About Us Revised
 *
 */

get_header(); ?>
 <section class="about-us heading-marg">
    <div class="about-oil-detail text-center">
    	<?php while (have_posts()) : the_post();  ?>
        <div class="container">
            <h1><?php the_title();?> </h1>
            <?php the_content(); ?>
        </div>
        <?php endwhile; ?>
    </div>
    </section>
    <section class="team text-center">
        <div class="container">
            <a href="<?php echo esc_url(home_url('/meet-our-team'))?>"><h2><?php $page = get_page_by_path( 'meet-our-team' ); echo get_the_title( $page ); ?></h2></a>
            <p><?php $uu=get_post($page->ID); echo $uu->post_content; ?></p>
            <div class="team-list">
                <ul>
                    <?php $query = new WP_Query( array( 'pagename' => 'meet-our-team' ) );
		      
		while ($query->have_posts()) : $query->the_post();  
		  if( have_rows('our_team') ): while ( have_rows('our_team') ) : the_row();
			 if( get_row_layout() == 'our_team' ):?>	
                    <li>
                       
                    <div class="team-pic">
			<img src="<?php echo get_sub_field('image');?>" alt="" /></div>
                    <h4><?php echo get_sub_field('title');?></h4>
                 
                    </li>
                    <?php endif;
						endwhile;
						endif;
                     endwhile; ?>
                </ul>
            </div>
        </div>
    </section>
</section>

<?php get_footer(); ?>
