<?php
/**
 * Template Name: Whyomega page
 *
 */

get_header(); ?>
<section class="why-us heading-marg">
    <div class="about-oil-detail abt-dt text-center">
        <div class="container">
            <h2><?php the_title(); ?></h2>
            <h3><?php echo get_field('whyomega_subtitle'); ?></h3>
            <?php echo get_field('whyomega_content'); ?>
        </div>
    </div>
    <div class="why-cont">
        <div class="container">
            <div class="row">
                 <?php  if( have_rows('whyomega_contents') ): while ( have_rows('whyomega_contents') ) : the_row();
		if( get_row_layout() == 'whyomega_contents' ):  ?>
                <div class="why-box">
                  
					<img src="<?php echo get_sub_field('image'); ?>" alt="" />
                   
                    <h3><?php echo get_sub_field('title'); ?></h3>
                    <p><?php echo wp_trim_words(get_sub_field('content'),38,'.'); ?></p>
                </div>
            <?php endif;endwhile;endif; ?>
               
            </div>
        </div>
    </div> 

</section>
<?php get_footer(); ?>
