<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<!-- favicon -->
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon">
	<?php wp_head(); ?>

    <!-- css link -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
    <link href="<?php echo get_template_directory_uri(); ?>/css/fonts.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo get_template_directory_uri(); ?>/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo get_template_directory_uri(); ?>/css/developer.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" rel="stylesheet" type="text/css" />

    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.12.0.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.placeholder.1.3.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/enscroll-0.6.0.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js" type="text/javascript"></script> 
    <!-- custom js -->
    
    <script src="<?php echo get_template_directory_uri(); ?>/js/custom.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/developer.js" type="text/javascript"></script>
    
</head>

<body <?php body_class('ldd'); ?>>
<!-- header part starts-->
    <header class="header header-rel checkout-header-top">
        <div class="container-fluid nav-fluid">
            <div class="navbar-header">
                <div class="nav_icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
              <?php if ( get_header_image() ) : ?>						
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="navbar-brand">
					<img src="<?php header_image(); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
				</a>		

			<?php endif; // End header image check. ?>

            </div>
        </div>
    </header>
    <?php if(is_cart()){  ?>
    <section class="shopping heading-marg">
        <div class="container">
    <?php } ?>
    <?php
    if(isset($_POST['login']) && !empty($_POST['login'])){
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#order4').trigger('click').delay(1000);
            });
        </script>
        <?php
    } ?>
<!--    header part closed-->
