<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<?php if(is_cart()){  ?>
</div></section>
<?php } ?>
   <section class="newsLettrFooter bgGrey">
        <div class="container">
            <header class="pull-left hdrnewslttr">
                <h6>Want More Omega3innovations?</h6>
                <p>Stay up to date with our latest news and products</p>
            </header>
            <div class="pull-left mc-newsltr subscrIn-newsLtr">
            	<form method="post">
	                <input type="email" id="subEml" placeholder="Your email">
	                <button type="submit" class="text-uppercase btn" id="subSign">Sign Up</button>
	                <p></p>
                </form>
            </div>
        </div>
    </section>
   <footer class="footerTop">
    <div class="container">
       <div class="footer_outer">
        <div class="row">
            <div class="col-sm-4">
                <h2>MORE INFORMATION</h2>
                
                <?php wp_nav_menu( array(
		                'menu'              => 'footermenu',
		                'theme_location'    => 'footermenu',
		                'menu_class'        => 'quick_link clearfix',
		               )
		            ); ?>
            </div>
            <div class="col-sm-4">
                <h2><?php echo get_theme_mod( 'contacttitle' ); ?></h2>
                <div class="address_cell">
                    <?php echo get_theme_mod( 'address' ); ?>
                </div>
                <div class="address_cell">
                    <span class="call-phn"></span>
                    <!--<span><?php //echo get_theme_mod( 'phonenumber' ); ?></span>-->
                    <a class="cl-phn" href="callto:1-866-414-0188">1-866-414-0188</a>,
                    <a class="cl-phn" href="callto:1-941-485-4400">1-941-485-4400</a>
                </div>
            </div>
            <div class="col-sm-4">
                <h2>Connect with us</h2>
                <ul class="social_links">
                   <?php if(get_theme_mod( 'facebook' )){ ?> <li><a target="_blank" href="<?php if(get_theme_mod( 'facebook' )){ echo get_theme_mod( 'facebook' ); }else{ echo 'javascript:void(0)'; } ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li><?php } ?>
                   <?php if(get_theme_mod( 'instagram' )){ ?> <li><a target="_blank" href="<?php if(get_theme_mod( 'instagram' )){ echo get_theme_mod( 'instagram' ); }else{ echo 'javascript:void(0)'; } ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li><?php } ?>
                   <?php if(get_theme_mod( 'google_plus' )){ ?><li><a target="_blank" href="<?php if(get_theme_mod( 'google_plus' )){ echo get_theme_mod( 'google_plus' ); }else{ echo 'javascript:void(0)'; } ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li><?php } ?>
                   <li><a target="_blank" href="mailto:abc@abc.com"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
        </div>
    </div>
    
    <div class="footerBottom">
        <div class="container"> 
           <div class="footer-bottom">
            <!--          
            <div class="footer-btm-r">
                <?php //echo get_theme_mod( 'poweredbytitle' ); ?>
                <a target="_blank" href="<?php echo get_theme_mod( 'copylogo-url' ); ?>"><img src="<?php echo get_theme_mod( 'copylogo' ); ?>" alt="footer" /></a>
            </div>
            -->
            <div class="footer-btm-l">
                <?php echo get_theme_mod( 'copyrighttext' ); ?>
            </div>
        </div>
    </div>
    </div>
</footer>
<?php wp_footer(); ?>
<?php if(is_page_template( 'template-new-product.php' )):?>
<script type='text/javascript' src='<?php echo site_url('/');?>wp-content/plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js?x11860'></script>
<script>
	$("a[rel^='prettyPhoto']").prettyPhoto();
</script>
<?php endif;?>
<script>
(function($) {
$(document).ajaxComplete(function() {
setTimeout(
function()
{
$('.wpcf7-mail-sent-ok').slideUp('slow');
$('.wpcf7-not-valid-tip').slideUp('slow');
$('.wpcf7-validation-errors').slideUp('slow');
}, 2000);
});
<?php if(isset($_POST['ywgc-recipient-email'])):?>
$(window).ready(function(){
	setTimeout(function(){
		$('.minicartdisplay').css('display','block');
	},1500);
});
<?php endif;?>

$('#subSign').click(function(e){
	e.preventDefault();
	var email = $('#subEml').val();
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if(!re.test(email)){
		$('.subscrIn-newsLtr p').css('color','red').text('Please enter a valid email.');
	}else{
		var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
		var query_string = 'action=mc_subscribe&email='+email;
		$('#subSign').prop('disabled',true);
		$.ajax({
			url : ajaxurl,
			cache: false,
			type : 'POST',
			data : query_string,
			success : function(results){
				console.log(results);
				$('#subEml').val('');
				$('#subSign').prop('disabled',false);
				if($.trim(results)=='200'){
					$('.subscrIn-newsLtr p').css('color','green').text('Please check your email and confirm your subscription.');
				}else{
					if($.trim(results)=='400'){
						$('.subscrIn-newsLtr p').css('color','red').text('This email address is already subscribed.');
					}else{
						$('.subscrIn-newsLtr p').css('color','green').text('Unable to contact mailchimp server. Try again later.');
					}
				}
			}
		});
	}
});
})(jQuery);
</script>
<!-- Button trigger modal -->
</body>
</html>
