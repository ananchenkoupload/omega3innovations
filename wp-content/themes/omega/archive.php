<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<section class="blog-detail heading-marg">
<div class="container">
        <div class="blog-top">
             <?php// echo do_shortcode( '[searchandfilter fields="search,category"]' ); ?>
             <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
            <!-- <div class="top-search list-top">
               <span>Top Searches:</span>
                <?php $page_for_posts = get_option( 'page_for_posts' );
                
                $terms = get_field('top_searches_option',$page_for_posts);
                      
					if( $terms ): ?>
                <ol class="breadcrumb">

	            <?php foreach( $terms as $term ): ?>
                  <li><a href="<?php echo get_term_link( $term ); ?>"><?php echo $term->name; ?></a></li>             
                  <?php endforeach; ?>
                </ol>
                <?php endif; ?>
            </div> -->
        </div>
    </div>
        <div class="blog-banner">
            <div class="container">            
                <div class="row">                   
                    <div class="col-sm-12 new-heading">    
                    	<?php if(is_author()){?>
                    		<h1>Author: <?php echo get_the_author(); ?></h1>       
                    	<?php } else { ?>           
                       	<h1>Tags: <?php echo $current_category = single_cat_title("", false); ?></h1>   
                       	<?php } ?>                   
                    </div>
                </div> 
            </div>
        </div>
        
	    <div class="container">
        <div class="row blog-containt">
            <div class="col-sm-9"> 
        	<?php 
               
        	    while ( have_posts() ) : the_post(); ?>
            
          <div class="blog-left">
                <div class="blog-box blog-listing clearfix">
                    <a href="<?php the_permalink(); ?>" class="blog-list-pic">
                        <?php $thumb_image=wp_get_attachment_url(get_post_thumbnail_id(), 'bottom-blog', true);   ?>
                        <div class="blog-picnew" style="background-image: url('<?php echo $thumb_image; ?>')"></div>
                    
                    </a>
                    <div class="blog-list-text">
                        <div class="blog-news-date">
                            <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo get_the_author_meta( 'display_name',$post->post_author); ?></a>    <?php echo get_the_time('M j,Y'); ?>
                        </div>
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <!-- <p><?php echo wp_trim_words( get_the_content(), 40, '...' ); ?><a href="<?php the_permalink(); ?>">Read full article</a></p> -->
                        <div class="customer-social list-social">
                            <ul>
                                <li><a href="<?php the_permalink(); ?>/#respond"><i class="fa fa-commenting-o" aria-hidden="true"></i> <?php if(get_comments_number(get_the_ID())){echo $vart=get_comments_number(get_the_ID()); } else { echo $vart='0'; } ?></a></li>
                                <li><?php if( function_exists('zilla_likes') ) zilla_likes(); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>      
           <?php endwhile; ?>

           <?php the_posts_pagination( array(
				'prev_text'          => __( '<i class="fa fa-angle-left" aria-hidden="true"></i>', 'twentysixteen' ),
				'next_text'          => __( '<i class="fa fa-angle-right" aria-hidden="true"></i>', 'twentysixteen' ),
				
			) ); ?>
            </div>
            <div class="col-sm-3">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</section>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
