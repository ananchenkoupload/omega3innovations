<?php 
/*Template Name: New Product*/
get_header();?>
<main id="main" class="site-main" role="main">	
    <div class="paginationIn heading-marg bgGrey">
        <div class="container">
            <ul>
                <li>
                    <a href="javascript:void(0)">
                        Products
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        Omega Cure Fish Oil
                    </a>
                </li>
            </ul>
        </div>
    </div>	
    <section class="productVw bgGrey">
        <div class="container">
            <div class="inProductRow">
                <div class="row">
                    <div class="col-md-7 col-sm-6">
                        <div class="productSlide">
                            <div class="prodSlider">
                                <div class="sliderProd-e">
                                    <a rel="prettyPhoto[pp_gal]" href="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png">
                                        <img src="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png" alt="">
                                    </a>
                                </div>
                                <div class="sliderProd-e">
                                    <a rel="prettyPhoto[pp_gal]" href="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png">
                                        <img src="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png" alt="">
                                    </a>
                                </div>
                                <div class="sliderProd-e">
                                    <a rel="prettyPhoto[pp_gal]" href="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png">
                                        <img src="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png" alt="">
                                    </a>
                                </div>
                                <div class="sliderProd-e">
                                    <a rel="prettyPhoto[pp_gal]" href="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png">
                                        <img src="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png" alt="">
                                    </a>
                                </div>
                                <div class="sliderProd-e">
                                    <a rel="prettyPhoto[pp_gal]" href="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png">
                                        <img src="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png" alt="">
                                    </a>
                                </div>
                            </div>

                            <div class="prodSlider-thumb">
                                <div class="sliderProd-e">
                                    <figure style="background-image: url(http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png);">
                                        <!-- <img src="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png" alt=""> -->
                                    </figure>
                                </div>
                                <div class="sliderProd-e">
                                    <figure style="background-image: url(http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png);">
                                        <!-- <img src="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png" alt=""> -->
                                    </figure>
                                </div>
                                <div class="sliderProd-e">
                                    <figure style="background-image: url(http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png);">
                                        <!-- <img src="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png" alt=""> -->
                                    </figure>
                                </div>
                                <div class="sliderProd-e">
                                    <figure style="background-image: url(http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png);">
                                        <!-- <img src="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png" alt=""> -->
                                    </figure>
                                </div>
                                <div class="sliderProd-e vidSlide">
                                    <figure style="background-image: url(http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png);">
                                        <!-- <img src="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png" alt=""> -->
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-6">
                        <div class="summary">
                            <h1>Omega Cure Fish Oil <span>$44.95</span></h1>
                            <div class="servingItem">
                                17 (15 mL) servings per bottle
                            </div>
                            <div class="showWriteRvw clearfix">
                                <div class="showRate pull-left">
                                    <figure>
                                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/starBg.png" alt="">
                                    </figure>
                                    <span><strong>4.0</strong>(1,230)</span>
                                </div>

                                <div class="linkRvw">
                                    <a href="#sec-review">
                                        Write a review
                                    </a>
                                </div>
                            </div>
                            <div class="typeItem cart-item">
                                <label class="label">
                                    FLAVOR
                                </label>
                                <div class="selectBox">
                                    <select>
                                        <option>
                                            Citrus
                                        </option>
                                        <option>
                                            Citrus
                                        </option>
                                        <option>
                                            Citrus
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="optionsItem">
                                <div class="btnCase btncart">
                                    <button class="addtoCart btn btnSplit text-uppercase" data-toggle="modal" data-target="#modalCounter">
                                        <span class="labelBtn">Add to cart</span>
                                        <span>$44.95</span>
                                    </button>
                                </div>
                                <div class="btnCase btnSubsc">
                                    <button class="subsC btn btnSplit text-uppercase">
                                            <span class="labelBtn">Subscription Available</span>
                                        <span>$37.95</span>
                                    </button>
                                </div>
                            </div>

                            <div class="deliverFtPref">
                                <div class="ftDeliver">
                                    <p><span><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/time.png" alt=""></span>Orders usually arrive within 3 business days</p>
                                    <p><span><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ship.png" alt=""></span>Free Shipping on orders above $110.</p>
                                    <p><span><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/stock.png" alt=""></span>In Stock. Cancel any time.</p>
                                </div>
                                <div class="prodInfosm">
                                    <ul>
                                        <li>
                                                Ideal for families and children
                                        </li>
                                        <li>
                                                Good for personalizing dose
                                        </li>
                                        <li>
                                                Helps ease aching joints
                                        </li>
                                        <li>
                                                Easy to mix with foods or enjoy plain
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sectionOverviewProduct">
        <div class="container">
            <div class="txtBlock">
                <h6>Overview</h6>
                <p>When it comes to omega-3 fish oil, oxidation levels demonstrate freshness; the fresher the oil, the less fishy the taste. No other brand can rival Omega Cure’s extremely low oxidation levels and virtual tastelessness. Providing 3000 mg of EPA/DHA in three teaspoons, Omega Cure allows you to get an effective dose of omega-3 every day without any fishy taste or smell.</p>
            
                <aside class="factsSupp">
                    <h6>Supplement Facts</h6>
                    <p>Serving Size: 3 teaspoons (15ml)</p>
                    <p>Serving per container 17</p>

                    <div class="tableInOvw">
                        <table>
                            <tr>
                                <th>
                                        Omega-3
                                </th>
                                <th>
                                        Amt per Serving
                                </th>
                                <th>
                                        % Daily Value
                                </th>
                            </tr>
                            <tr>
                                <td>
                                        EPA
                                </td>
                                <td>
                                        1500 mg
                                </td>
                                <td>
                                        not established
                                </td>
                            </tr>
                            <tr>
                                <td>
                                        DHA
                                </td>
                                <td>
                                        500 mg
                                </td>
                                <td>
                                        not established
                                </td>
                            </tr>
                            <tr>
                                <td>
                                        Other Omega-3s
                                </td>
                                <td>
                                        800mg
                                </td>
                                <td>
                                        not established
                                </td>
                            </tr>
                            <tr>
                                <td>
                                        Vitamin A
                                </td>
                                <td>
                                        1500 mg
                                </td>
                                <td>
                                        5%
                                </td>
                            </tr>
                        </table>
                    </div>

                    <h6>Calories 8 kcal/ml. Traces of vitamin D, E and rosemary extract.</h6>
                </aside>

                <h6>Recommended Dose</h6>
                <p>The recommended daily dose for adults is 2-3 teaspoons of Omega Cure.
                        For children between ages 1-4, take 1/2 teaspoon daily. For children ages 5-15, take 1 teaspoon daily.</p>

                <h6>Care Tips</h6>
                <p>Depending on the temperature, Omega Cure may congeal in the refrigerator. This is normal for all extra-virgin oils. If you find that the oil is excessively congealed and hard to pour, let the bottle thaw at room temperature for about 10-15 minutes before taking the oil.</p>
                <p>
                        Take your Omega Cure at the same time every day, preferably with food.
                </p>
            </div>
        </div>
    </section>

    <section class="productPara bgGrey">
        <div class="product-panel">
            <div class="seq-outer">
                <div class="seq-list" style="background-image: url(<?php echo esc_url(get_template_directory_uri()); ?>/images/p1.jpg);">
                
                </div>
                <div class="container text-container">
                    <div class="row">
                        <div class="col-sm-5 col-lg-5 col-sm-offset-7 col-lg-offset-7">
                            <div class="seq-inner">
                                <h2>The Dose You Need to Feel a Difference</h2>
                                <p>Most fish oil capsules don’t contain enough omega-3 to deliver results, at least if you only take one a day. In contrast, each recommended three-teaspoon dose of Omega Cure provides 3000 mg of EPA/DHA omega-3. That’s equivalent to taking 8-10 regular fish oil capsules or eating a 5-6 ounce fillet of wild-caught salmon.</p>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
            <div class="seq-outer">
                <div class="seq-list" style="background-image: url(<?php echo esc_url(get_template_directory_uri()); ?>/images/p2.jpg);">
                
                </div>
                <div class="container text-container">
                    <div class="row">
                        <div class="col-lg-5 col-sm-5">
                            <div class="seq-inner">
                                <h2>From Norway to Your Doorway</h2>
                                <p>Omega Cure comes from wild cod caught off the northwest coast of Norway, following the strict sustainable fishing regulations of the Norwegian government. After the fish are harvested, the cod livers are individually inspected before going on to squeezing. The oil is then purified in small batches with limited exposure to air, heat, and light, ensuring the lowest oxidation levels in the industry.</p>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sectionReviews" id="sec-review">
        <div class="container">
            <div class="caseInRvw">
                <header class="reviewsMain clearfix">
                    <h3 class="pull-left">Customer reviews</h3>
                    <div class="pull-right triggerAddrevw">
                        <a href="javascript:void(0)" class="triggerRvw">
                            Add a review
                        </a>
                    </div>
                </header>
                <div class="addReview-in">
                    <form>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="inRvw">
                                    <input type="text" placeholder="Name*">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="inRvw">
                                    <input type="email" placeholder="Email*">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="inRvw inputRvwStar">
                                    <label>
                                        Your Rating
                                    </label>
                                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/rvw.png" alt="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="inRvw reviewText">
                                    <input type="text" placeholder="Review title*">
                                    <textarea placeholder="Your review.."></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="inRvw captchaInt">
                                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/captcha.png" alt="">
                                </div>
                                
                                <div class="inRvw reviwSubmit btnCase">
                                    <button class="btn text-uppercase">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="showReviews-in">
                <h4>34 reviews for Omega Cure Fish Oil</h4>

                <div class="listedRvws">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="reviwListed-e">
                                <h5>Lowers triglycerides</h5>
                                <div class="ratedCourtesy">
                                    <div class="rating-e">
                                        <figure><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/starBg.png" alt=""></figure>
                                        <span>4.0</span>
                                    </div>
                                    by
                                    <a href="javascript:void(0)" class="reviewr">
                                            Jonathan Fulton
                                    </a>
                                    on Nov 13, 2017
                                </div>

                                <div class="rateBody">
                                    <p>
                                        Since I've been taking Omega Cure my triglycerides are down in the low normal range. My HDL is very high. I love the "no fishy" taste. I'm waiting for the new glass shot glasses to come. I broke my last one and really like to use one so I get the right amount.
                                    </p>
                                </div>
                            </div>
                            <div class="reviwListed-e">
                                <h5>Lowers triglycerides</h5>
                                <div class="ratedCourtesy">
                                    <div class="rating-e">
                                        <figure><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/starBg.png" alt=""></figure>
                                        <span>4.0</span>
                                    </div>
                                    by
                                    <a href="javascript:void(0)" class="reviewr">
                                            Jonathan Fulton
                                    </a>
                                    on Nov 13, 2017
                                </div>

                                <div class="rateBody">
                                    <p>
                                            This is probably the best fish oil on the market today. You can drink it straight with no fishy aftertaste, especially the citrus flavor. As a veterinarian, I understand the importance of incorporating omega 3's into our pets diets as well as our own. It provides relief from oteoarthritis pain/inflammation and increases skin health in allergic pets with no side effects. Finding a good quality supplement has always been a problem especially because fish oil supplements are not regulated by the FDA. They are often not tested for quality or purity and can sit in a hot store or warehouse for who knows how long before it reaches the customer. I take omega cure daily now especially because I am pregnant and there are NO equivalent high concentration epa/dha supplements on the market that can compare. Even the prenatal vitamins promoted by my doctor lack sufficient omega 3's! If you don't like to eat fish or other foods with good levels of omega 3's in them like me, then this is the way to go to achieve better health.
                                    </p>
                                </div>
                            </div>
                            <div class="reviwListed-e">
                                <h5>Lowers triglycerides</h5>
                                <div class="ratedCourtesy">
                                    <div class="rating-e">
                                        <figure><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/starBg.png" alt=""></figure>
                                        <span>4.0</span>
                                    </div>
                                    by
                                    <a href="javascript:void(0)" class="reviewr">
                                            Jonathan Fulton
                                    </a>
                                    on Nov 13, 2017
                                </div>

                                <div class="rateBody">
                                    <p>
                                        I broke my last one and really like to use one so I get the right amount.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="reviwListed-e">
                                <h5>Lowers triglycerides</h5>
                                <div class="ratedCourtesy">
                                    <div class="rating-e">
                                        <figure><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/starBg.png" alt=""></figure>
                                        <span>4.0</span>
                                    </div>
                                    by
                                    <a href="javascript:void(0)" class="reviewr">
                                            Jonathan Fulton
                                    </a>
                                    on Nov 13, 2017
                                </div>

                                <div class="rateBody">
                                    <p>
                                        My HDL is very high. I love the "no fishy" taste. I'm waiting for the new glass shot glasses to come. I broke my last one and really like to use one so I get the right amount.
                                    </p>
                                </div>
                            </div>                   
                            <div class="reviwListed-e">
                                <h5>Lowers triglycerides</h5>
                                <div class="ratedCourtesy">
                                    <div class="rating-e">
                                        <figure><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/starBg.png" alt=""></figure>
                                        <span>4.0</span>
                                    </div>
                                    by
                                    <a href="javascript:void(0)" class="reviewr">
                                            Jonathan Fulton
                                    </a>
                                    on Nov 13, 2017
                                </div>

                                <div class="rateBody">
                                    <p>
                                        Since I've been taking Omega Cure my triglycerides are down in the low normal range. My HDL is very high. I love the "no fishy" taste. I'm waiting for the new glass shot glasses to come. I broke my last one and really like to use one so I get the right amount.
                                    </p>
                                </div>
                            </div>
                            <div class="reviwListed-e">
                                <h5>Lowers triglycerides</h5>
                                <div class="ratedCourtesy">
                                    <div class="rating-e">
                                        <figure><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/starBg.png" alt=""></figure>
                                        <span>4.0</span>
                                    </div>
                                    by
                                    <a href="javascript:void(0)" class="reviewr">
                                            Jonathan Fulton
                                    </a>
                                    on Nov 13, 2017
                                </div>

                                <div class="rateBody">
                                    <p>
                                        I broke my last one and really like to use one so I get the right amount.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>


<!-- Modal -->
<div class="modal fade modalItems" id="modalCounter" tabindex="-1" role="dialog" aria-labelledby="modalCounterLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close text-uppercase" data-dismiss="modal" aria-label="Close">
                    Close <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="viewItemM text-center">
                    <form>
                        <figure class="">
                            <img src="http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/06095749/omega-cure.png" alt="">
                        </figure>
                        <p>How many do you need?</p>

                        <div class="spinnerItem">
                            <div class="number-spinner">
                                <span class="ns-btn">
                                        <a data-dir="dwn"><span class="icon-minus"></span></a>
                                </span>
                                <input type="text" class="pl-ns-value" value="1" maxlength=2>
                                <span class="ns-btn">
                                        <a data-dir="up"><span class="icon-plus"></span></a>
                                </span>
                            </div>
                        </div>

                        <div class="btnCase submitCounter">
                            <input type="submit" value="OK" class="btn text-uppercase">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer();?>