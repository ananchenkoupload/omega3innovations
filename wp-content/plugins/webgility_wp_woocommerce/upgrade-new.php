<?php
/*
===================================
� Copyright 2007-2016 � Webgility INC. all rights reserved.
----------------------------------------
This file and the source code contained herein are the property of Webgility INC
and are protected by United States copyright law. All usage is restricted as per
the terms & conditions of Webgility License Agreement. You may not alter or remove
any trademark, copyright or other notice from copies of the content.

The code contained herein may not be reproduced, copied, modified or redistributed in any form
without the express written consent by an officer of Webgility INC.

===================================
*/
error_reporting(E_ALL);

//die('working');
$req = $_POST;
//print_r($req);

if($req['api_key'] != 'asfjklh44srg4f65dg'){
    $return = 'Authentication Failed';
    return $return;
}

if($req['func_name'] == 'update_upgrade_file')
	update_upgrade_file();
else if($req['func_name'] == 'create_backup_zip')
	create_backup_zip();
else if($req['func_name'] == 'delete_backup_zip')
	delete_backup_zip();
else
	echo"Invalid Request";

//FUNCTION TO UPDATE upgrade.php FILE WITH LATEST UPLOADED ONE
function update_upgrade_file(){
	write_log('1 called');
	unlink('test.php');
    if(file_exists('upgrade.php') && file_exists('latest-upgrade.php') ){
        try{
            //FIRST RENAME EXISTING upgrade.php to upgarde-bk.php
			unlink('upgrade.php');
			//clearstatcache();
			sleep(5);
            //rename('upgrade.php','upgrade-bk.php');
			write_log('rm old upgrade');

            //THEN RENAME LATEST UPLOADED latest-upgarde.php to upgrade.php
            if(rename('latest-upgrade.php','upgrade.php')){
                //DELETE upgrade-bk.php 
				//unlink('upgrade-bk.php');
				write_log('rnm latest - success');
                
                $res = array('status'=>'success','message'=>'Upgrade file has been updated Successfully');
                $res = json_encode($res);
                echo $res;
            }
        }
        catch(Exception $e) {
			write_log('1 Exception -'.$e->getMessage());
            $res = array('status'=>'error','message'=>'Exception : '.$e->getMessage());
            $res = json_encode($res);
            echo $res;
        }
    }
    else{
		write_log('upgarde or latest-upgarde does not exist');
        $res = array('status'=>'error','message'=>'upgrade.php or latest-upgrade.php file does not exist');
        $res = json_encode($res);
        echo $res;
    }
    return true;
}

//FUNCTION TO CREATE SINGLE BACKUP ZIP FOR ALL AVAILABLE FILES AND FOLDERS IN DIRECTORY
function create_backup_zip(){
	write_log('2 called');
  // Get real path for our folder
  //$rootPath = realpath('backup_11-12-2018_1');

  $currentDirectory = getcwd();
  //echo $currentDirectory;
  $res = _make_zip($currentDirectory,$currentDirectory.'/module_backup.zip');
  $res = json_encode($res);
  echo $res;

  return true;
}

//FUNCTION TO DELETE ALL BACKUP FOLDER AND BACKUP ZIP FROM SERVER
function delete_backup_zip(){
	write_log('3 called');
    $allBackupDirs = array_filter(glob('*'), 'is_dir');
	//print_r( $allBackupDirs);

    try{
        foreach($allBackupDirs as $dirName){

            //IF FOLDER NAME DOES NOT CONTAINS backup_ THEN SKIP DELETING (WE HAVE TO DELETE ONLY BACKUP FOLDER)
            if(strpos($dirName, 'backup') === false) { //var_dump(strpos($dirName, 'backup'));
                continue;
            }
			write_log('rm d for -'.$dirName);
			_delete_recurive_folder($dirName.'/');
        }
        unlink('module_backup.zip');
		//unlink('upgrade-bk.php');
		write_log('rm zp - success');

        $res = array('status'=>'success','message'=>'Backup Successfully Deleted');
        $res = json_encode($res);
    	echo $res;
    }
    catch(Exception $e){
		write_log('3 Exception -'.$e->getMessage());
        $res = array('status'=>'error','message'=>'Exception : '.$e->getMessage());
        $res = json_encode($res);
    	echo $res;
    }
    return true;
}

/* 
 * php delete function that deals with directories recursively
 */
function _delete_recurive_folder($target) {
	write_log('3.1 called'.$target);
    if(is_dir($target)){
        $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

        foreach( $files as $file ){
            _delete_recurive_folder( $file );      
        }

        rmdir( $target );
    } elseif(is_file($target)) {
        unlink( $target );  
    }
}

//FUNCTION TO MAKE ZIP FILE
function _make_zip($source, $destination){
	write_log('2.1 called');
    $res['status'] = 'error';
    if (!extension_loaded('zip') || !file_exists($source)) {
		write_log('zp or src not available');
        $res['message'] = 'Zip Extension or Source file not available';
        return $res;
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
		write_log('dst not available');
        $res['message'] = 'Destination not available';
        return $res;
    }

    try{
        $source = str_replace('\\', '/', realpath($source));
        if (is_dir($source) === true)
        {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
            foreach ($files as $file){
                $file = str_replace('\\', '/', $file);
                if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                    continue;
                $file = realpath($file);
                if (is_dir($file) === true){
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                }else if (is_file($file) === true){
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        }else if (is_file($source) === true){
            $zip->addFromString(basename($source), file_get_contents($source));
        }
        $zip->close();
		write_log('bk zp - success');
        $res['status'] = 'success';
        $res['message'] = 'Backup Zip Successfully Created';
        return $res;
    }
    catch(Exception $e) {
		write_log('2 Exception -'.$e->getMessage());
        $res['message'] = 'Exception : '.$e->getMessage();
        return $res;
    }
}

//CREATE LOG
function write_log($logMsg){
    $logMessage = date("Y-m-d h:i:s")." : ".$logMsg."\r\n";
    file_put_contents('upgrade-log.txt', $logMessage, FILE_APPEND);
}
?>
