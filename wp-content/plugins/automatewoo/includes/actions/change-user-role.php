<?php

namespace AutomateWoo;

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * @class Action_Change_User_Role
 */
class Action_Change_User_Role extends Action {

	public $required_data_items = [ 'user' ];


	function init() {
		$this->title = __( 'Change User Role', 'automatewoo' );
		$this->group = __( 'User', 'automatewoo' );
	}


	function load_fields() {
		$user_type = new Fields\User_Role( false );
		$user_type->set_description( __( 'Users will be changed to this', 'automatewoo'  ) );
		$user_type->set_required();

		$this->add_field($user_type);
	}


	/**
	 * @return void
	 */
	function run() {

		$user = $this->workflow->get_data_item('user');
		$role = Clean::string( $this->get_option('user_type') );

		if ( $user instanceof \WP_User  ) {
			$user->set_role( $role );
		}
	}

}
