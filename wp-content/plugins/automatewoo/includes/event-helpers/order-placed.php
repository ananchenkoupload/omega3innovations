<?php

namespace AutomateWoo\Event_Helpers;

use AutomateWoo\Compat;

/**
 * @class Order_Placed
 */
class Order_Placed {


	static function init() {
		// add order place hook, limited to fire once per order
		add_action( 'woocommerce_api_create_order', [ __CLASS__, 'order_placed' ], 1000 );
		add_action( 'woocommerce_checkout_order_processed', [ __CLASS__, 'order_placed' ], 1000 );
	}


	/**
	 * @param $order_id int
	 */
	static function order_placed( $order_id ) {

		if ( ! $order_id || ! $order = wc_get_order( $order_id ) ) {
			return;
		}

		if ( Compat\Order::get_meta( $order, '_aw_checkout_order_processed' ) ) {
			return; // Ensure only order placed triggers once ever fire once per order
		}

		Compat\Order::update_meta( $order, '_aw_checkout_order_processed', true );

		do_action( 'automatewoo/order/placed', $order_id );
	}


}