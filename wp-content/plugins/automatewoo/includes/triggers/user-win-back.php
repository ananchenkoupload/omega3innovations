<?php

namespace AutomateWoo;

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * This trigger relies on the user meta field _aw_last_order_placed that is managed in AW_Order_Helper.
 *
 * @class Trigger_User_Win_Back
 */
class Trigger_User_Win_Back extends Trigger {

	public $supplied_data_items = [ 'user', 'order' ];


	function init() {
		$this->title = __( 'User Win Back', 'automatewoo' );
		$this->description = __( "The 'order based' variables, rules and actions used by this trigger refer to the user's last successful order. This trigger is processed daily in the background.", 'automatewoo' );
		$this->group = __( 'User', 'automatewoo' );
	}


	/**
	 * Add options to the trigger
	 */
	function load_fields() {

		$period = ( new Fields\Number() )
			->set_name( 'days_since_last_purchase' )
			->set_title( __( 'Days since last purchase', 'automatewoo' ) )
			->set_required();

		$repeat = ( new Fields\Checkbox() )
			->set_name( 'enable_repeats' )
			->set_title( __( 'Enable repeats', 'automatewoo' ) )
			->set_description( __( 'If checked this trigger will repeatedly fire after the set period passes and the user has not made a purchase. E.g. if set to 30 days the trigger will fire 30 days after the customers last purchase and every 30 days from then on until the customer makes another purchase, then the cycle starts again. If unchecked the trigger will not repeat until the customer makes a new purchase.', 'automatewoo' ) );

		$this->add_field( $period );
		$this->add_field( $repeat );
	}


	/**
	 * Run check daily
	 */
	function register_hooks() {

		add_action( 'automatewoo_daily_worker', [ $this, 'catch_hooks' ] );
		add_action( 'automatewoo/batch/user_absent', [ $this, 'process_batch' ], 10, 2 );
	}



	/**
	 * This trigger does not use $this->maybe_run() so we don't have to loop through every single user when processing
	 */
	function catch_hooks() {

		if ( ! $this->has_workflows() )
			return;

		if ( ! $workflows = $this->get_workflows() )
			return;

		foreach ( $workflows as $workflow ) {

			if ( ! $period = absint( $workflow->get_trigger_option( 'days_since_last_purchase' ) ) )
				continue;

			$date = new \DateTime();
			$date->modify( "-$period days" );

			// fetch users by date using our last order meta field
			$users = get_users([
				'fields' => 'ids',
				'meta_query' => [
					[
						'key' => '_aw_last_order_placed',
						'compare' => '!=',
						'value' => false
					],
					[
						'key' => '_aw_last_order_placed',
						'compare' => '<',
						'value' => $date->format( Format::MYSQL )
					]
				]
			]);

			if ( $users ) {
				$process = new Background_Process( 'automatewoo/batch/user_absent', $users, [ 'workflow_id' => $workflow->get_id() ] );
				$process->set_delay( ( aw_get_user_count_rough() > 4000 ) ? 3 : 5 ); // decrease delay if more users
				$process->dispatch();
			}
		}
	}


	/**
	 * @param $users
	 * @param $args
	 */
	function process_batch( $users, $args ) {

		if ( ! isset( $args['workflow_id'] ) )
			return;

		$workflow = AW()->get_workflow( absint( $args['workflow_id'] ) );

		if ( ! $workflow )
			return;

		foreach ( $users as $user_id ) {

			$user = get_user_by( 'id', $user_id );

			if ( ! $user ) {
				continue;
			}

			// make the user's last order data available for this trigger
			$orders = wc_get_orders([
				'customer' => $user_id,
				'status' => [ 'wc-completed', 'wc-processing' ],
				'limit' => 1
			]);

			if ( empty( $orders ) ) {
				continue;
			}

			$workflow->maybe_run([
				'user' => $user,
				'order' => current( $orders )
			]);
		}
	}


	/**
	 * @param Workflow $workflow
	 * @return \DateTime|bool
	 */
	function get_min_last_order_date( $workflow ) {

		$period = absint( $workflow->get_trigger_option( 'days_since_last_purchase' ) );

		if ( ! $period ) {
			return false;
		}

		$date = new \DateTime();
		$date->modify( "-$period days" );

		return $date;
	}


	/**
	 * @param $workflow Workflow
	 * @return bool
	 */
	function validate_workflow( $workflow ) {

		$user = $workflow->get_data_item( 'user' );
		$enable_repeats = absint( $workflow->get_trigger_option( 'enable_repeats' ) );
		$min_last_order_date = $this->get_min_last_order_date( $workflow );
		$last_order_date = get_user_meta( $user->ID, '_aw_last_order_placed', true );

		if ( ! $user || ! $min_last_order_date || ! $last_order_date ) {
			return false;
		}

		// fail if the user has not been a member for at least the set period
		if ( strtotime( $user->user_registered ) > $min_last_order_date->getTimestamp() ) {
			return false;
		}

		// check that the user has not made a purchase since the start of the delay period
		if ( strtotime( $last_order_date ) > $min_last_order_date->getTimestamp() ) {
			return false;
		}

		// if repeats are enabled the wait period should start at the last time the workflow was run or queued
		// if repeats are disabled the date range should start at the last order date
		$wait_period = $enable_repeats ? $min_last_order_date : $last_order_date;


		if ( $workflow->get_timing_type() != 'immediately' ) {
			// check workflow has not been added to the queue already
			$query = ( new Queue_Query() )
				->where( 'workflow_id', $workflow->get_id() )
				->where( 'created', $wait_period, '>' )
				->where_meta( 'data_item_user', $user->ID )
				->set_limit(1);

			if ( $query->get_results() )
				return false;
		}

		// check the workflow has not run already
		$query = ( new Log_Query() )
			->where( 'workflow_id', $workflow->get_id() )
			->where( 'date', $wait_period, '>' )
			->where_meta( 'user_id', $user->ID )
			->set_limit(1);

		if ( $query->get_results() )
			return false;
		
		return true;
	}


	/**
	 * @param Workflow $workflow
	 * @return bool
	 */
	function validate_before_queued_event( $workflow ) {

		if ( ! parent::validate_before_queued_event( $workflow ) ) {
			return false;
		}

		$user = $workflow->get_data_item( 'user' );
		$min_last_order_date = $this->get_min_last_order_date( $workflow );
		$last_order_date = get_user_meta( $user->ID, '_aw_last_order_placed', true );

		if ( ! $user || ! $min_last_order_date || ! $last_order_date ) {
			return false;
		}

		// check that the user has not made a purchase while the workflow was queued
		if ( strtotime( $last_order_date ) > $min_last_order_date->getTimestamp() ) {
			return false;
		}

		return true;
	}

}
