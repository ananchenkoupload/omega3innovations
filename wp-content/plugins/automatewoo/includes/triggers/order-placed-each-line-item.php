<?php

namespace AutomateWoo;

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * @class Trigger_Order_Placed_Each_Line_Item
 * @since 2.9
 */
class Trigger_Order_Placed_Each_Line_Item extends Trigger_Abstract_Order_Base {

	public $is_run_for_each_line_item = true;


	function init() {
		$this->title = __( 'Order Placed - Each Line Item', 'automatewoo' );
		$this->description = __( 'Triggers for each order line item as soon as an order is created in the database. At checkout this happens before payment is confirmed.', 'automatewoo' );
	}


	function register_hooks() {
		add_action( 'automatewoo/order/placed', [ $this, 'trigger_for_each_order_item' ] );
	}

}
