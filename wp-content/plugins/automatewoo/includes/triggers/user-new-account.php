<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * @class AW_Trigger_User_New_Account
 */
class AW_Trigger_User_New_Account extends AW_Trigger {

	public $name = 'user_new_account';

	public $supplied_data_items = [ 'user', 'shop' ];

	public $was_ultimate_members_frontend_signup = false;


	function init() {
		$this->title = __( 'New User Account Created', 'automatewoo' );
		$this->group = __( 'User', 'automatewoo' );

		parent::init();
	}


	/**
	 * When might this trigger run?
	 */
	function register_hooks() {
		add_action( 'user_register', [ $this, 'catch_hooks' ] );
	}


	/**
	 * @param int $user_id
	 */
	function catch_hooks( $user_id ) {

		// check for ultimate members frontend signup
		if ( did_action( 'um_add_user_frontend' ) && $this->was_ultimate_members_frontend_signup === false ) {
			$this->was_ultimate_members_frontend_signup = true; // use this property in case another user is registered in this same request
			add_action( 'um_after_new_user_register', [ $this, 'user_registered' ], 100, 2 ); // move trigger event for ultimate members
			return;
		}

		$this->user_registered( $user_id );
	}


	/**
	 * @param $user_id
	 */
	function user_registered( $user_id ) {

		if ( get_user_meta( $user_id, '_aw_user_registered', true ) )
			return;

		add_user_meta( $user_id, '_aw_user_registered', true );

		$user = get_user_by( 'id', $user_id );

		$this->maybe_run([
			'user' => $user
		]);
	}


	/**
	 * @param $workflow AW_Model_Workflow
	 * @return bool
	 */
	function validate_workflow( $workflow ) {

		if ( ! $user = $workflow->get_data_item('user') )
			return false;

		return true;
	}

}
