<?php
/**
 * @class       AW_Install
 * @package     AutomateWoo
 */

class AW_Install {

	/** @var array */
	private static $db_updates = [
		'2.1.0' => 'automatewoo-update-2.1.0.php',
		'2.3' => 'automatewoo-update-2.3.php',
		'2.4' => 'automatewoo-update-2.4.php',
		'2.6' => 'automatewoo-update-2.6.php',
		'2.6.1' => 'automatewoo-update-2.6.1.php',
		'2.7' => 'automatewoo-update-2.7.php'
	];


	/**
	 * Init
	 */
	public static function init() {
		add_action( 'admin_init', [ __CLASS__, 'admin_init' ], 5 );
		add_filter( 'plugin_action_links_' . AW()->plugin_basename, [ __CLASS__, 'plugin_action_links' ] );
	}



	/**
	 * Admin init
	 */
	public static function admin_init() {

		if ( defined( 'IFRAME_REQUEST' ) || is_ajax() )
			return;

		if ( AW()->options()->version != AW()->version ) {

			self::install();

			// check for required database update
			if ( self::database_upgrade_available() ) {
				add_action( 'admin_notices', [ __CLASS__, 'data_upgrade_prompt' ] );
			}
			else {
				self::update_version();
			}
		}

		foreach( AW()->addons()->get_all() as $addon ) {
			$addon->check_version();
		}

		if ( did_action( 'automatewoo_updated' ) || did_action( 'automatewoo_addon_updated' ) ) {
			// do API check-in after an update
			AW()->licenses->check_statuses();
		}

	}


	/**
	 * Install
	 */
	public static function install() {

		AW()->database_tables()->install_tables();

		do_action( 'automatewoo_installed' );
	}


	/**
	 * @return bool
	 */
	static function database_upgrade_available() {

		if ( AW()->options()->version == AW()->version )
			return false;

		return AW()->options()->version && version_compare( AW()->options()->version, max( array_keys( self::$db_updates ) ), '<' );
	}


	/**
	 * Handle updates
	 */
	public static function update() {

		@ini_set( 'memory_limit', apply_filters( 'admin_memory_limit', WP_MAX_MEMORY_LIMIT ) );

		foreach ( self::$db_updates as $version => $updater ) {
			if ( version_compare( AW()->options()->version, $version, '<' ) ) {
				include( AW()->path( '/includes/updates/' . $updater ) );
			}
		}

		self::update_version();
	}


	/**
	 * Update version to current
	 */
	private static function update_version() {
		update_option( 'automatewoo_version', AW()->version, true );
		do_action( 'automatewoo_updated' );
	}


	/**
	 * Renders prompt notice for user to update
	 */
	static function data_upgrade_prompt() {

		AW()->admin->get_view( 'data-upgrade-prompt', [
			'plugin_name' => __( 'AutomateWoo', 'automatewoo' ),
			'plugin_slug' => AW()->plugin_slug
		]);
	}



	/**
	 * @return bool
	 */
	static function is_data_update_screen() {

		$screen = get_current_screen();
		return $screen->id === 'automatewoo_page_automatewoo-data-upgrade';
	}


	/**
	 * Show action links on the plugin screen.
	 *
	 * @param	mixed $links Plugin Action links
	 * @return	array
	 */
	public static function plugin_action_links( $links ) {

		$action_links = [
			'settings' => '<a href="' . AW()->admin->page_url( 'settings' ) . '" title="' . esc_attr( __( 'View AutomateWoo Settings', 'automatewoo' ) ) . '">' . __( 'Settings', 'automatewoo' ) . '</a>',
		];

		return array_merge( $action_links, $links );
	}

}
