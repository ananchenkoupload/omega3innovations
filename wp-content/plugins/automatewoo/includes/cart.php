<?php

namespace AutomateWoo;

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * @class Cart
 * @since 2.0
 *
 * @property string $status
 * @property string $user_id
 * @property string $guest_id
 * @property string $last_modified
 * @property string $created
 * @property array $items
 * @property array $coupons
 * @property string $total
 * @property string $token
 * @property string $currency
 */
class Cart extends Model {

	/** @var string */
	public $table_id = 'carts';

	/** @var string  */
	public $object_type = 'cart';

	/** @var float */
	public $calculated_total = 0;

	/** @var float */
	public $calculated_tax_total = 0;

	/** @var float */
	public $calculated_subtotal = 0;

	/** @var array */
	public $_items_language_adjusted;


	/**
	 * @param bool|int $id
	 */
	function __construct( $id = false ) {
		if ( $id ) {
			$this->get_by( 'id', $id );
		}
	}


	/**
	 * @return string
	 */
	function get_status() {
		return $this->status ? Clean::string( $this->status ) : 'abandoned';
	}


	/**
	 * @param $status - active|abandoned
	 */
	function set_status( $status ) {
		$this->status = $status;
	}


	/**
	 * Update status, triggers change hooks
	 * @param $new_status - active|abandoned
	 */
	function update_status( $new_status ) {

		$old_status = $this->get_status();

		if ( $new_status == $old_status ) {
			return;
		}

		$this->set_status( $new_status );
		$this->save();
		do_action( 'automatewoo/cart/status_changed', $this, $old_status, $new_status );
	}


	/**
	 * @return int
	 */
	function get_user_id() {
		return (int) $this->user_id;
	}


	/**
	 * @param $user_id
	 */
	function set_user_id( $user_id ) {
		$this->user_id = $user_id;
	}


	/**
	 * @return int
	 */
	function get_guest_id() {
		return (int) $this->guest_id;
	}


	/**
	 * @param $guest_id
	 */
	function set_guest_id( $guest_id ) {
		$this->guest_id = $guest_id;
	}


	/**
	 * @return bool|\DateTime
	 */
	function get_date_last_modified() {
		return $this->get_date_column( 'last_modified' );
	}


	/**
	 * @param \DateTime|string $date
	 */
	function set_date_last_modified( $date ) {
		$this->set_date_column( 'last_modified', $date );
	}


	/**
	 * @return bool|\DateTime
	 */
	function get_date_created() {
		return $this->get_date_column( 'created' );
	}


	/**
	 * @param \DateTime $date
	 */
	function set_date_created( $date ) {
		$this->set_date_column( 'created', $date );
	}


	/**
	 * @return float
	 */
	function get_total() {
		return (float) $this->get_prop( 'total' );
	}


	/**
	 * @param $total
	 */
	function set_total( $total ) {
		$this->set_prop( 'total', $total );
	}


	/**
	 * @param $val
	 */
	function set_shipping_total( $val ) {
		$this->set_prop( 'shipping_total', $val );
	}


	/**
	 * @param $val
	 */
	function set_shipping_tax_total( $val ) {
		$this->set_prop( 'shipping_tax_total', $val );
	}

	/**
	 * @return float
	 */
	function get_shipping_total() {
		return (float) $this->get_prop( 'shipping_total' );
	}


	/**
	 * @return float
	 */
	function get_shipping_tax_total() {
		return (float) $this->get_prop( 'shipping_tax_total' );
	}


	/**
	 * @return string
	 */
	function get_shipping_total_html() {
		$total = get_option( 'woocommerce_tax_display_cart' ) === 'excl' ? $this->get_shipping_total() : $this->get_shipping_total() + $this->get_shipping_tax_total();
		if ( $total == 0 ) {
			return __( 'Free!', 'automatewoo' );
		}
		else {
			return $this->price( $total );
		}
	}


	/**
	 * @return bool
	 */
	function needs_shipping() {

		if ( ! wc_shipping_enabled() || 0 === wc_get_shipping_method_count( true ) ) {
			return false;
		}

		$needs_shipping = false;

		if ( $this->has_items() ) {
			foreach ( $this->get_items() as $cart_item ) {
				$product = $cart_item->get_product();
				if ( $product && $product->needs_shipping() ) {
					$needs_shipping = true;
					break;
				}
			}
		}

		return $needs_shipping;
	}


	/**
	 * @return string
	 */
	function get_token() {
		return $this->token;
	}


	/**
	 * @param bool $token (optional)
	 */
	function set_token( $token = false ) {
		if ( ! $token ) {
			$token = aw_generate_key( 32 );
		}

		$this->token = $token;
	}


	/**
	 * @return float
	 */
	function get_currency() {
		if ( $this->currency ) {
			return Clean::string( $this->currency );
		}
		return get_woocommerce_currency();
	}


	/**
	 * @param $currency
	 */
	function set_currency( $currency ) {
		$this->currency = $currency;
	}


	/**
	 * @return bool
	 */
	function has_coupons() {
		return sizeof( $this->get_coupons() ) > 0;
	}


	/**
	 * @return array
	 */
	function get_coupons() {
		return is_array( $this->coupons ) ? $this->coupons : [];
	}


	/**
	 * @param $coupons
	 */
	function set_coupons( $coupons ) {
		$this->coupons = $coupons;
	}


	/**
	 * @return bool
	 */
	function has_items() {
		return is_array( $this->items ) && sizeof( $this->items ) > 0;
	}


	/**
	 * @return Cart_Item[]
	 */
	function get_items() {
		$items = [];

		if ( is_array( $this->items ) ) {
			foreach( $this->items as $item_key => $item_data ) {
				$items[ $item_key ] = new Cart_Item( $item_key, $item_data );
			}

			if ( Language::is_multilingual() ) {
				$items = $this->get_language_adjusted_items( $items );
			}
		}

		return apply_filters( 'automatewoo/cart/get_items', $items );
	}



	/**
	 * @return array
	 */
	function get_items_raw() {
		$raw = [];
		foreach ( $this->get_items() as $item ) {
			$raw[ $item->get_key() ] = $item->get_data();
		}
		return $raw;
	}


	/**
	 * Adjust the cart items so are match the language of the cart
	 *
	 * @param Cart_Item[] $items
	 * @return array
	 */
	function get_language_adjusted_items( $items ) {

		if ( isset( $this->_items_language_adjusted ) ) {
			return $this->_items_language_adjusted;
		}

		$lang = $this->get_language();

		foreach ( $items as &$item ) {
			$item->set_product_id( icl_object_id( $item->get_product_id(), 'product', true, $lang ) );
			$item->set_variation_id( icl_object_id( $item->get_variation_id(), 'product', true, $lang ) );
		}

		$this->_items_language_adjusted = $items;
		return $items;
	}


	/**
	 * @param $items
	 */
	function set_items( $items ) {
		$this->_items_language_adjusted = null;
		$this->items = $items;
	}


	/**
	 * @return Guest|false
	 */
	function get_guest() {
		if ( ! $this->get_guest_id() ) {
			return false;
		}
		return AW()->get_guest( $this->get_guest_id() );
	}


	/**
	 * @return Customer|bool
	 */
	function get_customer() {
		if ( $this->get_user_id() ) {
			return Customer_Factory::get_by_user_id( $this->get_user_id() );
		}
		else {
			return Customer_Factory::get_by_guest_id( $this->get_guest_id() );
		}
	}

	/**
	 * @return string
	 */
	function get_language() {
		if ( $this->get_customer() ) {
			return $this->get_customer()->get_language();
		}
		return Language::get_default();
	}


	/**
	 * Updates the stored cart with the current time and cart items
	 */
	function sync() {

		$this->set_date_last_modified( new \DateTime() );
		$this->set_items( WC()->cart->get_cart_for_session() );

		$coupon_data = [];

		foreach( WC()->cart->get_applied_coupons() as $coupon_code ) {
			$coupon_data[$coupon_code] = [
				'discount_incl_tax' => WC()->cart->get_coupon_discount_amount( $coupon_code, false ),
				'discount_excl_tax' => WC()->cart->get_coupon_discount_amount( $coupon_code ),
				'discount_tax' => WC()->cart->get_coupon_discount_tax_amount( $coupon_code )
			];
		}

		$this->set_coupons( $coupon_data );
		$this->set_currency( get_woocommerce_currency() );
		$this->set_shipping_tax_total( WC()->cart->shipping_tax_total );
		$this->set_shipping_total( WC()->cart->shipping_total );

		$this->calculate_totals();

		$this->set_total( $this->calculated_total );

		if ( $this->get_status() == 'abandoned' ) {
			$this->update_status( 'active' );
		}
		else {
			$this->save();
		}
	}


	function calculate_totals() {

		$this->calculated_subtotal = 0;
		$this->calculated_tax_total = 0;
		$this->calculated_total = 0;

		$tax_display = get_option( 'woocommerce_tax_display_cart' );

		foreach( $this->get_items() as $item ) {
			$this->calculated_tax_total += $item->get_line_subtotal_tax();
			$this->calculated_total += $item->get_line_subtotal() + $item->get_line_subtotal_tax();
			$this->calculated_subtotal += $tax_display === 'excl' ? $item->get_line_subtotal() : $item->get_line_subtotal() + $item->get_line_subtotal_tax();
		}

		foreach ( $this->get_coupons() as $coupon_code => $coupon ) {
			$this->calculated_total -= $coupon[ 'discount_incl_tax' ];
			$this->calculated_tax_total -= $coupon['discount_tax'];
		}

		$this->calculated_tax_total += $this->get_shipping_tax_total();
		$this->calculated_total += $this->get_shipping_total();
		$this->calculated_total += $this->get_shipping_tax_total();
	}


	/**
	 * @param float $price
	 * @return string
	 */
	function price( $price ) {
		return wc_price( $price, apply_filters( 'automatewoo/cart/price_args', [], $this ) );
	}


	/**
	 * Save
	 */
	function save() {

		if ( ! $this->exists ) {
			$this->set_date_created( new \DateTime() );
		}

		parent::save();
	}

}

