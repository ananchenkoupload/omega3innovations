<?php
/**
 * @class AW_Rule_User_Order_Statuses
 */

if ( ! defined( 'ABSPATH' ) ) exit;


class AW_Rule_User_Order_Statuses extends AW_Rule_Abstract_Select {

	public $data_item = 'user';

	public $is_multi = true;


	function init() {
		$this->title = __( "User's Order Statuses", 'automatewoo' );
		$this->group = __( 'User', 'automatewoo' );
		unset( $this->compare_types[ 'matches_all' ] );
	}


	/**
	 * @return array
	 */
	function get_select_choices() {
		return wc_get_order_statuses();
	}


	/**
	 * @param $user WP_User|AW_Model_Order_Guest
	 * @param $compare
	 * @param $value
	 * @return bool
	 */
	function validate( $user, $compare, $value ) {

		$orders = wc_get_orders([
			'customer' => $user->ID === 0 ? $user->user_email : $user->ID,
			'limit' => -1
		]);

		$statuses = wp_list_pluck( $orders, 'post_status' );

		return $this->validate_select( $statuses, $compare, $value );
	}

}

return new AW_Rule_User_Order_Statuses();
