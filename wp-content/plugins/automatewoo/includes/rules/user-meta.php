<?php

namespace AutomateWoo\Rules;

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * @class User_Meta
 */
class User_Meta extends Abstract_Meta {

	public $data_item = 'user';


	function init() {
		$this->title = __( 'User Meta', 'automatewoo' );
		$this->group = __( 'User', 'automatewoo' );
	}


	/**
	 * @param $user \WP_User|\AutomateWoo\Order_Guest
	 * @param $compare_type
	 * @param $value_data
	 * @return bool
	 */
	function validate( $user, $compare_type, $value_data ) {

		if ( $user->ID == 0 ) {
			return false;
		}

		$value_data = $this->prepare_value_data( $value_data );

		if ( ! is_array( $value_data ) ) {
			return false;
		}

		return $this->validate_meta( get_user_meta( $user->ID, $value_data['key'], true ), $compare_type, $value_data['value'] );
	}
}

return new User_Meta();
