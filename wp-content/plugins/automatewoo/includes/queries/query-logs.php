<?php
/**
 * @class       AW_Query_Log
 * @package     AutomateWoo/Queries
 */

if ( ! defined( 'ABSPATH' ) ) exit;


class AW_Query_Logs extends AW_Query_Custom_Table {

	/** @var string */
	public $table_id = 'logs';

	/** @var string  */
	public $meta_table_id = 'log-meta';

	/** @var string */
	public $model = 'AW_Model_Log';

	/**
	 * @return AW_Model_Log[]
	 */
	function get_results() {
		return parent::get_results();
	}

}
