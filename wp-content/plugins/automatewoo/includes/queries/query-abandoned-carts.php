<?php
/**
 * @class       AW_Query_Abandoned_Carts
 * @since       2.0.0
 * @package     AutomateWoo/Queries
 */

if ( ! defined( 'ABSPATH' ) ) exit;


class AW_Query_Abandoned_Carts extends AW_Query_Custom_Table {

	/** @var string */
	public $table_id = 'carts';

	protected $model = 'AW_Model_Abandoned_Cart';


	/**
	 * @return AW_Model_Abandoned_Cart[]
	 */
	function get_results() {
		return parent::get_results();
	}

}