<?php
/**
 * @class       AW_Query_Queue
 * @since       2.1.0
 * @package     AutomateWoo/Queries
 */

if ( ! defined( 'ABSPATH' ) ) exit;


class AW_Query_Queue extends AW_Query_Custom_Table {

	/** @var string */
	public $table_id = 'queue';

	/** @var string  */
	protected $model = 'AW_Model_Queued_Event';


	/**
	 * @return AW_Model_Queued_Event[]
	 */
	function get_results() {
		return parent::get_results();
	}
}