<?php
/**
 * @class       AW_Query_Unsubscribes
 * @package     AutomateWoo/Queries
 */

if ( ! defined( 'ABSPATH' ) ) exit;


class AW_Query_Unsubscribes extends AW_Query_Custom_Table {

	/** @var string */
	public $table_id = 'unsubscribes';

	protected $model = 'AW_Model_Unsubscribe';


	/**
	 * @return AW_Model_Unsubscribe[]
	 */
	function get_results() {
		return parent::get_results();
	}

}
