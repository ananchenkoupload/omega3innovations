<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Not to be confused with AW_Model_Order_Guest
 *
 * This object should be used as a data-type 'guest' and can only be queued with certain other data items
 *
 * @class       AW_Model_Guest
 * @package     AutomateWoo/Models
 * @since       2.1.0
 *
 * @property string $id
 * @property string $email
 * @property string $tracking_key
 * @property string $created
 * @property string $last_active
 * @property string $language
 * @property string $ip
 */
class AW_Model_Guest extends AW_Model {

	/** @var string */
	public $table_id = 'guests';

	/** @var string  */
	public $object_type = 'guest';

	/** @var AW_Model_Abandoned_Cart|null|false */
	private $cart_id;


	/**
	 * @param $id
	 */
	function __construct( $id = false ) {
		if ( $id ) $this->get_by( 'id', $id );
	}


	/**
	 * @param $email
	 */
	function set_email( $email ) {
		$this->email = strtolower( $email );
	}


	/**
	 * @param $tracking_key
	 */
	function set_tracking_key( $tracking_key ) {
		$this->tracking_key = $tracking_key;
	}


	/**
	 * @param DateTime $date
	 */
	function set_date_created( $date ) {
		$this->set_date_column( 'created', $date );
	}


	/**
	 * @param DateTime $date
	 */
	function set_date_last_active( $date ) {
		$this->set_date_column( 'last_active', $date );
	}


	/**
	 * @param $language
	 */
	function set_language( $language ) {
		$this->language = $language;
	}


	/**
	 * @param $ip
	 */
	function set_ip( $ip ) {
		$this->ip = $ip;
	}


	/**
	 * @return AW_Model_Abandoned_Cart|false
	 */
	function get_cart() {

		if ( ! isset( $this->cart_id ) ) {
			$cart = new AW_Model_Abandoned_Cart();
			$cart->get_by( 'guest_id', $this->id );
			$this->cart_id = $cart->exists ? $cart->get_id() : false;
		}

		return AW()->get_cart( $this->cart_id );
	}


	/**
	 *
	 */
	function delete_cart() {
		if ( $this->exists && $cart = $this->get_cart() ) {
			$cart->delete();
		}
	}


	function delete() {
		$this->delete_cart();
		parent::delete();
	}

}
