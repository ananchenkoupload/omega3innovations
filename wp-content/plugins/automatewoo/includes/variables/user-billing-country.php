<?php

namespace AutomateWoo;

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * @class Variable_User_Billing_Country
 */
class Variable_User_Billing_Country extends Variable {

	protected $name = 'user.billing_country';


	function init() {
		$this->description = __( "Displays the name of the user's billing country.", 'automatewoo');
	}


	/**
	 * @param $user \WP_User|Order_Guest
	 * @param $parameters array
	 * @param $workflow Workflow
	 * @return string
	 */
	function get_value( $user, $parameters, $workflow ) {
		$countries = WC()->countries->get_countries();
		return isset( $countries[ $user->billing_country ] ) ? $countries[ $user->billing_country ] : '';
	}
}

return new Variable_User_Billing_Country();
