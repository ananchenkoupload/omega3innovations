<?php
/**
 * Plugin Name: Jetcommerce Myaccount
 * Description: Jetcommerce account form extra field insert,delete and account menu ordering
 * Version: 1.0
 * Author: Jetcommerce Developers
 * Author URI: http://jetcommerce.io/
 **/

/*Extra fields in Account Field*/
add_action( 'woocommerce_edit_account_form', 'my_woocommerce_edit_account_form' );
 
function my_woocommerce_edit_account_form() {
 
	$user_id = get_current_user_id();
	$user = get_userdata( $user_id );
 
	if ( !$user )
		return;
 
	$phone = get_user_meta( $user_id, 'phone', true );
 
?>
	<fieldset class="edit-accphone">    
		<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide check-input">
			<label for="phone">Phone:</label>
			<input class="form-control woocommerce-Input woocommerce-Input--password input-text" type="number" name="phone" maxlength="10" value="<?php echo esc_attr( $phone ); ?>" class="input-text" />
		</p>
	</fieldset>
 
<?php
 
} // end func

/**
 * This is to save user input into database
 * hook: woocommerce_save_account_details
 */
add_action( 'woocommerce_save_account_details', 'my_woocommerce_save_account_details' );
 
function my_woocommerce_save_account_details( $user_id ) {
	update_user_meta( $user_id, 'phone', htmlentities( $_POST[ 'phone' ] ) ); 
} // end function

//Hide Product Content part
add_action('admin_init', 'remove_product_editor');
function remove_product_editor() {
    remove_post_type_support('product', 'editor');  
}

//Redirect After Login
add_filter('woocommerce_registration_redirect', 'omega_login_redirect');
add_filter('woocommerce_login_redirect', 'omega_login_redirect');
function omega_login_redirect( $redirect ) {
	global $woocommerce;
	if ( isset($_REQUEST['_wp_http_referer']) && strpos($_REQUEST['_wp_http_referer'], 'checkout') !== false) {
    	$redirect = wc_get_page_permalink( 'checkout' );
    }else{
    	$redirect = wc_get_page_permalink( 'myaccount' );
    }
    return $redirect;
}

//Remove billing fields
add_filter( 'woocommerce_checkout_fields' , 'omega_override_checkout_fields' );
 
function omega_override_checkout_fields( $fields ) {
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['shipping']['shipping_company']);
    unset($fields['shipping']['shipping_address_2']);
    return $fields;
}


//Increase Week
function eg_do_not_allow_non_single_billing_intervals( $interval = '' ) {                                                                                            

	$intervals = array( 1 => _x( 'every', 'period interval (eg "$10 _every_ 2 weeks")', 'woocommerce-subscriptions' ) );

	foreach ( range( 2, 11 ) as $i ) {
		// translators: period interval, placeholder is ordinal (eg "$10 every _2nd/3rd/4th_", etc)
		$intervals[ $i ] = sprintf( _x( 'every %s', 'period interval with ordinal number (e.g. "every 2nd"', 'woocommerce-subscriptions' ), WC_Subscriptions::append_numeral_suffix( $i ) );
	}

	return $intervals;
}
add_filter( 'woocommerce_subscription_period_interval_strings', 'eg_do_not_allow_non_single_billing_intervals', 10 );

//move comment field ordering

function wpb_move_comment_field_to_bottom( $fields ) {
	global $post;
	if($post->post_type=='product'){
		$comment_field = $fields['comment'];
		unset( $fields['comment'] );
		$fields['comment'] = $comment_field;
	}
	
	return $fields;
}

add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );
//Remove admin bar link
function remove_admin_bar_links() {
    global $wp_admin_bar; 
    $wp_admin_bar->remove_menu('view-store');  
	$wp_admin_bar->remove_menu('wp-logo');      // Remove the site name menu
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

//remove password strength meter from create password
function remove_password_meter()
{     
    wp_deregister_script('password-strength-meter');
    wp_register_script('password-strength-meter', '', false, false);
    wp_enqueue_script('password-strength-meter');
}
add_action('template_redirect', 'remove_password_meter');

function omega_account_ordering() {
	$items = array(
		'dashboard'          => __( 'Account Information', 'woocommerce' ),
		'orders' 		 	 => __( 'My Orders', 'woocommerce' ),
		'subscriptions' 	 => __( 'Omega Subscriptions', 'woocommerce' ),
		'edit-address'       => __( 'Delivery Information', 'woocommerce' ),		
		'customer-logout'    => __( 'Sign Out', 'woocommerce' ),
	);
	
	return $items;
}
add_filter ( 'woocommerce_account_menu_items', 'omega_account_ordering' );

function custom_my_account_menu_items( $items ) {
    unset($items['edit-address']);
	//unset($items['payment-methods']);
    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'custom_my_account_menu_items' );
/*02-06-2017*/ // Remove each style one by one
add_filter( 'woocommerce_enqueue_styles', 'disable_styles1' );
function disable_styles1( $enqueue_styles ) {
	unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
	return $enqueue_styles;
}
//add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

// widget_cart always display
add_filter( 'woocommerce_widget_cart_is_hidden', 'always_show_cart1', 40, 0 );
function always_show_cart1() {
    return false;
}


//Multistep Form Override
add_filter('yith_wcms_load_checkout_template_from_plugin', '__return_false');


function lead_status1(){
	return array(
		'web' 					=> __('Web', 'wc_crm'),
		'amazon'     			=> __('Amazon', 'wc_crm'),
		'walkin'             	=> __('Walk In', 'wc_crm'),
		'custom_referral'       => __('Custom Referral', 'wc_crm'),
		'exhibition'            => __('Exhibition', 'wc_crm'),
		'farmers_market'        => __('Farmer\'s Market', 'wc_crm')
		);

}
add_filter('wc_crm_lead_status', 'lead_status1');

add_action( 'admin_menu', 'phone_order_menu1' );

/** Step 1. */
function phone_order_menu1() {
	add_options_page( 'Phone Order Menu', 'Phone Order', 'manage_options', 'my-phone-orders', 'process_admin_options' );
}
function get_tao_id1($email){
	global $wpdb;
	$table = $wpdb->prefix.'wptao_users';
	$userid = $wpdb->get_var( $wpdb->prepare( 
		"SELECT `id` 
			FROM $table 
			WHERE email = %s
		", 
		$email
	) );
	return $userid;
}
function save_additional_account_details1( $user_ID ){
	global $wpdb;
    $oldemail = ! empty( $_POST['old_account_email'] ) ? wc_clean( $_POST['old_account_email'] ) : '';
    $firstname = ! empty( $_POST['account_first_name'] ) ? wc_clean( $_POST['account_first_name'] ) : '';
    $lastname = ! empty( $_POST['account_last_name'] ) ? wc_clean( $_POST['account_last_name'] ) : '';
    $email = ! empty( $_POST['account_email'] ) ? wc_clean( $_POST['account_email'] ) : '';
    $table = $wpdb->prefix.'wptao_users';
    $userid = get_tao_id1($oldemail);
    $wpdb->update( 
	$table, 
	array( 
		'first_name' => $firstname,
		'last_name'  => $lastname,
		'email' 	 => $email
	), 
	array( 'ID' => $userid ), 
	array( '%s', '%s', '%s' )
);
}
add_action( 'woocommerce_save_account_details', 'save_additional_account_details1' );
