<?php
/**
 * Registration form.
 *
 * @author 	Jeroen Sormani
 * @package 	WooCommerce-Simple-Registration
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;
wp_enqueue_script( 'wc-password-strength-meter' );

?><div class="registration-form woocommerce">

	<?php wc_print_notices(); ?>	

	<form method="post" class="register">

		<h2 class="form_txt"><?php _e( 'Create Account', 'woocommerce' ); ?></h2>

		<?php do_action( 'woocommerce_register_form_start' ); ?>

		<div class="account">
		<div class="check-input">
			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
				<input type="text" class="form-control" placeholder="Username" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
			<?php endif; ?>
			<input type="email" class="form-control" placeholder="Email" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
		</div>
		<div class="check_in">
			<input id="order3" type="radio" name="radio-2" data-name="register" checked="checked">
			<label for="order3">
				<h4>New Customers</h4>
					<h5>Create an account with us for quick checkout, 
						to keep track of your order, and more.</h5>
			</label>   
		</div>
		<div class="check_in">
			<input id="order4" type="radio" name="radio-2" data-name="signin">
			<label for="order4">
				<h4>Returning Customers</h4>
			</label>   
		</div>
		<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
		<div class="check-input">
			<input type="password" class="form-control" placeholder="Password" name="password" id="reg_password" />
		</div>
		<?php endif; ?>
	

		<!-- Spam Trap -->
		<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

		<?php do_action( 'woocommerce_register_form' ); ?>
		<?php do_action( 'register_form' ); ?>

		<p class="woocomerce-FormRow form-row">
			<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
			<input type="submit" class="btn btn-default form_txt" name="register" value="<?php esc_attr_e( 'Create Account', 'woocommerce' ); ?>" />
		</p>
		</div>

		<?php do_action( 'woocommerce_register_form_end' ); ?>

	</form>

</div>
