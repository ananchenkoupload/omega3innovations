<?php
/*
Plugin Name: YITH WooCommerce Multi Step Checkout Custom Code
Plugin URI: http://yithemes.com/themes/plugins/yith-woocommerce-product-vendors/
Description: Disable Multi Step Checkout Cookie
Author: YITHEMES
Version: 1.0.0
Author URI: http://yithemes.com/
*/
/*
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly 

add_filter( 'yith_wcms_use_cookie', '__return_false' );