<?php
/**
 * Plugin Name: Jetcommerce Menu cart
 * Description: Jetcommerce cart menu for loggedin and non loggedin user
 * Version: 1.0
 * Author: Jetcommerce Developers
 * Author URI: http://jetcommerce.io/
 **/

//Mini cart
add_filter('wp_nav_menu_items','omega_menucart', 10, 2);
function omega_menucart($menu, $args) {

	if (is_user_logged_in()) {
		global $current_user;
		$fname = $current_user->user_firstname;
		if(empty($fname)){
			$fname = $current_user->user_email;
		}
		if($args->theme_location == 'topright'){			
       		$menu .= '<li class="hide_mobile myaccount"><a href="'. get_permalink( woocommerce_get_page_id( 'myaccount' ) ) .'"><span>'.$fname.'</span></a>
       					<ul class="header_right_mnu">
       						<li><a href="'. site_url( 'my-account' ) .'">My Account</a></li>
       						<li><a href="'. site_url( 'my-account/orders' ) .'">Orders</a></li>
       						<li><a href="'. site_url( 'my-account/subscriptions' ) .'">Omega Subscriptions</a></li>
       						<li><a href="'. site_url( 'my-account/customer-logout' )  .'">Sign Out</a></li>
   						</ul>
       				  </li>';
   		}elseif ($args->theme_location == 'primary') {
   			$menu .= '<li class="show_mobile account_menu"><a href="javascript:void(0);">My Account <span>('.$fname.') </span></a>
   						<ul class="header_right_mnu">
       						<li><a href="'. site_url( 'my-account' ) .'">My Account</a></li>
       						<li><a href="'. site_url( 'my-account/orders' ) .'">Orders</a></li>
       						<li><a href="'. site_url( 'my-account/subscriptions' ) .'">Omega Subscriptions</a></li>
       						<li><a href="'. site_url( 'my-account/customer-logout' )  .'">Sign Out</a></li>
   						</ul>
   					</li>';
   		}

   }

   elseif (!is_user_logged_in()) {

       if($args->theme_location == 'topright'){
       		$menu .= '
       		<li class="hide_mobile"><a href="'.  get_permalink( woocommerce_get_page_id( 'myaccount' ) ) .'">Sign in</a></li>';
   		}elseif ($args->theme_location == 'primary') {
   			$menu .= '
       		<li class="show_mobile"><a href="'.  site_url('/blog') .'">Blog</a></li>
       		<li class="show_mobile"><a href="#">FAQ</a></li>
       		<li class="show_mobile"><a href="'.  site_url('/contact') .'">Contact</a></li>
       		<li class="show_mobile"><a href="'.  get_permalink( woocommerce_get_page_id( 'myaccount' ) ) .'">Sign in</a></li>';
   		}

   }

	// Check if WooCommerce is active and add a new item to a menu assigned to Primary Navigation Menu location
	if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) )
		return $menu;

	// if($args->theme_location == 'topright'){
		// $menu .= '<li class="dropdown">
						// <a href="'.site_url('/cart').'" class="dropdown-toggle carticon" data-toggle="dropdown" title="">
							// <span class="hide_mobile">Cart</span>
                        // </a>
                    // </li>';	
	// }
	return $menu;

}