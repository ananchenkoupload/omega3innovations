<?php
	global $redux_builder_amp;

	if( isset($redux_builder_amp['ampforwp-cmt-section']) && $redux_builder_amp['ampforwp-cmt-section'] == 1 ){
		add_action('ampforwp_before_comment_hook','ampforwp_comment_form');
	}
	else{
		if( isset($redux_builder_amp['ampforwp-cmt-section']) &&  $redux_builder_amp['ampforwp-cmt-section'] == 2) {
			add_action('ampforwp_after_comment_hook','ampforwp_comment_form');
		}
	}
// add_action('ampforwp_post_after_design_elements','ampforwp_comment_form');
function ampforwp_comment_form( $args = array(), $post_id = null ) {
	global $redux_builder_amp;
	if ( null === $post_id )
		$post_id = get_the_ID();

	$commenter = wp_get_current_commenter();
	$user = wp_get_current_user();
	$user_identity = $user->exists() ? $user->display_name : '';

	$args = wp_parse_args( $args );
	if ( ! isset( $args['format'] ) )
		$args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';

	$req      = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
	$html_req = ( $req ? " required='required'" : '' );
	$html5    = 'html5' === $args['format'];
	$fields   =  array(
		'author' => '<p class="comment-form-author">' . '<label for="author">' .  esc_html__( $redux_builder_amp['amp-comments-translator-name-text'], 'accelerated-mobile-pages') . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
		            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" maxlength="245"' . $aria_req . $html_req . ' /></p>',
		'email'  => '<p class="comment-form-email"><label for="email">' .  esc_html__( $redux_builder_amp['amp-comments-translator-email-text'], 'accelerated-mobile-pages') . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
		            '<input id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" maxlength="100" aria-describedby="email-notes"' . $aria_req . $html_req  . ' /></p>',
		'url'    => '<p class="comment-form-url"><label for="url">' .  esc_html__( $redux_builder_amp['amp-comments-translator-website-text'], 'accelerated-mobile-pages') . '</label> ' .
		            '<input id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" maxlength="200" /></p>',
	);

	$required_text = sprintf( ' ' . esc_html__( $redux_builder_amp['amp-comments-translator-required-fields-text'], 'accelerated-mobile-pages') . '%s', '<span class="required">*</span>' );


 //esc_html__( $redux_builder_amp['amp-comments-translator-cancel-reply-text'], 'accelerated-mobile-pages');
	/**
	 * Filters the default comment form fields.
	 *
	 * @since 3.0.0
	 *
	 * @param array $fields The default comment fields.
	 */
	$submit_url =  admin_url('admin-ajax.php?action=ampajaxcomments');
	global $redux_builder_amp;
	$sitetkey = $redux_builder_amp['amp-cmt-Recaptcha-site'];
	$fields = apply_filters( 'comment_form_default_fields', $fields );
	$recaptchaHtml = '';
	if(isset($redux_builder_amp['amp-cmt-Recaptcha']) && $redux_builder_amp['amp-cmt-Recaptcha'] == 1){ 
		$recaptchaHtml = '<amp-recaptcha-input layout="nodisplay" name="g-recaptcha-response" data-sitekey="'.$sitetkey.'" data-action="g_recaptcha_response">
          </amp-recaptcha-input>';
	}
	$defaults = array(
		'fields'               => $fields,
		'comment_field'        => '<p [class]="formData.submit.success" class="comment-form-comment"><label for="comment">' . esc_html__( $redux_builder_amp['amp-comments-translator-Comment-text'], 'accelerated-mobile-pages') . '</label> <textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required"></textarea></p>

	      '.$recaptchaHtml.'
    
		 <input type="hidden" name="amp_cptch_hidden" value="1" >',
		/** This filter is documented in wp-includes/link-template.php */
		'must_log_in'          => '<p class="must-log-in">' . sprintf(
		                              /* translators: %s: login URL */
		                              __( 'You must be <a href="%s">logged in</a> to post a comment.' ),
		                              wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) )
		                          ) . '</p>',
		/** This filter is documented in wp-includes/link-template.php */
		'logged_in_as'         => '<p [class]="formData.submit.success" class="logged-in-as">' . sprintf(
		                              /* translators: 1: edit user link, 2: accessibility text, 3: user name, 4: logout URL */
		                              __( '<a href="%1$s" aria-label="%2$s">'.$redux_builder_amp['amp-comments-translator-loggedin-text'].' %3$s</a>. <a href="%4$s">'. $redux_builder_amp['amp-comments-translator-logout-text'] .'?</a>' ),
		                              get_edit_user_link(),
		                              /* translators: %s: user name */
		                              esc_attr( sprintf( __( 'Logged in as %s. Edit your profile.' ), $user_identity ) ),
		                              $user_identity,
		                              wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) )
		                          ) . '</p>',
		'comment_notes_before' => '<p class="comment-notes"><span id="email-notes">' .  	esc_html__( $redux_builder_amp['amp-comments-translator-your-email-address-text'], 'accelerated-mobile-pages') . '</span>'. ( $req ? $required_text : '' ) . '</p>',
		'comment_notes_after'  => '',
		'action'               => $submit_url,
		'id_form'              => 'commentform',
		'id_submit'            => 'submit',
		'class_form'           => 'comment-form',
		'class_submit'         => 'submit',
		'name_submit'          => 'submit',
		'title_reply'          => $redux_builder_amp['amp-comments-translator-leave-a-reply'],
		'title_reply_to'       => __( 'Leave a Reply to %s' ),
		'title_reply_before'   => '<h3 id="reply-title" class="comment-reply-title">',
		'title_reply_after'    => '</h3>',
		'cancel_reply_before'  => ' <small>',
		'cancel_reply_after'   => '</small>',
		'cancel_reply_link'    => esc_html__( $redux_builder_amp['amp-comments-translator-cancel-reply-text'], 'accelerated-mobile-pages'),
		'label_submit'         => esc_html__( $redux_builder_amp['amp-comments-translator-post-comment-text'], 'accelerated-mobile-pages'),
		'submit_button'        => '<input name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s" />',
		'submit_field'         => '<p id="ampCommentsButton" [class]="formData.submit.success" class="form-submit">%1$s %2$s</p>',
		'format'               => 'xhtml',
	);

	/**
	 * Filters the comment form default arguments.
	 *
	 * Use {@see 'comment_form_default_fields'} to filter the comment fields.
	 *
	 * @since 3.0.0
	 *
	 * @param array $defaults The default comment form arguments.
	 */
	$args = wp_parse_args( $args, apply_filters( 'comment_form_defaults', $defaults ) );

	// Ensure that the filtered args contain all required default values.
	$args = array_merge( $defaults, $args );

	if ( comments_open( $post_id ) ) : ?>
		<?php
		/**
		 * Fires before the comment form.
		 *
		 * @since 3.0.0
		 */
		$submit_url = $args['action'].'?amp';
		$actionXhrUrl = preg_replace('#^https?:#', '', $submit_url);

		do_action( 'comment_form_before' );
		?>
		<div id="respond" class="comment-respond ampforwp-comments amp-wp-content">
			<?php
			echo $args['title_reply_before'];

			comment_form_title( $args['title_reply'], $args['title_reply_to'] );

			echo $args['cancel_reply_before'];

			// cancel_comment_reply_link( $args['cancel_reply_link'] );

			echo $args['cancel_reply_after'];

			echo $args['title_reply_after'];

			if ( get_option( 'comment_registration' ) && !is_user_logged_in() ) :
				echo $args['must_log_in'];
				/**
				 * Fires after the HTML-formatted 'must log in after' message in the comment form.
				 *
				 * @since 3.0.0
				 */
				do_action( 'comment_form_must_log_in_after' );
			else : ?>
				<form action-xhr="<?php echo esc_url( $actionXhrUrl ); ?>" target="_top" method="post" id="<?php echo esc_attr( $args['id_form'] ); ?>"  on="submit-error:AMP.setState({FormData: submit.success})" >
					<?php
					/**
					 * Fires at the top of the comment form, inside the form tag.
					 *
					 * @since 3.0.0
					 */
					do_action( 'comment_form_top' );

					if ( is_user_logged_in() ) :
						/**
						 * Filters the 'logged in' message for the comment form for display.
						 *
						 * @since 3.0.0
						 *
						 * @param string $args_logged_in The logged-in-as HTML-formatted message.
						 * @param array  $commenter      An array containing the comment author's
						 *                               username, email, and URL.
						 * @param string $user_identity  If the commenter is a registered user,
						 *                               the display name, blank otherwise.
						 */
						echo apply_filters( 'comment_form_logged_in', $args['logged_in_as'], $commenter, $user_identity );

						/**
						 * Fires after the is_user_logged_in() check in the comment form.
						 *
						 * @since 3.0.0
						 *
						 * @param array  $commenter     An array containing the comment author's
						 *                              username, email, and URL.
						 * @param string $user_identity If the commenter is a registered user,
						 *                              the display name, blank otherwise.
						 */
						do_action( 'comment_form_logged_in_after', $commenter, $user_identity );

					else :

						echo $args['comment_notes_before'];

					endif; 
					// Prepare an array of all fields, including the textarea
					$comment_fields = array( 'comment' => $args['comment_field'] ) + (array) $args['fields'];
					/**
					 * Filters the comment form fields, including the textarea.
					 *
					 * @since 4.4.0
					 *
					 * @param array $comment_fields The comment fields.
					 */
					$comment_fields = apply_filters( 'comment_form_fields', $comment_fields );

					// Get an array of field names, excluding the textarea
					$comment_field_keys = array_diff( array_keys( $comment_fields ), array( 'comment' ) );

					// Get the first and the last field name, excluding the textarea
					$first_field = reset( $comment_field_keys );
					$last_field  = end( $comment_field_keys );

					foreach ( $comment_fields as $name => $field ) {

						if ( 'comment' === $name ) {

							/**
							 * Filters the content of the comment textarea field for display.
							 *
							 * @since 3.0.0
							 *
							 * @param string $args_comment_field The content of the comment textarea field.
							 */
							echo apply_filters( 'comment_form_field_comment', $field );

							echo $args['comment_notes_after'];

						} elseif ( ! is_user_logged_in() ) {

							if ( $first_field === $name ) {
								/**
								 * Fires before the comment fields in the comment form, excluding the textarea.
								 *
								 * @since 3.0.0
								 */
								do_action( 'comment_form_before_fields' );
							}

							/**
							 * Filters a comment form field for display.
							 *
							 * The dynamic portion of the filter hook, `$name`, refers to the name
							 * of the comment form field. Such as 'author', 'email', or 'url'.
							 *
							 * @since 3.0.0
							 *
							 * @param string $field The HTML-formatted output of the comment form field.
							 */
							echo apply_filters( "comment_form_field_{$name}", $field ) . "\n";

							if ( $last_field === $name ) {
								/**
								 * Fires after the comment fields in the comment form, excluding the textarea.
								 *
								 * @since 3.0.0
								 */
								do_action( 'comment_form_after_fields' );
							}
						}
					}

					$submit_button = sprintf(
						$args['submit_button'],
						esc_attr( $args['name_submit'] ),
						esc_attr( $args['id_submit'] ),
						esc_attr( $args['class_submit'] ),
						esc_attr( $args['label_submit'] )
					);

					/**
					 * Filters the submit button for the comment form to display.
					 *
					 * @since 4.2.0
					 *
					 * @param string $submit_button HTML markup for the submit button.
					 * @param array  $args          Arguments passed to `comment_form()`.
					 */
					$submit_button = apply_filters( 'comment_form_submit_button', $submit_button, $args );

					$submit_field = sprintf(
						$args['submit_field'],
						$submit_button,
						get_comment_id_fields( $post_id )				
					);
					//print_r(get_comment_id_fields( $post_id )); die;

					/*data- > amp-comments*/
					/**
					 * Filters the submit field for the comment form to display.
					 *
					 * The submit field includes the submit button, hidden fields for the
					 * comment form, and any wrapper markup.
					 *
					 * @since 4.2.0
					 *
					 * @param string $submit_field HTML markup for the submit field.
					 * @param array  $args         Arguments passed to comment_form().
					 */
					echo apply_filters( 'comment_form_submit_field', $submit_field, $args );

					/**
					 * Fires at the bottom of the comment form, inside the closing </form> tag.
					 *
					 * @since 1.5.0
					 *
					 * @param int $post_id The post ID.
					 */
					//do_action( 'comment_form', $post_id );
					?>
			 
					<input type="hidden" name="comment_parent" id="comment_parent" [value]="amp_comment_parent">
			<div submit-success>
				<template type="amp-mustache">
					{{response}}
				</template>
			</div>					 
			<div submit-error>
				<template type="amp-mustache">
			  	{{response}}
				</template>
			</div>
		</form>


			<?php endif; ?>
		</div><!-- #respond -->
		<?php
		/**
		 * Fires after the comment form.
		 *
		 * @since 3.0.0
		 */
		do_action( 'comment_form_after' );
	else :
		/**
		 * Fires after the comment form if comments are closed.
		 *
		 * @since 3.0.0
		 */
		do_action( 'comment_form_comments_closed' );
	endif;
}


if( isset($redux_builder_amp['wordpress-comments-support']) && comments_open() && true == $redux_builder_amp['wordpress-comments-support'] ) {
		add_action('ampforwp_after_header', 'amp_comments_form_state'); 
		function amp_comments_form_state() { ?>
			 <amp-state id="formData">
				      <script type="application/json">
				      { 
				        "submit": {
				          	"success": "cmnt-hide",
				          	"error"  : "show"
				        }
				      }
				      </script>
				    </amp-state>
					<?php 
		}
}


add_action('amp_post_template_css', 'amp_comments_add_styling'); 
function amp_comments_add_styling() {
	echo ".cmnt-hide{display:none}";
}