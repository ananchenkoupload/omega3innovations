<?php
// AMP Comments Settings

add_filter("redux/options/redux_builder_amp/sections", 'amp_comments_settings',100);

if(! function_exists('amp_comments_settings') ) {

	function amp_comments_settings( $sections ){
			$sections[] = array(
	        'title' => __('AMP Comments', 'redux-framework-demo'),
	        'icon' => 'el el-comment',
					'desc'  => '',
          	'id'         => 'ampforwp-cmt-subsection',
	        // 'fields' =>  amp_comments_create_controls(),
	        );

	        $sections[] = array(
           	  'title'      => __( 'Settings', 'accelerated-mobile-pages' ),
              'id'         => 'ampforwp-cmt-settings',
              'type'   => 'section',
              'subsection' => true,
              'fields'      => array(

              			array(
                        'id'       => 'ampforwp-cmt-section',
                        'type'     => 'select',
                        'title'    => __( 'Display comment form', 'accelerated-mobile-pages' ),
                        'options'  => array(
                             '1' => __('Above the comments list', 'accelerated-mobile-pages' ),
                             '2' => __('Below the comments list', 'accelerated-mobile-pages' ),
                         ),
                        'default'  => '2'
                       
                		),

                		array(
		                       'id'       => 'amp-cmt-reply',
		                       'type'     => 'switch',
		                       'title'    => __('Reply on specific comment', 'accelerated-mobile-pages'),
		                       'desc'  => __('Enable or disable the reply feature on specific comment','accelerated-mobile-pages'),
		                       'default'  => '0',
		                   ),
		                   array(
		                       'id'       => 'amp-cmt-Recaptcha',
		                       'type'     => 'switch',
		                       'title'    => __(' ReCAPTCHA v3 ', 'accelerated-mobile-pages'),
		                       'desc'  => __('Enable or disable the Recaptcha v3 feature','accelerated-mobile-pages'),
		                       'default'  => '0',
		                   ),	
		            array(
                        'id'            =>'amp-cmt-Recaptcha-site',
                        'type'          => 'text',
                       'title'         => esc_html__('Site Key','accelerated-mobile-pages'),
                       'tooltip-subtitle'  => esc_html__('You can get the site key from here ','accelerated-mobile-pages').'<a target="_blank" href="https://www.google.com/recaptcha/admin/create">'.esc_html__('form here','accelerated-mobile-pages').'</a>',
                        'default'       => '',
                        'required' => array(
                          array('amp-cmt-Recaptcha', '=' , '1')),
                    ),  
		            array(
                        'id'            =>'amp-cmt-Recaptcha-secerete',
                        'type'          => 'text',
                        'title'         => esc_html__('Secret Key','accelerated-mobile-pages'),
                        'tooltip-subtitle'  => esc_html__('You can get the Secret Key from here ','accelerated-mobile-pages').'<a target="_blank" href="https://www.google.com/recaptcha/admin/create">'.esc_html__('form here','accelerated-mobile-pages').'</a>',
                        'default'       => '',
                        'required' => array(
                          array('amp-cmt-Recaptcha', '=' , '1')),
                    ), 	                    

              ), );

	        $sections[] = array(
              		'title'      => __( 'Translation Panel', 'accelerated-mobile-pages' ),
              		'id'         => 'ampforwp-cmt-translation-panel',
              		'subsection' => true,
              		'type'   => 'section',
              		'fields'      => array(
						// Translation Panel
				 		array(
		                       'id' => 'translation',
		                       'type' => 'section',
		                       'title' => __('Translation', 'accelerated-mobile-pages'),
		                       'indent' => true,
		                    ),
		                    
		               array(
		                       'id'       => 'amp-comments-translator-leave-a-reply',
		                       'type'     => 'text',
		                       'title'    => __('Leave a Reply', 'accelerated-mobile-pages'),
		                       'default'  => __('Leave a Reply','accelerated-mobile-pages'),
		                       'required' => array( 'amp-use-pot', '=' , 0 )
		                   ),
		               array(
		                       'id'       => 'amp-comments-translator-specific-cmt-reply',
		                       'type'     => 'text',
		                       'title'    => __('Reply', 'accelerated-mobile-pages'),
		                       'default'  => __('Reply','accelerated-mobile-pages'),
		                       'required' => array( 'amp-use-pot', '=' , 0 )
		                   ),
		              
		                array(
		                       'id'       => 'amp-comments-Successful-message',
		                       'type'     => 'text',
		                       'title'    => __('Thank You Message', 'accelerated-mobile-pages'),
		                        'default'  => __('Thank you for submitting comment, we will review it and will get back to you.','accelerated-mobile-pages'),

		                       'required' => array( 'amp-use-pot', '=' , 0 )
		                   ),
		                    
		               array(
		                       'id'       => 'amp-comments-translator-cancel-reply-text',
		                       'type'     => 'text',
		                       'title'    => __('Cancel reply', 'accelerated-mobile-pages'),
		                       'default'  => __('Cancel reply','accelerated-mobile-pages'),
		                       'required' => array( 'amp-use-pot', '=' , 0 )
		                   ),
		               array(
		                       'id'       => 'amp-comments-translator-post-comment-text',
		                       'type'     => 'text',
		                       'title'    => __('Post Comment', 'accelerated-mobile-pages'),
		                       'default'  => __('Post Comment','accelerated-mobile-pages'),
		                   ),
		               array(
		                       'id'       => 'amp-comments-translator-loggedin-text',
		                       'type'     => 'text',
		                       'title'    => __('Logged in as', 'accelerated-mobile-pages'),
		                       'default'  => __('Logged in as','accelerated-mobile-pages'),
		                   ),
		               array(
		                       'id'       => 'amp-comments-translator-logout-text',
		                       'type'     => 'text',
		                       'title'    => __('Log Out', 'accelerated-mobile-pages'),
		                       'default'  => __('Log Out','accelerated-mobile-pages'),
		                   ),
		               array(
		                       'id'       => 'amp-comments-translator-name-text',
		                       'type'     => 'text',
		                       'title'    => __('Name', 'accelerated-mobile-pages'),
		                       'default'  => __('Name','accelerated-mobile-pages'),
		                   ),
		               array(
		                       'id'       => 'amp-comments-translator-email-text',
		                       'type'     => 'text',
		                       'title'    => __('Email', 'accelerated-mobile-pages'),
		                       'default'  => __('Email','accelerated-mobile-pages'),
		                   ),
		               array(
		                       'id'       => 'amp-comments-translator-website-text',
		                       'type'     => 'text',
		                       'title'    => __('Website', 'accelerated-mobile-pages'),
		                       'default'  => __('Website','accelerated-mobile-pages'),
		                   ),
		               array(
		                       'id'       => 'amp-comments-translator-your-email-address-text',
		                       'type'     => 'text',
		                       'title'    => __('Your email address will not be published.', 'accelerated-mobile-pages'),
		                       'default'  => __('Your email address will not be published.','accelerated-mobile-pages'),
		                   ),
		               array(
		                       'id'       => 'amp-comments-translator-required-fields-text',
		                       'type'     => 'text',
		                       'title'    => __('Required fields are marked', 'accelerated-mobile-pages'),
		                       'default'  => __('Required fields are marked','accelerated-mobile-pages'),
		                   ),
		               array(
		                       'id'       => 'amp-comments-translator-Comment-text',
		                       'type'     => 'text',
		                       'title'    => __('Comment', 'accelerated-mobile-pages'),
		                       'default'  => __('Comment','accelerated-mobile-pages'),
		                   ),
		         	),
			);


	    return $sections;
	}
}

/*function amp_comments_create_controls(){


	
						
		return $controls;
	}*/