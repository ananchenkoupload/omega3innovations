=== AMP Comments ===
Contributors: Ahmed Kaludi, Mohammed Kaludi
Tags: comments, amp, plugin
Requires at least: 3.0
Tested up to: 5.2
Stable tag: 2.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

You can now allow the same comment functionality on AMP.

== Description ==
While the Comments are an essential part of a website, it is also complex to implement in the AMP. So our team worked hard to get the same native comment functionality on AMP.

== Changelog ==
= 2.6 (06 Sep 2019) =
= Bug Fixed =
 * Minor Bug Fixed.

= 2.5 (31 Aug 2019) =
= Feature Added =
 * Recaptcha V3 Support Added.

= 2.4 (5 July 2019) =
= Bugs fixed: =
 * Reply to a comment issue resolved.
 * Validation error fixed.

= 2.3 (28 June 2019) =
= Bug fixed: =
 * Custom JS Validation error resolved.

= 2.2 (14 June 2019) =
= Bug fixed: =
 * Resolved WP debug notice error Trying to get property 'comment_status' of  
   non-object.

= 2.1 (10 May 2019) =
= Bug fixed: =

* Validation errors fixed.

= 2.0 (26 April 2019) =
* Added domain name for string translation of Amp-Comments fields label in Wpml.
* Admin page notice error resolved.

= 1.9 (14 Nov 2018) =
*  Added field validations for input fields.
*  Added server side validations for duplicate comments.

= 1.8 (30 July 2018) =
*  Latest version of amp-mustache extension .js added.

= 1.7 (5 May 2018) =
*  amp-state condition added

= 1.6 (2 May 2018) =
*  Translation for 'Reply' text added.
*  amp-state added to hide the field after submitting the form.
*  Priority number added to load the amp-bind script.

= 1.5 (20 April 2018) =
*  Upgrade Constants


= 1.4 (18 April 2018) =
*  Change some 'Update Proces' Code for better enhancement.

= 1.3.1 (06 April 2018) =
* Better Update Process Integrated
