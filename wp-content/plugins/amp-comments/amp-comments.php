<?php
/*
Plugin Name: AMP Comments
Plugin URI: https://ampforwp.com/
Description: Add Comments in to your AMP posts and pages
Version: 2.6
Author: Ahmed Kaludi, Mohammed Kaludi
Author URI: https://ampforwp.com/
Donate link: https://www.paypal.me/Kaludi/5
License: GPL2
*/
// Exit if accessed directly.

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! defined( 'AMP_COMMENTS_VERSION' ) ) {
    define( 'AMP_COMMENTS_VERSION', '2.6' );
}
// this is the URL our updater / license checker pings. This should be the URL of the site with EDD installed
define( 'AMP_COMMENTS_STORE_URL', 'https://accounts.ampforwp.com/' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

// the name of your product. This should match the download name in EDD exactly
define( 'AMP_COMMENTS_ITEM_NAME', 'AMP Comments' );

// the download ID. This is the ID of your product in EDD and should match the download ID visible in your Downloads list (see example below)
//define( 'AMPFORWP_ITEM_ID', 2502 );
// the name of the settings page for the license input to be displayed
define( 'AMP_COMMENTS_LICENSE_PAGE', 'amp-comments-license' );
if(! defined('AMP_COMMENTS_ITEM_FOLDER_NAME')){
    $folderName = basename(__DIR__);
    define( 'AMP_COMMENTS_ITEM_FOLDER_NAME', $folderName );
}

define('AMP_COMMENTS_DIR', plugin_dir_path( __FILE__ ));
define('AMP_COMMENTS_BASE_NAME', plugin_basename( __FILE__ ));


// If AMPforWP plugin is not activated then return
if ( ! class_exists( 'Ampforwp_Init' ) ) {
  return ;
}

require_once  AMP_COMMENTS_DIR .'comment-form.php' ;

add_filter( 'amp_post_template_data', 'ampforwp_add_comments_scripts', 12 );
function ampforwp_add_comments_scripts( $data ) {
    if ( is_singular() || ( function_exists('ampforwp_is_front_page') && ampforwp_is_front_page() ) || is_archive() ) {

        if ( empty( $data['amp_component_scripts']['amp-form'] ) ) {
            $data['amp_component_scripts']['amp-form'] = 'https://cdn.ampproject.org/v0/amp-form-0.1.js';
        }
            // Adding amp-bind Script
        if ( empty( $data['amp_component_scripts']['amp-bind'] ) ) {
            $data['amp_component_scripts']['amp-bind'] = 'https://cdn.ampproject.org/v0/amp-bind-0.1.js';
        }

        if( empty($data['amp_component_scripts']['amp-mustache'])) {
            $data['amp_component_scripts']['amp-mustache']='https://cdn.ampproject.org/v0/amp-mustache-latest.js';
         }
         global $redux_builder_amp;
        if(isset($redux_builder_amp['amp-cmt-Recaptcha']) && $redux_builder_amp['amp-cmt-Recaptcha'] == 1){ 

         if( empty($data['amp_component_scripts']['amp-recaptcha-input'])) {
            $data['amp_component_scripts']['amp-recaptcha-input']='https://cdn.ampproject.org/v0/amp-recaptcha-input-0.1.js';
         } 
       }
    }
  return $data;
}

/*add_action('amp_post_template_head', 'ampforwp_add_mustach_js');
function ampforwp_add_mustach_js() {
      if ( is_singular() ) { ?>
        <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
        <?php
      }
}*/
add_filter( 'ampforwp_standalone_mode_wrapper', 'amp_comments_standalone_mode_wrapper');

 function amp_comments_standalone_mode_wrapper($array){
  $array[] = 'ampforwp-cmt-settings';
  $array[] = 'ampforwp-cmt-translation-panel';
  return $array;
}
add_action('amp_post_template_css','ampforwp_comment_form_styling');
function ampforwp_comment_form_styling() { global $redux_builder_amp; ?>
<?php $color =  $redux_builder_amp['opt-color-rgba']['color']; ?>

#respond {  font-size:14px; display: block; margin: 2em auto 0.9em auto; width: 100%; }
.ampforwp-comments h3{ border-bottom: 1px solid #eee; padding-bottom: 8px; font-size: 14px; }
.comment-form-comment{ margin-bottom:0px; }
 #commentform #comment { border-color: #ccc; width: 100%; }
.comment-form-comment label{ margin-bottom: 5px; display: inline-block;}
.form-submit{ margin-top:15px; }
.comment-notes{font-size:10px}
.design_2_wrapper .form-submit #submit{  background: #555;
    border: 0;
    border-radius: 40px;
    padding: 11px 22px;
    font-size: 14px;
    color: #fff;
    line-height: 1;
    box-shadow: 0 0px 3px rgba(0,0,0,.04);
    border-radius: 80px; }

.design_3_wrapper .form-submit #submit{
    font-size: 14px; padding: 12px 22px 12px 22px; font-family: 'Roboto Slab', serif;
    border-radius: 2px; text-transform: uppercase; margin-left:0px; letter-spacing: 1px;
    color: #fff;border: 0;background: <?php global $redux_builder_amp;  echo $redux_builder_amp['amp-opt-color-rgba-colorscheme']['color']; ?>;
    }
.design_2_wrapper .ampforwp-comments{ background: #fff; padding: 10px 20px; box-shadow: 0 2px 3px rgba(0,0,0,.05); }

.design_1_wrapper .form-submit #submit{
border-style: solid;
    border-color: #c2c2c2;
    border-width: 1px 1px 2px;
    border-radius: 4px;
    background-color: transparent;
    font-size: 14px;
    font-weight: 600;
    text-align: center;
    line-height: 18px;
    margin: 0 auto;
    max-width: 200px;
    padding: 11px 16px;
    text-decoration: none;
}
#respond label{font-size:12px;color: #444; min-width: 55px; display: inline-block;}
#respond input{ padding: 8px 18px; border: 1px solid; border-color: #ccc;margin-left: 8px;}

.comments_list .hide{
  display:none;
}
.comments_list .show{
   display:block;
   position: relative;
}

.show #respond{
  text-align: left;
    margin: 0;
}
.comments_list .link{
    cursor:pointer;
    position: absolute;
    right: 0;
    top: 0px;
    z-index: 1;
    color: <?php global $redux_builder_amp; 
          include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
   
        if(isset($redux_builder_amp['amp-opt-color-rgba-link-design2']) && $redux_builder_amp['amp-opt-color-rgba-link-design2'] && $redux_builder_amp['amp-design-selector'] == 2) { echo $redux_builder_amp['amp-opt-color-rgba-link-design2'] ['rgba']; }
        elseif (isset($redux_builder_amp['amp-theme-anchor-link-color']) && $redux_builder_amp['amp-theme-anchor-link-color'] && $redux_builder_amp['amp-design-selector'] == 3 &&   is_plugin_active( 'AMP-Theme/ampforwp-custom-theme.php') ) {
            echo $redux_builder_amp['amp-theme-anchor-link-color'] ['color']; }
      elseif(isset($redux_builder_amp['amp-opt-color-rgba-link']) && $redux_builder_amp['amp-opt-color-rgba-link'] && $redux_builder_amp['amp-design-selector'] == 3) {
          echo $redux_builder_amp['amp-opt-color-rgba-link'] ['rgba']; }
        elseif (isset($redux_builder_amp['amp-opt-color-rgba-link-design1']) && $redux_builder_amp['amp-opt-color-rgba-link-design1'] && $redux_builder_amp['amp-design-selector'] == 1) {
          echo $redux_builder_amp['amp-opt-color-rgba-link-design1'] ['rgba']; } 
          ?>;
}
.ampforwp-comment-reply {
  text-align: right;
    display: inline-block;
    width: 100%;
}
.ampforwp-comment-reply button{
  border: #fff;
  background: #fff;
  font-size: 12px;
  color: <?php global $redux_builder_amp; 
        if(isset($redux_builder_amp['amp-opt-color-rgba-link-design2']) && $redux_builder_amp['amp-opt-color-rgba-link-design2'] && $redux_builder_amp['amp-design-selector'] == 2) { echo $redux_builder_amp['amp-opt-color-rgba-link-design2'] ['rgba']; }
        elseif (isset($redux_builder_amp['amp-theme-anchor-link-color']) && $redux_builder_amp['amp-theme-anchor-link-color'] && $redux_builder_amp['amp-design-selector'] == 3 &&   is_plugin_active( 'AMP-Theme/ampforwp-custom-theme.php') ) {
            echo $redux_builder_amp['amp-theme-anchor-link-color'] ['color']; }
      elseif(isset($redux_builder_amp['amp-opt-color-rgba-link']) && $redux_builder_amp['amp-opt-color-rgba-link'] && $redux_builder_amp['amp-design-selector'] == 3) {
          echo $redux_builder_amp['amp-opt-color-rgba-link'] ['rgba']; }
        elseif (isset($redux_builder_amp['amp-opt-color-rgba-link-design1']) && $redux_builder_amp['amp-opt-color-rgba-link-design1'] && $redux_builder_amp['amp-design-selector'] == 1) {
          echo $redux_builder_amp['amp-opt-color-rgba-link-design1'] ['rgba']; } ?>;  
}

<?php
}

//54. JetPack Invalidating when AMP comments and JetPack comments Enabled
add_action('pre_amp_render_post', 'ampforwp_making_jetpack_comments_validated' );
function ampforwp_making_jetpack_comments_validated() {
  include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
  if( ampforwp_is_amp_endpoint() && is_plugin_active('amp-comments/amp-comments.php') ) {
    // Selfishly remove everything from the existing comment form
    remove_all_actions( 'comment_form_before' );
    remove_all_actions( 'comment_form_after'  );
  }
}

add_filter('ampforwp_the_content_last_filter','amp_comments_minify_html_output');
function amp_comments_minify_html_output($content_buffer){
  $copy_date = preg_replace("/style=\"(.*?)\"/m", "", $content_buffer);
  return $copy_date;
}
// Settings
require 'amp-comments-settings.php';

function ampforwp_custom_reply_link($link_html, $args, $comment, $post) {
                    global $redux_builder_amp;
           $comment_id = get_comment_ID();
           $comment_author = get_comment_author();
           $post_url = user_trailingslashit(get_permalink());
           $amp_cmt_url = trailingslashit($post_url).AMPFORWP_AMP_QUERY_VAR;
                     $translate_reply = esc_html__( $redux_builder_amp['amp-comments-translator-specific-cmt-reply'], 'accelerated-mobile-pages');
                    
             $link_html =  '<button rel="nofollow" class="comment-reply-link" 
               on="tap:AMP.setState({amp_comment_parent: '. $comment_id .' })"
                 aria-label="Reply to '. htmlspecialchars($comment_author) .'">'.$translate_reply.'</button>
                 ';

          return $link_html ;
            // Return an html string here
 }
global $redux_builder_amp;
if( isset($redux_builder_amp['amp-cmt-reply']) &&  1 == $redux_builder_amp['amp-cmt-reply']) {
add_action('ampforwp_reply_comment_form','ampforwp_comment_reply_display',10,3);
if(! function_exists('ampforwp_comment_reply_display') ) {
function ampforwp_comment_reply_display($comment, $args, $depth){ ?>

<div class="ampforwp-comment-reply" id="comment-<?php comment_ID() ?>">
               <?php 
               add_filter( 'comment_reply_link', 'ampforwp_custom_reply_link', 10, 4 );
        
                $ampforwp_reply_link = comment_reply_link(array_merge( $args, array(
                   'reply_text' =>  'Reply',
                   'depth' => $depth,
                   'max_depth' => $args['max_depth']
               )));
                echo $ampforwp_reply_link;

                    $comment_id = get_comment_ID();
                        global $redux_builder_amp;
                        $cancel_reply_translate = esc_html__( $redux_builder_amp['amp-comments-translator-cancel-reply-text'], 'accelerated-mobile-pages');
                      echo '<div class="hide" [class]="amp_comment_parent=='. $comment_id .'? \'show\':\'hide\'">
                        <span class="link" role="cancel_reply" tabindex="1" on="tap:AMP.setState({amp_comment_parent: \'0\' })">'.$cancel_reply_translate.'</span>
                        ';
                        do_action('ampforwp_before_comment_hook');
                      do_action('ampforwp_after_comment_hook');

                      echo '</div>';
                           ?>
</div>
<?php } } }
/* Google Captcha removed */
/*if( isset($_POST['amp_cptch_hidden']) && $_POST['amp_cptch_hidden'] ){ 

    remove_action( 'init', 'gglcptch_init',9);
    remove_action( 'pre_comment_on_post', 'gglcptch_commentform_check',9);
}

add_action( 'amp_init', 'ampforwp_gglcptch' );
function ampforwp_gglcptch(){
    add_action( 'wp', 'ampforwp_endpoint_action_with_gglcptch' );
}

function ampforwp_endpoint_action_with_gglcptch() {

    $ampforwp_is_amp_endpoint = ampforwp_is_amp_endpoint();
 
    if ( $ampforwp_is_amp_endpoint ) {
           remove_action( 'comment_form_after_fields', 'gglcptch_commentform_display' );
           remove_action( 'comment_form_logged_in_after', 'gglcptch_commentform_display' );
     
        }         
}*/
/* Google Captcha removed -END HERE- */


add_action('init', 'ampforwp_cf7_recaptcha_removed2',9);
function ampforwp_cf7_recaptcha_removed2(){

   //  var_dump($_POST); die;

     // remove_action( 'init', 'gglcptch_init');

    $url_path = trim(parse_url(add_query_arg(array()), PHP_URL_PATH),'/' );

     
    $explode_path = explode('/', $url_path);
    
    if ( 'amp' === end( $explode_path) || ( isset($_POST['amp_cptch_hidden']) && $_POST['amp_cptch_hidden'] == 1 ) ) {  

      remove_action( 'init', 'gglcptch_init',10);
      remove_filter( 'gglcptch_display_recaptcha', 'gglcptch_display_custom', 10, 2 );
      remove_filter( 'gglcptch_verify_recaptcha', 'gglcptch_check_custom', 10, 3 );
      global $redux_builder_amp;
      if($redux_builder_amp['amp-cmt-Recaptcha']){
        add_action( 'pre_comment_on_post', 'amp_cmt_gglcptch_check_custom', 10,1 );
      }
      

     // remove_action( 'wpcf7_init', 'wpcf7_recaptcha_add_form_tag_recaptcha',10 ); 
    }
} 

// Removing Antispam Bee as it adds Javascript in AMP
add_action('init', 'remove_antispam_bee');
function remove_antispam_bee(){
remove_action('template_redirect',array('Antispam_Bee','prepare_comment_field'));
}

function amp_submit_ajax_comment(){
  global $redux_builder_amp;
  header("access-control-allow-credentials:true");
  header("access-control-allow-headers:Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token");
  header("Access-Control-Allow-Origin:".$_SERVER['HTTP_ORIGIN']);
  $siteUrl = parse_url(  get_site_url() );
  header("AMP-Access-Control-Allow-Source-Origin:".$siteUrl['scheme'] . '://' . $siteUrl['host']);
  header("access-control-expose-headers:AMP-Access-Control-Allow-Source-Origin");
  header("Content-Type:application/json;charset=utf-8");
  $comment = wp_handle_comment_submission( wp_unslash( $_POST ) );
  $text_data = '';
  if ($redux_builder_amp['amp-comments-Successful-message']){
    $text_data = esc_html__( $redux_builder_amp['amp-comments-Successful-message'], 'accelerated-mobile-pages');
    
  }
  //var_dump(( $comment ) );
        
  if ( is_wp_error( $comment ) ) {
    $error_data = intval( $comment->get_error_data() );
    if ( ! empty( $error_data ) ) {
      $comment_html = $comment->get_error_message();
      $comment_html = str_replace("&#8217;","'",$comment_html);
      $comment_status = array('response' => $comment_html );
      echo json_encode($comment_status);
      die;
      // wp_die( '<p>' . $comment->get_error_message() . '</p>', __( 'Comment Submission Failure' ), array( 'response' => $error_data, 'back_link' => true ) );
    } else {
      wp_die( 'Unknown error' );
    }
  }
 
  $user = wp_get_current_user();
  do_action('set_comment_cookies', $comment, $user);
 
  $comment_depth = 1;
  $comment_parent = $comment->comment_parent;
  while( $comment_parent ){
    $comment_depth++;
    $parent_comment = get_comment( $comment_parent );
    $comment_parent = $parent_comment->comment_parent;
  }
 
  $GLOBALS['comment'] = $comment;
  $GLOBALS['comment_depth'] = $comment_depth;
  $comment_html = $text_data;
  $comment_status = array('response' => $comment_html );
  echo json_encode($comment_status);
  //echo $comment_html;
  die;
}


require_once dirname( __FILE__ ) . '/updater/EDD_SL_Plugin_Updater.php';

// Check for updates
function amp_comments_plugin_updater() {

    // retrieve our license key from the DB
    //$license_key = trim( get_option( 'amp_ads_license_key' ) );
    $selectedOption = get_option('redux_builder_amp',true);
    $license_key = '';//trim( get_option( 'amp_ads_license_key' ) );
    $pluginItemName = '';
    $pluginItemStoreUrl = '';
    $pluginstatus = '';
    if( isset($selectedOption['amp-license']) && "" != $selectedOption['amp-license'] && isset($selectedOption['amp-license'][AMP_COMMENTS_ITEM_FOLDER_NAME])){

       $pluginsDetail = $selectedOption['amp-license'][AMP_COMMENTS_ITEM_FOLDER_NAME];
       $license_key = $pluginsDetail['license'];
       $pluginItemName = $pluginsDetail['item_name'];
       $pluginItemStoreUrl = $pluginsDetail['store_url'];
       $pluginstatus = $pluginsDetail['status'];
    }
    
    // setup the updater
    $edd_updater = new AMP_COMMENTS_EDD_SL_Plugin_Updater( AMP_COMMENTS_STORE_URL, __FILE__, array(
            'version'   => AMP_COMMENTS_VERSION,                // current version number
            'license'   => $license_key,                        // license key (used get_option above to retrieve from DB)
            'license_status'=>$pluginstatus,
            'item_name' => AMP_COMMENTS_ITEM_NAME,          // name of this plugin
            'author'    => 'Mohammed Kaludi',                   // author of this plugin
            'beta'      => false,
        )
    );
}
add_action( 'admin_init', 'amp_comments_plugin_updater', 0 );

// Notice to enter license key once activate the plugin

$path = plugin_basename( __FILE__ );
    add_action("after_plugin_row_{$path}", function( $plugin_file, $plugin_data, $status ) {
        global $redux_builder_amp;
        if(! defined('AMP_COMMENTS_ITEM_FOLDER_NAME')){
        $folderName = basename(__DIR__);
            define( 'AMP_COMMENTS_ITEM_FOLDER_NAME', $folderName );
        }
        $pluginstatus = '';
        if(isset($redux_builder_amp['amp-license'][AMP_COMMENTS_ITEM_FOLDER_NAME])){
        $pluginsDetail = $redux_builder_amp['amp-license'][AMP_COMMENTS_ITEM_FOLDER_NAME];
        $pluginstatus = $pluginsDetail['status'];
}
        if(empty($redux_builder_amp['amp-license'][AMP_COMMENTS_ITEM_FOLDER_NAME]['license'])){
            echo "<tr class='active'><td>&nbsp;</td><td colspan='2'><a href='".esc_url(  self_admin_url( 'admin.php?page=amp_options&tabid=opt-go-premium' )  )."'>Please enter the license key</a> to get the <strong>latest features</strong> and <strong>stable updates</strong></td></tr>";
                }elseif($pluginstatus=="valid"){

             $update_cache = get_site_transient( 'update_plugins' );
            $update_cache = is_object( $update_cache ) ? $update_cache : new stdClass();
            if(isset($update_cache->response[ AMP_COMMENTS_ITEM_FOLDER_NAME ]) 
                && empty($update_cache->response[ AMP_COMMENTS_ITEM_FOLDER_NAME ]->download_link) 
              ){
               unset($update_cache->response[ AMP_COMMENTS_ITEM_FOLDER_NAME ]);
            }
            set_site_transient( 'update_plugins', $update_cache );
            
        }
    }, 10, 3 );

add_action( 'wp_ajax_ampajaxcomments', 'amp_submit_ajax_comment' ); 
add_action( 'wp_ajax_nopriv_ampajaxcomments', 'amp_submit_ajax_comment' ); 

// recaptcha




if ( ! function_exists( 'amp_cmt_gglcptch_check_custom' ) ) {
  function amp_cmt_gglcptch_check_custom( $comment_post_ID ) {
    $post_response = $_POST['g-recaptcha-response'];
    $gglcptch_remote_addr = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP );
    global $redux_builder_amp;
    $secretkey = $redux_builder_amp['amp-cmt-Recaptcha-secerete']; 
    $args = array(
          'body' => array(
            'secret'   => $secretkey,
            'response' => stripslashes( esc_html( $post_response ) ),
            'remoteip' => $gglcptch_remote_addr,
          ),
          'sslverify' => false
        );
    $resp = wp_remote_post( 'https://www.google.com/recaptcha/api/siteverify', $args );
    $response =  json_decode( wp_remote_retrieve_body( $resp ), true );
         
    $expected_Response_Msg = array(
          'missing-input-secret' => 'The secret parameter is missing.',
          'invalid-input-secret'=> 'The secret parameter is invalid or malformed.',
          'missing-input-response'=> 'The response parameter is missing.',
          'invalid-input-response'=> 'The response parameter is invalid or malformed.',
          'bad-request'=> 'The request is invalid or malformed.',
          'timeout-or-duplicate'=> 'The response is no longer valid: either is too old or has been used previously.',

            );
    $result = array(
              'response' => false,
              'reason' => 'My changes'
          );
    if ( isset( $response['success'] ) && !! $response['success'] ) {
      if ($response['score'] > 0.5 ) {
                    $result = array(
                        'response' => true,
                        'reason' => 'RECAPTCHA_SUCCESS'
                    );
       }
    }else {
        $result = array(
              'response' => false,
              'reason' => $response['error-codes']
          );
       }
    if(!$result['response'] ){

      //return new WP_Error('RECAPCTHA_INVALID', 'invalid recaptcha value',403 );
      header("access-control-allow-credentials:true");
      header("access-control-allow-headers:Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token");
      header("Access-Control-Allow-Origin:".$_SERVER['HTTP_ORIGIN']);
      $siteUrl = parse_url(  get_site_url() );
      header("AMP-Access-Control-Allow-Source-Origin:".$siteUrl['scheme'] . '://' . $siteUrl['host']);
      header("access-control-expose-headers:AMP-Access-Control-Allow-Source-Origin");
      header("Content-Type:application/json;charset=utf-8");
      $comment_status = array('response' => $expected_Response_Msg[$response['error-codes']] );
      echo json_encode($comment_status);
     $sapi_type = php_sapi_name();
    if (substr($sapi_type, 0, 3) == 'cgi')
        header("Status: 404 Not Found");
    else
        header("HTTP/1.1 404 Not Found");
  die;
    }

  }
}