<?php
$prefix = su_get_shortcode_prefix();
$shortcodes = su_get_all_shortcodes();
	foreach ( $shortcodes as $id => $shortcode ) {
		if ( isset( $shortcode['callback'] ) && is_callable( $shortcode['callback'] ) ) {
			$callback = $shortcode['callback'];
		}elseif ( isset( $shortcode['function'] ) && is_callable( $shortcode['function'] ) ) {
			$callback = $shortcode['function'];
		}else {
			continue;
		}
	
		su_remove_shortcode( $id );
		switch ($id) {
			case 'heading':
				$callback = 'amp_shortcode_heading';
			break;
			case 'tabs':
				$callback = 'amp_shortcode_tabs';
			break;
			case 'spoiler':
				$callback = 'amp_shortcode_spoiler';
			break;
			case 'accordion':
				$callback = 'amp_shortcode_accordion';
			break;
			case 'divider':
				$callback = 'amp_shortcode_divider';
			break;
			case 'spacer':
				$callback = 'amp_shortcode_spacer';
			break;
			case 'highlight':
				$callback = 'amp_shortcode_highlight';
			break;
			case 'label':
				$callback = 'amp_shortcode_label';
			break;
			case 'quote':
				$callback = 'amp_shortcode_quote';
			break;
			case 'pullquote':
				$callback = 'amp_shortcode_pullquote';
			break;
			case 'dropcap':
				$callback = 'amp_shortcode_dropcap';
			break;
			case 'frame':
				$callback = 'amp_shortcode_frame';
			break;
			case 'row':
				$callback = 'amp_shortcode_row';
			break;
			case 'column':
				$callback = 'amp_shortcode_column';
			break;
			case 'list':
				$callback = 'amp_shortcode_list';
			break;
			case 'button':
				$callback = 'amp_shortcode_button';
			break;
			case 'service':
				$callback = 'amp_shortcode_service';
			break;
			case 'box':
				$callback = 'amp_shortcode_box';
			break;
			case 'note':
				$callback = 'amp_shortcode_note';
			break;
			case 'expand':
				$callback = 'amp_shortcode_expand';
			break;
			case 'lightbox':
				$callback = 'amp_shortcode_lightbox';
			break;
			case 'lightbox_content':
				$callback = 'amp_shortcode_lightbox_content';
			break;
			case 'private':
				$callback = 'amp_shortcode_private';
			break;
			case 'youtube':
				$callback = 'amp_shortcode_youtube';
			break;
			case 'tooltip':
				$callback = 'amp_shortcode_tooltip';
			break;
			case 'youtube_advanced':
				$callback = 'amp_shortcode_youtube_advanced';
			break;
			case 'vimeo':
				$callback = 'amp_shortcode_vimeo';
			break;
			case 'dailymotion':
				$callback = 'amp_shortcode_dailymotion';
			break;
			case 'screenr':
				$callback = 'amp_shortcode_screenr';
			break;
			case 'audio':
				$callback = 'amp_shortcode_audio';
			break;
			case 'video':
				$callback = 'amp_shortcode_video';
			break;
			case 'table':
				$callback = 'amp_shortcode_table';
			break;
			case 'permalink':
				$callback = 'amp_shortcode_permalink';
			break;
			case 'guests':
				$callback = 'amp_shortcode_guests';
			break;
			case 'feed':
				$callback = 'amp_shortcode_feed';
			break;
			case 'subpages':
				$callback = 'amp_shortcode_subpages';
			break;
			case 'siblings':
				$callback = 'amp_shortcode_siblings';
			break;
			case 'menu':
				$callback = 'amp_shortcode_menu';
			break;
			case 'document':
				$callback = 'amp_shortcode_document';
			break;
			case 'gmap':
				$callback = 'amp_shortcode_gmap';
			break;
			case 'slider':
				$callback = 'amp_shortcode_slider';
			break;
			case 'carousel':
				$callback = 'amp_shortcode_carousel';
			break;
			case 'custom_gallery':
				$callback = 'amp_shortcode_custom_gallery';
			break;
			case 'post':
				$callback = 'amp_shortcode_post';
			break;
			case 'posts':
				$callback = 'amp_shortcode_posts';
			break;
			case 'dummy_text':
				$callback = 'amp_shortcode_dummy_text';
			break;
			case 'dummy_image':
				$callback = 'amp_shortcode_dummy_image';
			break;
			case 'animate':
				$callback = 'amp_shortcode_animate';
			break;
			case 'user':
				$callback = 'amp_shortcode_user';
			break;
			case 'template':
				$callback = 'amp_shortcode_template';
			break;
			default:
				$callback = $callback;
			break;
		}
		
		add_shortcode( $prefix . $id, $callback );
	}