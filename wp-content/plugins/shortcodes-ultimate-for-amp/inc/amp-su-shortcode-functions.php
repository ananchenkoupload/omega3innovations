<?php

function amp_shortcode_heading( $atts = null, $content = null ){
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'style'  => 'default',
			'size'   => 13,
			'align'  => 'center',
			'margin' => '20',
			'class'  => ''
		), $atts, 'heading' );
	$amp_su_widget_data['heading'] = $atts;
	
	return '<div class="su-heading su-heading-style-' . $atts['style'] . ' su-heading-align-' . $atts['align'] . su_get_css_class( $atts ) . ' amp-su-heading-inline" ><div class="su-heading-inner">' . do_shortcode( $content ) . '</div></div>';
}

$shortcodes_ultimate_global_tabs = array();
$shortcodes_ultimate_global_tabs_count = 0;
function amp_shortcode_tabs( $atts = null, $content = null ){
	global $shortcodes_ultimate_global_tabs, $shortcodes_ultimate_global_tabs_count;
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'active'   => 1,
			'vertical' => 'no',
			'style'    => 'default', // 3.x
			'class'    => ''
		), $atts, 'tabs' );
	$amp_su_widget_data['tabs'] = $atts;
	if ( $atts['style'] === '3' ) {
		$atts['vertical'] = 'yes';
	}
	if ( $atts['vertical'] === 'yes' ) {
		$atts['class'] .= ' su-tabs-vertical';
	}
	do_shortcode( $content );
	$tabs = array();
	$panes = array();
	if ( ! is_array( $shortcodes_ultimate_global_tabs ) ) {
		return;
	}
	if ( $shortcodes_ultimate_global_tabs_count < $atts['active'] ) {
		$atts['active'] = $shortcodes_ultimate_global_tabs_count;
	}
	
	$i=1;
	$active_tab = (string) $atts['active'];
	foreach ( $shortcodes_ultimate_global_tabs as $tab ) {
		if($active_tab == $i){
			$selected = 'selected';
		}else{
			$selected = '';
		}
		$tabs[] = '<span role="tab" '.$selected.' option="'.$i.'" class="tabButton ' . su_get_css_class( $tab ) . $tab['disabled'] . '"' . $tab['anchor'] . $tab['url'] . $tab['target'] . '>' . su_do_attribute( $tab['title'] ) . '</span>';

		$panes[] = '<div role="tabpanel" class="tabContent su-tabs-pane su-clearfix' . su_get_css_class( $tab ) . '">' . $tab['content'] . '</div>';
		$i++;
	}
	
	$all_tabs =array_combine($tabs,$panes);
	$tabsPanes = array();
	foreach($all_tabs as $tab => $pane){
		$tabsPanes[] = $tab.''.$pane;
	}
	$output = '<amp-selector role="tablist" layout="container" class="ampTabContainer su-tabs su-tabs-style-' . $atts['style'] . su_get_css_class( $atts ) . '" data-active="' . (string) $atts['active'] . '">' . implode( '', $tabsPanes ) . '</amp-selector>';
	// Reset tabs
	$shortcodes_ultimate_global_tabs = array();
	$shortcodes_ultimate_global_tabs_count = 0;
	return $output;
}
function amp_shortcode_tab( $atts = null, $content = null ){
	global $shortcodes_ultimate_global_tabs, $shortcodes_ultimate_global_tabs_count;
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'title'    => __( 'Tab title', 'shortcodes-ultimate' ),
			'disabled' => 'no',
			'anchor'   => '',
			'url'      => '',
			'target'   => 'blank',
			'class'    => ''
		), $atts, 'tab' );
	$amp_su_widget_data['tab'] = $atts;
	$x = $shortcodes_ultimate_global_tabs_count;
	$shortcodes_ultimate_global_tabs[ $x ] = array(
		'title'    => $atts['title'],
		'content'  => do_shortcode( $content ),
		'disabled' => ( $atts['disabled'] === 'yes' ) ? ' su-tabs-disabled' : '',
		'anchor'   => ( $atts['anchor'] ) ? ' data-anchor="' . str_replace( array( ' ', '#' ), '', sanitize_text_field( $atts['anchor'] ) ) . '"' : '',
		'url'      => ' data-url="' . $atts['url'] . '"',
		'target'   => ' data-target="' . $atts['target'] . '"',
		'class'    => $atts['class']
	);

	$shortcodes_ultimate_global_tabs_count++;
}

function amp_shortcode_spoiler( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'title'  => __( 'Spoiler title', 'shortcodes-ultimate' ),
			'open'   => 'no',
			'style'  => 'default',
			'icon'   => 'plus',
			'anchor' => '',
			'class'  => ''
		), $atts, 'spoiler' );
	$amp_su_widget_data['spoiler'] = $atts;
	$atts['style'] = str_replace( array( '1', '2' ), array( 'default', 'fancy' ), $atts['style'] );
	$atts['anchor'] = ( $atts['anchor'] ) ? ' data-anchor="' . str_replace( array( ' ', '#' ), '', sanitize_text_field( $atts['anchor'] ) ) . '"' : '';
	if ( $atts['open'] !== 'yes' ) $atts['class'] .= ' su-spoiler-closed';
	do_action( 'su/shortcode/spoiler', $atts );

	return '<amp-accordion animate><section class="su-spoiler su-spoiler-style-' . $atts['style'] . ' su-spoiler-icon-' . $atts['icon'] . su_get_css_class( $atts ) . '"' . $atts['anchor'] . '>
		<h4 class="su-spoiler-title"><span class="su-spoiler-icon"></span>' . su_do_attribute( $atts['title'] ) . '</h4>
		<div class="su-clearfix">' . su_do_nested_shortcodes( $content, 'spoiler' ) . '</div>
		</section></amp-accordion>';
}

function amp_shortcode_accordion( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array( 'class' => '' ), $atts, 'accordion' );
	do_action( 'su/shortcode/accordion', $atts );

	$amp_su_widget_data['accordion'] = $atts;

	$accordionHtml = do_shortcode( $content );
	$string =  preg_replace('/<amp-accordion[^>]*>/i', '', $accordionHtml);
	$ampHtml = preg_replace('/<\/amp-accordion>/i', '', $string);
	return '<amp-accordion expand-single-section animate class="su-accordion' . su_get_css_class( $atts ) . '">' . $ampHtml . '</amp-accordion>';
}

function amp_shortcode_divider( $atts = null, $content = null ) {
		global $amp_su_widget_data;
		$atts = shortcode_atts( array(
			'top'           => 'yes',
			'text'          => __( 'Go to top', 'shortcodes-ultimate' ),
			'style'         => 'default',
			'divider_color' => '#999999',
			'link_color'    => '#999999',
			'size'          => '3',
			'margin'        => '15',
			'class'         => ''
		), $atts, 'divider' );
		$amp_su_widget_data['divider'] = $atts;
		// Prepare TOP link
		$top = $atts['top'] === 'yes'
		? '<a href="#" style="color:' . $atts['link_color'] . '">' . su_do_attribute( $atts['text'] ) . '</a>' : '';
		return '<div class="su-divider su-divider-style-' . $atts['style'] . su_get_css_class( $atts ) . ' amp-su-divider-inline">' . $top . '</div>';
}
function amp_shortcode_spacer( $atts = null, $content = null ) {
		global $amp_su_widget_data;
		$atts = shortcode_atts( array(
			'size'  => '20',
			'class' => ''
		), $atts, 'spacer' );
		$amp_su_widget_data['spacer'] = $atts;
		return '<div class="su-spacer' . su_ecssc( $atts ) . ' amp-su-spacer-inline" ></div>';
}

function amp_shortcode_highlight( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'background' => '#ddff99',
			'bg'         => null, // 3.x
			'color'      => '#000000',
			'class'      => ''
		), $atts, 'highlight' );
	$amp_su_widget_data['highlight'] = $atts;
	if ( $atts['bg'] !== null ) {
		$atts['background'] = $atts['bg'];
	}
	return '<span class="su-highlight amp-su-highlight-inline" >&nbsp;' . do_shortcode( $content ) . '&nbsp;</span>';
}

function amp_shortcode_label( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'type'  => 'default',
			'style' => null, // 3.x
			'class' => ''
		), $atts, 'label' );
	$amp_su_widget_data['label'] = $atts;
	if ( $atts['style'] !== null ) $atts['type'] = $atts['style'];
	return '<span class="su-label su-label-type-' . $atts['type'] . su_get_css_class( $atts ) . '">' . do_shortcode( $content ) . '</span>';
}

function amp_shortcode_quote( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'style' => 'default',
			'cite'  => false,
			'url'   => false,
			'class' => ''
		), $atts, 'quote' );
	$amp_su_widget_data['quote'] = $atts;
	$cite_link = ( $atts['url'] && $atts['cite'] ) ? '<a href="' . $atts['url'] . '" target="_blank">' . $atts['cite'] . '</a>'	: $atts['cite'];
	$cite = ( $atts['cite'] ) ? '<span class="su-quote-cite">' . $cite_link . '</span>' : '';
	$cite_class = ( $atts['cite'] ) ? ' su-quote-has-cite' : '';
	
	return '<div class="su-quote su-quote-style-' . $atts['style'] . $cite_class . su_get_css_class( $atts ) . '"><div class="su-quote-inner su-clearfix">' . do_shortcode( $content ) . su_do_attribute( $cite ) . '</div></div>';
}
function amp_shortcode_pullquote( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'align' => 'left',
			'class' => ''
		), $atts, 'pullquote' );
	$amp_su_widget_data['pullquote'] = $atts;
	return '<div class="su-pullquote su-pullquote-align-' . $atts['align'] . su_get_css_class( $atts ) . '">' . do_shortcode( $content ) . '</div>';
}
function amp_shortcode_dropcap( $atts = null, $content = null ) {
		global $amp_su_widget_data;
		$atts = shortcode_atts( array(
				'style' => 'default',
				'size'  => 3,
				'class' => ''
			), $atts, 'dropcap' );
		$amp_su_widget_data['dropcap'] = $atts;
		$atts['style'] = str_replace( array( '1', '2', '3' ), array( 'default', 'light', 'default' ), $atts['style'] ); // 3.x
		// Calculate font-size
		$em = $atts['size'] * 0.5 . 'em';

		return '<span class="su-dropcap su-dropcap-style-' . $atts['style'] . su_get_css_class( $atts ) . ' amp-su-dropcap-inline" >' . do_shortcode( $content ) . '</span>';
}

function amp_shortcode_frame( $atts = null, $content = null ) {
	global $amp_su_widget_data;
		$atts = shortcode_atts( array(
				'style' => 'default',
				'align' => 'left',
				'class' => ''
			), $atts, 'frame' );
		$amp_su_widget_data['frame'] = $atts;
		return '<span class="su-frame su-frame-align-' . $atts['align'] . ' su-frame-style-' . $atts['style'] . su_get_css_class( $atts ) . '"><span class="su-frame-inner">' . do_shortcode( $content ) . '</span></span>';
}
function amp_shortcode_row( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array( 'class' => '' ), $atts );
	$amp_su_widget_data['row'] = $atts;
	return '<div class="su-row' . su_get_css_class( $atts ) . '">' . su_do_nested_shortcodes( $content, 'row' ) . '</div>';
}
function amp_shortcode_column( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'size'   => '1/2',
			'center' => 'no',
			'last'   => null,
			'class'  => ''
		), $atts, 'column' );
	$amp_su_widget_data['column'] = $atts;
	if ( $atts['last'] !== null && $atts['last'] == '1' ) $atts['class'] .= ' su-column-last';
	if ( $atts['center'] === 'yes' ) $atts['class'] .= ' su-column-centered';
	
	return '<div class="su-column su-column-size-' . str_replace( '/', '-', $atts['size'] ) . su_get_css_class( $atts ) . '"><div class="su-column-inner su-clearfix">' . su_do_nested_shortcodes( $content, 'column' ) . '</div></div>';
}
function amp_shortcode_list( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
		'icon' => 'icon: star',
		'icon_color' => '#333',
		'style' => null,
		'class' => ''
	), $atts, 'list' );
	$amp_su_widget_data['list'] = $atts;
	// Backward compatibility // 4.2.3+
	if ( $atts['style'] !== null ) {
		switch ( $atts['style'] ) {
		case 'star':
			$atts['icon'] = 'icon: star';
			$atts['icon_color'] = '#ffd647';
			break;
		case 'arrow':
			$atts['icon'] = 'icon: arrow-right';
			$atts['icon_color'] = '#00d1ce';
			break;
		case 'check':
			$atts['icon'] = 'icon: check';
			$atts['icon_color'] = '#17bf20';
			break;
		case 'cross':
			$atts['icon'] = 'icon: remove';
			$atts['icon_color'] = '#ff142b';
			break;
		case 'thumbs':
			$atts['icon'] = 'icon: thumbs-o-up';
			$atts['icon_color'] = '#8a8a8a';
			break;
		case 'link':
			$atts['icon'] = 'icon: external-link';
			$atts['icon_color'] = '#5c5c5c';
			break;
		case 'gear':
			$atts['icon'] = 'icon: cog';
			$atts['icon_color'] = '#ccc';
			break;
		case 'time':
			$atts['icon'] = 'icon: time';
			$atts['icon_color'] = '#a8a8a8';
			break;
		case 'note':
			$atts['icon'] = 'icon: edit';
			$atts['icon_color'] = '#f7d02c';
			break;
		case 'plus':
			$atts['icon'] = 'icon: plus-sign';
			$atts['icon_color'] = '#61dc3c';
			break;
		case 'guard':
			$atts['icon'] = 'icon: shield';
			$atts['icon_color'] = '#1bbe08';
			break;
		case 'event':
			$atts['icon'] = 'icon: bullhorn';
			$atts['icon_color'] = '#ff4c42';
			break;
		case 'idea':
			$atts['icon'] = 'icon: sun';
			$atts['icon_color'] = '#ffd880';
			break;
		case 'settings':
			$atts['icon'] = 'icon: cogs';
			$atts['icon_color'] = '#8a8a8a';
			break;
		case 'twitter':
			$atts['icon'] = 'icon: twitter-sign';
			$atts['icon_color'] = '#00ced6';
			break;
		}
	}
	if ( strpos( $atts['icon'], 'icon:' ) !== false ) {
		$atts['icon'] = '<i class="fa fa-' . trim( str_replace( 'icon:', '', $atts['icon'] ) ) . ' amp-su-list-icon-color"></i>';
	}
	else $atts['icon'] = '<img src="' . $atts['icon'] . '" alt="" />';
	
	return '<div class="su-list su-list-style-' . $atts['style'] . su_get_css_class( $atts ) . '">' . str_replace( '<li>', '<li>' . $atts['icon'] . ' ', su_do_nested_shortcodes( $content, 'list' ) ) . '</div>';
}

function amp_shortcode_button( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'url'         => get_option( 'home' ),
			'link'        => null, // 3.x
			'target'      => 'self',
			'style'       => 'default',
			'background'  => '#2D89EF',
			'color'       => '#FFFFFF',
			'dark'        => null, // 3.x
			'size'        => 3,
			'wide'        => 'no',
			'center'      => 'no',
			'radius'      => 'auto',
			'icon'        => false,
			'icon_color'  => '#FFFFFF',
			'ts_color'    => null, // Dep. 4.3.2
			'ts_pos'      => null, // Dep. 4.3.2
			'text_shadow' => 'none',
			'desc'        => '',
			'onclick'     => '',
			'rel'         => '',
			'title'       => '',
			'id'          => '',
			'class'       => ''
		), $atts, 'button' );
	$amp_su_widget_data['button'] = $atts;

	if ( $atts['link'] !== null ) $atts['url'] = $atts['link'];
	if ( $atts['dark'] !== null ) {
		$atts['background'] = $atts['color'];
		$atts['color'] = ( $atts['dark'] ) ? '#000' : '#fff';
	}
	if ( is_numeric( $atts['style'] ) ){ 
		$atts['style'] = str_replace( array( '1', '2', '3', '4', '5' ), array( 'default', 'glass', 'bubbles', 'noise', 'stroked' ), $atts['style'] ); // 3.x
	}

	// Prepare vars
	$a_css = array();
	$span_css = array();
	$img_css = array();
	$small_css = array();
	$radius = '0px';
	$before = $after = '';
	
	// Text shadow values
	$shadows = array(
		'none'         => '0 0',
		'top'          => '0 -1px',
		'right'        => '1px 0',
		'bottom'       => '0 1px',
		'left'         => '-1px 0',
		'top-right'    => '1px -1px',
		'top-left'     => '-1px -1px',
		'bottom-right' => '1px 1px',
		'bottom-left'  => '-1px 1px'
	);

	// Common styles for button
	$styles = array(
		'size'     => round( ( $atts['size'] + 7 ) * 1.3 ),
		'ts_color' => ( $atts['ts_color'] === 'light' ) ? su_hex_shift( $atts['background'], 'lighter', 50 ) : su_hex_shift( $atts['background'], 'darker', 40 ),
		'ts_pos'   => ( $atts['ts_pos'] !== null ) ? $shadows[$atts['ts_pos']] : $shadows['none']
	);

	// Calculate border-radius
	if ( $atts['radius'] == 'auto' ) {
		$radius = round( $atts['size'] + 2 ) . 'px';
	}
	elseif ( $atts['radius'] == 'round' ) {
		$radius = round( ( ( $atts['size'] * 2 ) + 2 ) * 2 + $styles['size'] ) . 'px';
	}
	elseif ( is_numeric( $atts['radius'] ) ) {
		$radius = intval( $atts['radius'] ) . 'px';
	}

	// CSS rules for <a> tag
	$a_rules = array(
		'color'                 => $atts['color'],
		'background-color'      => $atts['background'],
		'border-color'          => su_hex_shift( $atts['background'], 'darker', 20 ),
		'border-radius'         => $radius,
		'-moz-border-radius'    => $radius,
		'-webkit-border-radius' => $radius
	);

	// CSS rules for <span> tag
	$span_rules = array(
		'color'                 => $atts['color'],
		'padding'               => ( $atts['icon'] ) ? round( ( $atts['size'] ) / 2 + 4 ) . 'px ' . round( $atts['size'] * 2 + 10 ) . 'px' : '0px ' . round( $atts['size'] * 2 + 10 ) . 'px',
		'font-size'             => $styles['size'] . 'px',
		'line-height'           => ( $atts['icon'] ) ? round( $styles['size'] * 1.5 ) . 'px' : round( $styles['size'] * 2 ) . 'px',
		'border-color'          => su_hex_shift( $atts['background'], 'lighter', 30 ),
		'border-radius'         => $radius,
		'-moz-border-radius'    => $radius,
		'-webkit-border-radius' => $radius,
		'text-shadow'           => $styles['ts_pos'] . ' 1px ' . $styles['ts_color'],
		'-moz-text-shadow'      => $styles['ts_pos'] . ' 1px ' . $styles['ts_color'],
		'-webkit-text-shadow'   => $styles['ts_pos'] . ' 1px ' . $styles['ts_color']
	);

	// Apply new text-shadow value
	if ( $atts['ts_color'] === null && $atts['ts_pos'] === null ) {
		$span_rules['text-shadow'] = $atts['text_shadow'];
		$span_rules['-moz-text-shadow'] = $atts['text_shadow'];
		$span_rules['-webkit-text-shadow'] = $atts['text_shadow'];
	}

	// CSS rules for <img> tag
	$img_rules = array(
		'width'     => round( $styles['size'] * 1.5 ) . 'px',
		'height'    => round( $styles['size'] * 1.5 ) . 'px'
	);
	// CSS rules for <small> tag
	$small_rules = array(
		'padding-bottom' => round( ( $atts['size'] ) / 2 + 4 ) . 'px',
		'color' => $atts['color']
	);
	// Create style attr value for <a> tag
	foreach ( $a_rules as $a_rule => $a_value ) {
		$a_css[] = $a_rule . ':' . $a_value;
	}
	// Create style attr value for <span> tag
	foreach ( $span_rules as $span_rule => $span_value ) {
		$span_css[] = $span_rule . ':' . $span_value;
	}
	// Create style attr value for <img> tag
	foreach ( $img_rules as $img_rule => $img_value ) {
		$img_css[] = $img_rule . ':' . $img_value;
	}
	// Create style attr value for <img> tag
	foreach ( $small_rules as $small_rule => $small_value ) {
		$small_css[] = $small_rule . ':' . $small_value;
	}

	// Prepare button classes
	$classes = array( 'su-button', 'su-button-style-' . $atts['style'] );
	// Additional classes
	if ( $atts['class'] ) {
		$classes[] = $atts['class'];
	}
	// Wide class
	if ( $atts['wide'] === 'yes' ) {
		$classes[] = 'su-button-wide';
	}
	// Prepare icon
	if ( $atts['icon'] ) {
		if ( strpos( $atts['icon'], 'icon:' ) !== false ) {

			$icon = '<i class="fa fa-' . trim( str_replace( 'icon:', '', $atts['icon'] ) ) . ' su-btn-icon"></i>';

			su_query_asset( 'css', 'font-awesome' );
		}
		else {
			$icon = '<img class="su-btn-img" src="' . $atts['icon'] . '" alt="' . esc_attr( $content ) . '" />';
		}

	}
	else {
		$icon = '';
	}

	// Prepare <small> with description
	$desc = $atts['desc']
		? '<small class="su-btn-sm">' . su_do_attribute( $atts['desc'] ) . '</small>': '';

	// Wrap with div if button centered
	if ( $atts['center'] === 'yes' ) {
		$before .= '<div class="su-button-center">';
		$after .= '</div>';
	}

	// Replace icon marker in content,
	// add float-icon class to rearrange margins
	if ( strpos( $content, '%icon%' ) !== false ) {
		$content = str_replace( '%icon%', $icon, $content );
		$classes[] = 'su-button-float-icon';
	}

	// Button text has no icon marker, append icon to begin of the text
	else {
		$content = $icon . ' ' . $content;
	}

	// Prepare onclick action
	$atts['onclick'] = $atts['onclick']
		? ' onClick="' . $atts['onclick'] . '"'	: '';

	// Prepare rel attribute
	$atts['rel'] = $atts['rel']
		? ' rel="' . $atts['rel'] . '"'	: '';

	// Prepare title attribute
	$atts['title'] = $atts['title']
		? ' title="' . su_do_attribute( $atts['title'] ) . '"': '';

	// Add ID attribute
	$atts['id'] = ! empty( $atts['id'] )
		? sprintf( ' id="%s"', esc_attr( $atts['id'] ) ): '';
	
	return $before . '<a href="' . su_do_attribute( $atts['url'] ) . '" class="' . implode( $classes, ' ' ) . ' amp-su-button-a" target="_' . $atts['target'] . '"' . $atts['onclick'] . $atts['rel'] . $atts['title'] . $atts['id'] . '><span class="amp-su-button-span">' . do_shortcode( stripcslashes( $content ) ) . $desc . '</span></a>' . $after;
}
function amp_shortcode_service( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'title'       => __( 'Service title', 'shortcodes-ultimate' ),
			'icon'        => plugins_url( 'assets/images/service.png', SU_PLUGIN_FILE ),
			'icon_color'  => '#333',
			'size'        => 32,
			'class'       => ''
		), $atts, 'service' );
	$amp_su_widget_data['service'] = $atts;
	// RTL
	$rtl = ( is_rtl() ) ? 'right' : 'left';
	// Built-in icon
	if ( strpos( $atts['icon'], 'icon:' ) !== false ) {
		$atts['icon'] = '<i class="fa fa-' . trim( str_replace( 'icon:', '', $atts['icon'] ) ) . ' amp-su-icon-inline"></i>';
		su_query_asset( 'css', 'font-awesome' );
	}
	// Uploaded icon
	else {
		$atts['icon'] = '<img src="' . $atts['icon'] . '" width="' . $atts['size'] . '" height="' . $atts['size'] . '" alt="' . $atts['title'] . '" />';
	}
	su_query_asset( 'css', 'su-box-shortcodes' );
	return '<div class="su-service' . su_get_css_class( $atts ) . '"><div class="su-service-title amp-su-service" >' . $atts['icon'] . ' ' . su_do_attribute( $atts['title'] ) . '</div><div class="su-service-content su-clearfix amp-su-service-content">' . do_shortcode( $content ) . '</div></div>';
}

function amp_shortcode_box( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	
	$atts = shortcode_atts( array(
			'title'       => __( 'This is box title', 'shortcodes-ultimate' ),
			'style'       => 'default',
			'box_color'   => '#333333',
			'title_color' => '#FFFFFF',
			'color'       => null, // 3.x
			'radius'      => '3',
			'class'       => ''
		), $atts, 'box' );
	$amp_su_widget_data['box'] = $atts;

	if ( $atts['color'] !== null ) {
		$atts['box_color'] = $atts['color'];
	}

	$atts['radius'] = is_numeric( $atts['radius'] ) ? intval( $atts['radius'] ) : 0;
	$atts['inner_radius'] = $atts['radius'] > 2 ? $atts['radius'] - 2 : 0;

	return '<div class="su-box su-box-style-'.$atts['style'].' amp-su-box-inline "><div class="su-box-title amp-su-box-inner" >'.$atts['title'].'</div><div class="su-box-content su-clearfix amp-su-box-content" >'.$content.'</div></div>';
}

function amp_shortcode_note( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'note_color' => '#FFFF66',
			'text_color' => '#333333',
			'background' => null, // 3.x
			'color'      => null, // 3.x
			'radius'     => '3',
			'class'      => ''
		), $atts, 'note' );
	$amp_su_widget_data['note'] = $atts;

	if ( $atts['color'] !== null ) $atts['note_color'] = $atts['color'];
	if ( $atts['background'] !== null ) $atts['note_color'] = $atts['background'];
	// Prepare border-radius
	$radius = ( $atts['radius'] != '0' ) ? 'border-radius:' . $atts['radius'] . 'px;-moz-border-radius:' . $atts['radius'] . 'px;-webkit-border-radius:' . $atts['radius'] . 'px;' : '';
	su_query_asset( 'css', 'su-box-shortcodes' );
	return '<div class="su-note' . su_get_css_class( $atts ) . ' amp-su-note-wrapper"><div class="su-note-inner su-clearfix amp-su-note-content" >' . su_do_nested_shortcodes( $content, 'note' ) . '</div></div>';
}
function amp_shortcode_expand( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'more_text'  => __( 'Show more', 'shortcodes-ultimate' ),
			'less_text'  => __( 'Show less', 'shortcodes-ultimate' ),
			'height'     => '100',
			'hide_less'  => 'no',
			'text_color' => '#333333',
			'link_color' => '#0088FF',
			'link_style' => 'default',
			'link_align' => 'left',
			'more_icon'  => '',
			'less_icon'  => '',
			'class'      => ''
		), $atts, 'expand' );
	$amp_su_widget_data['expand'] = $atts;
	// Prepare more icon
	$more_icon = ( $atts['more_icon'] ) ? su_html_icon( $atts['more_icon'] ) : '';
	$less_icon = ( $atts['less_icon'] ) ? su_html_icon( $atts['less_icon'] ) : '';

	if ( $more_icon || $less_icon ) su_query_asset( 'css', 'font-awesome' );
	// Prepare less link
	$less = ( $atts['hide_less'] !== 'yes' ) ? '<div class="show-less su-expand-link amp-su-exand-text"><a href="javascript:;" class="amp-su-expand-link">' . $less_icon . '<span class="amp-su-expand-span">' . $atts['less_text'] . '</span></a></div>' : '';
	
	return '<amp-accordion disable-session-states class="su-expand su-expand-collapsed su-expand-link-style-' . $atts['link_style'] . su_get_css_class( $atts ) . '" data-height="' . $atts['height'] . '"><section class="su-expand-content amp-su-expand-section" ><h4><div class="show-more su-expand-link amp-su-exand-text"><a href="javascript:;" class="amp-su-expand-link">' . $more_icon . '<span class="amp-su-expand-span">' . $atts['more_text'] . '</span></a></div>' . $less . '</h4><p>' . do_shortcode( $content ) . '</p></section></amp-accordion>';
}

function amp_shortcode_lightbox( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'src'   => false,
			'type'  => 'iframe',
			'class' => ''
		), $atts, 'lightbox' );
	$amp_su_widget_data['lightbox'] = $atts;
	
	if ( !$atts['src'] ){ 
		return su_error_message( 'Lightbox', __( 'please specify correct source', 'shortcodes-ultimate' ) );
	}
	if($atts['type'] == 'image'){
		$light_box = '<amp-lightbox id="my-lightbox" layout="nodisplay">
	    <div class="lightbox" on="tap:my-lightbox.close" role="button" tabindex="0"><img alt="A view of the sea" src="'.su_do_attribute( $atts['src'] ).'" width="900" height="675" layout="responsive"/></div>
	  </amp-lightbox>';
	  return  $light_box.'  <span role="button" class="su-lightbox' . su_get_css_class( $atts ) . '" data-mfp-src="' . su_do_attribute( $atts['src'] ) . '" data-mfp-type="' . $atts['type'] . '" on="tap:my-lightbox" tabindex="0">' . do_shortcode( $content ) . '</span>';
	}else if($atts['type'] == 'iframe'){
		$video_url = su_do_attribute( $atts['src'] );
		$video_id = preg_match( '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $video_url , $match )? $match[1]: false;
    	$domain = strpos( $video_url, 'youtube-nocookie.com' ) !== false	? 'www.youtube-nocookie.com': 'www.youtube.com';
		$light_box = '<amp-lightbox id="my-lightbox" layout="nodisplay">
	    <div class="lightbox" on="tap:my-lightbox.close" role="button" tabindex="0"><amp-iframe width="600" height="400" sandbox="allow-scripts allow-same-origin allow-popups" layout="fixed" allowfullscreen frameborder="0" src="https://'.$domain.'/embed/'.$video_id .'"></amp-iframe></div>
	  </amp-lightbox>';
	return $light_box.'<span class="su-lightbox' . su_get_css_class( $atts ) . '" data-mfp-src="' . su_do_attribute( $atts['src'] ) . '" data-mfp-type="' . $atts['type'] . '" on="tap:my-lightbox" role="button" tabindex="0">' . do_shortcode( $content ) . '</span>';
	}else if($atts['type'] == 'inline'){
		$light_box = '';
		// $light_box = '<amp-lightbox id="' . su_do_attribute( $atts['src'] ) . '" layout="nodisplay">
	 //    <div class="lightbox" on="tap:' . su_do_attribute( $atts['src'] ) . '.close" role="button" tabindex="0">
	 //    Hello World
	 //    </div>
	 //  </amp-lightbox>';
	return $light_box.'<span role="button" class="su-lightbox' . su_get_css_class( $atts ) . '" data-mfp-src="' . su_do_attribute( $atts['src'] ) . '" data-mfp-type="' . $atts['type'] . '" on="tap:' . trim( $atts['src'] , '#' ) . '" role="button" tabindex="0">' . do_shortcode( $content ) . '</span>';
	}

}

function amp_shortcode_lightbox_content( $atts = null, $content = null ) {
	global $amp_su_widget_data;
		$atts = shortcode_atts( array(
				'id'         => '',
				'width'      => '50%',
				'margin'     => '40',
				'padding'    => '40',
				'text_align' => 'center',
				'background' => '#FFFFFF',
				'color'      => '#333333',
				'shadow'     => '0px 0px 15px #333333',
				'class'      => ''
			), $atts, 'lightbox_content' );
		$amp_su_widget_data['lightbox_content'] = $atts;
		
		if ( ! $atts['id'] ) {
			return su_error_message( 'Lightbox content', __( 'please specify correct ID for this block. You should use same ID as in the Content source field (when inserting lightbox shortcode)', 'shortcodes-ultimate' ) );
		}
		$return = '<amp-lightbox class="su-lightbox-content ' . su_get_css_class( $atts ) . '" id="' . trim( $atts['id'], '#' ) . '" layout="nodisplay"><div class="lightbox" on="tap:'. trim( $atts['id'], '#' ) . '.close" role="button" tabindex="0"><div class="amp-su-inlinelightbox">' . do_shortcode( $content ) . '</div></div></amp-lightbox>';
		return did_action( 'su/generator/preview/before' )	? '<div class="su-lightbox-content-preview">' . $return . '</div>': $return;
}

function amp_shortcode_private( $atts = null, $content = null ) {
	global $amp_su_widget_data;
		$atts = shortcode_atts( array( 'class' => '' ), $atts, 'private' );
		$amp_su_widget_data['private'] = $atts;
		su_query_asset( 'css', 'su-other-shortcodes' );
		return ( current_user_can( 'publish_posts' ) ) ? '<div class="su-private' . su_get_css_class( $atts ) . '"><div class="su-private-shell">' . do_shortcode( $content ) . '</div></div>' : '';
}

function amp_shortcode_youtube( $atts = null, $content = null ) {
	// Prepare data
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'url'        => false,
			'width'      => 600,
			'height'     => 400,
			'autoplay'   => 'no',
			'mute'       => 'no',
			'responsive' => 'yes',
			'class'      => ''
		), $atts, 'youtube' );
	$amp_su_widget_data['youtube'] = $atts;
	if ( ! $atts['url'] ) {
		return su_error_message( 'YouTube', __( 'please specify correct url', 'shortcodes-ultimate' ) );
	}
	$atts['url'] = su_do_attribute( $atts['url'] );
	$video_id = preg_match( '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $atts['url'], $match )? $match[1]: false;
	// Check that url is specified
	if ( ! $video_id ) {
		return su_error_message( 'YouTube', __( 'please specify correct url', 'shortcodes-ultimate' ) );
	}
	$url_params = array();
	if ( $atts['autoplay'] === 'yes' ) {
		$url_params['autoplay'] = '1';
	}

	if ( $atts['mute'] === 'yes' ) {
		$url_params['mute'] = '1';
	}
	$domain = strpos( $atts['url'], 'youtube-nocookie.com' ) !== false	? 'www.youtube-nocookie.com': 'www.youtube.com';

	su_query_asset( 'css', 'su-shortcodes' );
	return '<div class="su-youtube su-responsive-media-'.$atts['responsive'] . su_get_css_class( $atts ) . '"><iframe width="' . $atts['width'] . '" height="' . $atts['height'] . '"  src="https://' . $domain . '/embed/' . $video_id . '?' . http_build_query( $url_params ) . '" frameborder="0" allowfullscreen="true"></iframe></div>';
}

function amp_shortcode_tooltip( $atts = null, $content = null ) {
	global $amp_su_widget_data;
		$atts = shortcode_atts( array(
			'style'        => 'yellow',
			'position'     => 'north',
			'shadow'       => 'no',
			'rounded'      => 'no',
			'size'         => 'default',
			'title'        => '',
			'content'      => __( 'Tooltip text', 'shortcodes-ultimate' ),
			'behavior'     => 'hover',
			'close'        => 'no',
			'class'        => ''
		), $atts, 'tooltip' );
		$amp_su_widget_data['tooltip'] = $atts;
	// Prepare style
	$atts['style'] = in_array( $atts['style'], array( 'light', 'dark', 'green', 'red', 'blue', 'youtube', 'tipsy', 'bootstrap', 'jtools', 'tipped', 'cluetip' ) )
		? $atts['style']
		: 'plain';

	// Position
	$atts['position'] = str_replace( array( 'top', 'right', 'bottom', 'left' ), array( 'north', 'east', 'south', 'west' ), $atts['position'] );
	$position = array(
		'my' => str_replace( array( 'north', 'east', 'south', 'west' ), array( 'bottom center', 'center left', 'top center', 'center right' ), $atts['position'] ),
		'at' => str_replace( array( 'north', 'east', 'south', 'west' ), array( 'top center', 'center right', 'bottom center', 'center left' ), $atts['position'] )
	);

	// Prepare classes
	$classes = array( 'su-qtip qtip-' . $atts['style'] );
	$classes[] = 'su-qtip-size-' . $atts['size'];

	if ( $atts['shadow'] === 'yes' ) {
		$classes[] = 'qtip-shadow';
	}

	if ( $atts['rounded'] === 'yes' ) {
		$classes[] = 'qtip-rounded';
	}

	// Query assets
	su_query_asset( 'css', 'qtip' );
	su_query_asset( 'css', 'su-shortcodes' );
	su_query_asset( 'js', 'jquery' );
	su_query_asset( 'js', 'qtip' );
	su_query_asset( 'js', 'su-other-shortcodes' );

	return '<span class="su-tooltip' . su_get_css_class( $atts ) . '" data-close="' . $atts['close'] . '" data-behavior="' . $atts['behavior'] . '" data-my="' . $position['my'] . '" data-at="' . $position['at'] . '" data-classes="' . implode( ' ', $classes ) . '" data-title="' . $atts['title'] . '" title="' . esc_attr( $atts['content'] ) . '">' . do_shortcode( $content ) . '</span>';
}

function amp_shortcode_youtube_advanced( $atts = null, $content = null ) {
		global $amp_su_widget_data;
		// Prepare data
		$return = array();
		$params = array();
		$atts = shortcode_atts( array(
			'url'            => false,
			'width'          => 600,
			'height'         => 400,
			'responsive'     => 'yes',
			'autohide'       => 'alt',
			'autoplay'       => 'no',
			'mute'           => 'no',
			'controls'       => 'yes',
			'fs'             => 'yes',
			'loop'           => 'no',
			'modestbranding' => 'no',
			'playlist'       => '',
			'rel'            => 'yes',
			'showinfo'       => 'yes',
			'theme'          => 'dark',
			'https'          => 'no',
			'wmode'          => '',
			'playsinline'    => 'no',
			'class'          => ''
		), $atts, 'youtube_advanced' );
		$amp_su_widget_data['youtube_advanced'] = $atts;
		if ( ! $atts['url'] ) {
		return su_error_message( 'YouTube Advanced', __( 'please specify correct url', 'shortcodes-ultimate' ) );
	}

	$atts['url'] = su_do_attribute( $atts['url'] );

	$video_id = preg_match( '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $atts['url'], $match )
		? $match[1]
		: false;

	if ( ! $video_id ) {
		return su_error_message( 'YouTube Advanced', __( 'please specify correct url', 'shortcodes-ultimate' ) );
	}

	$url_params = array();
	$yt_options = array(
		'autohide',
		'autoplay',
		'mute',
		'controls',
		'fs',
		'loop',
		'modestbranding',
		'playlist',
		'rel',
		'showinfo',
		'theme',
		'wmode',
		'playsinline'
	);

	foreach ( $yt_options as $yt_option ) {
		$url_params[ $yt_option ] = str_replace( array( 'no', 'yes', 'alt' ), array( '0', '1', '2' ), $atts[ $yt_option ] );
	}

	if ( $url_params['loop'] === '1' && $url_params['playlist'] === '' ) {
		$url_params['playlist'] = $video_id;
	}

	$url_params = http_build_query( $url_params );

	$protocol = $atts['https'] === 'yes'
		? 'https'
		: 'http';

	$domain = strpos( $atts['url'], 'youtube-nocookie.com' ) !== false
		? 'www.youtube-nocookie.com'
		: 'www.youtube.com';
	su_query_asset( 'css', 'su-shortcodes' );

	return '<div class="su-youtube su-responsive-media-' . $atts['responsive'] . su_get_css_class( $atts ) . '"><iframe width="' . $atts['width'] . '" height="' . $atts['height'] . '" src="' . $protocol . '://' . $domain . '/embed/' . $video_id . '?' . $url_params . '" frameborder="0" allowfullscreen="true"></iframe></div>';
		
}

function amp_shortcode_vimeo( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
		'url'        => '',
		'width'      => 600,
		'height'     => 400,
		'autoplay'   => 'no',
		'dnt'        => 'no',
		'mute'       => 'no',
		'responsive' => 'yes',
		'class'      => ''
	), $atts, 'vimeo' );
	$amp_su_widget_data['vimeo'] = $atts;
	if ( ! $atts['url'] ) {
		return su_error_message( 'Vimeo', __( 'please specify correct url', 'shortcodes-ultimate' ) );
	}

	$atts['url'] = su_do_attribute( $atts['url'] );

	$video_id = preg_match( '~(?:<iframe [^>]*src=")?(?:https?:\/\/(?:[\w]+\.)*vimeo\.com(?:[\/\w]*\/videos?)?\/([0-9]+)[^\s]*)"?(?:[^>]*></iframe>)?(?:<p>.*</p>)?~ix', $atts['url'], $match )
		? $match[1]
		: false;

	if ( ! $video_id ) {
		return su_error_message( 'Vimeo', __( 'please specify correct url', 'shortcodes-ultimate' ) );
	}

	$url_params = array(
		'title'    => 0,
		'byline'   => 0,
		'portrait' => 0,
		'color'    => 'ffffff',
		'autoplay' => $atts['autoplay'] === 'yes' ? 1 : 0,
		'dnt'      => $atts['dnt'] === 'yes' ? 1 : 0,
		'muted'    => $atts['mute'] === 'yes' ? 1 : 0,
	);

	su_query_asset( 'css', 'su-shortcodes' );

	return '<div class="su-vimeo su-responsive-media-' . $atts['responsive'] . su_get_css_class( $atts ) . '"><iframe width="' . $atts['width'] . '" height="' . $atts['height'] . '" src="//player.vimeo.com/video/' . $video_id . '?' . http_build_query( $url_params ) . '" frameborder="0" allowfullscreen="true"></iframe></div>';
}
function amp_shortcode_dailymotion( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
		'url'        => false,
		'width'      => 600,
		'height'     => 400,
		'responsive' => 'yes',
		'autoplay'   => 'no',
		'background' => '#FFC300',
		'foreground' => '#F7FFFD',
		'highlight'  => '#171D1B',
		'logo'       => 'yes',
		'quality'    => '380',
		'related'    => 'yes',
		'info'       => 'yes',
		'class'      => ''
	), $atts, 'dailymotion' );
	$amp_su_widget_data['dailymotion'] = $atts;
	if ( ! $atts['url'] ) {
		return su_error_message( 'Dailymotion', __( 'please specify correct url', 'shortcodes-ultimate' ) );
	}

	$atts['url'] = su_do_attribute( $atts['url'] );
	$id = strtok( basename( $atts['url'] ), '_' );

	if ( ! $id ) {
		return su_error_message( 'Screenr', __( 'please specify correct url', 'shortcodes-ultimate' ) );
	}

	$params = array();
	$dm_options = array( 'autoplay', 'background', 'foreground', 'highlight', 'logo', 'quality', 'related', 'info' );

	foreach ( $dm_options as $dm_option ) {
		$params[] = $dm_option . '=' . str_replace( array( 'yes', 'no', '#' ), array( '1', '0', '' ), $atts[ $dm_option ] );
	}

	su_query_asset( 'css', 'su-shortcodes' );

	return '<div class="su-dailymotion su-responsive-media-' . $atts['responsive'] . su_get_css_class( $atts ) . '"><iframe width="' . $atts['width'] . '" height="' . $atts['height'] . '" src="//www.dailymotion.com/embed/video/' . $id . '?' . implode( '&', $params ) . '" frameborder="0" allowfullscreen="true"></iframe></div>';
}

function amp_shortcode_screenr( $atts = null, $content = null ) {
	global $amp_su_widget_data;
		// Prepare data
		$return = array();
		$atts = shortcode_atts( array(
				'url'        => false,
				'width'      => 600,
				'height'     => 400,
				'responsive' => 'yes',
				'class'      => ''
			), $atts, 'screenr' );
		$amp_su_widget_data['screenr'] = $atts;
		if ( !$atts['url'] ) return Su_Tools::error( __FUNCTION__, __( 'please specify correct url', 'shortcodes-ultimate' ) );
		$atts['url'] = su_scattr( $atts['url'] );
		$id = ( preg_match( '~(?:<iframe [^>]*src=")?(?:https?:\/\/(?:[\w]+\.)*screenr\.com(?:[\/\w]*\/videos?)?\/([a-zA-Z0-9]+)[^\s]*)"?(?:[^>]*></iframe>)?(?:<p>.*</p>)?~ix', $atts['url'], $match ) ) ? $match[1] : false;
		// Check that url is specified
		if ( !$id ) return Su_Tools::error( __FUNCTION__, __( 'please specify correct url', 'shortcodes-ultimate' ) );
		// Create player
		$return[] = '<div class="su-screenr su-responsive-media-' . $atts['responsive'] . su_ecssc( $atts ) . '">';
		$return[] = '<iframe width="' . $atts['width'] . '" height="' . $atts['height'] . '" src="http://screenr.com/embed/' . $id . '" frameborder="0" allowfullscreen="true"></iframe>';
		$return[] = '</div>';
		su_query_asset( 'css', 'su-media-shortcodes' );
		// Return result
		return implode( '', $return );
}

function amp_shortcode_audio( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
		'url'      => false,
		'width'    => 'auto',
		'title'    => '',
		'autoplay' => 'no',
		'loop'     => 'no',
		'class'    => ''
	), $atts, 'audio' );
	$amp_su_widget_data['audio'] = $atts;
	if ( ! $atts['url'] ) {
		return su_error_message( 'Audio', __( 'please specify correct url', 'shortcodes-ultimate' ) );
	}

	$atts['url'] = su_do_attribute( $atts['url'] );

	$id = uniqid( 'su_audio_player_' );

	$width = ( $atts['width'] !== 'auto' )? 'max-width:' . $atts['width']: '';

	if ( ! $atts['url'] ) {
		return su_error_message( 'Audio', __( 'please specify correct url', 'shortcodes-ultimate' ) );
	}

	su_query_asset( 'css', 'su-shortcodes' );
	su_query_asset( 'js', 'jquery' );
	su_query_asset( 'js', 'jplayer' );
	su_query_asset( 'js', 'su-players-shortcodes' );

	return ' <amp-audio width="'.$atts['width'].'"  height="50" src="'.$atts['url'].'" controlsList="nodownload">
    <div fallback> <p>Your browser doesn’t support HTML5 audio</p></div></amp-audio>';
}

function amp_shortcode_video( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
		'url'      => false,
		'poster'   => false,
		'title'    => '',
		'width'    => 600,
		'height'   => 300,
		'controls' => 'yes',
		'autoplay' => 'no',
		'loop'     => 'no',
		'class'    => ''
	), $atts, 'video' );
	$amp_su_widget_data['video'] = $atts;
	if ( ! $atts['url'] ) {
		return su_error_message( 'Video', __( 'please specify correct url', 'shortcodes-ultimate' ) );
	}

	$atts['url'] = su_do_attribute( $atts['url'] );

	$id = uniqid( 'su_video_player_' );

	if ( ! $atts['url'] ) {
		return su_error_message( 'Video', __( 'please specify correct url', 'shortcodes-ultimate' ) );
	}

	$title = $atts['title']	? '<div class="jp-title">' . $atts['title'] . '</div>': '';
	$controls = ($atts['controls'] =='yes')? 'controls':'';
	$autoplay = ($atts['autoplay'] == "yes")? 'autoplay':'';
	su_query_asset( 'css', 'su-shortcodes' );
	su_query_asset( 'js', 'jquery' );
	su_query_asset( 'js', 'jplayer' );
	su_query_asset( 'js', 'su-players-shortcodes' );
		// Create player
	return '<amp-video '.$autoplay.' src="'.$atts['url'].'"  poster="'.$atts['poster'].'" title="'.$atts['title'].'" width="'.$atts['width'].'" height="'.$atts['height'].'" layout="responsive" '.$controls.'><source type="video/mp4" src="'.$atts['url'].'"></amp-video>';
}

function amp_shortcode_table( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
		'url'   => false,
		'responsive' => false,
		'class' => ''
	), $atts, 'table' );
	$amp_su_widget_data['table'] = $atts;
	if ( $atts['responsive'] ) {
		$atts['class'] .= ' su-table-responsive';
	}

	su_query_asset( 'css', 'su-shortcodes' );
	su_query_asset( 'js', 'jquery' );
	su_query_asset( 'js', 'su-other-shortcodes' );

	$table_data = $atts['url']
		? su_parse_csv( $atts['url'] ) :
		do_shortcode( $content );

	return '<div class="su-table' . su_get_css_class( $atts ) . '">' . $table_data . '</div>';
}
function amp_shortcode_permalink( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
		'id'     => 1,
		'p'      => null, // 3.x
		'target' => 'self',
		'title'  => '',
		'rel'    => '',
		'class'  => '',
	), $atts, 'permalink' );
	$amp_su_widget_data['permalink'] = $atts;
	if ( $atts['p'] !== null ) {
		$atts['id'] = $atts['p'];
	}

	$atts['id'] = su_do_attribute( $atts['id'] );

	if ( ! $content ) {
		$content = get_the_title( $atts['id'] );
	}
	return sprintf(
		'<a href="%s" title="%s" target="_%s" rel="%s" class="%s">%s</a>',
		get_permalink( $atts['id'] ),
		esc_attr( $atts['title'] ),
		esc_attr( $atts['target'] ),
		esc_attr( $atts['rel'] ),
		su_get_css_class( $atts ),
		do_shortcode( $content )
	);
}

function amp_shortcode_members( $atts = null, $content = null ) {
	global $amp_su_widget_data;
		$atts = shortcode_atts( array(
			'message'    => __( 'This content is for registered users only. Please %login%.', 'shortcodes-ultimate' ),
			'color'      => '#ffcc00',
			'style'      => null, // 3.x
			'login_text' => __( 'login', 'shortcodes-ultimate' ),
			'login_url'  => wp_login_url(),
			'login'      => null, // 3.x
			'class'      => ''
		), $atts, 'members' );
	$amp_su_widget_data['members'] = $atts;
	if ( $atts['style'] !== null ) {
		$atts['color'] = str_replace( array( '0', '1', '2' ), array( '#fff', '#FFFF29', '#1F9AFF' ), $atts['style'] );
	}
	if ( is_feed() ) {
		return;
	}
	if ( is_user_logged_in() ) {
		return do_shortcode( $content );
	}
	// 3.x
	if ( $atts['login'] !== null && $atts['login'] == '0' ) {
		return;
	}
	$login = '<a href="' . esc_attr( $atts['login_url'] ) . '">' . $atts['login_text'] . '</a>';

	su_query_asset( 'css', 'su-shortcodes' );
	return '<div class="su-members' . su_get_css_class( $atts ) . ' amp-su-member-inline" >' . str_replace( '%login%', $login, su_do_attribute( $atts['message'] ) ) . '</div>';
}
function amp_shortcode_guests( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array( 'class' => '' ), $atts, 'guests' );
	$amp_su_widget_data['guests'] = $atts;
	$return = '';
	if ( !is_user_logged_in() && !is_null( $content ) ) {
		su_query_asset( 'css', 'su-shortcodes' );
		$return = '<div class="su-guests' . su_get_css_class( $atts ) . '">' . do_shortcode( $content ) . '</div>';
	}
	return $return;
}

function amp_shortcode_feed( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
		'url'   => get_bloginfo_rss( 'rss2_url' ),
		'limit' => 3,
		'class' => ''
	), $atts, 'feed' );
	$amp_su_widget_data['feed'] = $atts;
	if ( !function_exists( 'wp_rss' ) ) include_once ABSPATH . WPINC . '/rss.php';
	ob_start();
	echo '<div class="su-feed' . su_get_css_class( $atts ) . '">';
	wp_rss( $atts['url'], $atts['limit'] );
	echo '</div>';
	$return = ob_get_contents();
	ob_end_clean();
	return $return;
}

function amp_shortcode_subpages( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
		'depth' => 1,
		'p'     => false,
		'class' => ''
	), $atts, 'subpages' );
	$amp_su_widget_data['subpages'] = $atts;
	global $post;
	$child_of = ( $atts['p'] ) ? $atts['p'] : get_the_ID();
	$return = wp_list_pages( array(
			'title_li' => '',
			'echo' => 0,
			'child_of' => $child_of,
			'depth' => $atts['depth']
		) );
	return ( $return ) ? '<ul class="su-subpages' . su_get_css_class( $atts ) . '">' . $return . '</ul>' : false;
}

function amp_shortcode_siblings( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
		'depth' => 1,
		'class' => ''
	), $atts, 'siblings' );
	$amp_su_widget_data['siblings'] = $atts;
	global $post;
	if ( empty( $post ) || empty( $post->post_parent ) ) {
		return;
	}
	$return = wp_list_pages( array(
			'title_li' => '',
			'echo'     => 0,
			'child_of' => $post->post_parent,
			'depth'    => $atts['depth'],
			'exclude'  => $post->ID
		) );

	return $return ? '<ul class="su-siblings' . su_get_css_class( $atts ) . '">' . $return . '</ul>': '';
}

function amp_shortcode_menu( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'name' => false,
			'class' => ''
		), $atts, 'menu' );
	$amp_su_widget_data['menu'] = $atts;
	$return = wp_nav_menu( array(
			'echo'        => false,
			'menu'        => $atts['name'],
			'container'   => false,
			'fallback_cb' => 'amp_shortcode_menu_fallback',
			'items_wrap'  => '<ul id="%1$s" class="%2$s' . su_ecssc( $atts ) . '">%3$s</ul>'
		) );
	return ( $atts['name'] ) ? $return : false;
}

function amp_shortcode_menu_fallback() {
	return __( 'This menu doesn\'t exists, or has no elements', 'shortcodes-ultimate' );
}

function amp_shortcode_document( $atts = null, $content = null ) {
	global $amp_su_widget_data;
		$atts = shortcode_atts( array(
				'url'        => '',
				'file'       => null, // 3.x
				'width'      => 600,
				'height'     => 400,
				'responsive' => 'yes',
				'class'      => ''
			), $atts, 'document' );
		$amp_su_widget_data['document'] = $atts;
		su_query_asset( 'css', 'su-media-shortcodes' );
		if ( $atts['file'] !== null ) $atts['url'] = $atts['file'];
		$file_type = substr(strrchr($atts['url'],'.'),1);
		if($file_type == 'pdf'){
			return '<amp-google-document-embed  src="'.$atts['url'].'" width="' . $atts['width'] . '" height="' . $atts['height'] . '" layout="responsive"></amp-google-document-embed>';	
		}else{
			return '<div class="su-document su-responsive-media-' . $atts['responsive'] . '"><iframe src="//docs.google.com/viewer?embedded=true&url=' . $atts['url'] . '" width="' . $atts['width'] . '" height="' . $atts['height'] . '" class="su-document' . su_ecssc( $atts ) . '"></iframe></div>';
		}
}

function amp_shortcode_gmap( $atts = null, $content = null ) {
	global $amp_su_widget_data;
		$atts = shortcode_atts( array(
			'width'      => 600,
			'height'     => 400,
			'responsive' => 'yes',
			'address'    => 'Moscow',
			'zoom'       => 0,
			'class'      => ''
		), $atts, 'gmap' );
		$amp_su_widget_data['gmap'] = $atts;
	$atts['zoom'] = is_numeric( $atts['zoom'] ) && $atts['zoom'] > 0 ? '&amp;z=' . $atts['zoom']: '';

	su_query_asset( 'css', 'su-shortcodes' );

	return sprintf(
		'<div class="su-gmap su-responsive-media-%s%s"><iframe width="%s" height="%s" src="//maps.google.com/maps?q=%s&amp;output=embed%s"></iframe></div>',
		esc_attr( $atts['responsive'] ),
		su_get_css_class( $atts ),
		intval( $atts['width'] ),
		intval( $atts['height'] ),
		urlencode( su_do_attribute( $atts['address'] ) ),
		$atts['zoom']
	);
}

function amp_shortcode_slider( $atts = null, $content = null ) {
	global $amp_su_widget_data;
		$return = '';
		$atts = shortcode_atts( array(
				'source'     => 'none',
				'limit'      => 20,
				'gallery'    => null, // Dep. 4.3.2
				'link'       => 'none',
				'target'     => 'self',
				'width'      => 600,
				'height'     => 300,
				'responsive' => 'yes',
				'title'      => 'yes',
				'centered'   => 'yes',
				'arrows'     => 'yes',
				'pages'      => 'yes',
				'mousewheel' => 'yes',
				'autoplay'   => 3000,
				'speed'      => 600,
				'class'      => ''
			), $atts, 'slider' );
		$amp_su_widget_data['slider'] = $atts;
		// Get slides
		$slides = (array) su_get_slides( $atts );
		// Loop slides
	if ( count( $slides ) ) {
		// Prepare unique ID
		$id = uniqid( 'su_slider_' );
		// Links target
		$target = ( $atts['target'] === 'yes' || $atts['target'] === 'blank' ) ? ' target="_blank"' : '';
		// Centered class
		$centered = ( $atts['centered'] === 'yes' ) ? ' su-slider-centered' : '';
		// Wheel control
		$mousewheel = ( $atts['mousewheel'] === 'yes' ) ? 'true' : 'false';
		// Prepare width and height
		$size = ( $atts['responsive'] === 'yes' ) ? 'width:100%' : 'width:' . intval( $atts['width'] ) . 'px;height:' . intval( $atts['height'] ) . 'px';
		// Add lightbox class
		if ( $atts['link'] === 'lightbox' ) {
			$atts['class'] .= ' su-lightbox-gallery';
			$lightbox = 'lightbox';
		}else{
			$lightbox = '';
		}
		// Open slider

		$return .= '<div id="' . $id . '" class="su-slider-inline su-slider' . $centered . ' su-slider-pages-' . $atts['pages'] . ' su-slider-responsive-' . $atts['responsive'] . su_get_css_class( $atts ) . '" style="' . $size . '" data-autoplay="' . $atts['autoplay'] . '" data-speed="' . $atts['speed'] . '" data-mousewheel="' . $mousewheel . '">
		<amp-carousel '.$lightbox.' width="400" height="300" layout="responsive" type="slides">';
		// Create slides
		foreach ( $slides as $slide ) {
			// Crop the image
		$image = su_image_resize( $slide['image'], $atts['width'], $atts['height'] );
			// Prepare slide title
			$title = ( $atts['title'] === 'yes' && $slide['title'] ) ? '<div class="caption">' . stripslashes( $slide['title'] ) . '</div>' : '';
			// Open slide
			//$return .= '<div class="slide su-slider-slide">';
			// Slide content with link
			if( $atts['link'] === 'lightbox' ){
				$return .= '<amp-img src="' . $image['url'] . '" alt="' . esc_attr( $slide['title'] ) . '" layout="responsive" height="300" width="400" /></amp-img>';
			}else{
				if ( $slide['link'] ) {
					$return .= '<a href="' . $slide['link'] . '" ' . $target . ' title="' . esc_attr( $slide['title'] ) . '"><amp-img src="' . $image['url'] . '" alt="' . esc_attr( $slide['title'] ) . '" layout="responsive" height="300" width="400" /></amp-img></a>';
				}
				// Slide content without link
				else {
					$return .= '<a><amp-img src="' . $image['url'] . '" alt="' . esc_attr( $slide['title'] ) . '" layout="responsive" height="300" width="400" /></amp-img></a>';
				}
			}
			// Close slide
			//$return .= '</div>';
		}
		// Close slides
		$return .= '</amp-carousel>';
		// Open nav section
		//$return .= '<div class="su-slider-nav">';
		// Append direction nav
		if ( $atts['arrows'] === 'yes' ){
			//$return .= '<div class="su-slider-direction"><span class="su-slider-prev"></span><span class="su-slider-next"></span></div>';
		} 
			
		// Append pagination nav
		//$return .= '<div class="su-slider-pagination"></div>';
		// Close nav section
		//$return .= '</div>';
		// Close slider
		$return .= '</div>';
		// Add lightbox assets
		if ( $atts['link'] === 'lightbox' ) {
			su_query_asset( 'css', 'magnific-popup' );
			su_query_asset( 'js', 'magnific-popup' );
		}
		su_query_asset( 'css', 'su-galleries-shortcodes' );
		su_query_asset( 'js', 'jquery' );
		su_query_asset( 'js', 'swiper' );
		su_query_asset( 'js', 'su-galleries-shortcodes' );
	}else {
	$return = su_error_message( 'Slider', __( 'images not found', 'shortcodes-ultimate' ) );
	}
	return $return;
}

function amp_shortcode_carousel( $atts = null, $content = null ) {
	global $amp_su_widget_data;
		$return = '';
		$atts = shortcode_atts( array(
				'source'     => 'none',
				'limit'      => 20,
				'gallery'    => null, // Dep. 4.3.2
				'link'       => 'none',
				'target'     => 'self',
				'width'      => 600,
				'height'     => 100,
				'responsive' => 'yes',
				'items'      => 3,
				'scroll'     => 1,
				'title'      => 'yes',
				'centered'   => 'yes',
				'arrows'     => 'yes',
				'pages'      => 'no',
				'mousewheel' => 'yes',
				'autoplay'   => 3000,
				'speed'      => 600,
				'class'      => ''
			), $atts, 'carousel' );
		$amp_su_widget_data['carousel'] = $atts;
		// Get slides
		$slides = (array) su_get_slides( $atts );

		//Get autoplay 
       if($atts['autoplay'] > 0) {
		   $autoplay = 'autoplay delay="'.$atts['autoplay'].'"';
	       }
	   else{
		   $autoplay = '';
	      }

		// Loop slides
		if ( count( $slides ) ) {
			// Prepare unique ID
			$id = uniqid( 'su_carousel_' );
			// Links target
			$target = ( $atts['target'] === 'yes' || $atts['target'] === 'blank' ) ? ' target="_blank"' : '';
			// Centered class
			$centered = ( $atts['centered'] === 'yes' ) ? ' su-carousel-centered' : '';
			// Wheel control
			$mousewheel = ( $atts['mousewheel'] === 'yes' ) ? 'true' : 'false';
			// Prepare width and height
			$size = ( $atts['responsive'] === 'yes' ) ? 'width:100%' : 'width:' . intval( $atts['width'] ) . 'px;height:' . intval( $atts['height'] ) . 'px';
			// Add lightbox class
			if ( $atts['link'] === 'lightbox' ) {
				$atts['class'] .= ' su-lightbox-gallery';
				$lightbox = 'lightbox';
			}else{
				$lightbox = '';
			}
			// Open slider
			$return .= '<div>
			<amp-carousel id="carousel-with-preview" width="400" height="300" layout="responsive" type="slides" '.$autoplay.' >';
			// Create slides

			foreach ( (array) $slides as $slide ) {
				// Crop the image
				//$image = su_image_resize( $slide['image'], ( round( $atts['width'] / $atts['items'] ) - 18 ), $atts['height'] );

				$image = $slide['image'];
				// Prepare slide title
				$title = ( $atts['title'] === 'yes' && $slide['title'] ) ? '<div class="caption">' . stripslashes( $slide['title'] ) . '</span>' : '';
				// Open slide
				//$return .= '<div class="slide su-carousel-slide">';
				// Slide content with link
				$return .= '<amp-img src="' . $image . '" width="400" height="300" layout="responsive" alt="a sample image"></amp-img>';
				// Slide content without link
				//else $return .= '<a><img src="' . $image['url'] . '" alt="' . esc_attr( $slide['title'] ) . '" />' . $title . '</a>';
				// Close slide
				//$return .= '</div>';
			}
			
			// Close slides
			$return .= '</amp-carousel>';
			$return .= '<div class="carousel-preview">';
			$i=0;
			foreach ( (array) $slides as $slide ) {
				// Crop the image
				$image = su_image_resize( $slide['image'], ( round( $atts['width'] / $atts['items'] ) - 18 ), $atts['height'] );
				

				$return .= '<button on="tap:carousel-with-preview.goToSlide(index='.$i.')"><amp-img src="'.$image['url'].'" width="80" height="80" layout="responsive" alt="a sample image"></amp-img></button>';
				$i++;
			}
			$return .= '</div>';
			// Open nav section
			//$return .= '<div class="su-carousel-nav">';
			// Append direction nav
			if ( $atts['arrows'] === 'yes'
			) //$return .= '<div class="su-carousel-direction"><span class="su-carousel-prev"></span><span class="su-carousel-next"></span></div>';
			// Append pagination nav
			//$return .= '<div class="su-carousel-pagination"></div>';
			// Close nav section
			//$return .= '</div>';
			// Close slider
			$return .= '</div>';
			// Add lightbox assets
			if ( $atts['link'] === 'lightbox' ) {
				su_query_asset( 'css', 'magnific-popup' );
				su_query_asset( 'js', 'magnific-popup' );
			}
			su_query_asset( 'css', 'su-galleries-shortcodes' );
			su_query_asset( 'js', 'jquery' );
			su_query_asset( 'js', 'swiper' );
			su_query_asset( 'js', 'su-galleries-shortcodes' );
		}
		// Slides not found
		else $return = Su_Tools::error( __FUNCTION__, __( 'images not found', 'shortcodes-ultimate' ) );
		return $return;
}

function amp_shortcode_custom_gallery( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$return = '';
	$atts = shortcode_atts( array(
			'source'  => 'none',
			'limit'   => 20,
			'gallery' => null, // Dep. 4.4.0
			'link'    => 'none',
			'width'   => 90,
			'height'  => 90,
			'title'   => 'hover',
			'target'  => 'self',
			'class'   => ''
		), $atts, 'custom_gallery' );
	$amp_su_widget_data['custom_gallery'] = $atts;
	$slides = (array) su_get_slides( $atts );
	// Loop slides
	if ( count( $slides ) ) {
		// Prepare links target
		$atts['target'] = ( $atts['target'] === 'yes' || $atts['target'] === 'blank' ) ? ' target="_blank"' : '';
		// Add lightbox class
		if ( $atts['link'] === 'lightbox' ) $atts['class'] .= ' su-lightbox-gallery';
		// Open gallery
		$return = '<div class="su-custom-gallery su-custom-gallery-title-' . $atts['title'] . su_ecssc( $atts ) . '"><amp-selector layout="container" name="carousel-selector" [selected]="selectedSlide" on="select:AMP.setState({selectedSlide: event.targetOption})"><amp-carousel controls width="auto" height="125">';
		// Create slides

		foreach ( $slides as $slide ) {
			// Crop image
			$image = su_image_resize( $slide['image'], $atts['width'], $atts['height'] );
			$image = $slide['image'];
			// Prepare slide title
			$title = ( $slide['title'] ) ? '<div class="caption class="su-custom-gallery-title">' . stripslashes( $slide['title'] ) . '</div>' : '';
			// Open slide
			$return .= '<div class="su-custom-gallery-slide">';
			// Slide content with link
			$return .='<amp-img src="'. $image . '" width="150" height="116" option="0"></amp-img>';
			// Close slide
			$return .= '</div>';
		}
		$return .= '</amp-carousel>';
		$return .= '</amp-selector>';
		// Clear floats
		$return .= '<div class="su-clear"></div>';
		// Close gallery
		$return .= '</div>';
		// Add lightbox assets
		if ( $atts['link'] === 'lightbox' ) {
			su_query_asset( 'css', 'magnific-popup' );
			su_query_asset( 'js', 'jquery' );
			su_query_asset( 'js', 'magnific-popup' );
			su_query_asset( 'js', 'su-galleries-shortcodes' );
		}
		su_query_asset( 'css', 'su-galleries-shortcodes' );
	}else {
		// Slides not found
		$return = su_error_message( 'Custom Gallery', __( 'images not found', 'shortcodes-ultimate' ) );
	}
	return $return;
}

function amp_shortcode_posts( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$original_atts = $atts;
	// Parse attributes
	$atts = shortcode_atts( array(
			'template'            => 'templates/default-loop.php',
			'id'                  => false,
			'posts_per_page'      => get_option( 'posts_per_page' ),
			'post_type'           => 'post',
			'taxonomy'            => 'category',
			'tax_term'            => false,
			'tax_operator'        => 'IN',
			'author'              => '',
			'tag'                 => '',
			'meta_key'            => '',
			'offset'              => 0,
			'order'               => 'DESC',
			'orderby'             => 'date',
			'post_parent'         => false,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 'no'
		), $atts, 'posts' );
	$amp_su_widget_data['posts'] = $atts;
	$author = sanitize_text_field( $atts['author'] );
	$id = $atts['id']; // Sanitized later as an array of integers
	$ignore_sticky_posts = ( bool ) ( $atts['ignore_sticky_posts'] === 'yes' ) ? true : false;
	$meta_key = sanitize_text_field( $atts['meta_key'] );
	$offset = intval( $atts['offset'] );
	$order = sanitize_key( $atts['order'] );
	$orderby = sanitize_key( $atts['orderby'] );
	$post_parent = $atts['post_parent'];
	$post_status = $atts['post_status'];
	$post_type = sanitize_text_field( $atts['post_type'] );
	$posts_per_page = intval( $atts['posts_per_page'] );
	$tag = sanitize_text_field( $atts['tag'] );
	$tax_operator = $atts['tax_operator'];
	$tax_term = sanitize_text_field( $atts['tax_term'] );
	$taxonomy = sanitize_key( $atts['taxonomy'] );
	// Set up initial query for post
	$args = array(
		'category_name'  => '',
		'order'          => $order,
		'orderby'        => $orderby,
		'post_type'      => explode( ',', $post_type ),
		'posts_per_page' => $posts_per_page,
		'tag'            => $tag
	);
	// Ignore Sticky Posts
	if ( $ignore_sticky_posts ) $args['ignore_sticky_posts'] = true;
	// Meta key (for ordering)
	if ( !empty( $meta_key ) ) $args['meta_key'] = $meta_key;
	// If Post IDs
	if ( $id ) {
		$posts_in = array_map( 'intval', explode( ',', $id ) );
		$args['post__in'] = $posts_in;
	}
	// Post Author
	if ( !empty( $author ) ) $args['author'] = $author;
	// Offset
	if ( !empty( $offset ) ) $args['offset'] = $offset;
	// Post Status
	$post_status = explode( ', ', $post_status );
	$validated = array();
	$available = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any' );
	foreach ( $post_status as $unvalidated ) {
		if ( in_array( $unvalidated, $available ) ) $validated[] = $unvalidated;
	}
	if ( !empty( $validated ) ) $args['post_status'] = $validated;
	// If taxonomy attributes, create a taxonomy query
	if ( !empty( $taxonomy ) && !empty( $tax_term ) ) {
		// Term string to array
		$tax_term = explode( ',', $tax_term );
		// Validate operator
		$tax_operator = str_replace( array( 0, 1, 2 ), array( 'IN', 'NOT IN', 'AND' ), $tax_operator );
		if ( !in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ) $tax_operator = 'IN';
		$tax_args = array( 'tax_query' => array( array(
					'taxonomy' => $taxonomy,
					'field' => ( is_numeric( $tax_term[0] ) ) ? 'id' : 'slug',
					'terms' => $tax_term,
					'operator' => $tax_operator ) ) );
		// Check for multiple taxonomy queries
		$count = 2;
		$more_tax_queries = false;
		while (
			isset( $original_atts['taxonomy_' . $count] ) &&
			! empty( $original_atts['taxonomy_' . $count] ) &&
			isset( $original_atts['tax_' . $count . '_term'] ) &&
			! empty( $original_atts['tax_' . $count . '_term'] )
		) {
			// Sanitize values
			$more_tax_queries = true;
			$taxonomy = sanitize_key( $original_atts['taxonomy_' . $count] );
			$terms = explode( ', ', sanitize_text_field( $original_atts['tax_' . $count . '_term'] ) );
			$tax_operator = isset( $original_atts['tax_' . $count . '_operator'] ) ? $original_atts[
			'tax_' . $count . '_operator'] : 'IN';
			$tax_operator = in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ? $tax_operator : 'IN';
			$tax_args['tax_query'][] = array( 'taxonomy' => $taxonomy,
				'field' => 'slug',
				'terms' => $terms,
				'operator' => $tax_operator );
			$count++;
		}
		if ( $more_tax_queries ) {
			$tax_relation = 'AND';

			if (
				isset( $original_atts['tax_relation'] ) &&
				in_array( $original_atts['tax_relation'], array( 'AND', 'OR' ) )
			) {
				$tax_relation = $original_atts['tax_relation'];
			}

			$args['tax_query']['relation'] = $tax_relation;
		}
		$args = array_merge( $args, $tax_args );
	}

	// If post parent attribute, set up parent
	if ( $post_parent ) {
		if ( 'current' == $post_parent ) {
			global $post;
			$post_parent = $post->ID;
		}
		$args['post_parent'] = intval( $post_parent );
	}
	// Save original posts
	global $posts;
	$original_posts = $posts;
	// Query posts
	$posts = new WP_Query( $args );
	// Buffer output
	ob_start();
	// Search for template in stylesheet directory
	if ( file_exists( STYLESHEETPATH . '/' . $atts['template'] ) ) load_template( STYLESHEETPATH . '/' . $atts['template'], false );
	// Search for template in theme directory
	elseif ( file_exists( TEMPLATEPATH . '/' . $atts['template'] ) ) load_template( TEMPLATEPATH . '/' . $atts['template'], false );
	// Search for template in plugin directory
	elseif ( file_exists( path_join( dirname( SU_PLUGIN_FILE ), $atts['template'] ) ) ) load_template( path_join( dirname( SU_PLUGIN_FILE ), $atts['template'] ), false );
	// Template not found
	else echo su_error_message( 'Posts', __( 'template not found', 'shortcodes-ultimate' ) );
	$output = ob_get_contents();
	ob_end_clean();
	// Return original posts
	$posts = $original_posts;
	// Reset the query
	wp_reset_postdata();
	su_query_asset( 'css', 'su-shortcodes' );
	return $output;
}

function amp_shortcode_post( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'field'     => 'post_title',
			'default'   => '',
			'before'    => '',
			'after'     => '',
			'post_id'   => '',
			'post_type' => 'post',
			'filter'    => ''
		), $atts, 'post' );
	$amp_su_widget_data['post'] = $atts;
	if ( ! $atts['post_id'] ) {
		$atts['post_id'] = get_the_ID();
	}

	if ( ! $atts['post_id'] ) {
		return su_error_message( 'Post', __( 'invalid post ID', 'shortcodes-ultimate' ) );
	}

	$post = is_numeric( $atts['post_id'] )
		? get_post( $atts['post_id'] )
		: get_page_by_path( $atts['post_id'], OBJECT, $atts['post_type'] );

	$data = empty( $post ) || empty( $post->{$atts['field']} )
		? $atts['default']
		: $post->{$atts['field']};

	if (
		$atts['filter'] &&
		su_is_filter_safe( $atts['filter'] ) &&
		function_exists( $atts['filter'] )
	) {
		$data = call_user_func( $atts['filter'], $data );
	}

	return $data ? $atts['before'] . $data . $atts['after'] : '';

}

function amp_shortcode_dummy_text( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'amount' => 1,
			'what'   => 'paras',
			'cache'  => 'yes',
			'class'  => ''
		), $atts, 'dummy_text' );
	$amp_su_widget_data['dummy_text'] = $atts;
	$transient = 'su/cache/dummy_text/' . sanitize_text_field( $atts['what'] ) . '/' . intval( $atts['amount'] );
	$return = get_transient( $transient );
	if ( $return && $atts['cache'] === 'yes' ) return $return;
	else {
		$xml = simplexml_load_file( 'http://www.lipsum.com/feed/xml?amount=' . $atts['amount'] . '&what=' . $atts['what'] . '&start=0' );
		$return = '<div class="su-dummy-text' . su_get_css_class( $atts ) . '">' . wpautop( str_replace( "\n", "\n\n", $xml->lipsum ) ) . '</div>';
		set_transient( $transient, $return, 60*60*24*30 );
		return $return;
	}
}
function amp_shortcode_dummy_image( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'width'  => 500,
			'height' => 300,
			'theme'  => 'any',
			'class'  => ''
		), $atts, 'dummy_image' );
	$amp_su_widget_data['dummy_image'] = $atts;
	$url = 'http://lorempixel.com/' . $atts['width'] . '/' . $atts['height'] . '/';
	if ( $atts['theme'] !== 'any' ) $url .= $atts['theme'] . '/' . rand( 0, 10 ) . '/';
	return '<img src="' . $url . '" alt="' . __( 'Dummy image', 'shortcodes-ultimate' ) . '" width="' . $atts['width'] . '" height="' . $atts['height'] . '" class="su-dummy-image' . su_get_css_class( $atts ) . '" />';
}

function amp_shortcode_animate( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'type'      => 'bounceIn',
			'duration'  => 1,
			'delay'     => 0,
			'inline'    => 'no',
			'class'     => ''
		), $atts, 'animate' );
	$amp_su_widget_data['animate'] = $atts;
	$tag = ( $atts['inline'] === 'yes' ) ? 'span' : 'div';
	$time = '-webkit-animation-duration:' . $atts['duration'] . 's;-webkit-animation-delay:' . $atts['delay'] . 's;animation-duration:' . $atts['duration'] . 's;animation-delay:' . $atts['delay'] . 's;';
	$return = '<' . $tag . ' class="su-animate' . su_get_css_class( $atts ) . '" style="visibility:hidden;' . $time . '" data-animation="' . $atts['type'] . '" data-duration="' . $atts['duration'] . '" data-delay="' . $atts['delay'] . '">' . do_shortcode( $content ) . '</' . $tag . '>';
	su_query_asset( 'css', 'animate' );
	su_query_asset( 'js', 'jquery' );
	su_query_asset( 'js', 'jquery-inview' );
	su_query_asset( 'js', 'su-other-shortcodes' );
	return $return;
}

function amp_shortcode_meta( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'key'     => '',
			'default' => '',
			'before'  => '',
			'after'   => '',
			'post_id' => '',
			'filter'  => ''
		), $atts, 'meta' );
	$amp_su_widget_data['meta'] = $atts;
	// Define current post ID
	if ( !$atts['post_id'] ) $atts['post_id'] = get_the_ID();
	// Check post ID
	if ( !is_numeric( $atts['post_id'] ) || $atts['post_id'] < 1 ) return sprintf( '<p class="su-error">Meta: %s</p>', __( 'post ID is incorrect', 'shortcodes-ultimate' ) );
	// Check key name
	if ( !$atts['key'] ) return sprintf( '<p class="su-error">Meta: %s</p>', __( 'please specify meta key name', 'shortcodes-ultimate' ) );
	// Get the meta
	$meta = get_post_meta( $atts['post_id'], $atts['key'], true );
	// Set default value if meta is empty
	if ( !$meta ) $meta = $atts['default'];
	// Apply cutom filter
	if ($atts['filter'] && su_is_filter_safe( $atts['filter'] ) && function_exists( $atts['filter'] )) {
		$meta = call_user_func( $atts['filter'], $meta );
	}
	// Return result
	return ( $meta ) ? $atts['before'] . $meta . $atts['after'] : '';
}

function amp_shortcode_user( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'field'   => 'display_name',
			'default' => '',
			'before'  => '',
			'after'   => '',
			'user_id' => '',
			'filter'  => ''
		), $atts, 'user' );
	$amp_su_widget_data['user'] = $atts;
	// Check for password requests
	if ( $atts['field'] === 'user_pass' ) return sprintf( '<p class="su-error">User: %s</p>', __( 'password field is not allowed', 'shortcodes-ultimate' ) );
	// Define current user ID
	if ( !$atts['user_id'] ) $atts['user_id'] = get_current_user_id();
	// Check user ID
	if ( !is_numeric( $atts['user_id'] ) || $atts['user_id'] < 0 ) return sprintf( '<p class="su-error">User: %s</p>', __( 'user ID is incorrect', 'shortcodes-ultimate' ) );
	// Get user data
	$user = get_user_by( 'id', $atts['user_id'] );
	// Get user data if user was found
	$user = ( $user && isset( $user->data->{$atts['field']} ) ) ? $user->data->{$atts['field']} : $atts['default'];
	// Apply cutom filter
	if ($atts['filter'] && su_is_filter_safe( $atts['filter'] ) &&function_exists( $atts['filter'] )) {
		$user = call_user_func( $atts['filter'], $user );
	}
	// Return result
	return ( $user ) ? $atts['before'] . $user . $atts['after'] : '';
}

function amp_shortcode_template( $atts = null, $content = null ) {
	global $amp_su_widget_data;
		$atts = shortcode_atts( array(
				'name' => ''
			), $atts, 'template' );
		$amp_su_widget_data['template'] = $atts;
		// Check template name
		if ( !$atts['name'] ) return sprintf( '<p class="su-error">Template: %s</p>', __( 'please specify template name', 'shortcodes-ultimate' ) );
		// Get template output
		ob_start();
		get_template_part( str_replace( '.php', '', $atts['name'] ) );
		$output = ob_get_contents();
		ob_end_clean();
		// Return result
		return $output;
}

function amp_shortcode_qrcode( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'data' => '',
			'title' => '',
			'size' => 200,
			'margin' => 0,
			'align' => 'none',
			'link' => '',
			'target' => 'blank',
			'color' => '#000000',
			'background' => '#ffffff',
			'class' => ''
		), $atts, 'qrcode' );
	$amp_su_widget_data['qrcode'] = $atts;
	// Check the data
	if ( !$atts['data'] ) {
		return su_error_message( 'QR code', __( 'please specify the data', 'shortcodes-ultimate' ) );
	}
	// Prepare link
	$href = ( $atts['link'] ) ? ' href="' . $atts['link'] . '"' : '';
	// Prepare clickable class
	if ( $atts['link'] ) $atts['class'] .= ' su-qrcode-clickable';
	// Prepare title
	$atts['title'] = esc_attr( $atts['title'] );
	// Query assets
	su_query_asset( 'css', 'su-shortcodes' );
	// Return result
	return '<span class="su-qrcode su-qrcode-align-' . $atts['align'] . su_get_css_class( $atts ) . '"><a' . $href . ' target="_' . $atts['target'] . '" title="' . $atts['title'] . '"><img src="https://api.qrserver.com/v1/create-qr-code/?data=' . urlencode( $atts['data'] ) . '&size=' . $atts['size'] . 'x' . $atts['size'] . '&format=png&margin=' . $atts['margin'] . '&color=' . su_hex2rgb( $atts['color'] ) . '&bgcolor=' . su_hex2rgb( $atts['background'] ) . '" alt="' . $atts['title'] . '" /></a></span>';
}

function amp_shortcode_scheduler( $atts = null, $content = null ) {
	global $amp_su_widget_data;
	$atts = shortcode_atts( array(
			'time'       => 'all',
			'days_week'  => 'all',
			'days_month' => 'all',
			'months'     => 'all',
			'years'      => 'all',
			'alt'        => ''
		), $atts, 'scheduler' );
	$amp_su_widget_data['scheduler'] = $atts;
	$timestamp = current_time( 'timestamp', 0 );
	$now = array(
		'time'      => $timestamp,
		'day_week'  => date( 'w', $timestamp ),
		'day_month' => date( 'j', $timestamp ),
		'month'     => date( 'n', $timestamp ),
		'year'      => date( 'Y', $timestamp ),
	);

	if ( $atts['years'] !== 'all' ) {

		$atts['years'] = preg_replace( "/[^0-9-,]/", '', $atts['years'] );

		$selected_years = su_parse_range( $atts['years'] );

		if ( ! in_array( $now['year'], $selected_years ) ) {
			return su_do_attribute( $atts['alt'] );
		}

	}

	if ( $atts['months'] !== 'all' ) {

		$atts['months'] = preg_replace( "/[^0-9-,]/", '', $atts['months'] );

		$selected_months = su_parse_range( $atts['months'] );

		if ( ! in_array( $now['month'], $selected_months ) ) {
			return su_do_attribute( $atts['alt'] );
		}

	}

	if ( $atts['days_month'] !== 'all' ) {

		$atts['days_month'] = preg_replace( "/[^0-9-,]/", '', $atts['days_month'] );

		$selected_days_month = su_parse_range( $atts['days_month'] );

		if ( ! in_array( $now['day_month'], $selected_days_month ) ) {
			return su_do_attribute( $atts['alt'] );
		}

	}

	if ( $atts['days_week'] !== 'all' ) {

		$atts['days_week'] = preg_replace( "/[^0-9-,]/", '', $atts['days_week'] );

		$selected_days_week = su_parse_range( $atts['days_week'] );

		if ( ! in_array( $now['day_week'], $selected_days_week ) ) {
			return su_do_attribute( $atts['alt'] );
		}

	}

	if ( $atts['time'] !== 'all' ) {

		$valid_time = false;
		$atts['time'] = preg_replace( "/[^0-9-,:]/", '', $atts['time'] );

		foreach ( explode( ',', $atts['time'] ) as $range ) {

			$range = explode( '-', $range );

			if ( ! isset( $range[1] ) ) {
				$range[1] = $range[0] . ':59:59';
			}

			if ( strpos( $range[0], ':' ) === false ) {
				$range[0] .= ':00:00';
			}
			if ( strpos( $range[1], ':' ) === false ) {
				$range[1] .= ':00:00';
			}

			if (
				$now['time'] >= strtotime( $range[0], $now['time'] ) &&
				$now['time'] <= strtotime( $range[1], $now['time'] )
			) {
				$valid_time = true;
				break;
			}

		}

		if ( ! $valid_time ) {
			return su_do_attribute( $atts['alt'] );
		}

	}

	return do_shortcode( $content );

}