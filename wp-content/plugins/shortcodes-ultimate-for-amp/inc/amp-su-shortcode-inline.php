<?php
function amp_su_shortcodes_inlinecss($id, $atts=null){
	global $amp_su_widget_data;
	$inlineCss = '';
	switch ($id) {
		case 'heading':
				$atts = shortcode_atts( array(
						'style'  => 'default',
						'size'   => 13,
						'align'  => 'center',
						'margin' => '20',
						'class'  => ''
					), $amp_su_widget_data['heading'], 'heading' );
				
				$inlineCss = '.amp-su-heading-inline{
					font-size:' . intval( $atts['size'] ) . 'px;
					margin-bottom:' . $atts['margin'] . 'px;
				}';
			return $inlineCss;
		break;
		case 'tabs':
			include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-tabs.css";
		break;
		case 'dropcap':
			$atts = shortcode_atts( array(
						'style' => 'default',
				'size'  => 3,
				'class' => ''
			), $amp_su_widget_data['dropcap'], 'dropcap' );
			$em = $atts['size'] * 0.5 . 'em';
			$inlineCss = '.amp-su-dropcap-inline{
				font-size:' . $em . ';
			}';
			return $inlineCss;
		break;
		case 'list':
			$atts = shortcode_atts( array(
				'icon' => 'icon: star',
				'icon_color' => '#333',
				'style' => null,
				'class' => ''
			), $amp_su_widget_data['list'], 'list' );

			$inlineCss = '.amp-su-list-icon-color{
				color:'.$atts['icon_color'].';
			}';
			return $inlineCss;
		break;
		case 'spoiler':
		case 'accordion':
			include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-spoiler.css";
		break;
		case 'divider':
				$atts = shortcode_atts( array(
					'top'           => 'yes',
					'text'          => __( 'Go to top', 'shortcodes-ultimate' ),
					'style'         => 'default',
					'divider_color' => '#999999',
					'link_color'    => '#999999',
					'size'          => '3',
					'margin'        => '15',
					'class'         => ''
				), $amp_su_widget_data['divider'], 'divider' );

				$inlineCss = '.amp-su-link-color{
						color:' . $atts['link_color'] . ';
					}
					.amp-su-divider-inline{
						margin:' . $atts['margin'] . 'px 0;
						border-width:' . $atts['size'] . 'px;
						border-color:' . $atts['divider_color'].';
					}';
			return $inlineCss;
		break;
		case 'spacer':
				$atts = shortcode_atts( array(
				'size'  => '20',
				'class' => ''
				), $amp_su_widget_data['spacer'], 'spacer' );
				$inlineCss = '.amp-su-spacer-inline{
					height:' . (string) $atts['size'] . 'px;
				}';
			return $inlineCss;
		break;
		case 'highlight':
			$atts = shortcode_atts( array(
				'background' => '#ddff99',
				'bg'         => null, // 3.x
				'color'      => '#000000',
				'class'      => ''
			), $amp_su_widget_data['highlight'], 'highlight' );
			$inlineCss = '.su-highlight {
					display: inline;
					padding: 1px 0;
				}
				.amp-su-highlight-inline{
						background:' . $atts['background'] . ';
						color:' . $atts['color'] . ';
				}';
			return $inlineCss;
		break;
		case 'label':
			$atts = shortcode_atts( array(
				'type'  => 'default',
				'style' => null, // 3.x
				'class' => ''
			), $amp_su_widget_data['label'], 'label' );
	
			$inlineCss = '.su-label {
					display: inline;
					margin-right: 0.3em;
					padding: 0.1em 0.3em;
					font-size: 0.8em;
					height: 0.8em;
					line-height: 0.8em;
					color: #fff;
					border-radius: 3px;
					-moz-border-radius: 3px;
					-webkit-border-radius: 3px;
				}
				.su-label-type-default { background: #999 }
				.su-label-type-success { background: #090 }
				.su-label-type-warning { background: #f90 }
				.su-label-type-important { background: #f03 }
				.su-label-type-black { background: #111 }
				.su-label-type-info { background: #09c }
				.amp-su-label-inline {
					
				}';
			return $inlineCss;
		break;
		
		case 'button':
			$atts = shortcode_atts( array(
				'url'         => get_option( 'home' ),
				'link'        => null, // 3.x
				'target'      => 'self',
				'style'       => 'default',
				'background'  => '#2D89EF',
				'color'       => '#FFFFFF',
				'dark'        => null, // 3.x
				'size'        => 3,
				'wide'        => 'no',
				'center'      => 'no',
				'radius'      => 'auto',
				'icon'        => false,
				'icon_color'  => '#FFFFFF',
				'ts_color'    => null, // Dep. 4.3.2
				'ts_pos'      => null, // Dep. 4.3.2
				'text_shadow' => 'none',
				'desc'        => '',
				'onclick'     => '',
				'rel'         => '',
				'title'       => '',
				'id'          => '',
				'class'       => ''
			), $amp_su_widget_data['button'], 'button' );
		if ( $atts['link'] !== null ) $atts['url'] = $atts['link'];
		if ( $atts['dark'] !== null ) {
			$atts['background'] = $atts['color'];
			$atts['color'] = ( $atts['dark'] ) ? '#000' : '#fff';
		}
		// Prepare vars
		$a_css = array();
		$span_css = array();
		$img_css = array();
		$small_css = array();
		$radius = '0px';

		// Text shadow values
		$shadows = array(
			'none'         => '0 0',
			'top'          => '0 -1px',
			'right'        => '1px 0',
			'bottom'       => '0 1px',
			'left'         => '-1px 0',
			'top-right'    => '1px -1px',
			'top-left'     => '-1px -1px',
			'bottom-right' => '1px 1px',
			'bottom-left'  => '-1px 1px'
		);
		// Common styles for button
		$styles = array(
			'size'     => round( ( $atts['size'] + 7 ) * 1.3 ),
			'ts_color' => ( $atts['ts_color'] === 'light' ) ? su_hex_shift( $atts['background'], 'lighter', 50 ) : su_hex_shift( $atts['background'], 'darker', 40 ),
			'ts_pos'   => ( $atts['ts_pos'] !== null ) ? $shadows[$atts['ts_pos']] : $shadows['none']
		);
		// Calculate border-radius
		if ( $atts['radius'] == 'auto' ) $radius = round( $atts['size'] + 2 ) . 'px';
		elseif ( $atts['radius'] == 'round' ) $radius = round( ( ( $atts['size'] * 2 ) + 2 ) * 2 + $styles['size'] ) . 'px';
		elseif ( is_numeric( $atts['radius'] ) ) $radius = intval( $atts['radius'] ) . 'px';
		// CSS rules for <a> tag
		$a_rules = array(
			'color'                 => $atts['color'],
			'background-color'      => $atts['background'],
			'border-color'          => su_hex_shift( $atts['background'], 'darker', 20 ),
			'border-radius'         => $radius,
			'-moz-border-radius'    => $radius,
			'-webkit-border-radius' => $radius
		);
		// CSS rules for <span> tag
		$span_rules = array(
			'color'                 => $atts['color'],
			'padding'               => ( $atts['icon'] ) ? round( ( $atts['size'] ) / 2 + 4 ) . 'px ' . round( $atts['size'] * 2 + 10 ) . 'px' : '0px ' . round( $atts['size'] * 2 + 10 ) . 'px',
			'font-size'             => $styles['size'] . 'px',
			'line-height'           => ( $atts['icon'] ) ? round( $styles['size'] * 1.5 ) . 'px' : round( $styles['size'] * 2 ) . 'px',
			'border-color'          => su_hex_shift( $atts['background'], 'lighter', 30 ),
			'border-radius'         => $radius,
			'-moz-border-radius'    => $radius,
			'-webkit-border-radius' => $radius,
			'text-shadow'           => $styles['ts_pos'] . ' 1px ' . $styles['ts_color'],
			'-moz-text-shadow'      => $styles['ts_pos'] . ' 1px ' . $styles['ts_color'],
			'-webkit-text-shadow'   => $styles['ts_pos'] . ' 1px ' . $styles['ts_color']
		);
		// Apply new text-shadow value
		if ( $atts['ts_color'] === null && $atts['ts_pos'] === null ) {
			$span_rules['text-shadow'] = $atts['text_shadow'];
			$span_rules['-moz-text-shadow'] = $atts['text_shadow'];
			$span_rules['-webkit-text-shadow'] = $atts['text_shadow'];
		}
		// CSS rules for <img> tag
		$img_rules = array(
			'width'     => round( $styles['size'] * 1.5 ) . 'px',
			'height'    => round( $styles['size'] * 1.5 ) . 'px'
		);
		// CSS rules for <small> tag
		$small_rules = array(
			'padding-bottom' => round( ( $atts['size'] ) / 2 + 4 ) . 'px',
			'color' => $atts['color']
		);


		// Create style attr value for <a> tag
		foreach ( $a_rules as $a_rule => $a_value ) $a_css[] = $a_rule . ':' . $a_value;
		// Create style attr value for <span> tag
		foreach ( $span_rules as $span_rule => $span_value ) $span_css[] = $span_rule . ':' . $span_value;
		// Create style attr value for <img> tag
		foreach ( $img_rules as $img_rule => $img_value ) $img_css[] = $img_rule . ':' . $img_value;
		// Create style attr value for <img> tag
		foreach ( $small_rules as $small_rule => $small_value ) $small_css[] = $small_rule . ':' . $small_value;

		$inlineCss = '.amp-su-button-a{
			'.implode( $a_css, ';' ).'
		}
		.amp-su-button-span{
			'.implode( $span_css, ';' ).'
		}
		.su-btn-img{
			'.implode( $img_css, ';' ).'
		}
		.su-btn-sm{
			'.implode( $small_css, ';' ).'
		}
		.su-btn-icon{
			font-size:' . $styles['size'] . 'px;color:' . $atts['icon_color'] . '
		}';
		return $inlineCss;
		break;
		case 'service':
			$atts = shortcode_atts( array(
					'title'       => __( 'Service title', 'shortcodes-ultimate' ),
					'icon'        => plugins_url( 'assets/images/service.png', SU_PLUGIN_FILE ),
					'icon_color'  => '#333',
					'size'        => 32,
					'class'       => ''
				), $amp_su_widget_data['service'], 'service' );
			$rtl = ( is_rtl() ) ? 'right' : 'left';
			$inlineCss = '.amp-su-service{
					padding-' . $rtl . ':' . round( $atts['size'] + 14 ) . 'px;
					min-height:' . $atts['size'] . 'px;
					line-height:' . $atts['size'] . 'px;
				}
				.amp-su-icon-inline{
					font-size:' . $atts['size'] . 'px;
					color:' . $atts['icon_color'] . ';
				}
				.amp-su-service-content{
					 padding-' . $rtl . ':' . round( $atts['size'] + 14 ) . 'px;
				}';
			return $inlineCss;
		break;
		case 'box':
			$atts = shortcode_atts( array(
				'title'       => __( 'This is box title', 'shortcodes-ultimate' ),
				'style'       => 'default',
				'box_color'   => '#333333',
				'title_color' => '#FFFFFF',
				'color'       => null, // 3.x
				'radius'      => '3',
				'class'       => ''
			), $amp_su_widget_data['box'], 'box' );
			if ( $atts['color'] !== null ) {
					$atts['box_color'] = $atts['color'];
			}

			$atts['radius'] = is_numeric( $atts['radius'] ) ? intval( $atts['radius'] ) : 0;
			$atts['inner_radius'] = $atts['radius'] > 2 ? $atts['radius'] - 2 : 0;

			$hex_color = su_hex_shift( $atts['box_color'], 'darker', 20 );

			$inlineCss = '.amp-su-box-inline{
					border-color:'.$hex_color.';
					border-radius:'.$atts['radius'].'px;
				}
				.amp-su-box-inner{
					background-color:'.$atts['box_color'].';
					color:'.$atts['title_color'].';
					border-top-left-radius:'.$atts['inner_radius'].'px;
					border-top-right-radius:'.$atts['inner_radius'].'px;
				}
				.amp-su-box-content{
					border-bottom-left-radius:'.$atts['inner_radius'].'px;
					border-bottom-right-radius:'.$atts['inner_radius'].'px;
				}';
			return $inlineCss;
		break;
		case 'note':
		global $amp_su_widget_data;
		$atts = shortcode_atts( array(
				'note_color' => '#FFFF66',
				'text_color' => '#333333',
				'background' => null, // 3.x
				'color'      => null, // 3.x
				'radius'     => '3',
				'class'      => ''
			), $amp_su_widget_data['note'], 'note' );
		
		if ( $atts['color'] !== null ) $atts['note_color'] = $atts['color'];
		if ( $atts['background'] !== null ) $atts['note_color'] = $atts['background'];
		// Prepare border-radius
			$radius = ( $atts['radius'] != '0' ) ? 'border-radius:' . $atts['radius'] . 'px;-moz-border-radius:' . $atts['radius'] . 'px;-webkit-border-radius:' . $atts['radius'] . 'px;' : '';
			$inlineCss = '.su-note {
					margin: 0 0 1.5em 0;
					border-width: 1px;
					border-style: solid;
				}
				.su-note-inner {
					padding: 1em;
					border-width: 1px;
					border-style: solid;
				}
				.amp-su-note-wrapper{
					border-color:'.su_hex_shift( $atts['note_color'], 'darker', 10 ).';
					' . $radius . ';
				}
				.amp-su-note-content{
					background-color:' . $atts['note_color'] . ';
					border-color:'.su_hex_shift( $atts['note_color'],'lighter', 80 ).';
					color:' . $atts['text_color'] . ';
					' . $radius . ';
				}';
			return $inlineCss;
		break;
		case 'expand':
		$atts = shortcode_atts( array(
				'more_text'  => __( 'Show more', 'shortcodes-ultimate' ),
				'less_text'  => __( 'Show less', 'shortcodes-ultimate' ),
				'height'     => '100',
				'hide_less'  => 'no',
				'text_color' => '#333333',
				'link_color' => '#0088FF',
				'link_style' => 'default',
				'link_align' => 'left',
				'more_icon'  => '',
				'less_icon'  => '',
				'class'      => ''
			), $amp_su_widget_data['expand'], 'expand' );
		$more_icon = ( $atts['more_icon'] ) ? Su_Tools::icon( $atts['more_icon'] ) : '';
		$less_icon = ( $atts['less_icon'] ) ? Su_Tools::icon( $atts['less_icon'] ) : '';
		if ( $more_icon || $less_icon ) su_query_asset( 'css', 'font-awesome' );
		// Prepare less link
		$inlineCss = '.amp-su-exand-text {
					text-align:' . $atts['link_align'].';
				}
				.amp-su-expand-link{
					color:' . $atts['link_color'] . ';
					border-color:' . $atts['link_color'] . ';
				}
				.amp-su-expand-span{
					border-color:' . $atts['link_color'] . ';
				}
				.amp-su-expand-section{
					color:' . $atts['text_color'] . ';
					/*max-height:' . intval( $atts['height'] ) . 'px;*/
					overflow:hidden;
				}
				amp-accordion section[expanded] .show-more {
				    display: none;
				}
				amp-accordion section:not([expanded]) .show-less {
				    display: none;
				}
				.nested-accordion h4 {
					font-size: 14px;
					background-color: #ddd;
				}
				amp-accordion#hidden-header section[expanded] h4 {
					border: none;
				}';
				return $inlineCss;
			break;
		case 'lightbox_content':
			$atts = shortcode_atts( array(
					'id'         => '',
					'width'      => '50%',
					'margin'     => '40',
					'padding'    => '40',
					'text_align' => 'center',
					'background' => '#FFFFFF',
					'color'      => '#333333',
					'shadow'     => '0px 0px 15px #333333',
					'class'      => ''
				), $amp_su_widget_data['lightbox_content'], 'lightbox_content' );
			$inlineCss ='.su-lightbox-content {
							position: relative;
							margin: 0 auto;
						}
						.mfp-content .su-lightbox-content,
						#su-generator .su-lightbox-content { display: block !important; }
						.su-lightbox-content-preview {
							width: 100%;
							min-height: 300px;
							background: #444;
							overflow: hidden;
						}
						.su-lightbox-content h1,
						.su-lightbox-content h2,
						.su-lightbox-content h3,
						.su-lightbox-content h4,
						.su-lightbox-content h5,
						.su-lightbox-content h6 { color: inherit; }
						.amp-su-inlinelightbox {
							width:' . $atts['width'] . ';
							margin-top:' . $atts['margin'] . 'px;
							margin-bottom:' . $atts['margin'] . 'px;
							padding:' . $atts['padding'] . 'px;
							background-color:' . $atts['background'] . ';
							color:' . $atts['color'] . ';
							box-shadow:' . $atts['shadow'] . ';
							text-align:' . $atts['text_align'] . ';
						}';

			return $inlineCss;
		break;
		case 'lightbox':
			$inlineCss = 'div[overflow] {
						  	text-align: center;
						  	background: #f9f7f7;
						  	line-height: 36px;
						}
						.lightbox {
				      background: rgba(0,0,0,0.8);
				      width: 100%;
				      height: 100%;
				      position: absolute;
				      display: flex;
				      align-items: center;
				      justify-content: center;
			    	}';
			return  $inlineCss; 
		break;
		case 'carousel':
			$inlineCss = '#carousel-with-preview amp-img {
				      transition: opacity 1s ease-in-out;
				    }
				    .carousel-preview {
				      display: flex;
				      align-items: center;
				      justify-content: center;
				    }
				    .carousel-preview button {
				      height: 80px;
				      width: 80px;
				      min-width: 60px;
				      margin: 4px;
				      padding: 0;
				    }
				    .carousel-preview .amp-carousel-slide {
				      margin: 4px;
				      padding: 0;
				    }
				    .carousel-preview button:active {
				      opacity: 0.5;
				    }';
			return  $inlineCss;
		break;
		default:

			break;
	}
}