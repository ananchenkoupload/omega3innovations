<?php
global $amp_su_widget_data;
$prefix = su_get_shortcode_prefix();
$atts = array();
foreach ( $amp_su_widget_data as $id => $attributes ) {
					$atts[] = $attributes;
				switch ($id) {
					case 'heading':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-heading.css";
						echo amp_su_shortcodes_inlinecss($id, $atts);
					break;
					case 'tabs':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-tabs.css";
					break;
					case 'spoiler':
					case 'accordion':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-spoiler.css";
					break;
					case 'divider':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-divider.css";
						echo amp_su_shortcodes_inlinecss($id, $atts);
					break;
					case 'spacer':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-spacer.css";
						echo amp_su_shortcodes_inlinecss($id, $atts);
					break;
					case 'highlight':
						echo amp_su_shortcodes_inlinecss($id, $atts);
					break;
					case 'label':
						echo amp_su_shortcodes_inlinecss($id, $atts);
					break;
					case 'quote':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-quote.css";
					break;
					case 'pullquote':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-pullquote.css";
					break;
					case 'dropcap':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-dropcap.css";
						echo amp_su_shortcodes_inlinecss($id, $atts);
					break;
					case 'frame':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-frame.css";
					break;
					case 'column':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-column.css";
					break;
					case 'list':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-list.css";
						echo amp_su_shortcodes_inlinecss($id, $atts);
					break;
					case 'button':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-button.css";
						echo amp_su_shortcodes_inlinecss($id, $atts);
					break;
					case 'service':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-service.css";
						echo amp_su_shortcodes_inlinecss($id, $atts);
					break;
					case 'box':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-box.css";
						echo amp_su_shortcodes_inlinecss($id, $atts);
					break;
					case 'note':
						echo amp_su_shortcodes_inlinecss($id, $atts);
					break;
					case 'expand':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-expand.css";
						echo amp_su_shortcodes_inlinecss($id, $atts);
					break;
					case 'custom_gallery':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-custom-gallery.css";
						echo amp_su_shortcodes_inlinecss($id, $atts);
    				break;
					case 'carousel':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-carousel.css";
						echo amp_su_shortcodes_inlinecss($id, $atts);
    				break;
					case 'slider':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-slider.css";
    				break;
    				case 'table':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-table.css";
    				break;
    				case 'posts':
						include_once AMP_SU_PLUGIN_DIR."assets/css/amp-su-posts.css";
    				break;
    				case 'expand':
    					echo amp_su_shortcodes_inlinecss($id, $atts);
    				break;
    				case 'lightbox':
    					echo amp_su_shortcodes_inlinecss($id, $atts);
    				break;
    				case 'lightbox_content':
    					echo amp_su_shortcodes_inlinecss($id, $atts);
    				break;
    				case 'member':
    				$member_inline = '.amp-su-member-inline{
					background-color:'.su_hex_shift( $atts['color'], 'lighter', 50 ).';
					border-color:'.su_hex_shift( $atts['color'], 'darker', 20 ).';
					color:' .su_hex_shift( $atts['color'], 'darker', 60 ) .';}';
    				echo $member_inline;
    				break;
					default:
						
						break;
				}
			}