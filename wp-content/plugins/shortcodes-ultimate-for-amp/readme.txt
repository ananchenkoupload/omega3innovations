=== Shortcodes Ultimate for AMP ===
Contributors: mohammed_kaludi, ahmedkaludi, ampforwp
Requires at least: 3.0
Tested up to: 5.1.1
Stable tag: 1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==
This is an extension for 'Shortcode Ultimate' Plugin for AMP Compatibility.


== Changelog ==

= 1.2 (12 April 2019) =
* Feature Added:
    * Autoplay feature added in carousel.

* Bugs Fixed:
	* 'style amp-custom' Validation errors resolved.	
= 1.1
* code improvement for License Key.

= 1.0
* Initial release