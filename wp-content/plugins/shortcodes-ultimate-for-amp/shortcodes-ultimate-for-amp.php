<?php
/*
Plugin Name: AMP Shortcodes Ultimate
Plugin URI: https://ampforwp.com/shortcodes-ultimate/
Description: This is an extension for 'Shortcode Ultimate' Plugin for AMP Compatibility.
Version: 1.2
Author: AMPforWP Team
Author URI: https://ampforwp.com/
Donate link: https://www.paypal.me/Kaludi/5
License: GPL2
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

define('AMP_SU_PLUGIN_DIR', plugin_dir_path( __FILE__ ));
define('AMP_SU_PLUGIN_DIR_URI', plugin_dir_url(__FILE__));
define('AMP_SU_IMAGE_DIR',plugin_dir_url(__FILE__).'assets/images');
define('AMP_SU_MAIN_PLUGIN_DIR', plugin_dir_path( __DIR__ ) );
define('AMP_SU_INC_PLUGIN_DIR', plugin_dir_path( __FILE__ )."/inc/" );

define('AMP_SU_VERSION','1.2');

define('MINIMUM_SU_VERSION', '5.0.6');
/*License Checker*/
// this is the URL our updater / license checker pings. This should be the URL of the site with EDD installed
define( 'AMP_SU_STORE_URL', 'https://accounts.ampforwp.com/' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

// the name of your product. This should match the download name in EDD exactly
define( 'AMP_SU_ITEM_NAME', 'Shortcodes Ultimate' );

// the download ID. This is the ID of your product in EDD and should match the download ID visible in your Downloads list (see example below)
//define( 'AMPFORWP_ITEM_ID', 2502 );
// the name of the settings page for the license input to be displayed
define( 'AMP_SU_LICENSE_PAGE', 'shortcodes-ultimate' );
if(! defined('AMP_SU_ITEM_FOLDER_NAME')){
    $folderName = basename(__DIR__);
    define( 'AMP_SU_ITEM_FOLDER_NAME', $folderName );
}
/*License Checker*/


//register_activation_hook( __FILE__, 'amp_su_shortcodes_activation_hook' );

add_action('plugins_loaded','amp_su_shortcodes_ulitmate_notices');
function amp_su_shortcodes_ulitmate_notices(){
	/* Create transient data */
    set_transient( 'amp-su-shortcodes-admin-notice', true, DAY_IN_SECONDS );
    // Check if Elementor installed and activated
	if ( !class_exists('Shortcodes_Ultimate_Shortcodes')) {
		add_action( 'admin_notices', 'admin_notice_missing_shortcode_plugin' );
		return;
	}
	if( class_exists('Shortcodes_Ultimate_Shortcodes') && ! version_compare( SU_PLUGIN_VERSION, MINIMUM_SU_VERSION, '>=' )){
		add_action( 'admin_notices', 'amp_su_shortcodes_admin_notice' );
	}
}
/* Add admin notice */


function admin_notice_missing_shortcode_plugin() {
	
	if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

	$message = sprintf(
		/* translators: 1: Plugin name 2: Elementor */
		esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'shortcodes-ultimate' ),
		'<strong>' . esc_html__( 'Shortcodes Ultimate for AMP', 'shortcodes-ultimate' ) . '</strong>',
		'<strong>' . esc_html__( 'Shortcode Ultimate', 'amp-elementor-extension' ) . '</strong>'
	);

	printf( '<div class="notice notice-error is-dismissible"><p>%1$s</p></div>', $message );

}

function amp_su_shortcodes_admin_notice(){
	if ( ! function_exists( 'plugins_api' ) ) {
      require_once( ABSPATH . 'wp-admin/includes/plugin-install.php' );
	}
	// set the arguments to get latest info from repository via API ##
	$args = array(
	    'slug' => 'shortcodes-ultimate',
	    'fields' => array(
	        'version' => true,
	    )
	);

	/** Prepare our query */
	$call_api = plugins_api( 'plugin_information', $args );

	/** Check for Errors & Display the results */
	if ( is_wp_error( $call_api ) ) {
	    $api_error = $call_api->get_error_message();
	} else {
	    //echo $call_api; // everything ##
	    if ( ! empty( $call_api->version ) ) {
	        $version_latest = $call_api->version;
	    }
	}
	/* Check transient, if available display notice */
    if( get_transient( 'amp-su-shortcodes-admin-notice' ) ){
    	$plugin_version = SU_PLUGIN_VERSION;
        	echo '<div class="notice notice-error is-dismissible"><p>"AMP Shortcode Ultimate" plugin is not compatible with current version('.$plugin_version.') of your "Shortcode Ultimate" plugin, <strong>Please Update to Latest Version('.$version_latest.')</strong></p>
        </div>';
        
        /* Delete transient, only display this notice once. */
        delete_transient( 'amp-su-shortcodes-admin-notice' );
    }
}
 

require_once AMP_SU_INC_PLUGIN_DIR.'amp-su-shortcode-functions.php';
require_once AMP_SU_INC_PLUGIN_DIR.'amp-su-shortcode-inline.php';

add_action( 'wp', 'amp_su_shortcode_override' );
$amp_su_widget_data = array();
function amp_su_shortcode_override() {
	if ( (function_exists( 'ampforwp_is_amp_endpoint' ) && ampforwp_is_amp_endpoint()) ||  (function_exists( 'is_wp_amp' ) && is_wp_amp()) || (function_exists( 'is_amp_endpoint' ) && is_amp_endpoint()) ) {
		
		if ( class_exists('Shortcodes_Ultimate_Shortcodes')) {
			require_once AMP_SU_INC_PLUGIN_DIR.'amp-su-add-shortcodes.php';
		}
	}
}

add_filter('amp_post_template_css',function(){
	if ( class_exists('Shortcodes_Ultimate_Shortcodes')) {
		$common_style = '.su-clearfix:before,.su-clearfix:after {display: table; content: " ";}	.su-clearfix:after { clear: both; }';
		echo $common_style;
		require_once AMP_SU_INC_PLUGIN_DIR.'amp-su-shortcode-styles.php';
	}
});
add_filter( 'amp_post_template_data', 'amp_su_shortcode_scripts', 20);
function amp_su_shortcode_scripts($data){
	if ( class_exists('Shortcodes_Ultimate_Shortcodes')) {
		global $amp_su_widget_data;
		$data['amp_component_scripts']['amp-selector'] = 'https://cdn.ampproject.org/v0/amp-selector-0.1.js';
		$data['amp_component_scripts']['amp-bind'] = 'https://cdn.ampproject.org/v0/amp-bind-0.1.js';
		$data['amp_component_scripts']['amp-accordion'] = 'https://cdn.ampproject.org/v0/amp-accordion-0.1.js';
		$data['amp_component_scripts']['amp-lightbox'] = 'https://cdn.ampproject.org/v0/amp-lightbox-0.1.js';
		$data['amp_component_scripts']['amp-audio'] = 'https://cdn.ampproject.org/v0/amp-audio-0.1.js';
		$data['amp_component_scripts']['amp-video'] = 'https://cdn.ampproject.org/v0/amp-video-0.1.js';
		$data['amp_component_scripts']['amp-iframe'] = 'https://cdn.ampproject.org/v0/amp-iframe-0.1.js';
		$data['amp_component_scripts']['amp-image-lightbox'] = 'https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js';
		$data['amp_component_scripts']['amp-carousel'] = 'https://cdn.ampproject.org/v0/amp-carousel-0.1.js';
		$data['amp_component_scripts']['amp-fit-text'] = 'https://cdn.ampproject.org/v0/amp-fit-text-0.1.js';
		$data['amp_component_scripts']['amp-youtube'] = 'https://cdn.ampproject.org/v0/amp-youtube-0.1.js';
		$data['amp_component_scripts']['amp-lightbox-gallery'] = 'https://cdn.ampproject.org/v0/amp-lightbox-gallery-0.1.js';
		foreach ( $amp_su_widget_data as $id => $attributes ) {
			if($id == 'document'){
				$data['amp_component_scripts']['amp-google-document-embed'] = 'https://cdn.ampproject.org/v0/amp-google-document-embed-0.1.js';
			}
		}
	}
	return $data;
}
add_action( 'amp_post_template_head', 'amp_su_shortcodes_canonical_link' );
function amp_su_shortcodes_canonical_link(){
	if ( class_exists('Shortcodes_Ultimate_Shortcodes')) {
	?>
	<link rel='stylesheet' id='font-awesome-css'  href='https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css?ver=4.6.3' type='text/css' media='all' />
	<?php
	}
}


/**
*
* Update Codes
*
*
**/

// Notice to enter license key once activate the plugin

$path = plugin_basename( __FILE__ );
add_action("after_plugin_row_{$path}", function( $plugin_file, $plugin_data, $status ) {
    global $redux_builder_amp;
     if(! defined('AMP_SU_ITEM_FOLDER_NAME')){
    $folderName = basename(__DIR__);
        define( 'AMP_SU_ITEM_FOLDER_NAME', $folderName );
    }
    $pluginsDetail = @$redux_builder_amp['amp-license'][AMP_SU_ITEM_FOLDER_NAME];
    $pluginstatus = $pluginsDetail['status'];

    if(empty($redux_builder_amp['amp-license'][AMP_SU_ITEM_FOLDER_NAME]['license'])){
        echo "<tr class='active'><td>&nbsp;</td><td colspan='2'><a href='".esc_url(  self_admin_url( 'admin.php?page=amp_options&tabid=opt-go-premium' )  )."'>Please enter the license key</a> to get the <strong>latest features</strong> and <strong>stable updates</strong></td></tr>";
            }elseif($pluginstatus=="valid"){
                $update_cache = get_site_transient( 'update_plugins' );
        $update_cache = is_object( $update_cache ) ? $update_cache : new stdClass();
        if(isset($update_cache->response[ AMP_SU_ITEM_FOLDER_NAME ]) 
            && empty($update_cache->response[ AMP_SU_ITEM_FOLDER_NAME ]->download_link) 
          ){
           unset($update_cache->response[ AMP_SU_ITEM_FOLDER_NAME ]);
        }
        set_site_transient( 'update_plugins', $update_cache );
        
   }
}, 10, 3 );

/**
*    Plugin Update Method
**/
require_once dirname( __FILE__ ) . '/updater/EDD_SL_Plugin_Updater.php';
// Check for updates
function amp_su_plugin_updater() {

    // retrieve our license key from the DB
    //$license_key = trim( get_option( 'amp_ads_license_key' ) );
    $selectedOption = get_option('redux_builder_amp',true);
    $license_key = '';//trim( get_option( 'amp_ads_license_key' ) );
    $pluginItemName = '';
    $pluginItemStoreUrl = '';
    $pluginstatus = '';
    if( isset($selectedOption['amp-license']) && "" != $selectedOption['amp-license'] && isset($selectedOption['amp-license'][AMP_SU_ITEM_FOLDER_NAME])){

       $pluginsDetail = $selectedOption['amp-license'][AMP_SU_ITEM_FOLDER_NAME];
       $license_key = $pluginsDetail['license'];
       $pluginItemName = $pluginsDetail['item_name'];
       $pluginItemStoreUrl = $pluginsDetail['store_url'];
       $pluginstatus = $pluginsDetail['status'];
    }
    
    // setup the updater
    $edd_updater = new AMP_SU_EDD_SL_Plugin_Updater( AMP_SU_STORE_URL, __FILE__, array(
            'version'   => AMP_SU_VERSION,                // current version number
            'license'   => $license_key,                        // license key (used get_option above to retrieve from DB)
            'license_status'=>$pluginstatus,
            'item_name' => AMP_SU_ITEM_NAME,          // name of this plugin
            'author'    => 'Mohammed Kaludi',                   // author of this plugin
            'beta'      => false,
        )
    );
}
add_action( 'admin_init', 'amp_su_plugin_updater', 0 );
 