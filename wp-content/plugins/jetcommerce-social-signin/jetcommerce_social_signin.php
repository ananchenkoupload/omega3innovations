<?php
   /*
   Plugin Name: Jetcommerce Custom social signin
   Description: A plugin to signin with facebook and google+
   Version: 0.1
   Author: Esolz
   */


function my_shortcode_login_form( $atts, $content = null ) {

    ob_start();
    ?>
    <?php
    global $woocommerce;
    $cart = $woocommerce->cart;
	if (!session_id())
    	session_start();
	$_SESSION['old_cart'] = $cart;
	?>
    <script src="https://apis.google.com/js/platform.js?onload=startApp" async defer></script>
    <script>
var googleUser = {};
  function startApp() {
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
        client_id: '<?php echo get_option('jetcommerce_google_client_id'); ?>',
        cookiepolicy: 'single_host_origin',
        approvalprompt: 'force'
      });
      attachSignin(document.getElementById('customBtn'));
    });
  }
  function attachSignin(element) {
    console.log(element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser) {
        	jQuery('body').addClass('ldd');
          	var name=googleUser.getBasicProfile().getName();
        	var email=googleUser.getBasicProfile().getEmail();
        	var userid=googleUser.getBasicProfile().getId();
        	 var data = {
					'action': 'gplus_login',
					'name': name,
					'email': email,
					'id': userid
				
				};
				jQuery.post("<?php echo admin_url( 'admin-ajax.php' );?>", data, function(response) {
					//alert(response);
					//window.location.replace(response);
					
					if(response.indexOf('success')>-1){
						window.location.href = "<?php the_permalink(get_the_ID());?>";
					}else{
						window.location.href = "<?php the_permalink(get_the_ID());?>?msg=failed";
					}
					
					
				});
        }, function(error) {
          alert('Popup closed. Please login again.');
        });
  }
  logInWithFacebook = function() {
    FB.login(function(response) {
      if (response.status === 'connected') {
        FB.api('/me?fields=name,first_name,last_name,email', function(response) {
           jQuery('body').addClass('ldd');
           var name = response.name;
	       var email = response.email;
	       var userid = response.id;
	       var first_name= response.first_name;
	       var last_name= response.last_name;
	       var data = {
				'action': 'fb_login',
				'name': name,
				'email': email,
				'id': userid,
				'first_name': first_name,
				'last_name': last_name
			};
		  jQuery.post("<?php echo admin_url( 'admin-ajax.php' );?>", data, function(response) {
		  		//alert(response);
		  		
				if(response.indexOf('success')>-1){
					window.location.href = "<?php the_permalink(get_the_ID());?>";
				}else{
					window.location.href = "<?php the_permalink(get_the_ID());?>?msg=failed";
				}
			});
        });
      } else {
        alert('Popup closed. Please login again.');
      }
    }, {scope: 'email'});
    return false;
  };
  window.fbAsyncInit = function() {
    FB.init({
      appId: '<?php echo get_option('jetcommerce_facebook_app_id'); ?>',
      cookie: true, // This is important, it's not enabled by default
      version: 'v2.2'
    });
  };

  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  
  if(typeof fbq === 'undefined') {
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '123123123213121');
    fbq('track', 'PageView');
    fbq('track', '{{fBPixelType}}');
  }
  else {
    fbq('track', '{{fBPixelType}}');
  }
</script>

<div class="text-center jc-social-btn">
<a id="fbLogin" href="javascript:void(0);" onclick="logInWithFacebook();">
<!--<i class="fa fa-facebook-official" aria-hidden="true"></i>-->
<figure><img src="<?php echo get_template_directory_uri().'/images/fbColor.png';?>" alt="" /></figure>
   <span>
      Facebook
   </span>
</a>
<a id="customBtn" href="javascript:void(0);" onclick="startApp();">
<figure><img src="<?php echo get_template_directory_uri().'/images/gColor.png';?>" alt="" /></figure>
   <span>
      Google
   </span>
</a>
</div>
<span class="login_other_option express_other"><span class="express_inner">Login Using Username</span></span></br>
    <?php
    $output_string = ob_get_contents();
    ob_end_clean();

    return $output_string;
}

add_shortcode('social_login', 'my_shortcode_login_form');

function my_shortcode_signup_form( $atts, $content = null ) {

    ob_start();
    ?>
    <?php
    global $woocommerce;
    $cart = $woocommerce->cart;
	if (!session_id())
    	session_start();
	$_SESSION['old_cart'] = $cart;
	?>
    <script src="https://apis.google.com/js/platform.js?onload=startApp" async defer></script>
    <script>
var googleUser1 = {};
  function startApp1() {
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
        client_id: '<?php echo get_option('jetcommerce_google_client_id'); ?>',
        cookiepolicy: 'single_host_origin',
        approvalprompt: 'force'
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      attachSignin1(document.getElementById('customBtn1'));
    });
  }
  startApp1();
  function attachSignin1(element) {
    console.log(element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser1) {
        	jQuery('body').addClass('ldd');
          	var name=googleUser1.getBasicProfile().getName();
        	var email=googleUser1.getBasicProfile().getEmail();
        	var userid=googleUser1.getBasicProfile().getId();
        	 var data = {
					'action': 'gplus_login',
					'name': name,
					'email': email,
					'id': userid
				
				};
				jQuery.post("<?php echo admin_url( 'admin-ajax.php' );?>", data, function(response) {
					//alert(response);
					//window.location.replace(response);
					
					if(response.indexOf('success')>-1){
						window.location.href = "<?php the_permalink(get_the_ID());?>";
					}else{
						window.location.href = "<?php the_permalink(get_the_ID());?>?msg=failed";
					}
				});
        }, function(error) {
          alert('Popup closed. Please login again.');
        });
  }

  logInWithFacebook1 = function() {
    FB.login(function(response) {
      if (response.status === 'connected') {
        FB.api('/me?fields=name,first_name,last_name,email', function(response) {
           jQuery('body').addClass('ldd');
           var name = response.name;
	       var email = response.email;
	       var userid = response.id;
	       var first_name= response.first_name;
	       var last_name= response.last_name;
	       var data = {
				'action': 'fb_login',
				'name': name,
				'email': email,
				'id': userid,
				'first_name': first_name,
				'last_name': last_name
			};
		  jQuery.post("<?php echo admin_url( 'admin-ajax.php' );?>", data, function(response) {
		  		//alert(response);
		  		
				if(response.indexOf('success')>-1){
					window.location.href = "<?php the_permalink(get_the_ID());?>";
				}else{
					window.location.href = "<?php the_permalink(get_the_ID());?>?msg=failed";
				}
			});
        });
      } else {
        alert('Popup closed. Please login again.');
      }
    }, {scope: 'email'});
    return false;
  };
  window.fbAsyncInit = function() {
    FB.init({
      appId: '<?php echo get_option('jetcommerce_facebook_app_id'); ?>',
      cookie: true, // This is important, it's not enabled by default
      version: 'v2.2'
    });
  };

  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  
  if(typeof fbq === 'undefined') {
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '123123123213121');
    fbq('track', 'PageView');
    fbq('track', '{{fBPixelType}}');
  }
  else {
    fbq('track', '{{fBPixelType}}');
  }
</script>
<div class="text-center jc-social-btn">
<a id="fbLogin1" href="javascript:void(0);" onclick="logInWithFacebook1();">
<figure><img src="<?php echo get_template_directory_uri().'/images/fbColor.png';?>" alt="" /></figure>
   <span>
      Facebook
   </span>
</a>
<a id="customBtn1" href="javascript:void(0);" onclick="startApp1();">
<figure><img src="<?php echo get_template_directory_uri().'/images/gColor.png';?>" alt="" /></figure>
   <span>
      Google
   </span>
</a>
</div>
<span class="login_other_option express_signup_email"><span class="express_inner">Signup Using Email</span></span></br>
    <?php
    $output_string = ob_get_contents();
    ob_end_clean();

    return $output_string;
}

add_shortcode('social_signup', 'my_shortcode_signup_form');

add_action( 'wp_ajax_fb_login', 'fb_login' );
add_action( 'wp_ajax_nopriv_fb_login', 'fb_login' );
function fb_login(){
	global $wpdb;
	global $woocommerce;
	$results = $wpdb->get_row( "SELECT ID FROM {$wpdb->users} WHERE user_email = '".$_REQUEST['email']."'", ARRAY_A );
	if(isset($results['ID']) && $results['ID']){
		$user_id = $results['ID'];
		clean_user_cache( $user_id );
	    wp_logout();
		wp_set_auth_cookie( $user_id, true, '' );
		$woocommerce->cart->empty_cart();
		$woocommerce->cart = $_SESSION['old_cart'];
		echo 'success';
	}else{
        $userdata = array(
			'user_pass'  =>  $_REQUEST['id'],
			'user_login'    =>  $_REQUEST['email'],
			'user_nicename'   =>  $_REQUEST['name'],
			'user_url'		=> '',
			'user_status'	=> 0,
			'user_email'   =>  $_REQUEST['email'],
			'first_name'	=> $_REQUEST['first_name'],
			'last_name'		=> $_REQUEST['last_name'],
			'user_registered'=> date('Y-m-d H:i:s'),
			//'role'			=> 'customer'
		);
		$user_id = wp_insert_user( $userdata ) ;
		if ( ! is_wp_error( $user_id ) ) {
			$to=$_REQUEST['email'];
			$subject = "Your account on Omega3Innovations";
			$message = '
			<html>
			<head>
			<title>Your account on Omega3Innovations</title>
			</head>
			<body>
				<div style="background-color:#f5f5f5;margin:0;padding:70px 0 70px 0;width:100%">
					<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
						<tbody>
							<tr>
								<td valign="top" align="center">
									<table style="background-color:#fdfdfd;border:1px solid #dcdcdc;border-radius:3px!important" width="600" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td valign="top" align="center">
													<table style="background-color:#b41019;border-radius:3px 3px 0 0!important;color:#ffffff;border-bottom:0;font-weight:bold;line-height:100%;vertical-align:middle;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif" width="600" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr>
																<td style="padding:36px 48px;display:block">
																	<h1 style="color:#ffffff;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:30px;font-weight:300;line-height:150%;margin:0;text-align:left">Welcome to Omega3 Innovations</h1>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td valign="top" align="center">
													<table width="600" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr>
																<td style="background-color:#fdfdfd" valign="top">
																	<table width="100%" cellspacing="0" cellpadding="20" border="0">
																		<tbody>
																			<tr>
																				<td style="padding:48px" valign="top">
																					<div style="color:#737373;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:14px;line-height:150%;text-align:left">
																						<p style="margin:0 0 16px">Thanks for creating an account on Omega3 Innovations with your <strong>Facebook</strong> account.</p>
																						<p style="margin:0 0 16px">You can access your account area to view your orders and change your password here: <a href="'.site_url('/').'my-account/" rel="nofollow" style="color:#b41019;font-weight:normal;text-decoration:underline" target="_blank">'.site_url('/').'my-account/</a>.</p>
																					</div>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td valign="top" align="center">
													<table width="600" cellspacing="0" cellpadding="10" border="0">
														<tbody>
															<tr>
																<td style="padding:0" valign="top">
																	<table width="100%" cellspacing="0" cellpadding="10" border="0">
																		<tbody>
																			<tr>
																				<td colspan="2" style="padding:0 48px 48px 48px;border:0;color:#d27075;font-family:Arial;font-size:12px;line-height:125%;text-align:center" valign="middle">
																					<p>Omega3 Innovations &ndash; Powered by JetCommerce.io</p>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</body>
			</html>';
			$admin_email = get_option('admin_email', true);
			$site_name = get_option('blogname', true);
			// Always set content-type when sending HTML email
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			// More headers
			$headers .= 'From:  '.$site_name.'<'.$admin_email.'>' . "\r\n";
			wp_mail($to,$subject,$message,$headers);
			
			$info['user_login'] = $_REQUEST['email'];
	    	$info['user_password'] = $_REQUEST['id'];
	    	$info['remember'] = true;
	    	$user_signon_now = wp_signon( $info, false );
			$woocommerce->cart->empty_cart();
			$woocommerce->cart = $_SESSION['old_cart'];
			echo 'success';
		}else{
			
			echo 'failed';
		}
	}
	
	die();
}


add_action( 'wp_ajax_gplus_login', 'gplus_login' );
add_action( 'wp_ajax_nopriv_gplus_login', 'gplus_login' );
function gplus_login(){
	global $wpdb;
	global $woocommerce;
	$results = $wpdb->get_row( "SELECT ID FROM {$wpdb->users} WHERE user_email = '".$_REQUEST['email']."'", ARRAY_A );
	//print_r($cart); die();
	if(isset($results['ID']) && $results['ID']){
		$user_id = $results['ID'];
		clean_user_cache( $user_id );
	    wp_logout();
		wp_set_auth_cookie( $user_id, true, '' );
		$woocommerce->cart->empty_cart();
		$woocommerce->cart = $_SESSION['old_cart'];
		echo 'success';
	}else{
		$userdata = array(
			'user_login' 	=> $_REQUEST['email'],
			'user_pass'  	=> $_REQUEST['id'],
			'user_nicename'	=> $_REQUEST['name'],
			'user_email' 	=> $_REQUEST['email'],
			'user_url'		=> '',
			'user_status'	=> 0,
			'display_name'	=> $_REQUEST['name'],
			'user_registered'=> date('Y-m-d H:i:s'),
			//'role'			=> 'customer'
		);
		$user_id = wp_insert_user( $userdata ) ;
		if ( ! is_wp_error( $user_id ) ) {
			$to=$_REQUEST['email'];
			$subject = "Your account on Omega3Innovations";
			$message = '
			<html>
			<head>
			<title>Your account on Omega3Innovations</title>
			</head>
			<body>
				<div style="background-color:#f5f5f5;margin:0;padding:70px 0 70px 0;width:100%">
					<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
						<tbody>
							<tr>
								<td valign="top" align="center">
									<table style="background-color:#fdfdfd;border:1px solid #dcdcdc;border-radius:3px!important" width="600" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td valign="top" align="center">
													<table style="background-color:#b41019;border-radius:3px 3px 0 0!important;color:#ffffff;border-bottom:0;font-weight:bold;line-height:100%;vertical-align:middle;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif" width="600" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr>
																<td style="padding:36px 48px;display:block">
																	<h1 style="color:#ffffff;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:30px;font-weight:300;line-height:150%;margin:0;text-align:left">Welcome to Omega3 Innovations</h1>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td valign="top" align="center">
													<table width="600" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr>
																<td style="background-color:#fdfdfd" valign="top">
																	<table width="100%" cellspacing="0" cellpadding="20" border="0">
																		<tbody>
																			<tr>
																				<td style="padding:48px" valign="top">
																					<div style="color:#737373;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:14px;line-height:150%;text-align:left">
																						<p style="margin:0 0 16px">Thanks for creating an account on Omega3 Innovations with your <strong>Google</strong> account.</p>
																						<p style="margin:0 0 16px">You can access your account area to view your orders and change your password here: <a href="'.site_url('/').'my-account/" rel="nofollow" style="color:#b41019;font-weight:normal;text-decoration:underline" target="_blank">'.site_url('/').'my-account/</a>.</p>
																					</div>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td valign="top" align="center">
													<table width="600" cellspacing="0" cellpadding="10" border="0">
														<tbody>
															<tr>
																<td style="padding:0" valign="top">
																	<table width="100%" cellspacing="0" cellpadding="10" border="0">
																		<tbody>
																			<tr>
																				<td colspan="2" style="padding:0 48px 48px 48px;border:0;color:#d27075;font-family:Arial;font-size:12px;line-height:125%;text-align:center" valign="middle">
																					<p>Omega3 Innovations &ndash; Powered by JetCommerce.io</p>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</body>
			</html>';
			$admin_email = get_option('admin_email', true);
			$site_name = get_option('blogname', true);
			// Always set content-type when sending HTML email
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			// More headers
			$headers .= 'From:  '.$site_name.'<'.$admin_email.'>' . "\r\n";
			wp_mail($to,$subject,$message,$headers);
			$info['user_login'] = $_REQUEST['email'];
	    	$info['user_password'] = $_REQUEST['id'];
	    	$info['remember'] = true;
	    	$user_signon_now = wp_signon( $info, false );
			$woocommerce->cart->empty_cart();
			$woocommerce->cart = $_SESSION['old_cart'];
			echo 'success';
		}else{
			echo 'failed';
		}
	}
	die();
}
add_action( 'password_reset', 'my_password_reset', 10, 2 );

function my_password_reset( $user, $new_pass ) {
	$redirect_url = isset($_REQUEST['reset_referer']) && $_REQUEST['reset_referer']?rawurldecode($_REQUEST['reset_referer']):site_url('/').'my-account';
	$user_id = $user->data->ID;
	$user_login = $user->data->user_login;
	global $wpdb;
	$password = md5($new_pass);
	$update = $wpdb->query($wpdb->prepare("UPDATE {$wpdb->users} SET `user_pass` = %s WHERE `ID` = %d",array(wp_hash_password($new_pass),$user_id)));
	if(!is_wp_error($update))
	{
		global $woocommerce;
    	$cart = $woocommerce->cart;
	    wp_cache_delete($user_id,'users');
	    wp_cache_delete($user->data->user_login,'userlogins');
	    wp_logout();
	    if (wp_signon(array('user_login'=>$user->data->user_login,'user_password'=>$new_pass),false)):
			$woocommerce->cart = $cart;
	        wp_safe_redirect($redirect_url);
		else:
			$woocommerce->cart = $cart;
			wp_set_auth_cookie( $user_id, true, '');
	    endif;
	    ob_start();
	}
	wp_safe_redirect($redirect_url);
	die();
}
function change_referer_url(){
	if(isset($_REQUEST['referer'])){
		session_start();
		$_SESSION['referer'] = $_REQUEST['referer'];
	}
}
add_action('init', 'change_referer_url');

function jetcommerce_social_settings() {
   add_option( 'jetcommerce_facebook_app_id', '');
   add_option( 'jetcommerce_facebook_app_secret', '');
   add_option( 'jetcommerce_google_client_id', '');
   add_option( 'jetcommerce_google_client_secret', '');
   register_setting( 'jetcommerce_social_settings_group', 'jetcommerce_facebook_app_id' );
   register_setting( 'jetcommerce_social_settings_group', 'jetcommerce_facebook_app_secret' );
   register_setting( 'jetcommerce_social_settings_group', 'jetcommerce_google_client_id' );
   register_setting( 'jetcommerce_social_settings_group', 'jetcommerce_google_client_secret' );
}
add_action( 'admin_init', 'jetcommerce_social_settings' );

function jetcommerce_register_social_page() {
  add_options_page('Jetcommerce Social Settings', 'Jetcommerce Social Settings', 'manage_options', 'jetcommerce-social-settings', 'jetcommerce_social_settings_view');
}
add_action('admin_menu', 'jetcommerce_register_social_page');

function jetcommerce_social_settings_view()
{
?>
	<?php //screen_icon(); ?>
	<div class="wrap">
		<h1>Jetcommerce Social Settings</h1>
		<form method="post" action="options.php">
			<?php settings_fields( 'jetcommerce_social_settings_group' ); ?>
			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row">
							<label for="facebook_app_id">Facebook App ID</label>
						</th>
						<td>
							<input type="text" class="widefat" id="jetcommerce_facebook_app_id" name="jetcommerce_facebook_app_id" value="<?php echo get_option('jetcommerce_facebook_app_id'); ?>" />
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="facebook_app_secret">Facebook App Secret</label>
						</th>
						<td>
							<input type="text" class="widefat" id="jetcommerce_facebook_app_secret" name="jetcommerce_facebook_app_secret" value="<?php echo get_option('jetcommerce_facebook_app_secret'); ?>" />
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="google_client_id">Google Client ID</label>
						</th>
						<td>
							<input type="text" class="widefat" id="jetcommerce_google_client_id" name="jetcommerce_google_client_id" value="<?php echo get_option('jetcommerce_google_client_id'); ?>" />
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="google_client_secret">Google Client Secret</label>
						</th>
						<td>
							<input type="text" class="widefat" id="jetcommerce_google_client_secret" name="jetcommerce_google_client_secret" value="<?php echo get_option('jetcommerce_google_client_secret'); ?>" />
						</td>
					</tr>
				</tbody>
			</table>
			<?php submit_button(); ?>
		</form>
	</div>
<?php
}

add_action( 'init', 'session_cart_init' );
function session_cart_init() {
	if(is_user_logged_in()){
		global $woocommerce;
		if (!session_id())
		    session_start();
		if(isset($_SESSION['old_cart']) && $_SESSION['old_cart']){
			//print_r($_SESSION['old_cart']);
			//$woocommerce->cart->empty_cart();
			//$woocommerce->cart = $_SESSION['old_cart'];
			//unset($_SESSION['old_cart']);
		}
 	}
}