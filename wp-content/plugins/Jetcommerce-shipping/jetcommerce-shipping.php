<?php
/**
 * Plugin Name: Jetcommerce Shipping
 * Description: Jetcommerce Shipping min rates and Freeshipping method selection in checkout page.
 * Version: 1.0
 * Author: Jetcommerce Developers
 * Author URI: http://jetcommerce.io/
 **/

//Free Shipping Min Amount
function get_shipping_min_rate()
{
	global $wpdb;
	$table = $wpdb->prefix.'woocommerce_shipping_zone_methods';
	$qry = $wpdb->get_results("SELECT * FROM $table WHERE `zone_id`= 1 AND `method_id`='free_shipping'",ARRAY_A);
	if(isset($qry) && !empty($qry)){
		foreach($qry as $qryt){
			$zone = $qryt['instance_id'];
		}
	}
	$free_shipping_settings = get_option( "woocommerce_free_shipping_{$zone}_settings" );
    $min_amount = $free_shipping_settings['min_amount']; 
    return $min_amount;
}
/// end //////////////

//Free Shipping method in checkout
function my_hide_shipping_when_free_is_available( $rates ) {
if(isset($rates['free_shipping:6'])){
	unset($rates['flat_rate:10']);
} 
return $rates;
//return ! empty( $free ) ? $free : $rates;
}
add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 10, 2 );

?>
