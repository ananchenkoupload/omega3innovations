=== AMP Layouts ===
Contributors:  Ahmed Kaludi, Mohammed Kaludi
Tags: layouts, amp, theme, design, plugin
Requires at least: 3.0
Tested up to: 5.1.1
Stable tag: 1.5.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

AMP Layouts is a revolutionary modular layout system built for AMP that makes easy to create your own AMP templates.

== Description ==
AMP Layouts is a revolutionary modular layout system built for AMP that makes easy to create your own AMP templates.


== Changelog ==

= 1.5.1 (26 Apr 2019) =
= Bug Fixed: =
* Added Next,Previous Links in Single Design 3(Layout).

= 1.5.0 (20 Apr 2019) =
= Bug Fixed: =
* Single Post Designs Repeating problem is fixed.

= 1.4.9 (19 Apr 2019) =
= Features: =
* In AMP layouts Journal theme, added a field to show either category or tag as badge.

= Bug Fixed: =
*Undefined index Notice fixed 

= 1.4.8 (09 Mar 2019) =
* Fatal error due to new activation of amp layouts plugin has been resolved.
* Added Menu Social profiles feature for sidebar menu.

= 1.4.7 (07 Feb 2019) =
* Indentations issue for ul and ol lists has been resolved.
* Implementation of font family has been improved.
* Fatal error due to AMP layout(weekly magaine theme) deactivation has been resolved.
* Added translation for "connect" word in footer design of AMP layout.

= 1.4.6 (17 Jan 2019) =
* Added 'Back to Top' scrolling feature.
* 'amp-animation' validation error has been resolved.

= 1.4.5 (9 Nov 2018) =
* Content Presentation Module Layout Design Images problem is fixed. 

= 1.4.3 (5 Nov 2018) =
* Sidebar Menu Scrolling in Swift is fixed in the layout. 

= 1.4.2 (6 oct 2018) =
* Header menu and mobile menu problem is fixed in all the layouts 

= 1.4.1 (7 Sept 2018) =
* Fatal error for Sidebar issue in swift theme fixed.
* Function name renamed for Sidebar issue in swift theme.

= 1.4 (5 July 2018) =
* New Design Added: Journal https://ampforwp.com/demo/journal/amp/
* New Design Added: Business Blog https://ampforwp.com/demo/business-blog/amp/
* New Design Added: App https://ampforwp.com/demo/app/amp
* New Design Added: Creative Services https://ampforwp.com/demo/creative-services/amp/
* New Features are added.
* Minor Bugs Fixed.

= 1.3.1 (22 May 2018) =
* AMP option panel breaking solved

= 1.3 (5 May 2018) =
* New Design Added: Agency https://ampforwp.com/demo/agency/amp/
* New Design Added: Studio https://ampforwp.com/demo/studio/amp/
* Team Module added
* New design type for the Testimonial module
* Minor Bugs fixed

= 1.2 (17 April 2018) =
* New Design Added https://ampforwp.com/layouts-3/
* Content Presentation Module added
* Many design options added
* Better Update Process Integrated
* and small bug fixes

= 1.1 (22 March 2018) =
* New Design Added https://ampforwp.com/layouts-2/
* Better Update Process Integrated
