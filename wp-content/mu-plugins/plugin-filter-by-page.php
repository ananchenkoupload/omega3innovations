<?php
// returns the path of the request URI without the query string
$request_uri = parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH );

$is_admin = strpos( $request_uri, '/wp-admin/' );

// add filter in front pages only
if( false === $is_admin ){
    add_filter( 'option_active_plugins', 'omega_option_active_plugins' );
}

/**
 * Filters active plugins
 *
 * @param array   $plugins An array of active plugins.
 */
function omega_option_active_plugins( $plugins ){
    global $request_uri;
    $is_woo_checkout_page = strpos( $request_uri, '/checkout/' );
    $is_woo_account_page  = strpos( $request_uri, '/my-account/' );
    $is_woo_cart_page     = strpos( $request_uri, '/cart/' );
    $is_woo_omegarestore_page = strpos( $request_uri, '/omega-restore/' );

    $unnecessary_plugins = array();

    // filter out WPBakery Page Builder plugin on WooCommerce Checkout page
    if( false !== $is_woo_checkout_page || false !== $is_woo_account_page || false !== $is_woo_cart_page || false !== $is_woo_omegarestore_page){
        $unnecessary_plugins[] = 'js_composer/js_composer.php';
    }

    foreach ( $unnecessary_plugins as $plugin ) {
        $k = array_search( $plugin, $plugins );
        if( false !== $k ){
            unset( $plugins[$k] );
        }
    }

    return $plugins;
}