<!DOCTYPE html><html
lang="en"><head><meta
http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>Home</title><meta http-equiv="X-UA-Compatible" content="IE=8" /><meta
name="description" content="Maker's of America's #1 fresh fish oil for horses with natural EPA and DHA omega-3. Shop Wellpride. Wellpride FAQs. Wellpride Success Stories. " /><meta
name="keywords" content="" /><meta
name="robots" content="INDEX,FOLLOW" /><meta
property="og:title" content="Home" /><meta
property="og:type" content="article" /><meta
property="og:url" content="https://www.wellpride.com/" /><meta
property="og:description" content="Maker's of America's #1 fresh fish oil for horses with natural EPA and DHA omega-3. Shop Wellpride. Wellpride FAQs. Wellpride Success Stories. " /><meta
property="og:site_name" content="Wellpride Store" /><meta
name="viewport" content="width=device-width, initial-scale=1.0"/><link
rel="icon" href="https://www.wellpride.com/skin/frontend/smartwave/porto_child/images/favicon.ico" type="image/x-icon" /><link
rel="shortcut icon" href="https://www.wellpride.com/skin/frontend/smartwave/porto_child/images/favicon.ico" type="image/x-icon" />   
      <link
href='//fonts.googleapis.com/css?family=PT+Sans|Didact+Gothic' rel='stylesheet' type='text/css'><meta
name="norton-safeweb-site-verification" content="uh8yrsbhu775xxp3-dfr3f2qhu7s340m70b26mkbqqu1otk4qfgdtctmzqdyfmstx8pvqnnf1l60ssmi4vnpge12fijp-92zh9fjej2vnu67g3gkfciojgfn5w2lqdgp" /><meta
name="google-site-verification" content="0B_FgJ-6UkCBYTKugq77QCkwgaL-Mh8sBuY4pCXdFCs" /> </head><body
class=" cms-index-index cms-porto-home-2-new"><div
class="wrapper">
<noscript><div
class="global-site-notice noscript"><div
class="notice-inner"><p>
<strong>JavaScript seems to be disabled in your browser.</strong><br
/>
You must have JavaScript enabled in your browser to utilize the functionality of this website.</p></div></div>
</noscript><div
class="page"><div
class="header-container type8"><div
class="header container"><h1 class="logo"><strong>Wellpride</strong><a
href="https://www.wellpride.com/" title="Wellpride Logo" class="logo"><img
src="https://www.wellpride.com/skin/frontend/smartwave/porto_child/images/Wellpride-Logo-White.svg" alt="Wellpride Logo" /></a></h1><div
class="header-wrapper"><div
class="main-nav"><div
class="container"><div
class="menu-wrapper"><div
class="menu-all-pages-container"><ul
class="menu"><li
class=" ">
<a
href="//www.wellpride.com/buy-online.html">Buy Online</a></li><li
class="menu-item menu-item-has-children menu-parent-item fl-right"><a
href="#">About Wellpride</a><div
class="nav-sublist-dropdown" style="display: none; list-style: none;"><div
class="container"><ul><li
class="menu-item " style="list-style: none;"><a
class="level1" href="/about-the-oil"><span>About the Oil</span></a></li><li
class="menu-item " style="list-style: none;"><a
class="level1" href="/benefits"><span>Wellpride Benefits</span></a></li><li
class="menu-item " style="list-style: none;"><a
class="level1" href="/faqs"><span>Wellpride FAQs</span></a></li><li
class="menu-item " style="list-style: none;"><a
class="level1" href="/wellpride-success-stories"><span>Success Stories</span></a></li><li
class="menu-item " style="list-style: none;"><a
class="level1" href="/press-room"><span>Press Room</span></a></li></ul></div></div></li><li
class="menu-item menu-item-has-children menu-parent-item fl-right"><a
href="#">Articles</a><div
class="nav-sublist-dropdown" style="display: none; list-style: none;"><div
class="container"><ul><li
class="menu-item " style="list-style: none;"><a
class="level1" href="/omega-3-articles"><span>Omega-3 Articles</span></a></li><li
class="menu-item " style="list-style: none;"><a
class="level1" href="/equine-health"><span>Equine Ailments</span></a></li></ul></div></div></li><li><a
href="/about-us">About Us</a></li><li
class="fl-right"><a
href="http://blog.wellpride.com" target="_blank">Blog</a></li></ul></div></div></div></div></div>  <div
class="cart-area"><div
class="search-area">
<a
href="javascript:void(0)" class="search-icon"><i
class="icon-search"></i></a><form
id="search_mini_form" action="//www.wellpride.com/catalogsearch/result/" method="get"><div
class="form-search ">
<label
for="search">Search:</label>
<input
id="search" type="text" name="q" class="input-text" />
<select
id="cat" name="cat"><option
value="">All Categories</option><option
value="10">Buy Online</option>
</select>
<button
type="submit" title="Search" class="button"><i
class="icon-search"></i></button><div
id="search_autocomplete" class="search-autocomplete"></div><div
class="clearer"></div></div></form>  </div><div
class="menu-icon"><a
href="javascript:void(0)" title="Menu"><i
class="fa fa-bars"></i></a></div><div
class="switcher-area"><div
class="mini-cart">
<a
href="javascript:void(0)" class="mybag-link"><i
class="icon-mini-cart"></i><span
class="cart-info"><span
class="cart-qty">0</span><span>item(s)</span></span></a><div
class="topCartContent block-content theme-border-color"><div
class="inner-wrapper"><p
class="cart-empty">
You have no items in your shopping cart.</p></div></div>  </div></div></div></div></div><div
class="mobile-nav side-block container"><div
class="menu-all-pages-container"><ul
class="menu"><li
class=" ">
<a
href="//www.wellpride.com/buy-online.html">Buy Online</a></li><li
class="menu-item menu-item-has-children menu-parent-item"><a
href="#">About Wellpride</a><ul><li
class="menu-item " style="list-style: none;"><a
class="level1" href="/about-the-oil"><span>About the Oil</span></a></li><li
class="menu-item " style="list-style: none;"><a
class="level1" href="/benefits"><span>Wellpride Benefits</span></a></li><li
class="menu-item " style="list-style: none;"><a
class="level1" href="/faqs"><span>Wellpride FAQs</span></a></li><li
class="menu-item " style="list-style: none;"><a
class="level1" href="/wellpride-success-stories"><span>Wellpride Stories</span></a></li><li
class="menu-item " style="list-style: none;"><a
class="level1" href="/press-room"><span>Press Room</span></a></li></ul></li><li
class="menu-item menu-item-has-children menu-parent-item"><a
href="#">Articles</a><ul><li
class="menu-item " style="list-style: none;"><a
class="level1" href="/omega-3-articles"><span>Omega-3 Articles</span></a></li><li
class="menu-item " style="list-style: none;"><a
class="level1" href="/equine-health"><span>Equine Ailments</span></a></li></ul></li><li
class="menu-item"><a
href="/about-us">About Us</a></li><li
class="menu-item"><a
href="http://blog.wellpride.com" target="_blank">Blog</a></li></ul></div></div><div
class="mobile-nav-overlay close-mobile-nav"></div><div
class="top-container"><div
id="slideshow"><div
class="full-screen-slider"><div
id="banner-slider-demo-2-new" class="owl-carousel owl-theme owl-middle-narrow owl-banner-carousel"><div
class="item" style="background:url(https://www.wellpride.com/skin/frontend/smartwave/porto_child/images/slides/slide1.jpg) no-repeat; background-size:cover; position: relative;"><div
class="slide-1" style="position: absolute; top: 25%; width:100%;"><div
class="container" style="text-align: left;"><div
class="text-content" style="display: inline-block;">
<em
style="font-style: normal; text-align: left; display: block; color: #fff;font-weight:600;"></em><h2 style="color: #fff; line-height: 1;font-weight:800;margin:0;">DIGESTION</h2><p><span
style="color: #fff; vertical-align: top; font-weight: 300;">Try America's <b
style="font-weight:800;">#1</b> Fish Oil for Horses.</span></p>
<a
href="/buy-online.html" class="btn btn-default" style="background-color: #fff; color: #333; vertical-align: top; font-weight: 600;">SHOP NOW</a></div></div></div></div><div
class="item" style="background:url(https://www.wellpride.com/skin/frontend/smartwave/porto_child/images/slides/slide2.jpg) no-repeat; background-size:cover; position: relative;"><div
class="slide-2" style="position: absolute; top: 25%; width:100%;"><div
class="container" style="text-align: left;"><div
class="text-content" style="display: inline-block;">
<em
style="font-style: normal; text-align: left; display: block; color: #fff;font-weight:600;"></em><h2 style="color: #fff; line-height: 1;font-weight:800;margin:0;">FOCUS</h2><p><span
style="color: #fff; vertical-align: top; font-weight: 300;">Try America's <b
style="font-weight:800;">#1</b> Fish Oil for Horses.</span></p>
<a
href="/buy-online.html" class="btn btn-default" style="background-color: #fff; color: #333; vertical-align: top; font-weight: 600;">SHOP NOW</a></div></div></div></div></div>  </div><div
class="container"><div
class="single-images" style="padding-top: 30px;"><div
class="row"><div
class="col-sm-4" style="padding-bottom: 15px;"><a
class="image-link" href="/benefits"> <img
alt="Horse running | Wellpride" src="https://www.wellpride.com/media/wysiwyg/Wellpride_Website_Pictures_whyfish.jpg" width="370" /> <span
class="category-title">WHY FISH OIL</span> </a></div><div
class="col-sm-4" style="padding-bottom: 15px;"><a
class="image-link" href="/faqs"> <img
alt="Horse fish logo | Wellpride" src="https://www.wellpride.com/media/wysiwyg/Wellpride_Website_Pictures_horsefish.jpg" width="370" /> <span
class="category-title">WELLPRIDE FAQS</span> </a></div><div
class="col-sm-4" style="padding-bottom: 15px;"><a
class="image-link" href="/wellpride-success-stories"> <img
alt="Horse jumping | Wellpride" src="https://www.wellpride.com/media/wysiwyg/Wellpride_Website_Pictures_stories.jpg" width="370" /> <span
class="category-title">SUCCESS STORIES</span> </a></div></div></div><div
class="row"><div
class="col-sm-12"><h2 style="margin-top:30px; font-size:19px;font-weight:600;" class="theme-color">FEATURED PRODUCTS</h2><div
id="featured_product" class="hide-addtocart hide-addtolinks"><div
class="filter-products"><div
class="products"><div
class="item"><div
class="item-area"><div
class="product-image-area"><div
class="loader-container"><div
class="loader">
<i
class="ajax-loader medium animate-spin"></i></div></div>
<a
href="https://www.wellpride.com/wellpride-6-pack.html" title="Wellpride Fish Oil for Horses (6 Pack)" class="product-image">
<img
class="defaultImage" src="https://www.wellpride.com/media/catalog/product/cache/2/small_image/300x/17f82f742ffe127f42dca9de82fb58b1/n/e/new_wellpride_website_-_product_details_6_pack_1.png" alt="Wellpride Fish Oil for Horses (6 Pack)"/>
<img
class="hoverImage" src="https://www.wellpride.com/media/catalog/product/cache/2/thumbnail/300x/17f82f742ffe127f42dca9de82fb58b1/n/e/new_wellpride_website_-_product_details_6_pack_1.png" alt="Wellpride Fish Oil for Horses (6 Pack)"/>
</a></div><div
class="details-area"><h2 class="product-name"><a
href="https://www.wellpride.com/wellpride-6-pack.html" title="Wellpride (6 Pack) ">Wellpride (6 Pack) </a></h2><div
class="ratings"><div
class="rating-box"><div
class="rating" style="width:0"></div></div></div><div
class="price-box">
<span
class="regular-price" id="product-price-127">
<span
class="price">$269.70</span>                                    </span></div><div
class="actions">
<a
href="https://www.wellpride.com/checkout/cart/add/uenc/aHR0cHM6Ly93d3cud2VsbHByaWRlLmNvbS8,/product/127/form_key/ooXqhMSE5AE1HWbO/" class="addtocart" title="Add to Cart" ><i
class="icon-cart"></i><span>&nbsp;Add to Cart</span></a><div
class="clearer"></div></div></div></div></div><div
class="item"><div
class="item-area"><div
class="product-image-area"><div
class="loader-container"><div
class="loader">
<i
class="ajax-loader medium animate-spin"></i></div></div>
<a
href="https://www.wellpride.com/wellpride-1-bottle.html" title="Wellpride Fish Oil for Horses (1 Bottle)" class="product-image">
<img
class="defaultImage" src="https://www.wellpride.com/media/catalog/product/cache/2/small_image/300x/17f82f742ffe127f42dca9de82fb58b1/n/e/new_wellpride_website_-_product_details_1_bottle_1.png" alt="Wellpride Fish Oil for Horses (1 Bottle)"/>
<img
class="hoverImage" src="https://www.wellpride.com/media/catalog/product/cache/2/thumbnail/300x/17f82f742ffe127f42dca9de82fb58b1/n/e/new_wellpride_website_-_product_details_1_bottle_1.png" alt="Wellpride Fish Oil for Horses (1 Bottle)"/>
</a></div><div
class="details-area"><h2 class="product-name"><a
href="https://www.wellpride.com/wellpride-1-bottle.html" title="Wellpride (1 Bottle)">Wellpride (1 Bottle)</a></h2><div
class="ratings"><div
class="rating-box"><div
class="rating" style="width:0"></div></div></div><div
class="price-box">
<span
class="regular-price" id="product-price-126">
<span
class="price">$49.95</span>                                    </span></div><div
class="actions">
<a
href="https://www.wellpride.com/checkout/cart/add/uenc/aHR0cHM6Ly93d3cud2VsbHByaWRlLmNvbS8,/product/126/form_key/ooXqhMSE5AE1HWbO/" class="addtocart" title="Add to Cart" ><i
class="icon-cart"></i><span>&nbsp;Add to Cart</span></a><div
class="clearer"></div></div></div></div></div><div
class="item"><div
class="item-area"><div
class="product-image-area"><div
class="loader-container"><div
class="loader">
<i
class="ajax-loader medium animate-spin"></i></div></div>
<a
href="https://www.wellpride.com/wellpride-12-pack.html" title="Wellpride Fish Oil for Horses (12 Pack)" class="product-image">
<img
class="defaultImage" src="https://www.wellpride.com/media/catalog/product/cache/2/small_image/300x/17f82f742ffe127f42dca9de82fb58b1/n/e/new_wellpride_website_-_product_details_12_pack_1.png" alt="Wellpride Fish Oil for Horses (12 Pack)"/>
<img
class="hoverImage" src="https://www.wellpride.com/media/catalog/product/cache/2/thumbnail/300x/17f82f742ffe127f42dca9de82fb58b1/n/e/new_wellpride_website_-_product_details_12_pack_1.png" alt="Wellpride Fish Oil for Horses (12 Pack)"/>
</a></div><div
class="details-area"><h2 class="product-name"><a
href="https://www.wellpride.com/wellpride-12-pack.html" title="Wellpride (12 Pack)">Wellpride (12 Pack)</a></h2><div
class="ratings"><div
class="rating-box"><div
class="rating" style="width:0"></div></div></div><div
class="price-box">
<span
class="regular-price" id="product-price-128">
<span
class="price">$479.40</span>                                    </span></div><div
class="actions">
<a
href="https://www.wellpride.com/checkout/cart/add/uenc/aHR0cHM6Ly93d3cud2VsbHByaWRlLmNvbS8,/product/128/form_key/ooXqhMSE5AE1HWbO/" class="addtocart" title="Add to Cart" ><i
class="icon-cart"></i><span>&nbsp;Add to Cart</span></a><div
class="clearer"></div></div></div></div></div></div></div><div
class="clear">&nbsp;</div></div></div></div></div>  <div
id="parallax_01" style="background:url(https://www.wellpride.com/skin/frontend/smartwave/porto_child/images/parallax_img.jpg) 80% 0 no-repeat fixed; color: #fff; margin: 0 auto; padding: 100px 0; position: relative;"><div
class="overlay overlay-color" style="background-color: #000000; opacity: 0.65; filter: alpha(opacity=65);top:0;"></div><div
class="parallax-slider" style="position: relative; z-index: 3;"><div
class="container"><div
id="parallax-slider-demo-5" class="owl-carousel owl-theme"><div
class="item"><h2 style="font-weight: 600;">FOR THE RIDER</h2><p
style="font-weight: 300;">Get an omega-3 solution for your aching joints, from our sister company Omega3 Innovations.</p>
<a
style="font-weight: 300;" href="https://www.omega3innovations.com/">Learn More</a></div></div></div>  </div></div></div></div><div
class="main-container col1-layout"><div
class="main container"><div
class="col-main"><div
class="std"><div
class="row"><div
class="col-sm-12"><h2 class="theme-color a-center" style="font-size: 19px; font-weight: 600; margin-top: 20px;">FROM THE BLOG</h2></div></div><div
class="row"><div
class="col-sm-12"><div
id="latest_news" class="owl-top-narrow"><div
class="recent-posts"><div
class="owl-carousel owl-theme"><div
class="item"><div
class="row"><div
class="col-sm-5"><div
class="post-image"><img
title="A Holiday Gift Guide for the Horse People In Your Life" alt="Holiday Gifts with a Horse Toy" src="https://www.wellpride.com/media/wysiwyg/Wellpride_Blog_Post_for_Homepage_HolidayGifts.jpg" /></div></div><div
class="col-sm-7"><div
class="post-date"><span
class="day">8</span> <span
class="month">Dec</span></div><div
class="postTitle"><h2><a
title="A Holiday Gift Guide for the Horse People in Your Life" href="http://blog.wellpride.com/2016/12/08/a-holiday-gift-guide-for-the-horse-people-in-your-life/">A Holiday Gift Guide for the Horse People in Your Life</a></h2></div><div
class="postContent"><p>For those of you shopping for horsey friends, we've gotten perspectives from both western and english riders...</p></div>
<a
class="readmore" href="http://blog.wellpride.com/2016/12/08/a-holiday-gift-guide-for-the-horse-people-in-your-life/">Read more &gt;</a></div></div></div><div
class="item"><div
class="row"><div
class="col-sm-5"><div
class="post-image"><img
title="What the Studies Say about Equine Asthma and Omega-3" alt="Horse breathing" src="https://www.wellpride.com/media/wysiwyg/Wellpride_Blog_Post_for_Homepage_Equine_Asthma.jpg" /></div></div><div
class="col-sm-7"><div
class="post-date"><span
class="day">18</span> <span
class="month">Nov</span></div><div
class="postTitle"><h2><a
title="What the Studies Say about Equine Asthma and Omega-3" href="http://blog.wellpride.com/2016/11/18/what-the-studies-say-about-equine-asthma-and-omega-3/">What the Studies Say about Equine Asthma and Omega-3</a></h2></div><div
class="postContent"><p><span>There have been a lot of studies published that support the benefits of omega-3 supplementation in animals.</span>..</p></div>
<a
class="readmore" href="http://blog.wellpride.com/2016/11/18/what-the-studies-say-about-equine-asthma-and-omega-3/">Read more &gt;</a></div></div></div></div></div></div>  </div></div></div></div></div></div><div
class="footer-container "><div
class="footer"><div
class="footer-middle"><div
class="container"><div
class="row"><div
class="col-sm-3"><div
class="block"><div
class="block-title"><strong><span>My Account</span></strong></div><div
class="block-content"><ul
class="links"><li><a
title="Contact us" href="https://www.wellpride.com/contacts">Contact us</a></li><li><a
href="https://www.wellpride.com/customer/account/login/">Login</a> | <a
href="https://www.wellpride.com/customer/account/logout/">Logout</a></li><li><a
title="My account" href="https://www.wellpride.com/customer/account">My account</a></li><li><a
title="Orders history" href="https://www.wellpride.com/sales/order/history">Orders history</a></li></ul></div></div></div><div
class="col-sm-3"><div
class="block"><div
class="block-title"><strong><span>Contact Information</span></strong></div><div
class="block-content"><ul
class="contact-info"><li><i
class="icon-location">&nbsp;</i><p><b>Address:</b><br
/>727 Commerce Dr. Venice, FL 34292</p></li><li><i
class="icon-phone">&nbsp;</i><p><b>Phone:</b><br
/>866.414.0188</p></li><li><i
class="icon-mail">&nbsp;</i><p><b>Email:</b><br
/><a
href="mailto:contact@wellpride.com">contact@wellpride.com</a></p></li><li><i
class="icon-clock">&nbsp;</i><p><b>Working Days/Hours:</b><br
/>Mon - Fri / 8:30AM - 4:30PM</p></li></ul></div></div></div><div
class="col-sm-3"><div
class="block"><div
class="block-title"><strong><span>Wellpride Benefits</span></strong></div><div
class="block-content"><ul><li><a
href="/benefits">Power and stamina<br
/><br
/></a></li><li><a
href="/benefits">Focus<br
/><br
/></a></li><li><a
href="/benefits">Flexibility<br
/><br
/></a></li><li><a
href="/benefits">Fertility<br
/><br
/></a></li><li><a
href="/benefits">Digestion</a></li></ul></div></div></div><div
class="col-sm-3"><div
class="block block-subscribe"><div
class="block-title">
<strong><span>Be the First to Know</span></strong></div><form
name="Wellpride Newsletter Signup" action="//wellpride.us4.list-manage.com/subscribe/post?u=3689faabafe08edbf98e39a4f&amp;id=eb544cc865" method="post" type="POST" id="footer-newsletter-validate-detail"><div
class="block-content"><p>Get the latest information on events,<br/>sales and offers. Sign up for newsletter today.</p><div
class="input-box"><p
class="label">Enter your email address</p>
<input
type="email" name="EMAIL" id="mce-EMAIL" title="Sign up for our newsletter" class="input-text required-entry validate-email" /><div
style="position: absolute; left: -5000px;" aria-hidden="true"><input
type="text" name="b_3689faabafe08edbf98e39a4f_eb544cc865" tabindex="-1" value=""></div>
<button
type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"><span><span>Submit</span></span></button><div
class="clearer"></div></div></div></form>  </div></div></div></div></div><div
class="footer-bottom"><div
class="container">
<a
href="https://www.wellpride.com/" class="logo"><img
src="https://www.wellpride.com/skin/frontend/smartwave/porto_child/images/Wellpride-Logo.svg" alt="Wellpride Logo"/></a><div
class="social-icons">
<a
href="https://www.facebook.com/wellpride" style="background-position:-60px 0; width:30px; height:30px;" class="icon1-class" title="Facebook" target="_blank">&nbsp;</a><a
href="https://www.instagram.com/wellpride" style="background-position:-150px -90px; width:30px; height:30px;" class="icon2-class" title="Instagram" target="_blank">&nbsp;</a></div>
<address>Copyright &copy; 2016 Wellpride LLLP. All rights reserved.</address></div></div></div></div>
<a
href="#" id="totop"><i
class="icon-up-open"></i></a>  </div></div><!--[if gt IE 7]><link
rel="stylesheet" type="text/css" href="https://www.wellpride.com/media/css_secure/8f4f799302bf193a2ce5ad21c524c1c2.css" media="all" />
<![endif]--><link
rel="stylesheet" href='//fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C400%2C400italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&subset=latin' type='text/css' /><link
href='//fonts.googleapis.com/css?family=Shadows+Into+Light' rel='stylesheet' type='text/css'/><link
rel="stylesheet" type="text/css" href="https://www.wellpride.com/media/css_secure/19e530ee5a21e2ac19d5c11bd020c0ff.css" media="all" /><!--[if lt IE 7]> <script type="text/javascript" src="https://www.wellpride.com/media/js/6ab0fd53dc8b25adc827c2565c030b68.js"></script> <![endif]--><!--[if lt IE 9]> <script type="text/javascript" src="https://www.wellpride.com/media/js/55698bc76be26c5eb6761dbe60b2abf8.js"></script> <![endif]--><script type="text/javascript" src="https://www.wellpride.com/media/js/a98dd86e0481d7880167b0370082b399.js"></script><script type="text/javascript" src="https://www.wellpride.com/media/js/3ef7834bcd19993f2963d46c58e6ea4c.js" async></script><script type="text/javascript">Mage.Cookies.path='/';Mage.Cookies.domain='.www.wellpride.com';</script><script type="text/javascript">optionalZipCountries=["TV"];</script><script type="text/javascript">(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create','UA-80160818-1','auto');ga('send','pageview');</script><script type="text/javascript">jQuery(function($){var scrolled=false;$(window).scroll(function(){if(140<$(window).scrollTop()&&!scrolled){if(!$('.header-container .menu-wrapper .mini-cart').length&&!$('.header-container .menu-wrapper .sticky-logo').length){$('.header-container').addClass("sticky-header");var minicart=$('.header-container .mini-cart').html();$('.header-container .menu-wrapper').append('<div class="mini-cart">'+minicart+'</div>');$('.header-container .header-wrapper > div').each(function(){if($(this).hasClass("container")){$(this).addClass("already");}else{$(this).addClass("container");}});scrolled=true;}}
if(140>=$(window).scrollTop()&&scrolled){$('.header-container').removeClass("sticky-header");$('.header-container .menu-wrapper .mini-cart').remove();scrolled=false;$('.header-container .header-wrapper > div').each(function(){if($(this).hasClass("already")){$(this).removeClass("already");}else{$(this).removeClass("container");}});}});});</script><script type="text/javascript">var Translator=new Translate([]);</script><script type="text/javascript">if(typeof dailydealTimeCountersCategory=='undefined'){var dailydealTimeCountersCategory=new Array();var i=0;}</script><script type="text/javascript">var SW_MENU_POPUP_WIDTH=0;</script><script type="text/javascript">var searchForm=new Varien.searchForm('search_mini_form','search','Search...');searchForm.initAutocomplete('https://www.wellpride.com/catalogsearch/ajax/suggest/','search_autocomplete');</script><script type="text/javascript">jQuery(function($){$('.mini-cart').mouseover(function(e){$(this).children('.topCartContent').fadeIn(200);return false;}).mouseleave(function(e){$(this).children('.topCartContent').fadeOut(200);return false;});});</script><script type="text/javascript">jQuery(function($){$("#banner-slider-demo-2-new").owlCarousel({autoPlay:true,stopOnHover:true,pagination:false,autoPlay:true,navigation:true,navigationText:["<i class='icon-chevron-left'></i>","<i class='icon-chevron-right'></i>"],slideSpeed:500,paginationSpeed:500,singleItem:true});});function gohome(){jQuery(".header-container").scrollToJustMe();}</script><script type="text/javascript">jQuery(function($){$('#parallax_01').parallax("80%",0.1);});</script><script type="text/javascript">jQuery(function($){$("#parallax-slider-demo-5").owlCarousel({navigation:false,slideSpeed:300,paginationSpeed:400,stopOnHover:true,autoPlay:true,singleItem:true});});</script><script type="text/javascript">jQuery(function($){$("#latest_news .owl-carousel").owlCarousel({lazyLoad:true,itemsCustom:[[0,1],[320,1],[480,1],[640,2],[768,2],[992,2],[1200,2]],responsiveRefreshRate:50,slideSpeed:200,paginationSpeed:500,scrollPerPage:false,stopOnHover:true,rewindNav:true,rewindSpeed:600,pagination:false,navigation:true,autoPlay:true,navigationText:["<i class='icon-left-open'></i>","<i class='icon-right-open'></i>"]});});</script><script type="text/javascript">var footernewsletterSubscriberFormDetail=new VarienForm('footer-newsletter-validate-detail');</script><script type="text/javascript">var windowScroll_t;jQuery(window).scroll(function(){clearTimeout(windowScroll_t);windowScroll_t=setTimeout(function(){if(jQuery(this).scrollTop()>100){jQuery('#totop').fadeIn();}else{jQuery('#totop').fadeOut();}},500);});jQuery('#totop').click(function(){jQuery('html, body').animate({scrollTop:0},600);return false;});jQuery(function($){$(".cms-index-index .footer-container.fixed-position .footer-top,.cms-index-index .footer-container.fixed-position .footer-middle").remove();});</script></body></html>